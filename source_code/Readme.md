# Source Code

The source code of our wireless sensor system for fatigue strain cycles monitoring, published in "Power-efficient piezoelectric fatigue measurement using long-range wireless sensor networks", Smart Materials and Structures, 2019, [https://doi.org/10.1088/1361-665X/ab2c46](https://doi.org/10.1088/1361-665X/ab2c46).

The simulation and experimental data, data analysis scripts, and the source code of our wireless sensor system for fatigue strain cycles monitoring,

The code is intended for the [Contiki 3.x](http://www.contiki-os.org/) operating system (OS).

## Supported platforms

Currently, only the [NZ32-SC151](http://wiki.modtronix.com/doku.php?id=products:nz-stm32:nz32-sc151) board with an inAir SX1276/SX1278 LoRa module from [Modtronix Engineering](http://modtronix.com/) is supported.

The NZ32-SC151 is a wireless development platform that is assembled with a STM32L151RC microcontroller and an on-board battery charger for a 3.7V Li-Ion or Li-Polymer battery. This board can be powered via USB connector, external battery (plugged into JST PH 2.0mm connector) or external 5V provided via pin headers. The wireless interface is based on the SX1276 radio from [Semtech](https://www.semtech.com/). Modules are available for the 430 MHz, 868 MHz and 915 MHz bands. For detailed information, see the [Wiki page](http://wiki.modtronix.com/doku.php?id=products:nz-stm32:nz32-sc151).

## Directory structure

This is a separate repository from the main Contiki 3.x. The Contiki 3.x repository is a [git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) and therefore not part of this repository. Relevant directories and files include:

```
.
+-- apps
+-- @contiki
+-- core
|   +-- dev
|   +-- net
|       +-- mac
|           +-- contikimac
|       +-- net
|           +-- loramac
|   +-- sys
+-- cpu
|   +-- arm
|       +-- stm32l151
+-- dev
|   +-- sx1276
+-- examples
|   +-- test-loramac
+-- lib
|   +-- lora-lib
|   +-- peak-detect
|   +-- rainflow
|       +-- rainflow.c
|       +-- rainflow.h
+-- platform
|   +-- nz32
|       +-- apps
|       +-- dev
|           +-- sensors
|               +-- fatigue-sensor.c
|               +-- fatigue-sensor.h
|       +-- lib
|       +-- contiki-conf.h
|       +-- contiki-nz32-main.c
|       +-- lorawan-conf.h
|       +-- Makefile.nz32
|       +-- platform-conf.h
+-- projects
|   +-- wireless-fatigue
|       +-- test-system
|           +-- bucket.c
|           +-- bucket.h
|           +-- Makefile
|           +-- project-conf.h
|           +-- test-system.c
|       +-- laboratory
|           +-- adc-continuous-collect
|               +-- main.c
|               +-- Makefile
|               +-- project-conf.h
|           +-- analogue-sensor-system-collect
|               +-- main.c
|               +-- Makefile
|               +-- project-conf.h
+-- tools
+-- .gitmodules.html
```
## Installation

After cloning the repository, add the `contiki` module as follows:

```
git submodule update --init --recursive
```

Please make sure the correct [GNU ARM Embedded Toolchain](https://developer.arm.com/tools-and-software/open-source-software/gnu-toolchain/gnu-rm/downloads) is installed. The version of the toolchain used in our work is the *GNU Tools for ARM Embedded Processors 6-2017-q2-update*.

## Running

An example application that allows to showcase our system has been developed and is located under

```
projects/wireless-fatigue/test-system
```

This application sends fatigue strain data via wireless (using LoRa) every hour. A LoRa gateway should be used for reception. Relevant LoRa configuration parameters can be found in the `lorawan-conf.h` file.

To build this example and program your device, type:

```
make
make test-system.flash
```

The application example can be modified to log any data via a serial-to-usb connection to a computer. By default, it is configured to use USART2 interface of the NZ32-SC151 board. Configuration of a baud rate of at least 115200 on the host computer is recommended. To visualize any data from the serial-to-usb connection, a serial port terminal application such as [CoolTerm](https://freeware.the-meiers.org/) can be used.

## Application configuration

A number of parameters can be configured to create different applications (in addition to the `projects/wireless-fatigue/test-system` application) using our repository. Please see:

```
platform/nz32/lorawan-conf.h
platform/nz32/Makefile.nz32
platform/nz32/platform-conf.h
```

The directory `examples` includes several examples for use as guidance in application development.

## Contact

* David Rodenas-Herraiz: david.rodenas@eng.cam.ac.uk
* Paul R. A. Fidler:     praf1@cam.ac.uk
