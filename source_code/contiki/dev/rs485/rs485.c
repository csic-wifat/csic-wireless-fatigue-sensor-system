/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \file
 *         RS-485 device driver
 *         Device Driver for the RS485 transceiver module from Modtronix
 *         Engineering
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lib/ringbuf.h"
#include "rs485.h"
/*---------------------------------------------------------------------------*/
#ifdef RS485_CONF_BUFSIZE
#define BUFSIZE (RS485_CONF_BUFSIZE)
#else /* RS485_CONF_BUFSIZE */
/*! Modbus_Application_Protocol_V1_1b.pdf Chapter 4 Section 1 Page 5
  * RS232 / RS485 ADU = 253 bytes + slave (1 byte) + CRC (2 bytes) = 256 bytes
  */
#define BUFSIZE (256)
#endif /* RS485_CONF_BUFSIZE */
/*---------------------------------------------------------------------------*/
static struct ringbuf rxbuf;
static uint8_t rxbuf_data[BUFSIZE];
static int ptr = 0;
static uint8_t len = 0;
/*---------------------------------------------------------------------------*/
static volatile uint8_t wait_for_rx = 0;
/*---------------------------------------------------------------------------*/
/*!
  * This event signals the RS485 line is idle
  */
static process_event_t rs485_idle_event;
/*---------------------------------------------------------------------------*/
PROCESS(rs485_process, "RS485 transceiver driver");
process_event_t rs485_event_message;
/*---------------------------------------------------------------------------*/
int
rs485_input_byte(unsigned char c)
{
	static uint8_t overflow = 0; /* Buffer overflow: ignore until END */

	if(!overflow)
	{
		/* Add character */
    if(ringbuf_put(&rxbuf, c) == 0) {
      /* Buffer overflow: ignore the rest of the line */
      overflow = 1;
    }
	}
	else
	{
		/* Buffer overflowed:
		* Only (try to) add terminator characters, otherwise skip */
		if(ringbuf_put(&rxbuf, c) != 0) {
			overflow = 0;
		}
	}
	/* Wake up consumer process */
  process_poll(&rs485_process);

	return 0;
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(rs485_process, ev, data)
{
	static char buf[BUFSIZE];

  PROCESS_BEGIN();

  rs485_event_message = process_alloc_event();

  ptr = 0;

	while(1) {
		/* Fill application buffer until newline or empty */
		int c = ringbuf_get(&rxbuf);
		if( c == -1) {
			/* Buffer empty, wait for poll */
      PROCESS_YIELD();

      if(ev == rs485_idle_event)
      {
      	if(ptr)
      	{
         	/* Add number of received characters at the beginning of the
         	 * buffer */
        	buf[0] = len;
        	/* Data */
		      process_post(PROCESS_BROADCAST, rs485_event_message, buf);

		      /* Wait until all processes have handled the serial line event */
		      if(PROCESS_ERR_OK ==
		        process_post(PROCESS_CURRENT(), PROCESS_EVENT_CONTINUE, NULL)) {
		        PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_CONTINUE);
		      }
	      	ptr = 0;
	      	len = 0;
      	}
      }
		} else {
			if(ptr < BUFSIZE-1) {
				buf[++ptr] = (uint8_t)c;
				len++;
			} else {
       	/* Ignore character (wait for rs485 line idle) */
      }
		}
	}
	PROCESS_END();
}
/*---------------------------------------------------------------------------*/
void
rs485_idle_callback(void)
{
	if(rs485_idle_event)
	{
		process_post(PROCESS_BROADCAST, rs485_idle_event, (process_data_t)NULL);
	}
}
/*---------------------------------------------------------------------------*/
rs485_result_t
rs485_send(const unsigned char *buffer, int size, rs485_wait_t wait_rx)
{
	wait_for_rx = wait_rx;
	return rs485_arch_write(buffer, size);
}
/*---------------------------------------------------------------------------*/
uint8_t
rs485_active(void)
{
	return wait_for_rx;
}
/*---------------------------------------------------------------------------*/
rs485_result_t
rs485_init(uint32_t baudrate, uint8_t bytesize, uint8_t stopbits,
					 uint8_t parity)
{
	static int init_done;

	wait_for_rx = 0;

	if (rs485_arch_init(rs485_idle_callback, baudrate,
		bytesize, stopbits, parity) != RS485_RESULT_OK)
	{
		return RS485_RESULT_ERROR;
	}

	if(!init_done)
	{
		ringbuf_init(&rxbuf, rxbuf_data, (uint8_t)sizeof(rxbuf_data));
		rs485_idle_event = process_alloc_event();
		process_start(&rs485_process, NULL);
		init_done = 1;
	}

	return RS485_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
rs485_result_t
rs485_deinit(void)
{
	wait_for_rx = 0;
	ptr = 0;
	len = 0;
	return rs485_arch_deinit();
}
/*---------------------------------------------------------------------------*/