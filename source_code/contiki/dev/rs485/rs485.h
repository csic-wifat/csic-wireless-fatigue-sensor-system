/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/*---------------------------------------------------------------------------*/
#ifndef _RS485_H_
#define	_RS485_H_

#include <stdint.h>
/*---------------------------------------------------------------------------*/
typedef enum
{
	RS485_RESULT_OK = 0,
	RS485_RESULT_NOT_SUPPORTED,
  RS485_RESULT_INVALID_VALUE,
	RS485_RESULT_ERROR
} rs485_result_t;
/*---------------------------------------------------------------------------*/
typedef enum {
	RS485_NO_WAIT = 0,
	RS485_WAIT = 1
}rs485_wait_t;
/*---------------------------------------------------------------------------*/
extern process_event_t rs485_event_message;
/*---------------------------------------------------------------------------*/
rs485_result_t rs485_init(uint32_t baudrate, uint8_t bytesize,
	uint8_t stopbits, uint8_t parity);
rs485_result_t rs485_deinit(void);
rs485_result_t rs485_send(const unsigned char *buffer, int size, rs485_wait_t wait_rx);

uint8_t rs485_active(void);
/**
 * Input byte from the RS485 transceiver module.
 *
 * This function is called by the RS232/SIO device driver to pass
 * incoming bytes from the RS485 module. The function can be called
 * from an interrupt context.
 *
 * For systems using low-power CPU modes, the return value of the
 * function can be used to determine if the CPU should be woken up or
 * not. If the function returns non-zero, the CPU should be powered
 * up. If the function returns zero, the CPU can continue to be
 * powered down.
 *
 * \param c The data that is to be received from the sensor
 *
 * \return Non-zero if the CPU should be powered up, zero otherwise.
 */
int rs485_input_byte(unsigned char c);

void rs485_idle_callback(void);
/*---------------------------------------------------------------------------*/
/*
 * These machine dependent functions and an interrupt service routine
 * must be provided externally (rs485_arch.c).
 */
rs485_result_t rs485_arch_init(void (*c)(void), uint32_t baudrate,
	uint8_t bytesize, uint8_t stopbits, uint8_t parity);
rs485_result_t rs485_arch_deinit(void);
rs485_result_t rs485_arch_writeb(unsigned char ch);
rs485_result_t rs485_arch_write(const unsigned char *buffer, int size);
/*---------------------------------------------------------------------------*/
#endif /* _RS485_H_ */
