/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup sx1276
 * @{
 *
 * Implementation of sx1276 Contiki radio interface
 *
 * @{
 *
 * \file
 * Driver for sx1276 Contiki radio interface
 *
 * \author  David Rodenas-Herraiz
 */
/*---------------------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>
#include "contiki.h"

#include "sx1276-radio-driver.h"
#include "sx1276-arch.h"
#include "lora-radio.h"
#include "sx1276.h"

#include "dev/leds.h"

#include "net/rime/rime.h"
#include "net/netstack.h"
/*---------------------------------------------------------------------------*/
#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...) do {} while (0)
#endif
/*---------------------------------------------------------------------------*/
#define CLEAR_RXBUF() (sx1276_rxbuf[0] = 0)
#define IS_RXBUF_EMPTY() (sx1276_rxbuf[0] == 0)
/*---------------------------------------------------------------------------*/
#if defined( LORA_CONF_RF_FREQUENCY )

#define RF_FREQUENCY                                LORA_CONF_RF_FREQUENCY

#else /* LORA_CONF_RF_FREQUENCY */

#if defined( REGION_AS923 )

#define RF_FREQUENCY                                923000000 // Hz

#elif defined( REGION_AU915 )

#define RF_FREQUENCY                                915000000 // Hz

#elif defined( REGION_CN470 )

#define RF_FREQUENCY                                470000000 // Hz

#elif defined( REGION_CN779 )

#define RF_FREQUENCY                                779000000 // Hz

#elif defined( REGION_EU433 )

#define RF_FREQUENCY                                433000000 // Hz

#elif defined( REGION_EU868 )

#define RF_FREQUENCY                                868000000 // Hz

#elif defined( REGION_KR920 )

#define RF_FREQUENCY                                920000000 // Hz

#elif defined( REGION_IN865 )

#define RF_FREQUENCY                                865000000 // Hz

#elif defined( REGION_US915 )

#define RF_FREQUENCY                                915000000 // Hz

#elif defined( REGION_US915_HYBRID )

#define RF_FREQUENCY                                915000000 // Hz

#else
    #error "Please define a frequency band in the compiler options."
#endif

#endif /* LORA_CONF_RF_FREQUENCY */
/*---------------------------------------------------------------------------*/
// Set current params to initial value
int32_t sx1276_cca_threshold    = LORA_CLEAR_CHANNEL_RSSI_THRESHOLD;
/*---------------------------------------------------------------------------*/
extern SX1276_t SX1276;
/*---------------------------------------------------------------------------*/
/* Incoming data buffer, the first byte will contain the length of the packet */
static uint8_t sx1276_rxbuf[LORA_MAX_PAYLOAD_SIZE + 1];
static RadioEvents_t RadioEvents;
static int packet_is_prepared = 0;
static const void *packet_payload;
static unsigned short packet_payload_len = 0;
int16_t sx1276_last_rssi = 0;
int8_t sx1276_last_snr = 0;
/*---------------------------------------------------------------------------*/
static int sx1276_init(void);
static void sx1276_reset(void);
static int sx1276_prepare(const void *payload, unsigned short payload_len);
static int sx1276_transmit(unsigned short payload_len);
static int sx1276_send(const void *data, unsigned short len);
static int sx1276_read(void *buf, unsigned short bufsize);
static int sx1276_channel_clear(void);
static int sx1276_receiving_packet(void);
static int sx1276_pending_packet(void);
static int sx1276_on(void);
static int sx1276_off(void);
static radio_result_t sx1276_set_value(radio_param_t param, radio_value_t value);
static radio_result_t sx1276_get_value(radio_param_t param, radio_value_t *value);
static radio_result_t sx1276_get_object(radio_param_t param, void *dest, size_t size);
static radio_result_t sx1276_set_object(radio_param_t param, const void *src, size_t size);
/*---------------------------------------------------------------------------*/
#ifdef SX1276_CONF_RESET_RX_DURATION
#define RESET_RX_DURATION           ( SX1276_CONF_RESET_RX_DURATION )
#else /* SX1276_CONF_RESET_RX_DURATION */
#define RESET_RX_DURATION           ( 1800 * CLOCK_SECOND )
#endif /* SX1276_CONF_RESET_RX_DURATION */

#ifdef SX1276_CONF_CARRIER_SENSE_TIME
#define SX1276_CARRIER_SENSE_TIME   ( SX1276_CONF_CARRIER_SENSE_TIME )
#else /* SX1276_CONF_CARRIER_SENSE_TIME */
#define SX1276_CARRIER_SENSE_TIME   ( 6 )
#endif /* SX1276_CONF_CARRIER_SENSE_TIME */

struct ctimer rx_reset_ctimer;
static void sx1276_rx_reset_callback(void *p);
/*---------------------------------------------------------------------------*/
static int8_t
get_max_tx_power(void)
{
  int8_t power;
  uint8_t paConfig = SX1276Read( REG_PACONFIG );
  uint8_t paDac = SX1276Read( REG_PADAC );

  paConfig = ( paConfig & RF_PACONFIG_PASELECT_MASK ) | SX1276GetPaSelect( SX1276.Settings.Channel );
  paConfig = ( paConfig & RF_PACONFIG_MAX_POWER_MASK ) | 0x70;

  if( ( paConfig & RF_PACONFIG_PASELECT_PABOOST ) == RF_PACONFIG_PASELECT_PABOOST )
  {
    if( ( paDac & RF_PADAC_20DBM_ON ) == RF_PADAC_20DBM_ON ) {
      power = 20;
    } else {
      power = 17;
    }
  }
  else
  {
    power = 14;
  }
  return power;
};
/*---------------------------------------------------------------------------*/
static int8_t
get_min_tx_power(void)
{
  int8_t power;
  uint8_t paConfig = SX1276Read( REG_PACONFIG );
  uint8_t paDac = SX1276Read( REG_PADAC );

  paConfig = ( paConfig & RF_PACONFIG_PASELECT_MASK ) | SX1276GetPaSelect( SX1276.Settings.Channel );
  paConfig = ( paConfig & RF_PACONFIG_MAX_POWER_MASK ) | 0x70;

  if( ( paConfig & RF_PACONFIG_PASELECT_PABOOST ) == RF_PACONFIG_PASELECT_PABOOST )
  {
    if( ( paDac & RF_PADAC_20DBM_ON ) == RF_PADAC_20DBM_ON ) {
      power = 5;
    } else {
      power = 2;
    }
  }
  else
  {
    power = -1;
  }
  return power;
};
/*---------------------------------------------------------------------------*/
lora_radio_callbacks_t radio_callbacks = { NULL, NULL, NULL, NULL, NULL, NULL };
/*---------------------------------------------------------------------------*/
PROCESS(sx1276_contiki_process, "sx1276 radio process");
/*---------------------------------------------------------------------------*/
static int
sx1276_init(void)
{
  PRINTF("SX1276 (%s)\n", __FUNCTION__);

  /* Radio initialization */
  RadioEvents.TxDone = OnTxDone;
  RadioEvents.RxDone = OnRxDone;
  RadioEvents.TxTimeout = OnTxTimeout;
  RadioEvents.RxTimeout = OnRxTimeout;
  RadioEvents.RxError = OnRxError;
  if (strcmp(NETSTACK_RDC.name, "LoRaMac") != 0)
  {
    RadioEvents.CadDone = OnCadDone;
  }

  Radio.Init(&RadioEvents);
  Radio.SetChannel(RF_FREQUENCY);

#if defined(USE_MODEM_LORA)
  Radio.SetTxConfig(MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
                LORA_SPREADING_FACTOR, LORA_CODINGRATE,
                LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON, LORA_CRC_ON,
                LORA_FREQUENCY_HOPPING_ON, LORA_HOPPING_PERIOD,
                LORA_IQ_INVERSION_ON, TX_TIMEOUT_VALUE);
  Radio.SetRxConfig(MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
                LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
                LORA_FIXED_PAYLOAD_LENGTH, LORA_CRC_ON,
                LORA_FREQUENCY_HOPPING_ON, LORA_HOPPING_PERIOD,
                LORA_IQ_INVERSION_ON, RX_CONTINUOUS_MODE);

  sx1276_cca_threshold    = LORA_CLEAR_CHANNEL_RSSI_THRESHOLD;

  PRINTF("LoRa radio initialized with:\n");

  PRINTF(" Freq: %d Hz\n", RF_FREQUENCY);
  PRINTF(" Tx power: %ddBm\n", TX_OUTPUT_POWER);
  /*PRINTF(" BW: (%d) %dHz\n", LORA_BANDWIDTH,
    (LORA_BANDWIDTH == 0) ? 7800 :
    (LORA_BANDWIDTH == 1) ? 10400 :
    (LORA_BANDWIDTH == 2) ? 15600 :
    (LORA_BANDWIDTH == 3) ? 20800 :
    (LORA_BANDWIDTH == 4) ? 31200 :
    (LORA_BANDWIDTH == 5) ? 41400 :
    (LORA_BANDWIDTH == 6) ? 62500 :
    (LORA_BANDWIDTH == 7) ? 125000 :
    (LORA_BANDWIDTH == 8) ? 250000 : 500000
    );*/

  PRINTF(" BW: (%d) %dHz\n", LORA_BANDWIDTH,
    (LORA_BANDWIDTH == 0) ? 125000 :
    (LORA_BANDWIDTH == 1) ? 250000 :
    (LORA_BANDWIDTH == 2) ? 500000 : 0
    );

  PRINTF(" Spreading Factor: (%d) %d chips\n", LORA_SPREADING_FACTOR,
    (LORA_SPREADING_FACTOR == 6)?64:((LORA_SPREADING_FACTOR == 7) ? 128:((LORA_SPREADING_FACTOR == 8) ? 256:((LORA_SPREADING_FACTOR == 9) ? 512:((LORA_SPREADING_FACTOR == 10) ? 1024:((LORA_SPREADING_FACTOR == 11) ? 2048:4096))))));
  PRINTF(" Error Coding: (%d) 4/%d\n", LORA_CODINGRATE,
    (LORA_CODINGRATE == 0)?5:((LORA_CODINGRATE == 1) ? 6:((LORA_CODINGRATE == 2) ? 7:8)));

#elif defined(USE_MODEM_FSK)
  Radio.SetTxConfig(MODEM_FSK, TX_OUTPUT_POWER, FSK_FDEV, 0,
                FSK_DATARATE, 0,
                FSK_PREAMBLE_LENGTH, FSK_FIX_LENGTH_PAYLOAD_ON,
                FSK_CRC_ON, 0, 0, 0, TX_TIMEOUT_VALUE);
  Radio.SetRxConfig(MODEM_FSK, FSK_BANDWIDTH, FSK_DATARATE,
                0, FSK_AFC_BANDWIDTH, FSK_PREAMBLE_LENGTH,
                0, FSK_FIX_LENGTH_PAYLOAD_ON, FSK_FIXED_PAYLOAD_LENGTH,
                FSK_CRC_ENABLED, 0, 0, 0, RX_CONTINUOUS_MODE);
#else
#error "Please define a modem in the compiler options."
#endif

  process_start(&sx1276_contiki_process, NULL);
  return 0;
}
/*---------------------------------------------------------------------------*/
static void
sx1276_reset(void)
{
  sx1276_off( );
  sx1276_on( );
}
/*---------------------------------------------------------------------------*/
static int
sx1276_prepare(const void *payload, unsigned short payload_len)
{
  PRINTF("SX1276 (%s): %u bytes\n", __FUNCTION__, payload_len);
  packet_is_prepared = 0;

  /* Checks if the payload length is supported */
  if (payload_len > LORA_MAX_PAYLOAD_SIZE) {
    return RADIO_TX_ERR;
  }

  packet_payload = payload;
  packet_payload_len = payload_len;
  packet_is_prepared = 1;

  return RADIO_TX_OK;
}
/*---------------------------------------------------------------------------*/
static int
sx1276_transmit(unsigned short payload_len)
{
  PRINTF("SX1276 (%s) prepared %u\n", __FUNCTION__, packet_is_prepared);
  if (!packet_is_prepared) {
    return RADIO_TX_ERR;
  }
#if 0
  {
    uint8_t i = 0;
    printf("TX >> ");
    while(i < packet_payload_len){
      printf("%02X", *((uint8_t *)packet_payload + i));
      if(++i % 4 == 0) {
        printf(" ");
      }
    }
    printf("\n");
  }
#endif
  ENERGEST_OFF(ENERGEST_TYPE_LISTEN);
  ENERGEST_ON(ENERGEST_TYPE_TRANSMIT);

  Radio.Send((uint8_t *)packet_payload, packet_payload_len);
  packet_is_prepared = 0;

  PRINTF("SX1276 (%s): packet sent (size %d)\n", __FUNCTION__, payload_len);

  return RADIO_TX_OK;
}
/*---------------------------------------------------------------------------*/
static int
sx1276_send(const void *payload, unsigned short payload_len)
{
  PRINTF("SX1276 (%s)\n", __FUNCTION__);

  if (sx1276_prepare(payload, payload_len) == RADIO_TX_ERR) {
    return RADIO_TX_ERR;
  }
  return sx1276_transmit(payload_len);
}
/*---------------------------------------------------------------------------*/
static int
sx1276_read(void *buf, unsigned short bufsize)
{
  /* Checks if the RX buffer is empty */
  if (IS_RXBUF_EMPTY()) {
    PRINTF("sx1276 (%s): rx buffer empty\n", __FUNCTION__);
    return 0;
  }

  /* Checks if buffer has the correct size */
  if (bufsize < sx1276_rxbuf[0]) {
    PRINTF("sx1276 (%s): too small buffer\n", __FUNCTION__);
    return 0;
  }

  /* Copies the packet received */
  memcpy(buf, sx1276_rxbuf + 1, sx1276_rxbuf[0]);
  packetbuf_set_attr(PACKETBUF_ATTR_RSSI, sx1276_last_rssi);
  bufsize = sx1276_rxbuf[0];
  CLEAR_RXBUF();

  return bufsize;
}
/*---------------------------------------------------------------------------*/
static int
sx1276_channel_clear(void)
{
  PRINTF("SX1276 (%s)\n", __FUNCTION__);
  bool channel_clear;

  if( SX1276.Settings.Modem == MODEM_LORA ) {
    channel_clear = Radio.IsChannelFree(MODEM_LORA,
                                        SX1276.Settings.Channel,
                                        sx1276_cca_threshold,
                                        SX1276_CARRIER_SENSE_TIME);
  } else {
    channel_clear = Radio.IsChannelFree(MODEM_FSK,
                                        SX1276.Settings.Channel,
                                        sx1276_cca_threshold,
                                        SX1276_CARRIER_SENSE_TIME);
  }

  PRINTF("SX1276 (%s)\n", __FUNCTION__);
  return (int)channel_clear;
}
/*---------------------------------------------------------------------------*/
static int
sx1276_receiving_packet(void)
{
  return 0;
}
/*---------------------------------------------------------------------------*/
static int
sx1276_pending_packet(void)
{
  PRINTF("SX1276 (%s)\n", __FUNCTION__);
  return !IS_RXBUF_EMPTY();
}
/*---------------------------------------------------------------------------*/
static int
sx1276_off(void)
{
  ctimer_stop(&rx_reset_ctimer);
  ENERGEST_OFF(ENERGEST_TYPE_LISTEN);

  Radio.Sleep( );
  PRINTF("SX1276 (%s)\n", __FUNCTION__);
  return 0;
}
/*---------------------------------------------------------------------------*/
static int
sx1276_on(void)
{
  PRINTF("SX1276 (%s)\n", __FUNCTION__);
  /* Turn radio into rx mode */
  Radio.Rx( 0 );

  ENERGEST_ON(ENERGEST_TYPE_LISTEN);

  /* Start reset timer */
  ctimer_set(&rx_reset_ctimer, RESET_RX_DURATION,
               sx1276_rx_reset_callback, NULL);

  return 0;
}
/*---------------------------------------------------------------------------*/
static void
sx1276_rx_reset_callback(void *p)
{
  PRINTF("%s\n", __FUNCTION__);
  // Reset RX to redo startup calibrations
  sx1276_reset();
}
/*---------------------------------------------------------------------------*/
static radio_result_t
sx1276_get_value(radio_param_t param, radio_value_t *value)
{
  if(!value) {
    return RADIO_RESULT_INVALID_VALUE;
  }

  switch(param) {
    case RADIO_PARAM_POWER_MODE:
      *value = (Radio.GetStatus() != RF_IDLE) ? RADIO_POWER_MODE_ON : RADIO_POWER_MODE_OFF;
      return RADIO_RESULT_OK;

    case RADIO_PARAM_CHANNEL:
      *value = SX1276.Settings.Channel;
      return RADIO_RESULT_OK;

    case RADIO_PARAM_PAN_ID:
    case RADIO_PARAM_16BIT_ADDR:
    case RADIO_PARAM_RX_MODE:
    case RADIO_PARAM_TX_MODE:
      return RADIO_RESULT_NOT_SUPPORTED;

    case RADIO_PARAM_TXPOWER:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.Power;
      }
      else
      {
        *value = SX1276.Settings.Fsk.Power;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_CCA_THRESHOLD:
    case RADIO_PARAM_RSSI_THRESHOLD:
      *value = (radio_value_t)sx1276_cca_threshold;
      return RADIO_RESULT_OK;

    case RADIO_PARAM_RSSI:
      /* Return the RSSI value in dBm */
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276ReadRssi(MODEM_LORA);
      }
      else
      {
        *value = SX1276ReadRssi(MODEM_FSK);
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_LAST_SNR:
      /* SNR of the last packet received */
      *value = sx1276_last_snr;
      return RADIO_RESULT_OK;

    case RADIO_PARAM_64BIT_ADDR:
      return RADIO_RESULT_NOT_SUPPORTED;

    case RADIO_CONST_CHANNEL_MIN:
      return RADIO_RESULT_NOT_SUPPORTED;

    case RADIO_CONST_CHANNEL_MAX:
      return RADIO_RESULT_NOT_SUPPORTED;

    case RADIO_PARAM_LAST_RSSI:
      /* RSSI of the last packet received */
      *value = sx1276_last_rssi;
      return RADIO_RESULT_OK;

    case RADIO_CONST_TXPOWER_MIN:
      *value = get_min_tx_power();
      return RADIO_RESULT_OK;

    case RADIO_CONST_TXPOWER_MAX:
      *value = get_max_tx_power();
      return RADIO_RESULT_OK;

    case RADIO_PARAM_RADIOSTATE:
      *value = Radio.GetStatus();
      return RADIO_RESULT_OK;

    case RADIO_PARAM_MODEM:
      *value = SX1276.Settings.Modem;
      return RADIO_RESULT_OK;

    case RADIO_PARAM_BANDWIDTH:
      if( SX1276.Settings.Modem == MODEM_FSK )
      {
        *value = SX1276.Settings.Fsk.Bandwidth;
      }
      else
      {
        *value = SX1276.Settings.LoRa.Bandwidth - 7;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_DATARATE:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.Datarate;
      }
      else
      {
        *value = SX1276.Settings.Fsk.Datarate;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_CODINGRATE:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.Coderate;
        return RADIO_RESULT_OK;
      }
      else
      {
        return RADIO_RESULT_NOT_SUPPORTED;
      }

    case RADIO_PARAM_AFC_BANDWIDTH:
      if( SX1276.Settings.Modem == MODEM_FSK )
      {
        *value = SX1276.Settings.Fsk.BandwidthAfc;
        return RADIO_RESULT_OK;
      }
      else
      {
        return RADIO_RESULT_NOT_SUPPORTED;
      }

    case RADIO_PARAM_PREAMBLE_LENGTH:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.PreambleLen;
      }
      else
      {
        *value = SX1276.Settings.Fsk.PreambleLen;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_FIXED_PAYLOAD_LENGTH_ON:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.FixLen ? 1 : 0;
      }
      else
      {
        *value = SX1276.Settings.Fsk.FixLen ? 1 : 0;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_PAYLOAD_LENGTH:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.PayloadLen;
      }
      else
      {
        *value = SX1276.Settings.Fsk.PayloadLen;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_PUBLIC_NETWORK:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.PublicNetwork ? 1 : 0;
        return RADIO_RESULT_OK;
      }
      else
      {
        return RADIO_RESULT_NOT_SUPPORTED;
      }

    case RADIO_PARAM_CRC_ON:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.CrcOn ? 1 : 0;
      }
      else
      {
        *value = SX1276.Settings.Fsk.CrcOn ? 1 : 0;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_FREQ_HOP_ON:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.FreqHopOn ? 1 : 0;
        return RADIO_RESULT_OK;
      }
      else
      {
        return RADIO_RESULT_NOT_SUPPORTED;
      }

    case RADIO_PARAM_HOP_PERIOD:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.HopPeriod;
        return RADIO_RESULT_OK;
      }
      else
      {
        return RADIO_RESULT_NOT_SUPPORTED;
      }

    case RADIO_PARAM_IQ_INVERTED:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.IqInverted ? 1 : 0;
      }
      else
      {
        *value = SX1276.Settings.Fsk.IqInverted ? 1 : 0;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_RX_CONTINUOUS:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.RxContinuous ? 1 : 0;
      }
      else
      {
        *value = SX1276.Settings.Fsk.RxContinuous ? 1 : 0;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_TX_TIMEOUT:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        *value = SX1276.Settings.LoRa.TxTimeout;
      }
      else
      {
        *value = SX1276.Settings.Fsk.TxTimeout;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_RX_SINGLE_TIMEOUT:
      if( SX1276.Settings.Modem == MODEM_FSK )
      {
        *value = SX1276.Settings.Fsk.RxSingleTimeout;
        return RADIO_RESULT_OK;
      }
      else
      {
        return RADIO_RESULT_NOT_SUPPORTED;
      }

    case RADIO_PARAM_RANDOM:
      *value = Radio.Random( );
      return RADIO_RESULT_OK;

    default:
      return RADIO_RESULT_NOT_SUPPORTED;
  }
}
/*---------------------------------------------------------------------------*/
static radio_result_t
sx1276_set_value(radio_param_t param, radio_value_t value)
{
  static volatile SX1276_t sx1276_params;

  sx1276_params.Settings.Modem    = SX1276.Settings.Modem;
  sx1276_params.Settings.Channel  = SX1276.Settings.Channel;
  if( sx1276_params.Settings.Modem == MODEM_LORA )
  {
    sx1276_params.Settings.LoRa.Power               = SX1276.Settings.LoRa.Power;
    sx1276_params.Settings.LoRa.Bandwidth           = SX1276.Settings.LoRa.Bandwidth;
    sx1276_params.Settings.LoRa.Datarate            = SX1276.Settings.LoRa.Datarate;
    sx1276_params.Settings.LoRa.LowDatarateOptimize = SX1276.Settings.LoRa.LowDatarateOptimize;
    sx1276_params.Settings.LoRa.Coderate            = SX1276.Settings.LoRa.Coderate;
    sx1276_params.Settings.LoRa.PreambleLen         = SX1276.Settings.LoRa.PreambleLen;
    sx1276_params.Settings.LoRa.FixLen              = SX1276.Settings.LoRa.FixLen;
    sx1276_params.Settings.LoRa.PayloadLen          = SX1276.Settings.LoRa.PayloadLen;
    sx1276_params.Settings.LoRa.CrcOn               = SX1276.Settings.LoRa.CrcOn;
    sx1276_params.Settings.LoRa.FreqHopOn           = SX1276.Settings.LoRa.FreqHopOn;
    sx1276_params.Settings.LoRa.HopPeriod           = SX1276.Settings.LoRa.HopPeriod;
    sx1276_params.Settings.LoRa.IqInverted          = SX1276.Settings.LoRa.IqInverted;
    sx1276_params.Settings.LoRa.RxContinuous        = SX1276.Settings.LoRa.RxContinuous;
    sx1276_params.Settings.LoRa.TxTimeout           = SX1276.Settings.LoRa.TxTimeout;
    sx1276_params.Settings.LoRa.PublicNetwork       = SX1276.Settings.LoRa.PublicNetwork;
  }
  else
  {
    sx1276_params.Settings.Fsk.Power                = SX1276.Settings.Fsk.Power;
    sx1276_params.Settings.Fsk.Fdev                 = SX1276.Settings.Fsk.Fdev;
    sx1276_params.Settings.Fsk.Bandwidth            = SX1276.Settings.Fsk.Bandwidth;
    sx1276_params.Settings.Fsk.BandwidthAfc         = SX1276.Settings.Fsk.BandwidthAfc;
    sx1276_params.Settings.Fsk.Datarate             = SX1276.Settings.Fsk.Datarate;
    sx1276_params.Settings.Fsk.PreambleLen          = SX1276.Settings.Fsk.PreambleLen;
    sx1276_params.Settings.Fsk.FixLen               = SX1276.Settings.Fsk.FixLen;
    sx1276_params.Settings.Fsk.PayloadLen           = SX1276.Settings.Fsk.PayloadLen;
    sx1276_params.Settings.Fsk.CrcOn                = SX1276.Settings.Fsk.CrcOn;
    sx1276_params.Settings.Fsk.IqInverted           = SX1276.Settings.Fsk.IqInverted;
    sx1276_params.Settings.Fsk.RxContinuous         = SX1276.Settings.Fsk.RxContinuous;
    sx1276_params.Settings.Fsk.TxTimeout            = SX1276.Settings.Fsk.TxTimeout;
    sx1276_params.Settings.Fsk.RxSingleTimeout      = SX1276.Settings.Fsk.RxSingleTimeout;
  }


  switch(param) {
    case RADIO_PARAM_POWER_MODE:
      if(value == RADIO_POWER_MODE_ON)
      {
        sx1276_on( );
        return RADIO_RESULT_OK;
      }
      if(value == RADIO_POWER_MODE_OFF)
      {
        sx1276_off( );
        return RADIO_RESULT_OK;
      }
      return RADIO_RESULT_INVALID_VALUE;

    case RADIO_PARAM_CHANNEL:
      Radio.SetChannel(value);
      //sx1276_reset();
      return RADIO_RESULT_OK;

    case RADIO_PARAM_PAN_ID:
    case RADIO_PARAM_16BIT_ADDR:
      return RADIO_RESULT_NOT_SUPPORTED;

    case RADIO_PARAM_RX_MODE:
      PRINTF("Radio turned on for %u msec\n", value);
      Radio.Rx(value);
      return RADIO_RESULT_OK;

    case RADIO_PARAM_TX_MODE:
      return RADIO_RESULT_NOT_SUPPORTED;

    case RADIO_PARAM_CCA_THRESHOLD:
    case RADIO_PARAM_RSSI_THRESHOLD:
      sx1276_cca_threshold = (int32_t)value;
      return RADIO_RESULT_OK;

    case RADIO_PARAM_TXPOWER:
      if(value < get_min_tx_power() || value > get_max_tx_power())
      {
        return RADIO_RESULT_INVALID_VALUE;
      }

      SX1276SetRfTxPower( (int8_t)value );

      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        SX1276.Settings.LoRa.Power = (int8_t)value;
      }
      else
      {
        SX1276.Settings.Fsk.Power = (int8_t)value;
      }

      return RADIO_RESULT_OK;

    case RADIO_PARAM_64BIT_ADDR:
      return RADIO_RESULT_NOT_SUPPORTED;

     case RADIO_PARAM_MODEM:
      Radio.SetModem( value );
      return RADIO_RESULT_OK;

    case RADIO_PARAM_BANDWIDTH:
      if( SX1276.Settings.Modem == MODEM_FSK )
      {
        sx1276_params.Settings.Fsk.Bandwidth = (uint32_t)value;
      }
      else
      {
        value += 7;
        if( (uint32_t)value > 9 )
        {
          return RADIO_RESULT_INVALID_VALUE;
        }
        sx1276_params.Settings.LoRa.Bandwidth = (uint32_t)value;
      }
      break;

    case RADIO_PARAM_DATARATE:
      if( (uint32_t)value > 5 && (uint32_t)value < 13)
      {
        if( SX1276.Settings.Modem == MODEM_LORA )
        {
          sx1276_params.Settings.LoRa.Datarate = (uint32_t)value;
        }
        else
        {
          sx1276_params.Settings.Fsk.Datarate = (uint32_t)value;
        }
      }
      else
      {
          return RADIO_RESULT_INVALID_VALUE;
      }
      break;

    case RADIO_PARAM_CODINGRATE:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        if( (uint8_t)value > 0 && (uint8_t)value < 5 )
        {
          sx1276_params.Settings.LoRa.Coderate = (uint8_t)value;
        }
        else
        {
          return RADIO_RESULT_INVALID_VALUE;
        }
      }
      else
      {
        return RADIO_RESULT_NOT_SUPPORTED;
      }
      break;

    case RADIO_PARAM_AFC_BANDWIDTH:
      if( SX1276.Settings.Modem == MODEM_FSK )
      {
        if( (uint32_t)value >= 2600 && (uint32_t)value <= 250000 )
        {
          sx1276_params.Settings.Fsk.Fdev = (uint32_t)value;
          break;
        }
        else
        {
          return RADIO_RESULT_INVALID_VALUE;
        }
      }
      else
      {
        return RADIO_RESULT_NOT_SUPPORTED;
      }

    case RADIO_PARAM_PREAMBLE_LENGTH:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        sx1276_params.Settings.LoRa.PreambleLen = (uint16_t)value;
      }
      else
      {
        sx1276_params.Settings.Fsk.PreambleLen = (uint16_t)value;
      }
      break;

    case RADIO_PARAM_FIXED_PAYLOAD_LENGTH_ON:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        if((bool)value == sx1276_params.Settings.LoRa.FixLen) {
          return RADIO_RESULT_OK;
        }
        sx1276_params.Settings.LoRa.FixLen = !sx1276_params.Settings.LoRa.FixLen;
      }
      else
      {
        if((bool)value == sx1276_params.Settings.Fsk.FixLen) {
          return RADIO_RESULT_OK;
        }
        sx1276_params.Settings.Fsk.FixLen = !sx1276_params.Settings.Fsk.FixLen;
      }
      break;

    case RADIO_PARAM_PAYLOAD_LENGTH:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        sx1276_params.Settings.LoRa.PayloadLen = (uint8_t)value;
      }
      else
      {
        sx1276_params.Settings.Fsk.PayloadLen = (uint8_t)value;
      }
      break;

    case RADIO_PARAM_PUBLIC_NETWORK:
      Radio.SetPublicNetwork(value ? true : false);
      return RADIO_RESULT_OK;

    case RADIO_PARAM_CRC_ON:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        if((bool)value == sx1276_params.Settings.LoRa.CrcOn) {
          return RADIO_RESULT_OK;
        }
        sx1276_params.Settings.LoRa.CrcOn = !sx1276_params.Settings.LoRa.CrcOn;
      }
      else
      {
        if((bool)value == sx1276_params.Settings.Fsk.CrcOn) {
          return RADIO_RESULT_OK;
        }
        sx1276_params.Settings.Fsk.CrcOn = !sx1276_params.Settings.Fsk.CrcOn;
      }
      break;

    case RADIO_PARAM_FREQ_HOP_ON:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        if((bool)value == sx1276_params.Settings.LoRa.FreqHopOn) {
          return RADIO_RESULT_OK;
        }
        sx1276_params.Settings.LoRa.FreqHopOn = !sx1276_params.Settings.LoRa.FreqHopOn;
        break;
      }
      else
      {
        return RADIO_RESULT_NOT_SUPPORTED;
      }

    case RADIO_PARAM_HOP_PERIOD:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        sx1276_params.Settings.LoRa.HopPeriod = (uint8_t)value;
        break;
      }
      else
      {
        return RADIO_RESULT_NOT_SUPPORTED;
      }

    case RADIO_PARAM_IQ_INVERTED:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        if((bool)value == sx1276_params.Settings.LoRa.IqInverted) {
          return RADIO_RESULT_OK;
        }
        sx1276_params.Settings.LoRa.IqInverted = !sx1276_params.Settings.LoRa.IqInverted;
        break;
      }
      else
      {
        return RADIO_RESULT_NOT_SUPPORTED;
      }

    case RADIO_PARAM_RX_CONTINUOUS:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        if((bool)value == sx1276_params.Settings.LoRa.RxContinuous) {
          return RADIO_RESULT_OK;
        }
        sx1276_params.Settings.LoRa.RxContinuous = !sx1276_params.Settings.LoRa.RxContinuous;
      }
      else
      {
        if((bool)value == sx1276_params.Settings.Fsk.RxContinuous) {
          return RADIO_RESULT_OK;
        }
        sx1276_params.Settings.Fsk.RxContinuous = !sx1276_params.Settings.Fsk.RxContinuous;
      }
      break;

    case RADIO_PARAM_TX_TIMEOUT:
      if( SX1276.Settings.Modem == MODEM_LORA )
      {
        sx1276_params.Settings.LoRa.TxTimeout = (uint32_t)value;
      }
      else
      {
        sx1276_params.Settings.Fsk.TxTimeout = (uint32_t)value;
      }
      break;

    case RADIO_PARAM_STANDBY_MODE:
      PRINTF("SX1276 (Standby)\n");
      Radio.Standby( );
      return RADIO_RESULT_OK;

    default:
      return RADIO_RESULT_NOT_SUPPORTED;
  }

  if( SX1276.Settings.Modem == MODEM_LORA )
  {
    if (param != RADIO_PARAM_RX_CONTINUOUS)
    {
      Radio.SetTxConfig(MODEM_LORA,
                        sx1276_params.Settings.LoRa.Power,
                        0,
                        sx1276_params.Settings.LoRa.Bandwidth,
                        sx1276_params.Settings.LoRa.Datarate,
                        sx1276_params.Settings.LoRa.Coderate,
                        sx1276_params.Settings.LoRa.PreambleLen,
                        sx1276_params.Settings.LoRa.FixLen,
                        sx1276_params.Settings.LoRa.CrcOn,
                        sx1276_params.Settings.LoRa.FreqHopOn,
                        sx1276_params.Settings.LoRa.HopPeriod,
                        sx1276_params.Settings.LoRa.IqInverted,
                        sx1276_params.Settings.LoRa.TxTimeout);
    }
    if (param != RADIO_PARAM_TX_TIMEOUT)
    {
      Radio.SetRxConfig(MODEM_LORA,
                        sx1276_params.Settings.LoRa.Bandwidth,
                        sx1276_params.Settings.LoRa.Datarate,
                        sx1276_params.Settings.LoRa.Coderate,
                        0,
                        sx1276_params.Settings.LoRa.PreambleLen,
                        LORA_SYMBOL_TIMEOUT,
                        sx1276_params.Settings.LoRa.FixLen,
                        sx1276_params.Settings.LoRa.PayloadLen,
                        sx1276_params.Settings.LoRa.CrcOn,
                        sx1276_params.Settings.LoRa.FreqHopOn,
                        sx1276_params.Settings.LoRa.HopPeriod,
                        sx1276_params.Settings.LoRa.IqInverted,
                        sx1276_params.Settings.LoRa.RxContinuous);
    }
  }
  else
  {
    if (param != RADIO_PARAM_RX_CONTINUOUS)
    {
      Radio.SetTxConfig(MODEM_FSK,
                        sx1276_params.Settings.Fsk.Power,
                        sx1276_params.Settings.Fsk.Fdev,
                        0,
                        sx1276_params.Settings.Fsk.Datarate,
                        0,
                        sx1276_params.Settings.Fsk.PreambleLen,
                        sx1276_params.Settings.Fsk.FixLen,
                        sx1276_params.Settings.Fsk.CrcOn,
                        0,
                        0,
                        0,
                        sx1276_params.Settings.Fsk.TxTimeout);
    }

    if (param != RADIO_PARAM_TX_TIMEOUT)
    {
      Radio.SetRxConfig(MODEM_FSK,
                        sx1276_params.Settings.Fsk.Bandwidth,
                        sx1276_params.Settings.Fsk.Datarate,
                        0,
                        sx1276_params.Settings.Fsk.BandwidthAfc,
                        sx1276_params.Settings.Fsk.PreambleLen,
                        0,
                        sx1276_params.Settings.Fsk.FixLen,
                        sx1276_params.Settings.Fsk.PayloadLen,
                        sx1276_params.Settings.Fsk.CrcOn,
                        0,
                        0,
                        0,
                        sx1276_params.Settings.Fsk.RxContinuous);
    }
  }

  /* IMPORTANT. At this point, radio is turned to sleep mode
   * and will remain in this state unless otherwise specified */
  //sx1276_reset();

  return RADIO_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
static radio_result_t
sx1276_get_object(radio_param_t param, void *dest, size_t size)
{
  if(!dest) {
    return RADIO_RESULT_INVALID_VALUE;
  }
/*
  switch (param) {
    case RADIO_PARAM_OBJECT_TIME_ON_AIR:
      if(size == sizeof(sx1276_object_param_t))
      {
        sx1276_object_param_t * r_param = (sx1276_object_param_t *) dest;
        r_param->value = Radio.TimeOnAir( r_param->modem, (uint8_t)r_param->u8 );
        return RADIO_RESULT_OK;
      }
      else
      {
        return RADIO_RESULT_INVALID_VALUE;
      }
    default:
      return RADIO_RESULT_NOT_SUPPORTED;
  }
  */
  return RADIO_RESULT_NOT_SUPPORTED;
}
/*---------------------------------------------------------------------------*/
static radio_result_t
sx1276_set_object(radio_param_t param, const void *src, size_t size)
{
  if(!src) {
    return RADIO_RESULT_INVALID_VALUE;
  }

  switch(param) {
    case RADIO_PARAM_OBJECT_TX_CONFIG:
      if(size == sizeof(tx_config_t))
      {
        tx_config_t *conf = (tx_config_t *) src;
        PRINTF("%s: TX: %lu %lu %u %u %u %u %u %lu\n", __FUNCTION__, conf->bandwidth, conf->datarate , conf->coderate, conf->preambleLen, conf->fixLen, conf->crcOn, conf->FreqHopOn, conf->timeout);
        Radio.SetTxConfig( conf->modem      , conf->power    , conf->fdev,
                           conf->bandwidth  , conf->datarate , conf->coderate,
                           conf->preambleLen, conf->fixLen   , conf->crcOn,
                           conf->FreqHopOn  , conf->HopPeriod, conf->iqInverted,
                           conf->timeout );
      }
      else
      {
        return RADIO_RESULT_INVALID_VALUE;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_OBJECT_RX_CONFIG:
      if(size == sizeof(rx_config_t))
      {
        rx_config_t *conf = (rx_config_t *) src;
        PRINTF("%s: RX: %lu %lu %u \n", __FUNCTION__, conf->bandwidth, conf->datarate , conf->coderate);

        Radio.SetRxConfig( conf->modem      , conf->bandwidth   , conf->datarate,
                           conf->coderate   , conf->bandwidthAfc, conf->preambleLen,
                           conf->symbTimeout, conf->fixLen      , conf->payloadLen,
                           conf->crcOn      , conf->freqHopOn   , conf->hopPeriod,
                           conf->iqInverted , conf->rxContinuous );
      }
      else
      {
        return RADIO_RESULT_INVALID_VALUE;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_OBJECT_TX_CONTINUOUS_WAVE:
      if(size == sizeof(txCW_t))
      {
        txCW_t *txCWave = (txCW_t *) src;
        PRINTF("%s: TXCW: %lu %u %u\n", __FUNCTION__, txCWave->frequency, txCWave->power, txCWave->timeout);
        Radio.SetTxContinuousWave( txCWave->frequency, txCWave->power, txCWave->timeout );
      }
      else
      {
        return RADIO_RESULT_INVALID_VALUE;
      }
      return RADIO_RESULT_OK;

    case RADIO_PARAM_OBJECT_MAX_PAYLOAD_LENGTH:
    /*
      if(size == sizeof(sx1276_object_param_t))
      {
        sx1276_object_param_t * r_param = (sx1276_object_param_t *) src;
        Radio.SetMaxPayloadLength( r_param->modem, (uint8_t) r_param->u8 );
        return RADIO_RESULT_OK;
      }
      else
      {
        return RADIO_RESULT_INVALID_VALUE;
      }*/
    default:
      return RADIO_RESULT_NOT_SUPPORTED;
  }

  return RADIO_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(sx1276_contiki_process, ev, data)
{
  int len;
  PROCESS_BEGIN();

  PRINTF("sx1276_process: started\n");

  while(1) {
    PROCESS_YIELD_UNTIL(ev == PROCESS_EVENT_POLL);

    PRINTF("sx1276_process: calling receiver callback\n");

    packetbuf_clear();
    len = sx1276_read(packetbuf_dataptr(), PACKETBUF_SIZE);

    if (len > 0) {
      packetbuf_set_datalen(len);
      NETSTACK_RDC.input();
    }

    if(!IS_RXBUF_EMPTY()) {
      process_poll(&sx1276_contiki_process);
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
void
OnTxDone(void)
{
  PRINTF("SX1276 (%s) packet sent\n", __FUNCTION__);

#ifdef ENERGEST_CONF_LEVELDEVICE_LEVELS
  radio_value_t tx_power_t;
  sx1276_get_value(RADIO_PARAM_TXPOWER, &tx_power_t);
  ENERGEST_OFF_LEVEL(ENERGEST_TYPE_TRANSMIT, tx_power_t);
#endif
  ENERGEST_OFF(ENERGEST_TYPE_TRANSMIT);

  if ( radio_callbacks.tx_done != NULL )
  {
    radio_callbacks.tx_done();
  }
  else
  {
    ENERGEST_ON(ENERGEST_TYPE_LISTEN);
    sx1276_on( );
  }
}
/*---------------------------------------------------------------------------*/
void
OnRxDone(uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr)
{
  memcpy(sx1276_rxbuf + 1, payload, size);
  sx1276_rxbuf[0] = size;
  PRINTF("SX1276 (%s): packet received (size %d, rssi %d, snr %d)\n", __FUNCTION__, size, rssi, snr);
#if 0
  {
    uint8_t i=0;
    printf("RX >> ");
    while(i < size){
      printf("%02X", *((sx1276_rxbuf + 1) + i));
      if(++i % 4 == 0) {
        printf(" ");
      }
    }
    printf("\n");
  }
#endif

  // Save RSSI and SNR
  sx1276_last_rssi = rssi;
  sx1276_last_snr = snr;

  if ( radio_callbacks.rx_done != NULL )
  {
    PRINTF("Calling rx done callback\n");
    radio_callbacks.rx_done(payload, size, rssi, snr);
  }
  else
  {
    process_poll(&sx1276_contiki_process);
    // Restart rx after tx
    sx1276_reset();
  }
}
/*---------------------------------------------------------------------------*/
void
OnTxTimeout(void)
{
  PRINTF("SX1276 (%s): tx timeout\n", __FUNCTION__);
#ifdef ENERGEST_CONF_LEVELDEVICE_LEVELS
  radio_value_t tx_power_t;
  sx1276_get_value(RADIO_PARAM_TXPOWER, &tx_power_t);
  ENERGEST_OFF_LEVEL(ENERGEST_TYPE_TRANSMIT, tx_power_t);
#endif
  ENERGEST_OFF(ENERGEST_TYPE_TRANSMIT);

  if(radio_callbacks.tx_timeout != NULL)
  {
    radio_callbacks.tx_timeout();
  }
  else
  {
    sx1276_reset();
    ENERGEST_ON(ENERGEST_TYPE_LISTEN);
    // sx1276_on( );
  }
}
/*---------------------------------------------------------------------------*/
void
OnRxTimeout(void)
{
  PRINTF("SX1276 (%s): rx timeout\n", __FUNCTION__);

  if ( radio_callbacks.rx_timeout != NULL )
  {
    radio_callbacks.rx_timeout( );
  }
  else
  {
    sx1276_reset( );
  }
}
/*---------------------------------------------------------------------------*/
void
OnRxError(void)
{
  PRINTF("SX1276 (%s): rx error\n", __FUNCTION__);
  if ( radio_callbacks.rx_error != NULL )
  {
    radio_callbacks.rx_error( );
  }
  else
  {
    sx1276_reset( );
  }
}
/*---------------------------------------------------------------------------*/
void OnCadDone(bool channelActivityDetected)
{
  PRINTF("SX1276 (%s): CAD %s\n", __FUNCTION__, channelActivityDetected?"true":"false");
  if ( radio_callbacks.cad_done != NULL )
  {
    radio_callbacks.cad_done(channelActivityDetected);
  }
  else
  {
    sx1276_on( );
  }
}
/*---------------------------------------------------------------------------*/
const struct radio_driver sx1276_radio_driver =
{
  sx1276_init,
  sx1276_prepare,
  sx1276_transmit,
  sx1276_send,
  sx1276_read,
  sx1276_channel_clear,
  sx1276_receiving_packet,
  sx1276_pending_packet,
  sx1276_on,
  sx1276_off,
  sx1276_get_value,
  sx1276_set_value,
  sx1276_get_object,
  sx1276_set_object
};
/*---------------------------------------------------------------------------*/