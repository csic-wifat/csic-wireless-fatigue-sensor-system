/*
 * Copyright (c) 2015, University of Cambridge
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

/**
 * \file
 *         rainflow.c
 *
 * \author
 *         Paul Fidler <praf1@cam.ac.uk>
 *
 * \description
 *         This file implements the version of the Rainflow Counting
 *         Algorithm as described in Section 5.4.4 of ASTM Standard
 *         No. E1049-85 "Standard Practices for Cycle Counting in
 *         Fatigue Analysis".
 * \note
 *         This implementation currently uses 'int' as the type
 *         of the reading.
 *
 *         This code does not produce load/strain histograms. Instead
 *         callers of this code provide a callback function to be
 *         called whenever a cycle or half cycle is found in the
 *         data. It is the caller's reposibility to group the
 *         resulting ranges into bins and produce a histogram.
 *
 */


#include "rainflow.h"

#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...) do {} while (0)
#endif

void
rainflow_init(
    struct rainflow_count *rc,
    int                   *buffer,
    size_t                 buflen,
    void                 (*callback)(int, int, int))
{
  PRINTF("rainflow_init(%p, %p, %i, %p)\n", rc, buffer, buflen, callback);
  rc->buffer    = buffer;
  rc->buflen    = buflen;
  rc->callback  = callback;
  rc->n         = 0;
}



void
rainflow_push(struct rainflow_count *rc, int reading)
{
  //PRINTF("rainflow_push(%p, %i)\n", rc, reading);
  /* Step 1 */
  // PRINTF("Step 1:\n");

  if (rc->n < rc->buflen) {
    rc->buffer[rc->n++] = reading;
  }
  else
  {
    /* Variation of Step 6 (or 5) */

    /* At this point the buffer is full and there is no
     * match for any of the half-cycle ranges already
     * in the buffer.
     *
     * Treat |buffer[0] - buffer[1]| as a half cycle
     * and remove buffer[0]. In effect shift everything
     * back one place.
     */
    //PRINTF("Step 6(v):\n");

    rc->callback(RAINFLOW_HALFCYCLE, rc->buffer[0], rc->buffer[1]);

    size_t i;

    for(i = 0; i < rc->n -1; i++)
      rc->buffer[i] = rc->buffer[i+1];

    rc->buffer[rc->n - 1] = reading;
  }

  /* Step 2: If there are less than 3 points, goto Step 1 */
  while(rc->n > 2) {

    //PRINTF("Step 2:\n");
    size_t x1,x2, y1,y2;

    x2 = rc->n - 1;
    x1 = x2 - 1;

    y2 = x1;
    y1 = y2 - 1;

    /* Step 3: compare absolute values of X and Y */
    //PRINTF("Step 3:\n");
    int X = abs(rc->buffer[x2] - rc->buffer[x1]);
    int Y = abs(rc->buffer[y2] - rc->buffer[y1]);

    if (X < Y)
    { // If X < Y, go to Step 1
      return;
    }

    if (y1 > 0) { /* Y does not contain S */

      /* Step 4: count range Y as on cycle */
      //PRINTF("Step 4:\n");
      rc->callback(RAINFLOW_CYCLE, rc->buffer[y1], rc->buffer[y2]);

      /*discard peak and valley of Y */
      size_t i;
      for (i = y1 ; i < rc->n - 1 ; i++)
      {
        rc->buffer[i] = rc->buffer[i + 2];
      }

      rc->n -= 2;

    } else { /* y1 == S */

      /* Step 5: Count range Y as one-half cycle */
      //PRINTF("Step 5\n");
      rc->callback(RAINFLOW_HALFCYCLE, rc->buffer[y1], rc->buffer[y2]);

      /* discard the first point (peak or valley) in Range Y
       * move the starting point to the second point in the
       * range Y, go to Step 2 */

      /* (for simplicity, shift everything from y2 onward
       * down one place) = S == 0
       */

      size_t i;

      for(i = y1 ; i < rc->n - 1 ; i++)
      {
        rc->buffer[i] = rc->buffer[i+1];
      }

      rc->n--;
    }
  }
}
