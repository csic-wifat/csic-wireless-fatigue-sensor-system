/*
 * Copyright (c) 2015, University of Cambridge
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

/**
 * \file
 *         rainflow-test.c
 *
 * \author
 *         Paul Fidler <praf1@cam.ac.uk>
 *
 * \description
 *         Plain C (not Contiki) test program for the rainflow counting
 *         Algorithm.
 *
 */

#include "rainflow.h"

#include <stdio.h>
#include <stdint.h>

void emit(int type, int min, int max)
{
  int range = min - max;
  if(range < 0) range = -range;

  if(type == 1)
    printf("-------------  Half-cycle: ");
  else
    printf("|||||||||||||| Cycle:      ");

  printf("%i -> %i  = %i\n", min, max, range);
}


int main(int argc, char* argv[])
{
  int buffer[128];

  struct rainflow_count count;

  rainflow_init(&count, buffer, 128, emit);



  rainflow_push(&count,   0);
  rainflow_push(&count, 100);
  rainflow_push(&count, -50);
  rainflow_push(&count, 50);
  rainflow_push(&count, -100);
  rainflow_push(&count, 75 );
  rainflow_push(&count, 0  );
  rainflow_push(&count, 50 );
  rainflow_push(&count, -75);
  rainflow_push(&count, 0  );
  rainflow_push(&count, -50);
  rainflow_push(&count, 100);
  rainflow_push(&count, 0  );
return 0;

  rainflow_push(&count, 43);
  rainflow_push(&count, 81);
  rainflow_push(&count, 27);
  rainflow_push(&count, 65);
  rainflow_push(&count, 10);
  rainflow_push(&count, 97);
  rainflow_push(&count, 65);
  rainflow_push(&count, 100);
  rainflow_push(&count, 43);
  rainflow_push(&count, 61);
  rainflow_push(&count, 27);

  rainflow_push(&count, 65);
  rainflow_push(&count, 4);
//  rainflow_push(&count, 65);

  return 0;
}
