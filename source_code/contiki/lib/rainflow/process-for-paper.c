/*
 * Copyright (c) 2019, David Rodenas-Herraiz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

/**
 * \file
 *         main.c
 *
 * \author
 *         David Rodenas-Herraiz <dr424@cam.ac.uk>
 *
 * \description
 *         Plain C (not Contiki) program for processing peaks and
 *         troughs in signal captured using the conventional approach
 *
 */
/*---------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "rainflow.h"
/*---------------------------------------------------------------------------*/
#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...) do {} while (0)
#endif
/*---------------------------------------------------------------------------*/
#define RAINFLOW_BUFFER   ( 256 )
#define FILENAME_READ     "../data/processed/conventional_peakAndTroughs.csv"
#define FILENAME_WRITE    "../data/processed/conventional_cycleCounting.csv"
/*---------------------------------------------------------------------------*/
FILE *fp_w;
/*---------------------------------------------------------------------------*/
void
emit(int type, int min, int max)
{
  int range = abs(min - max);

  if( type == 1 )
  {
    PRINTF("-------------  Half-cycle: ");
  }
  else
  {
    PRINTF("|||||||||||||| Cycle:      ");
  }

  PRINTF( "%i %i %i %i\n", type, min, max, range );
  fprintf( fp_w, "%i,%i,%i,%i\n" , type, min, max, range );
}
/*---------------------------------------------------------------------------*/
int
main(int argc, char* argv[])
{
  FILE *fp_r;
  int voltage;

  int buffer[ RAINFLOW_BUFFER ];
  struct rainflow_count count;

  char filename_write[80];

  sprintf(filename_write, FILENAME_WRITE);

  rainflow_init( &count, buffer, RAINFLOW_BUFFER, emit );

  fp_r = fopen(FILENAME_READ, "r");
  if( fp_r == NULL )
  {
    fprintf( stderr, "Error reading file for read\n" );
    return ( 1 );
  }

  fp_w = fopen(filename_write, "w+");
  if( fp_w == NULL )
  {
    fprintf( stderr, "Error opening file for write\n" );
    fclose( fp_r );
    return ( 1 );
  }

  fprintf( fp_w, "type,from,to,range\n");

  while ( fscanf( fp_r, "%d", &voltage) == 1 )
  {
    PRINTF( "%d\n", csv_data.voltage);

    rainflow_push( &count, voltage);
  }

  fclose( fp_r );
  fclose( fp_w );

  return ( 0 );
}
/*---------------------------------------------------------------------------*/
