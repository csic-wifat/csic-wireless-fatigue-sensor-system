/*
 * Copyright (c) 2015, University of Cambridge
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

/**
 * \file
 *         rainflow.h
 *
 * \author
 *         Paul Fidler <praf1@cam.ac.uk>
 *
 * \description
 *         This file defines an API for a version of the Rainflow Counting
 *         Algorithm. Rainflow counting is often used in fatigue monitoring
 *         applications.
 *
 */

#ifndef RAINFLOW_H_
#define RAINFLOW_H_

#define RAINFLOW_HALFCYCLE 1
#define RAINFLOW_CYCLE     2

#include <stdlib.h>

struct rainflow_count
{
  int*     buffer;
  size_t   buflen;
  size_t   n;
  void   (*callback)(int type, int min, int max);
};


extern void rainflow_init(
    struct rainflow_count *rc,
    int                   *buffer,
    size_t                 buflen,
    void                 (*callback)(int, int, int));

extern void rainflow_push(struct rainflow_count *rc, int reading);

#endif

