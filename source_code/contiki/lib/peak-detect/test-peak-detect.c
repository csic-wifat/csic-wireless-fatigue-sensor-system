/*
 * Copyright (c) 2018, David Rodenas-Herraiz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \file
 *         test-peak-detect.c
 *
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 *
 * \description
 *         Plain C test program for the peak-detect algorithm implemented by
 *         Hong Xu (https://github.com/xuphys/peakdetect), which in turn is
 *         based on Eli Billauer's peakdet function for MATLAB
 *         (http://billauer.co.il/peakdet.html)
 */
/*---------------------------------------------------------------------------*/
#include "peak-detect.h"

#include <stdio.h>
#include <stdint.h>

#define TEST_INPUT_DATALEN	(20)
double testInput[TEST_INPUT_DATALEN] =
{
	1.0		,	2.0		,	3.0		, 2.0		, 1.0,
	0.0		,	2.0		,	4.0		, 6.0		, 8.0,
	12.0	,	11.0	, 10.0	, 11.0	, 9.0,
	7.0		,	5.0		, 3.0		, 2.0		, 1.0
};

int main(int argc, char ** argv)
{
#define MAX_PEAK    200
  int         emi_peaks[MAX_PEAK];
  int         absorp_peaks[MAX_PEAK];
  int         emi_count = 0;
  int         absorp_count = 0;

  int         i;
  double      delta = 1e-6;
  int         emission_first = 0;

  if(detect_peak(testInput, TEST_INPUT_DATALEN,
                emi_peaks, &emi_count, MAX_PEAK,
                absorp_peaks, &absorp_count, MAX_PEAK,
                delta, emission_first))
  {
   fprintf(stderr, "There are too many peaks.\n");
   exit(1);
  }

  printf("Emission peaks:");
  for(i = 0; i < emi_count; ++i)
  {
  	printf(" %0.1f", testInput[emi_peaks[i]]);
  }

  printf("\nAbsorption peaks:");
  for(i = 0; i < absorp_count; ++i)
  {
    printf(" %0.1f", testInput[absorp_peaks[i]]);
  }
  printf("\n");

	return 0;
}
/*---------------------------------------------------------------------------*/