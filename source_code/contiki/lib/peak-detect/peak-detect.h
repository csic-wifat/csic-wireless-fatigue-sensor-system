/*
 * Copyright (c) 2018, David Rodenas-Herraiz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \file
 *         peak-detect.h
 *
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 *
 * \description
 *         This file defines an API for a version of Hong Xu's implementation
 *         in C (https://github.com/xuphys/peakdetect) of Eli Billauer's
 *         peakdet function for MATLAB (http://billauer.co.il/peakdet.html)
 */
/*---------------------------------------------------------------------------*/
#ifndef _PEAK_DETECT_H_
#define _PEAK_DETECT_H_

#include <stdlib.h>

typedef enum {
  PEAK_DETECT_RESULT_OK = 0,
  PEAK_DETECT_RESULT_EMI_ERROR,
  PEAK_DETECT_RESULT_ABSOP_ERROR
} peak_detect_result_t;

/*!
 * This function is used to detect peaks.
 *
 * @param[in]   data            The data
 * @param[in]   data_count      Row count of dat
 * @param[out]  emi_peaks       Emission peaks will be put here
 * @param[out]  num_emi_peaks   Number of emission peaks found
 * @param[in]   max_emi_peaks   Maximum number of emission peaks
 * @param[out]  absop_peaks     Absorption peaks will be put here
 * @param[out]  num_absop_peaks Number of absorption peaks found
 * @param[in]   max_absop_peaks Maximum number of absorption peaks
 * @param[in]   delta           Delta used for distinguishing peaks
 * @param[in]   emi_first       should we search emission peak first of
 *                              absorption peak first?
 * @return                      This function returns zero if success;
 *                              otherwise, it returns a negative error code
 *
 * Note: This implementation currently uses 'int' as the type of the reading.
 *
 */
peak_detect_result_t detect_peak(
        const double*   data,
        int             data_count,
        int*            emi_peaks,
        int*            num_emi_peaks,
        int             max_emi_peaks,
        int*            absop_peaks,
        int*            num_absop_peaks,
        int             max_absop_peaks,
        double          delta,
        int             emi_first
        );

#endif /* _PEAK_DETECT_H_ */
/*---------------------------------------------------------------------------*/