/*
 * Copyright (c) 2018, David Rodenas-Herraiz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \file
 *         peak-detect.c
 *
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 *
 * \description
 *         This file contains the version of Hong Xu's implementation
 *         in C (https://github.com/xuphys/peakdetect) of Eli Billauer's
 *         peakdet function for MATLAB (http://billauer.co.il/peakdet.html)
 */
/*---------------------------------------------------------------------------*/

#include "peak-detect.h"

peak_detect_result_t detect_peak(
        const double*   data, /* the data */
        int             data_count, /* row count of data */
        int*            emi_peaks, /* emission peaks will be put here */
        int*            num_emi_peaks, /* number of emission peaks found */
        int             max_emi_peaks, /* maximum number of emission peaks */
        int*            absop_peaks, /* absorption peaks will be put here */
        int*            num_absop_peaks, /* number of absorption peaks found */
        int             max_absop_peaks, /* maximum number of absorption peaks
                                            */
        double          delta, /* delta used for distinguishing peaks */
        int             emi_first /* should we search emission peak first of
                                     absorption peak first? */
        )
{
  int     i;
  double  mx;
  double  mn;
  int     mx_pos = 0;
  int     mn_pos = 0;
  int     is_detecting_emi = emi_first;

  mx = data[0];
  mn = data[0];

  *num_emi_peaks = 0;
  *num_absop_peaks = 0;

  for(i = 1; i < data_count; ++i)
  {
    if(data[i] > mx)
    {
      mx_pos = i;
      mx = data[i];
    }
    if(data[i] < mn)
    {
      mn_pos = i;
      mn = data[i];
    }

    if(is_detecting_emi && data[i] < mx - delta)
    {
      if(*num_emi_peaks >= max_emi_peaks) /* not enough spaces */
        return PEAK_DETECT_RESULT_EMI_ERROR;

      emi_peaks[*num_emi_peaks] = mx_pos;
      ++ (*num_emi_peaks);

      is_detecting_emi = 0;

      i = mx_pos - 1;

      mn = data[mx_pos];
      mn_pos = mx_pos;
    }
    else if((!is_detecting_emi) && data[i] > mn + delta)
    {
      if(*num_absop_peaks >= max_absop_peaks)
        return PEAK_DETECT_RESULT_ABSOP_ERROR;

      absop_peaks[*num_absop_peaks] = mn_pos;
      ++ (*num_absop_peaks);

      is_detecting_emi = 1;

      i = mn_pos - 1;

      mx = data[mn_pos];
      mx_pos = mn_pos;
    }
  }

  return PEAK_DETECT_RESULT_OK;
}
