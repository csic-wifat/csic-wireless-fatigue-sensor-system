/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
 /**
 * \file
 *         Intended for a process that periodically checks the time spent in
 *         radio tx, radio rx, radio lpm and cpu on.
 */
#include "contiki.h"
#include "simple-energest.h"
#include <stdio.h>
/*---------------------------------------------------------------------------*/
static uint32_t last_tx, last_rx, last_lpm, last_cpu;
static uint32_t curr_tx, curr_rx, curr_lpm, curr_cpu;
static uint32_t delta_tx, delta_rx, delta_lpm, delta_cpu;
/*---------------------------------------------------------------------------*/
void
simple_energest_init(void)
{
  energest_flush();
  last_tx  = energest_type_time(ENERGEST_TYPE_TRANSMIT);
  last_rx  = energest_type_time(ENERGEST_TYPE_LISTEN);
  last_cpu = energest_type_time(ENERGEST_TYPE_CPU);
  last_lpm = energest_type_time(ENERGEST_TYPE_LPM);
}
/*---------------------------------------------------------------------------*/
simple_energest_result_t
simple_energest_step(simple_energest_t * senergest)
{
	if(!senergest) {
		return SIMPLE_ENERGEST_ERROR;
	}

  energest_flush();

  curr_tx  = energest_type_time(ENERGEST_TYPE_TRANSMIT);
  curr_rx  = energest_type_time(ENERGEST_TYPE_LISTEN);
  curr_cpu = energest_type_time(ENERGEST_TYPE_CPU);
  curr_lpm = energest_type_time(ENERGEST_TYPE_LPM);

  delta_tx  = curr_tx - last_tx;
  delta_rx  = curr_rx - last_rx;
  delta_lpm = curr_lpm - last_lpm;
  delta_cpu = curr_cpu - last_cpu;

  last_tx  = curr_tx;
  last_rx  = curr_rx;
  last_cpu = curr_cpu;
  last_lpm = curr_lpm;

  senergest->tx  = delta_tx;
  senergest->rx  = delta_rx;
  senergest->cpu = delta_cpu;
  senergest->lpm = delta_lpm;

  return SIMPLE_ENERGEST_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
