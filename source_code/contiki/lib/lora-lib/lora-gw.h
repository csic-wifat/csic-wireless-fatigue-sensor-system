/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef __LC_LORA_GW_H__
#define __LC_LORA_GW_H__

#define LC_LORA_GW_MAX_PAYLOAD_SIZE     ( LORA_MAX_PAYLOAD_SIZE - 4 )

/* Packet type (4 bits) */
#define LC_LORA_GW_TYPE_MASK            0xf0
#define LC_LORA_GW_TYPE_SHIFT           4

#define LC_LORA_GW_TYPE_DATA            (0x01 << LC_LORA_GW_TYPE_SHIFT)
#define LC_LORA_GW_TYPE_ACK             (0x02 << LC_LORA_GW_TYPE_SHIFT)

/* Ack requested flag (1 bit) */
#define LC_LORA_GW_ACK_REQ_MASK         0x04
#define LC_LORA_GW_ACK_REQ_SHIFT        3

#define LC_LORA_GW_ACK_REQ_NONE         (0x00 << LC_LORA_GW_ACK_REQ_SHIFT)
#define LC_LORA_GW_ACK_REQ              (0x01 << LC_LORA_GW_ACK_REQ_SHIFT)

/* Data encrypted (1 bit) */
#define LC_LORA_GW_DATA_ENC_MASK        0x08
#define LC_LORA_GW_DATA_ENC_SHIFT       2

#define LC_LORA_GW_DATA_ENC_NONE        (0x00 << LC_LORA_GW_DATA_ENC_SHIFT)
#define LC_LORA_GW_DATA_ENC             (0x01 << LC_LORA_GW_DATA_ENC_SHIFT)

/* With application key flag (1 bit) */
#define LC_LORA_GW_WITH_APP_KEY_MASK    0x02
#define LC_LORA_GW_WITH_APP_KEY_SHIFT   1

#define LC_LORA_GW_APP_KEY_NONE         (0x00 << LC_LORA_GW_WITH_APP_KEY_SHIFT)
#define LC_LORA_GW_APP_KEY              (0x01 << LC_LORA_GW_WITH_APP_KEY_SHIFT)

/* Is binary flag (1 bit) */
#define LC_LORA_GW_IS_BINARY_MASK       0x01
#define LC_LORA_GW_IS_BINARY_SHIFT      0

#define LC_LORA_GW_IS_BINARY_NONE       (0x00 << LC_LORA_GW_IS_BINARY_SHIFT)
#define LC_LORA_GW_IS_BINARY            (0x01 << LC_LORA_GW_IS_BINARY_SHIFT)

typedef struct lc_loragw_frame
{
  uint8_t daddr;
  uint8_t ptype;
  uint8_t saddr;
  uint8_t seqn;
  uint8_t payload [LC_LORA_GW_MAX_PAYLOAD_SIZE];
} lc_loragw_frame_t;

#endif /* __LC_LORA_GW_H__ */
