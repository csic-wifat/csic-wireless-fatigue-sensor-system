/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
#include <contiki.h>
#include "lora-timer.h"
#include <stdio.h>
/*---------------------------------------------------------------------------*/
/* Convert from us to timer units */
#ifndef US_TO_TIMER
#define US_TO_TIMER( micro_sec )    round( (double)( micro_sec * CLOCK_SECOND ) / 1000000 )
#endif /* US_TO_TIMER */
/*---------------------------------------------------------------------------*/
void
TimerInit( TimerEvent_t *obj, void ( *callback )( void *) )
{
  obj->interval = 0;
  obj->callback = callback;
  obj->next = NULL;
}
/*---------------------------------------------------------------------------*/
void
TimerStart( TimerEvent_t *obj )
{
  TimerTime_t wait_time;

  /* The timer value is in us */
  TimerTime_t interval = obj->interval * 1000;
  /* Minimum time for a timer measure in ms */
  TimerTime_t min_tick_time = ( 1000 / CLOCK_SECOND);

  if( ( interval / 1000 ) < min_tick_time )
  {
    wait_time = min_tick_time;
  }
  else
  {
    wait_time = US_TO_TIMER (interval);
    if( interval % 1000 != 0 )
    {
      wait_time += min_tick_time;
    }
  }
  ctimer_set(&obj->timer, wait_time, obj->callback, NULL);
}
/*---------------------------------------------------------------------------*/
void
TimerStop( TimerEvent_t *obj )
{
  ctimer_stop( &obj->timer );
}
/*---------------------------------------------------------------------------*/
void
TimerReset( TimerEvent_t *obj )
{
  ctimer_restart( &obj->timer );
}
/*---------------------------------------------------------------------------*/
void
TimerSetValue( TimerEvent_t *obj, TimerTime_t value )
{
  obj->interval = value;
}
/*---------------------------------------------------------------------------*/
TimerTime_t
TimerGetCurrentTime( void )
{
  /* Return clock time in ms */
  double current_time = 0.0;
  current_time = round( ( (double)clock_time() * 1000) / CLOCK_SECOND );
  return (TimerTime_t) current_time;
}
/*---------------------------------------------------------------------------*/
TimerTime_t
TimerGetElapsedTime( TimerTime_t eventInTime )
{
  TimerTime_t elapsedTime = 0;

  /* Needed at boot, cannot compute with 0 or elapsed time will be equal to
     current time */
  if ( eventInTime == 0)
  {
    return 0;
  }

  elapsedTime = TimerGetCurrentTime();
  if( elapsedTime < eventInTime )
  {
    printf("%s: Roll (%lu < %lu)\n", __FUNCTION__, elapsedTime, eventInTime);
    // roll over of the counter
    return ( elapsedTime + (0xFFFFFFFF - eventInTime ) );
  }
  else
  {
    return ( elapsedTime - eventInTime );
  }
}
/*---------------------------------------------------------------------------*/
uint8_t
TimerIsRunning( TimerEvent_t *obj )
{
  if ( ctimer_expired( &obj->timer ) != 0 )
  { // The timer has expired
    return 0;
  }
  else
  { // The timer has not expired yet
    return 1;
  }
}
/*---------------------------------------------------------------------------*/
