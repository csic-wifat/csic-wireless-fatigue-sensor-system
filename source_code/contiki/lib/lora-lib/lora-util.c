/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include "lora-util.h"
/*---------------------------------------------------------------------------*/
#define RAND_LOCAL_MAX 2147483647L
/*---------------------------------------------------------------------------*/
static uint32_t next = 1;
/*---------------------------------------------------------------------------*/
int32_t
rand1( void )
{
    return ( ( next = next * 1103515245L + 12345L ) % RAND_LOCAL_MAX );
}
/*---------------------------------------------------------------------------*/
void
srand1( uint32_t seed )
{
    next = seed;
}
/*---------------------------------------------------------------------------*/
int32_t
randr( int32_t min, int32_t max )
{
  return ( int32_t )rand1( ) % ( max - min + 1 ) + min;
}
/*---------------------------------------------------------------------------*/
void
memcpy1( uint8_t *dst, const uint8_t *src, uint16_t size )
{
  while( size-- )
  {
    *dst++ = *src++;
  }
}
/*---------------------------------------------------------------------------*/
void
memcpyr( uint8_t *dst, const uint8_t *src, uint16_t size )
{
  dst = dst + ( size - 1 );
  while( size-- )
  {
    *dst-- = *src++;
  }
}
/*---------------------------------------------------------------------------*/
void
memset1( uint8_t *dst, uint8_t value, uint16_t size )
{
  while( size-- )
  {
    *dst++ = value;
  }
}
/*---------------------------------------------------------------------------*/
int8_t
Nibble2HexChar( uint8_t a )
{
  if( a < 10 )
  {
    return '0' + a;
  }
  else if( a < 16 )
  {
    return 'A' + ( a - 10 );
  }
  else
  {
    return '?';
  }
}
/*---------------------------------------------------------------------------*/
