/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "stm32l151.h"
#include "dev/watchdog.h"
#include "dev/soc-adc.h"
#include "sys/energest.h"
/*---------------------------------------------------------------------------*/
void cpu_core_init(void);
/*---------------------------------------------------------------------------*/
EXTIIrqHandler *ExtiIRQ[16];
/*---------------------------------------------------------------------------*/
static void
init_ports(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /* Configure all GPIO as analog to reduce current consumption
     on non used IOs */
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_All;

  GPIO_Init(GPIOA, &GPIO_InitStructure);
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  GPIO_Init(GPIOC, &GPIO_InitStructure);
  GPIO_Init(GPIOD, &GPIO_InitStructure);
  GPIO_Init(GPIOE, &GPIO_InitStructure);
}
/*---------------------------------------------------------------------------*/
void
cpu_core_init(void)
{
  /* Essential on STM32 Cortex-M devices */
  NVIC_PriorityGroupConfig( NVIC_PriorityGroup_4 );

  /* Set flash WS latency */
  FLASH_SetLatency( STM32L1_FLASH_LATENCY );

  /* Enable the GPIOs clocks. */
  RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB |
                         RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOD |
                         RCC_AHBPeriph_GPIOE | RCC_AHBPeriph_GPIOH, ENABLE );

  /* Enable comparator clocks. */
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_COMP, ENABLE );

  /* Enable SYSCFG clocks. */
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_SYSCFG , ENABLE );

  /* Set internal voltage regulator */
  PWR_VoltageScalingConfig( STM32L1_VCORE );

  /* Wait Until the Voltage Regulator is ready. */
  while( PWR_GetFlagStatus( PWR_FLAG_VOS ) != RESET );
}
/*---------------------------------------------------------------------------*/
void
stm32l1xx_cpu_init(void)
{
  dint();

#if defined( USE_BOOTLOADER )
  // Set the Vector Table base location at 0x3000
  SCB->VTOR = FLASH_BASE | 0x3000;
#endif

  watchdog_init();
  init_ports();

  cpu_core_init();

  eint();
}
/*---------------------------------------------------------------------------*/
void
stm32l1xx_cpu_deinit(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /* De-initialize soc adc */
  SENSORS_DEACTIVATE(soc_adc);

  /* Turn on HSE Clock */
  RCC_AHBPeriphClockCmd( OSC_HSE_CLK, ENABLE );
  GPIO_InitStructure.GPIO_Pin   = OSC_HSE_IN_PIN | OSC_HSE_OUT_PIN;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
  GPIO_Init(OSC_HSE_GPIO_PORT, &GPIO_InitStructure);
  GPIO_WriteBit(OSC_HSE_GPIO_PORT, OSC_HSE_IN_PIN | OSC_HSE_OUT_PIN,  Bit_SET);

  /* Turn off LSE Clock */
  RCC_AHBPeriphClockCmd(OSC_LSE_CLK, ENABLE);
  GPIO_InitStructure.GPIO_Pin   = OSC_LSE_IN_PIN | OSC_LSE_OUT_PIN;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
  GPIO_Init(OSC_LSE_GPIO_PORT, &GPIO_InitStructure);
  GPIO_WriteBit(OSC_LSE_GPIO_PORT, OSC_LSE_IN_PIN | OSC_LSE_OUT_PIN,  Bit_SET);
}
/*---------------------------------------------------------------------------*/
/* Interrupt handlers -------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void NMI_Handler(void)
{
}
/*---------------------------------------------------------------------------*/
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1);
}
/*---------------------------------------------------------------------------*/
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1);
}
/*---------------------------------------------------------------------------*/
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1);
}
/*---------------------------------------------------------------------------*/
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1);
}
/*---------------------------------------------------------------------------*/
void DebugMon_Handler(void)
{
 /* Go to infinite loop when Hard Fault exception occurs */
  while (1);
}
/*---------------------------------------------------------------------------*/
/*void PPP_IRQHandler(void)
{
}*/
/*---------------------------------------------------------------------------*/
void EXTI0_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);
  if(EXTI_GetITStatus(EXTI_Line0) != RESET)
  {
    /* Clear the EXTI line 0 pending bit */
    if(ExtiIRQ[0X00] != NULL) {
      ExtiIRQ[0x00]();
    }
    EXTI_ClearITPendingBit(EXTI_Line0);
  }
  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
/*---------------------------------------------------------------------------*/
void EXTI1_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);
  if(EXTI_GetITStatus(EXTI_Line1) != RESET)
  {
    if(ExtiIRQ[0X01] != NULL) {
      ExtiIRQ[0x01]();
    }
    /* Clear the EXTI line 1 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line1);
  }
  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
/*---------------------------------------------------------------------------*/
void EXTI2_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);
  if(EXTI_GetITStatus(EXTI_Line2) != RESET)
  {
    if(ExtiIRQ[0X02] != NULL) {
      ExtiIRQ[0x02]();
    }
    /* Clear the EXTI line 2 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line2);
  }
  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
/*---------------------------------------------------------------------------*/
void EXTI3_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);
  if(EXTI_GetITStatus(EXTI_Line3) != RESET)
  {
    if(ExtiIRQ[0X03] != NULL) {
      ExtiIRQ[0x03]();
    }
    /* Clear the EXTI line 3 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line3);
  }
  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
/*---------------------------------------------------------------------------*/
void EXTI4_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);
  if(EXTI_GetITStatus(EXTI_Line4) != RESET)
  {
    if(ExtiIRQ[0X04] != NULL) {
      ExtiIRQ[0x04]();
    }
    /* Clear the EXTI line 4 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line4);
  }
  ENERGEST_OFF(EXTI_PinSource9);
}
/*---------------------------------------------------------------------------*/
void EXTI9_5_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);
  if(EXTI_GetITStatus(EXTI_Line5) != RESET)
  {
    if(ExtiIRQ[0X05] != NULL) {
      ExtiIRQ[0x05]();
    }
    /* Clear the EXTI line 5 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line5);
  }
  else if(EXTI_GetITStatus(EXTI_Line6) != RESET)
  {
    if(ExtiIRQ[0X06] != NULL) {
      ExtiIRQ[0x06]();
    }
    /* Clear the EXTI line 6 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line6);
  }
  else if(EXTI_GetITStatus(EXTI_Line7) != RESET)
  {
    if(ExtiIRQ[0X07] != NULL) {
      ExtiIRQ[0x07]();
    }
    /* Clear the EXTI line 7 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line7);
  }
  else if(EXTI_GetITStatus(EXTI_Line8) != RESET)
  {
    if(ExtiIRQ[0X08] != NULL) {
      ExtiIRQ[0x08]();
    }
    /* Clear the EXTI line 8 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line8);
  }
  else if(EXTI_GetITStatus(EXTI_Line9) != RESET)
  {
    if(ExtiIRQ[0X09] != NULL) {
      ExtiIRQ[0x09]();
    }
    /* Clear the EXTI line 9 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line9);
  }
  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
/*---------------------------------------------------------------------------*/
void EXTI15_10_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);
  if(EXTI_GetITStatus(EXTI_Line10) != RESET)
  {
    if(ExtiIRQ[0X0a] != NULL) {
      ExtiIRQ[0X0a]();
    }
    /* Clear the EXTI line 10 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line10);
  }
  if(EXTI_GetITStatus(EXTI_Line11) != RESET)
  {
    if(ExtiIRQ[0X0b] != NULL) {
      ExtiIRQ[0x0b]();
    }
    /* Clear the EXTI line 11 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line11);
  }
  if(EXTI_GetITStatus(EXTI_Line12) != RESET)
  {
    if(ExtiIRQ[0X0c] != NULL) {
      ExtiIRQ[0x0c]();
    }
    /* Clear the EXTI line 12 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line12);
  }
  if(EXTI_GetITStatus(EXTI_Line13) != RESET)
  {
    if(ExtiIRQ[0X0d] != NULL) {
      ExtiIRQ[0x0d]();
    }
    /* Clear the EXTI line 13 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line13);
  }
  if(EXTI_GetITStatus(EXTI_Line14) != RESET)
  {
    if(ExtiIRQ[0X0e] != NULL) {
      ExtiIRQ[0x0e]();
    }
    /* Clear the EXTI line 14 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line14);
  }
  if(EXTI_GetITStatus(EXTI_Line15) != RESET)
  {
    if(ExtiIRQ[0X0f] != NULL) {
      ExtiIRQ[0x0f]();
    }
    /* Clear the EXTI line 15 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line15);
  }
  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
/*---------------------------------------------------------------------------*/
