/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */
/**
 * \addtogroup stm32l1xx
 * @{
 *
 * \defgroup stm32l1xx-i2c stm32l1xx I2C Control
 *
 * stm32l1xx I2C Control Module
 * @{
 *
 * \file
 * Header file with declarations for the I2C Control module
 *
 * \author
 * David Rodenas-Herraiz
 */
/*----------------------------------------------------------------------------*/
#ifndef I2C_H_
#define I2C_H_

/* Maximum Timeout values for flags and events waiting loops. These timeouts are
   not based on accurate values, they just guarantee that the application will
   not remain stuck if the I2C communication is corrupted.
   You may modify these timeout values depending on CPU frequency and application
   conditions (interrupts routines ...). */
#define FLAG_TIMEOUT         ((uint32_t)0x1000)
#define LONG_TIMEOUT         ((uint32_t)(10 * FLAG_TIMEOUT))
/*----------------------------------------------------------------------------*/
typedef enum {
  I2C_SUCCESS = 0,
  I2C_ERROR
} i2c_result_t;

typedef enum {
  I2C_ADDR_SIZE_8 = 0,
  I2C_ADDR_SIZE_16
} i2c_addr_size_t;
/*----------------------------------------------------------------------------*/
typedef uint16_t i2c_addr_t;
/*----------------------------------------------------------------------------*/
void i2c_init(I2C_TypeDef* I2Cx, uint32_t baudrate, uint16_t mode,
              uint16_t duty_cycle, i2c_addr_t own_addr1, uint16_t ack_en,
              uint16_t acked_addr);

void i2c_deinit(I2C_TypeDef* I2Cx);

void i2c_acknowledge_config(I2C_TypeDef* I2Cx, FunctionalState NewState);

i2c_result_t i2c_write_buf(I2C_TypeDef* I2Cx,
													 const uint8_t *buf,
													 uint8_t len);

i2c_result_t i2c_read_buf(I2C_TypeDef* I2Cx,
													uint8_t *buf,
													i2c_addr_t slaveAddress,
													uint8_t len);

i2c_result_t i2c_start_xfer(I2C_TypeDef* I2Cx,
											 			uint8_t xferDirection,
											 			i2c_addr_t slaveAddress);

i2c_result_t i2c_stop_xfer(I2C_TypeDef* I2Cx);

i2c_result_t i2c_read_ack(I2C_TypeDef* I2Cx, uint8_t *buf);

i2c_result_t i2c_read_nack(I2C_TypeDef* I2Cx, uint8_t *buf);

i2c_result_t i2c_wait_standby_state(I2C_TypeDef* I2Cx, i2c_addr_t slaveAddress);

void i2c_set_addr_size(i2c_addr_size_t size);
/*----------------------------------------------------------------------------*/
/* Arch functions */
/*----------------------------------------------------------------------------*/
void i2c_arch_init(I2C_TypeDef* I2Cx);
void i2c_arch_deinit(I2C_TypeDef* I2Cx);
/*----------------------------------------------------------------------------*/
/**
  * @brief  This function handles I2C2 Error interrupt request.
  * @param  None
  * @retval None
  */
void I2C1_ER_IRQHandler(void);
/*----------------------------------------------------------------------------*/
#endif /* I2C_H_ */
/**
 * @}
 * @}
 */
