/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup stm32l1xx-opamp
 * @{
 *
 * Implementation of STM32L1xx operational amplifier (OPAMP) operation functionality
 *
 * @{
 *
 * \file
 * Driver for STM32L1xx operational amplifier (OPAMP) operation
 */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include "contiki.h"
#include "stm32l1xx.h"
#include "soc-opamp.h"
/*---------------------------------------------------------------------------*/
soc_opamp_result_t
soc_opamp_init(soc_opamp_dev opamp)
{
  GPIO_InitTypeDef   GPIO_InitStructure;

  switch(opamp)
  {
    case OPAMP1:
      /* GPIOA Peripheral clock enable */
      RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

      /* Configure PA3 (OPAMP1 output) in analog mode */
      GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_Init(GPIOA, &GPIO_InitStructure);

      /* Configure PA1 (OPAMP1 positive input) and PA2 (OPAMP1 negative input) in analog mode */
      GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_Init(GPIOA, &GPIO_InitStructure);

      /* COMP Peripheral clock enable: COMP and OPAMP share the same Peripheral clock enable */
      RCC_APB1PeriphClockCmd(RCC_APB1Periph_COMP, ENABLE);

      /* Enable OPAMP1 */
      OPAMP_Cmd(OPAMP_Selection_OPAMP1, ENABLE);

      /* Close S4 and S5 swicthes */
      OPAMP_SwitchCmd(OPAMP_OPAMP1Switch4 | OPAMP_OPAMP1Switch5, ENABLE);
      break;
    case OPAMP2:
      /* GPIOA and GPIOB Peripheral clock enable */
      RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB, ENABLE);

      /* Configure PB0 (OPAMP2 output) in analog mode */
      GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_Init(GPIOB, &GPIO_InitStructure);

      /* Configure PA6 (OPAMP2 positive input) and PA7 (OPAMP2 negative input) in analog mode */
      GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_Init(GPIOA, &GPIO_InitStructure);

      /* COMP Peripheral clock enable: COMP and OPAMP share the same Peripheral clock enable */
      RCC_APB1PeriphClockCmd(RCC_APB1Periph_COMP, ENABLE);

      /* Enable OPAMP1 */
      OPAMP_Cmd(OPAMP_Selection_OPAMP2, ENABLE);

      /* Close S4 and S5 swicthes */
      OPAMP_SwitchCmd(OPAMP_OPAMP2Switch4 | OPAMP_OPAMP2Switch5, ENABLE);
      break;
    case OPAMP3:
      /* GPIOC Peripheral clock enable */
      RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

      /* Configure PC3 (OPAMP3 output) in analog mode */
      GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_Init(GPIOC, &GPIO_InitStructure);

      /* Configure PC1 (OPAMP3 positive input) and PC2 (OPAMP3 negative input) in analog mode */
      GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_Init(GPIOC, &GPIO_InitStructure);

      /* COMP Peripheral clock enable: COMP and OPAMP share the same Peripheral clock enable */
      RCC_APB1PeriphClockCmd(RCC_APB1Periph_COMP, ENABLE);

      /* Enable OPAMP1 */
      OPAMP_Cmd(OPAMP_Selection_OPAMP3, ENABLE);

      /* Close S4 and S5 swicthes */
      OPAMP_SwitchCmd(OPAMP_OPAMP3Switch4 | OPAMP_OPAMP3Switch5, ENABLE);
      break;
    default:
      return OPAMP_ERROR;
  }
  return OPAMP_SUCCESS;
}
/*---------------------------------------------------------------------------*/
