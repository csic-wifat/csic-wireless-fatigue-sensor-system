/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */
/* -------------------------------------------------------------------------- */
/**
 * \addtogroup stm32l1xx
 * @{
 *
 * \defgroup stm32l1xx-rtc STM32L1xx operational amplifier (OPAMP)
 *
 * STM32L1xx operational amplifier (OPAMP)
 *
 * @{
 *
 * \file
 * Header file for the management of STM32L1 operational amplifier (OPAMP)
 */
/* -------------------------------------------------------------------------- */
#ifndef _SOC_OPAMP_H_
#define _SOC_OPAMP_H_

typedef enum {
  OPAMP_SUCCESS = 0,
  OPAMP_ERROR
} soc_opamp_result_t;

typedef enum soc_opamp_dev_t
{
  OPAMP1 = 1,
  OPAMP2,
  OPAMP3
} soc_opamp_dev;

/**
 * \brief Initialize the OPAMPx
 */
soc_opamp_result_t soc_opamp_init(soc_opamp_dev opamp);

#endif /* _SOC_OPAMP_H_ */
