/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup stm32l1xx-rtc
 * @{
 *
 * Implementation of STM32L1xx real-time clock (RTC) operation functionality
 *
 * @{
 *
 * \file
 * Driver for STM32L1xx real-time clock (RTC) operation
 */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include "contiki.h"
#include "soc-rtc.h"
#include "lpm.h"
#include "stm32l1xx.h"
/*---------------------------------------------------------------------------*/
#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...) do {} while (0)
#endif
/*---------------------------------------------------------------------------*/
#define RTC_STATUS_TIME_OK          0x32F2
/*---------------------------------------------------------------------------*/
/*!
 * Number of seconds in a minute
 */
static const uint8_t SecondsInMinute = 60;

/*!
 * Number of seconds in an hour
 */
static const uint16_t SecondsInHour = 3600;

/*!
 * Number of seconds in a day
 */
static const uint32_t SecondsInDay = 86400;

/*!
 * Number of hours in a day
 */
static const uint8_t HoursInDay = 24;

/*!
 * Number of seconds in a leap year
 */
static const uint32_t SecondsInLeapYear = 31622400;

/*!
 * Number of seconds in a year
 */
static const uint32_t SecondsInYear = 31536000;

/*!
 * Number of days in each month on a normal year
 */
static const uint8_t DaysInMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

/*!
 * Number of days in each month on a leap year
 */
static const uint8_t DaysInMonthLeapYear[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

/*!
 * Number of days in a year
 */
static const uint16_t DaysInYear = 365;

/*!
 * Number of days in a leap year
 */
static const uint16_t DaysInLeapYear = 366;

/*!
 * Number of days in a century
 */
static const double DaysInCentury = 36524.219;
/*---------------------------------------------------------------------------*/
static void soc_rtc_clock_init(void);
static void soc_rtc_time_regulate(void);
static uint8_t check_leap_year(uint8_t val);
static soc_rtc_result_t soc_rtc_check_td_format(soc_rtc_td_map *data,
                                                uint8_t alarm_state);
/*---------------------------------------------------------------------------*/
static void soc_rtc_rtc_time_to_alarm_ticks(soc_rtc_td_map *alarm,
                                           const soc_rtc_td_map *now,
                                           soc_rtc_clock_t timeCounter);
static void soc_rtc_set_wakeup_alarm(soc_rtc_clock_t timeoutValue);
/*---------------------------------------------------------------------------*/
/* Uncomment the corresponding line to select the RTC Clock source */
#define RTC_CLOCK_SOURCE_LSE   /* LSE used as RTC source clock */
/* #define RTC_CLOCK_SOURCE_LSI */ /* LSI used as RTC source clock. The RTC Clock
                                      may varies due to LSI frequency dispersion. */
/*---------------------------------------------------------------------------*/
#if RTC_TIMESTAMP_ENABLED
#define TIMESTAMP_EDGE  RTC_TimeStampEdge_Falling
#endif /* RTC_TIMESTAMP_ENABLED */
/*---------------------------------------------------------------------------*/
/* Subsecond number of bits */
#define N_PREDIV_S                11

/* Synchronous prediv  */
#define PREDIV_S                  ( ( 1 << N_PREDIV_S ) - 1 )
/* Asynchronous prediv   */
#define PREDIV_A                  ( 1 << ( 15 - N_PREDIV_S ) ) - 1

/* RTC Time Base in us */
#define USEC_NUMBER               1000000
#define MSEC_NUMBER               ( USEC_NUMBER / 1000 )
#define RTC_ALARM_TIME_BASE       ( USEC_NUMBER >> N_PREDIV_S )

#define COMMON_FACTOR             3
#define CONV_NUMER                ( MSEC_NUMBER >> COMMON_FACTOR )
#define CONV_DENOM                ( 1 << ( N_PREDIV_S - COMMON_FACTOR ) )
/*---------------------------------------------------------------------------*/
__IO uint32_t AsynchPrediv = 0, SynchPrediv = 0;
static uint32_t hourFormat;

/*!
 * \brief Flag to indicate if the timestamps until the next event is long enough
 * to set the MCU into low power mode
 */
volatile uint8_t soc_rtc_timer_event_allows_lpm = 0;

volatile uint8_t non_scheduled_wakeup = 0;

static uint8_t soc_rtc_isWakeUpAlarm[2] = {0, 0};

/*!
 * Current RTC timer context
 */
soc_rtc_td_map calendar_context;

/*!
 * \brief Hold the Wake-up time duration in ms
 */
volatile soc_rtc_clock_t mcu_wakeup_time = 0;

/*---------------------------------------------------------------------------*/
RTC_AlarmIrqHandler *RTC_AlarmIRQ[2];
/*!
 * \brief RTC_Alarm_B IRQ callback
 */
void rtc_alarm_B_IRQHandler(void);
/*---------------------------------------------------------------------------*/
static void
soc_rtc_time_regulate(void)
{
  RTC_TimeTypeDef RTC_TimeStructure;
  RTC_DateTypeDef RTC_DateStructure;

   /* Configure the RTC time and date registers */
  RTC_TimeStructInit(&RTC_TimeStructure);
  RTC_DateStructInit(&RTC_DateStructure);
  if((RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure) == ERROR)
      || (RTC_SetDate(RTC_Format_BIN, &RTC_DateStructure) == ERROR))
  {
    PRINTF("\n>> !! RTC Set Time and Date failed. !! <<\n");
  }
  else
  {
    /* Indicator for the RTC configuration */
    RTC_WriteBackupRegister(RTC_BKP_DR0, RTC_STATUS_TIME_OK);
  }
}
/*---------------------------------------------------------------------------*/
static void
soc_rtc_clock_init(void)
{
  /* Enable the PWR clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

  /* Allow access to RTC */
  PWR_RTCAccessCmd(ENABLE);

  /* Reset RTC Domain */
  RCC_RTCResetCmd(ENABLE);
  RCC_RTCResetCmd(DISABLE);

#if defined (RTC_CLOCK_SOURCE_LSI)  /* LSI used as RTC source clock*/
  /* The RTC Clock may vary due to LSI frequency dispersion. */
  /* Enable the LSI OSC */
  RCC_LSICmd(ENABLE);

  /* Wait till LSI is ready */
  while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET);

  /* Select the RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

  AsynchPrediv = PREDIV_A;
  SynchPrediv = PREDIV_S;
#elif defined (RTC_CLOCK_SOURCE_LSE) /* LSE used as RTC source clock */
  /* Enable the LSE OSC */
  RCC_LSEConfig(RCC_LSE_ON);

  /* Wait till LSE is ready */
  while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET);

  /* Select the RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

  AsynchPrediv = PREDIV_A;
  SynchPrediv = PREDIV_S;
#else
  #error Please select the RTC Clock source inside the main.c file
#endif /* RTC_CLOCK_SOURCE_LSI */

  /* Enable the RTC Clock */
  RCC_RTCCLKCmd(ENABLE);

  soc_rtc_time_regulate();

  /* Wait for RTC APB registers synchronization */
  if(RTC_WaitForSynchro() == ERROR) {
    PRINTF("%s(Error RTC_WaitForSynchro)\n", __FUNCTION__);
  }

  RTC_BypassShadowCmd(ENABLE);

#if RTC_TIMESTAMP_ENABLED
  /* Enable The TimeStamp */
  RTC_TimeStampCmd(TIMESTAMP_EDGE, ENABLE);
#endif
}
/*---------------------------------------------------------------------------*/
static uint8_t
check_leap_year(uint8_t val)
{
  return ((val % 4) && (val % 100)) || (val % 400);
}
/*---------------------------------------------------------------------------*/
static soc_rtc_result_t
soc_rtc_check_td_format(soc_rtc_td_map *data, uint8_t alarm_state)
{
  uint8_t year = data->year % 100;
  /*if( !IS_RTC_H12(data->h12) ) {
    PRINTF("h12 problem %02i\n", data->h12);
    return SOC_RTC_RESULT_ERROR;
  }*/

  if( !IS_RTC_SECONDS(data->second) ) {
    PRINTF("second problem %02i\n", data->second);
    return SOC_RTC_RESULT_ERROR;
  }

  if( !IS_RTC_MINUTES(data->minute) ) {
    PRINTF("minute problem %02i\n", data->minute);
    return SOC_RTC_RESULT_ERROR;
  }

  if( !IS_RTC_HOUR24(data->hour) && !IS_RTC_HOUR12(data->hour) ) {
    PRINTF("hour problem %02i\n", data->hour);
    return SOC_RTC_RESULT_ERROR;
  }

  if( !IS_RTC_DATE(data->day) ) {
    PRINTF("day problem %02i\n", data->day);
    return SOC_RTC_RESULT_ERROR;
  }

  if( !IS_RTC_MONTH(data->month) ) {
    PRINTF("month problem %02i\n", data->month);
    return SOC_RTC_RESULT_ERROR;
  }

  if( !IS_RTC_YEAR(year) ) {
    PRINTF("year problem %04i\n", year);
    return SOC_RTC_RESULT_ERROR;
  }

  /*if( !IS_RTC_WEEKDAY(data->dow) ) {
    PRINTF("dow problem %02i\n", data->dow);
    return SOC_RTC_RESULT_ERROR;
  }*/

  /* Fixed condition for February (month 2) */
  if(data->month == 2) {
    if(check_leap_year(year)) {
      if(data->day > 29) {
        PRINTF("leap year problem %04i\n", year);
        return SOC_RTC_RESULT_ERROR;
      }
    } else {
      if(data->day > 28) {
        PRINTF("leap year problem %04i\n", year);
        return SOC_RTC_RESULT_ERROR;
      }
    }
  }
  return SOC_RTC_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
static void
soc_rtc_rtc_time_to_alarm_ticks(soc_rtc_td_map *alarm,
                                const soc_rtc_td_map *now,
                                soc_rtc_clock_t timeCounter)
{
  soc_rtc_clock_t timeoutValue = 0;

  alarm->subsecond = now->subsecond;
  alarm->second = now->second;
  alarm->minute = now->minute;
  alarm->hour = now->hour;
  alarm->day = now->day;

  uint16_t milliseconds = 0;
  uint16_t seconds = now->second;
  uint16_t minutes = now->minute;
  uint16_t hours = now->hour;
  uint16_t days = now->day;

  timeoutValue = timeCounter;

  milliseconds = PREDIV_S - now->subsecond;
  milliseconds += (timeoutValue & PREDIV_S);

  /* Convert timeout to seconds */
  timeoutValue >>= N_PREDIV_S;

  /* Convert milliseconds to RTC format and add to now */
  while( timeoutValue >= SecondsInDay )
  {
    timeoutValue -= SecondsInDay;
    days++;
  }

  /* Calculate hours */
  while( timeoutValue >= SecondsInHour )
  {
    timeoutValue -= SecondsInHour;
    hours++;
  }

  /* Calculate minutes */
  while( timeoutValue >= SecondsInMinute )
  {
    timeoutValue -= SecondsInMinute;
    minutes++;
  }

  /* Calculate seconds */
  seconds += timeoutValue;

  /* Correct for modulo */
  while( milliseconds >= (PREDIV_S + 1) )
  {
    milliseconds -= (PREDIV_S + 1);
    seconds++;
  }

  while( seconds >= SecondsInMinute )
  {
    seconds -= SecondsInMinute;
    minutes++;
  }

  while( minutes >= 60)
  {
    minutes -= 60;
    hours++;
  }

  while( hours >= HoursInDay)
  {
    hours -= HoursInDay;
    days++;
  }

  if( (now->year == 0) || (now->year % 4) == 0 )
  {
    if( days > DaysInMonthLeapYear[now->month - 1] )
    {
      days = days % DaysInMonthLeapYear[now->month - 1];
      alarm->month++;
    }
  }
  else
  {
    if( days > DaysInMonth[now->month - 1] )
    {
      days = days % DaysInMonth[now->month - 1];
      alarm->month++;
    }
  }

  alarm->subsecond = PREDIV_S - milliseconds;
  alarm->second = seconds;
  alarm->minute = minutes;
  alarm->hour = hours;
  alarm->day = days;
}
/*---------------------------------------------------------------------------*/
soc_rtc_clock_t
soc_rtc_time_msec_to_rtc_clock(soc_rtc_clock_t msec)
{
  double val = 0.0;
  val = round( ( (double)msec * CONV_DENOM) / CONV_NUMER );
  return (soc_rtc_clock_t) val;
}
/*---------------------------------------------------------------------------*/
soc_rtc_clock_t
soc_rtc_rtc_clock_to_time_msec(soc_rtc_clock_t rtc_clock)
{
  double val = 0.0;
  val = round( ( (double)rtc_clock * CONV_NUMER) / CONV_DENOM );
  return (soc_rtc_clock_t)val;
}
/*---------------------------------------------------------------------------*/
static void
soc_rtc_set_wakeup_alarm(soc_rtc_clock_t timeoutValue)
{
  soc_rtc_td_map now;
  soc_rtc_td_map alarm_timer;
  RTC_AlarmTypeDef RTC_AlarmStructure;

  /* Clear previous alarm */
  RTC_ClearFlag(RTC_FLAG_ALRAF);
  RTC_AlarmCmd(LPM_RTC_ALARM, DISABLE);

  if( timeoutValue <= 3 )
  {
    timeoutValue = 3;
  }

  /* Get current RTC calendar */
  soc_rtc_get_datetime(&now);
  // Remove first two digits of year
  now.year -= (uint16_t)RTC_ReadBackupRegister(RTC_BKP_DR1) * 100;

  /* Save the calendar into calendar_context to be able to calculate
   the elapsed time */
  calendar_context = now;

  // timeoutValue in rtc ticks
  soc_rtc_rtc_time_to_alarm_ticks(&alarm_timer, &now, timeoutValue);

  /* RTC Alarm A configuration */
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = alarm_timer.second;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = alarm_timer.minute;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours   = alarm_timer.hour;
  RTC_AlarmSubSecondConfig(LPM_RTC_ALARM,
                           alarm_timer.subsecond,
                           RTC_AlarmSubSecondMask_None);

  RTC_AlarmStructure.RTC_AlarmDateWeekDay    = alarm_timer.day;
  RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
  RTC_AlarmStructure.RTC_AlarmMask           = RTC_AlarmMask_None;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_H12   = RTC_H12_AM;
  /*
  printf("%s:: (now) %u %u:%u:%u.%lu", __FUNCTION__,
    now.day, now.hour, now.minute, now.second, now.subsecond);

  printf(" (al) %u %u:%u:%u.%lu\n",
    alarm_timer.day, alarm_timer.hour, alarm_timer.minute,
    alarm_timer.second, alarm_timer.subsecond);
  */
  /* Configure the RTC Alarm A register */
  RTC_SetAlarm(RTC_Format_BIN, LPM_RTC_ALARM, &RTC_AlarmStructure);

  /* Enable the alarm  A */
  if(RTC_AlarmCmd(LPM_RTC_ALARM, ENABLE) == ERROR) {
    PRINTF("Error RTC alarm A\n");
  }
}
/*---------------------------------------------------------------------------*/
static void
soc_rtc_compute_wakeup_time(void)
{
  static uint8_t wakeup_time_init;  // Indicates if the RTC Wake Up Time is calibrated or not

  uint32_t start = 0;
  uint32_t stop = 0;
  soc_rtc_td_map now;

  if( !wakeup_time_init )
  {
    soc_rtc_get_datetime(&now);

    start = PREDIV_S - RTC_GetAlarmSubSecond(LPM_RTC_ALARM);
    stop = PREDIV_S - now.subsecond;
    if(stop > start)
    {
      mcu_wakeup_time = stop - start;
    }
    wakeup_time_init = 1;
  }
}
/*---------------------------------------------------------------------------*/
soc_rtc_result_t
soc_rtc_set_datetime(soc_rtc_td_map *data)
{
  RTC_TimeTypeDef RTC_TimeStructure;
  RTC_DateTypeDef RTC_DateStructure;
  RTC_InitTypeDef RTC_InitStructure;
  uint32_t hourFormat_t;
  uint16_t year_upper_digits, year_lower_digits;

  if(soc_rtc_check_td_format(data, 0) == SOC_RTC_RESULT_ERROR) {
    PRINTF("\nRTC: Invalid time/date values !!\n");
    return SOC_RTC_RESULT_ERROR;
  }

  /* Change Hour Format to either 12h or 24h*/
  hourFormat_t = IS_RTC_HOUR24(data->hour) ? RTC_HourFormat_24 : RTC_HourFormat_12;
  if(hourFormat != hourFormat_t) {

    RTC_InitStructure.RTC_AsynchPrediv = AsynchPrediv;
    RTC_InitStructure.RTC_SynchPrediv  = SynchPrediv;
    RTC_InitStructure.RTC_HourFormat   = hourFormat_t;
    hourFormat = hourFormat_t;

    /* Check on RTC init */
    if (RTC_Init(&RTC_InitStructure) == ERROR)
    {
      PRINTF("\n        /!\\***** RTC Prescaler Config failed ********/!\\ \n");
    }
  }

  /* Configure the RTC time register */
  RTC_TimeStructure.RTC_Hours   = data->hour;
  RTC_TimeStructure.RTC_Minutes = data->minute;
  RTC_TimeStructure.RTC_Seconds = data->second;
  RTC_TimeStructure.RTC_H12     = data->h12;

  if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure) == ERROR)
  {
    PRINTF("\n>> !! RTC Set Time and Date failed. !! <<\n");
    return SOC_RTC_RESULT_ERROR;
  }
  else
  {
    /* Indicator for the RTC configuration */
    RTC_WriteBackupRegister(RTC_BKP_DR0, RTC_STATUS_TIME_OK);
  }

  /* Configure the RTC date registers */
  RTC_DateStructure.RTC_WeekDay = data->dow;
  RTC_DateStructure.RTC_Month = data->month;
  RTC_DateStructure.RTC_Date = data->day;

  year_lower_digits = data->year % 100;
  year_upper_digits = data->year / 100;

  RTC_DateStructure.RTC_Year = year_lower_digits;

  if(RTC_SetDate(RTC_Format_BIN, &RTC_DateStructure) == ERROR)
  {
    PRINTF("\n>> !! RTC Set Time and Date failed. !! <<\n");
    return SOC_RTC_RESULT_ERROR;
  }
  else
  {
    /* Indicator for the RTC configuration */
    RTC_WriteBackupRegister(RTC_BKP_DR0, RTC_STATUS_TIME_OK);
    /* Indicator for the RTC upper digits */
    RTC_WriteBackupRegister(RTC_BKP_DR1, year_upper_digits);
  }

  return SOC_RTC_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
void
soc_rtc_init(void)
{
//#if USE_LPM_STOP_MODE
  static uint8_t rtc_initialized;
//#endif /* USE_LPM_STOP_MODE */
  /* Initialize the RTC */
  RTC_InitTypeDef RTC_InitStructure;

//#if USE_LPM_STOP_MODE
  if(rtc_initialized == 0)
//#else /* USE_LPM_STOP_MODE */
//  if (RTC_ReadBackupRegister(RTC_BKP_DR0) != RTC_STATUS_TIME_OK)
//#endif /* USE_LPM_STOP_MODE */
  {
    /* RTC configuration  */
    soc_rtc_clock_init();

    /* Configure the RTC data register and RTC prescaler */
    RTC_InitStructure.RTC_AsynchPrediv = AsynchPrediv;
    RTC_InitStructure.RTC_SynchPrediv  = SynchPrediv;
    RTC_InitStructure.RTC_HourFormat   = RTC_HourFormat_24;

    hourFormat = RTC_HourFormat_24;

    /* Check on RTC init */
    if (RTC_Init(&RTC_InitStructure) == ERROR)
    {
      PRINTF("\n        /!\\***** RTC Prescaler Config failed ********/!\\ \n");
    }

//#if USE_LPM_STOP_MODE
    rtc_initialized = 1;
//#endif /* USE_LPM_STOP_MODE */
  } else {
    /* Check if the Power On Reset flag is set */
    if (RCC_GetFlagStatus(RCC_FLAG_PORRST) != RESET)
    {
      PRINTF("\n Power On Reset occurred....\n");
    }
    /* Check if the Pin Reset flag is set */
    else if (RCC_GetFlagStatus(RCC_FLAG_PINRST) != RESET)
    {
      PRINTF("\n External Reset occurred....\n");
    }

    PRINTF("\n No need to configure RTC....\n");

    /* Enable the PWR clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

    /* Allow access to RTC */
    PWR_RTCAccessCmd(ENABLE);

    /* Wait for RTC APB registers synchronization */
    if(RTC_WaitForSynchro() == ERROR) {
      PRINTF("%s(Error RTC_WaitForSynchro)\n", __FUNCTION__);
    }
  }
}
/*---------------------------------------------------------------------------*/
void
soc_rtc_get_datetime(soc_rtc_td_map *data)
{
  RTC_TimeTypeDef RTC_TimeStructure;
  RTC_DateTypeDef RTC_DateStructure;
  uint16_t year_upper_digits;
  uint32_t first_read = 0, second_read = 0;

  RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);

  /* Unfreeze the RTC DR Register */
  (void)RTC->DR;

  /* Get the current Date */
  RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
  first_read = (uint32_t)RTC_GetSubSecond();
  RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
  second_read = (uint32_t)RTC_GetSubSecond();

  /* Make sure it is correct due to the asynchronous nature of the RTC */
  while( first_read != second_read )
  {
    first_read = second_read;
    RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
    second_read = (uint32_t)RTC_GetSubSecond();
  }

  RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);


  //data->subsecond = 1000 - (((uint32_t) RTC_GetSubSecond() * 1000) / PREDIV_S);
  data->subsecond = (uint32_t) RTC_GetSubSecond();
  data->second = RTC_TimeStructure.RTC_Seconds;
  data->minute = RTC_TimeStructure.RTC_Minutes;
  data->hour   = RTC_TimeStructure.RTC_Hours;
  data->day    = RTC_DateStructure.RTC_Date;
  data->dow    = RTC_DateStructure.RTC_WeekDay;
  data->month  = RTC_DateStructure.RTC_Month;
  year_upper_digits = (uint16_t) RTC_ReadBackupRegister(RTC_BKP_DR1) * 100;
  data->year   = year_upper_digits + RTC_DateStructure.RTC_Year;
  data->h12    = RTC_TimeStructure.RTC_H12;
}
/*---------------------------------------------------------------------------*/
uint32_t
soc_rtc_get_milliseconds_from_subseconds(const soc_rtc_td_map *data)
{
  if(!data)
  {
    return SOC_RTC_RESULT_ERROR;
  }

  return (uint32_t)( 1000 - ( ( data->subsecond * 1000 ) / PREDIV_S ) );
}
/*---------------------------------------------------------------------------*/
void
soc_rtc_get_datetime_from_rtc_clock(soc_rtc_td_map *rtc_time,
                                    soc_rtc_clock_t rtc_clock)
{
  soc_rtc_clock_t timeoutValue = 0;

  uint16_t milliseconds = 0;
  uint16_t seconds = 0;
  uint16_t minutes = 0;
  uint16_t hours = 0;
  uint16_t days = 0;
  uint16_t months = 1;  // Start at 1, month 0 does not exist
  uint16_t years = 0;

  milliseconds += ( rtc_clock & PREDIV_S );

  /* Convert timeout to seconds */
  timeoutValue = rtc_clock >>= N_PREDIV_S;

  /* Convert milliseconds to RTC format and add to now */
  while( timeoutValue >= SecondsInDay )
  {
    timeoutValue -= SecondsInDay;
    days++;
  }

  /* Calculate hours */
  while( timeoutValue >= SecondsInHour )
  {
    timeoutValue -= SecondsInHour;
    hours++;
  }

  /* Calculate minutes */
  while( timeoutValue >= SecondsInMinute )
  {
    timeoutValue -= SecondsInMinute;
    minutes++;
  }

  // Calculate seconds
  seconds += timeoutValue;

  // Correct for modulo
  while ( milliseconds >= ( PREDIV_S + 1 ) )
  {
    milliseconds -= (PREDIV_S + 1);
    seconds++;
  }

  while( seconds >= SecondsInMinute )
  {
    seconds -= SecondsInMinute;
    minutes++;
  }

  while( minutes >= 60 )
  {
    minutes -= 60;
    hours++;
  }

  while( hours >= HoursInDay )
  {
    hours -= HoursInDay;
    days++;
  }

  while( days > DaysInMonthLeapYear[months - 1] )
  {
    days -= DaysInMonthLeapYear[months - 1];
    months++;
  }

  rtc_time->subsecond = PREDIV_S - milliseconds;
  rtc_time->second    = seconds;
  rtc_time->minute    = minutes;
  rtc_time->hour      = hours;
  rtc_time->day       = days;
  rtc_time->month     = months;
  rtc_time->year      = years +
                        (uint16_t)RTC_ReadBackupRegister(RTC_BKP_DR1) * 100;
  rtc_time->h12       = RTC_H12_AM;  // default
}
/*---------------------------------------------------------------------------*/
void
soc_rtc_get_rtc_clock_from_datetime(const soc_rtc_td_map *rtc_time,
                                    soc_rtc_clock_t *rtc_clock)
{
  static uint8_t previous_year = 0;
  static uint8_t century = 0;

  soc_rtc_td_map now;
  uint32_t timeCounterTemp = 0.0;
  uint32_t i = 0;

  if( rtc_time == NULL )
  {
    soc_rtc_get_datetime(&now);
  }
  else
  {
    now = *rtc_time;
  }

  /* Wait for RTC APB registers synchronization */
  if( RTC_WaitForSynchro() == ERROR ) {
    PRINTF("%s(Error RTC_WaitForSynchro)\n", __FUNCTION__);
  }

  // Remove first two digits of year
  now.year -= (uint16_t)RTC_ReadBackupRegister(RTC_BKP_DR1) * 100;

  /* Previous year */
  if( (previous_year == 99) && (now.year == 0) ) {
    century++;
  }
  previous_year = now.year;

  for(i = 0 ; i < century; i++) {
    timeCounterTemp += ( uint32_t ) (DaysInCentury * SecondsInDay);
  }

  // Years (calculation valid up to year 2099)
  for( i = 0 ; i < now.year ; i++ ) {
    if( (i == 0) || (i % 4) == 0 )
    {
      timeCounterTemp += ( uint32_t ) SecondsInLeapYear;
    }
    else
    {
      timeCounterTemp += ( uint32_t ) SecondsInYear;
    }
  }

  // Months (calculation valid up to year 2099)
  if( (now.year == 0) || (now.year % 4) == 0 )
  {
    for ( i = 0 ; i < (now.month - 1) ; i++ )
    {
      timeCounterTemp += ( uint32_t ) (DaysInMonthLeapYear[i] * SecondsInDay);
    }
  }
  else
  {
    for ( i = 0 ; i < (now.month - 1) ; i++ )
    {
      timeCounterTemp += ( uint32_t ) (DaysInMonth[i] * SecondsInDay);
    }
  }

  timeCounterTemp += ( uint32_t )(( uint32_t )now.second +
                  ( ( uint32_t )now.minute * SecondsInMinute ) +
                  ( ( uint32_t )now.hour * SecondsInHour ) +
                  ( ( uint32_t )( now.day * SecondsInDay ) ) );

  *rtc_clock = ( (soc_rtc_clock_t)round(timeCounterTemp) << N_PREDIV_S) +
               ( PREDIV_S - now.subsecond );
}
/*---------------------------------------------------------------------------*/
void
soc_rtc_alarm_init(uint32_t RTC_Alarm, uint8_t isWakeUpAlarm)
{
  RTC_AlarmTypeDef RTC_AlarmStructure;
  EXTI_InitTypeDef EXTI_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  if( isWakeUpAlarm )
  {
    if( RTC_Alarm == RTC_Alarm_A )
    {
      soc_rtc_isWakeUpAlarm[0] = 1;
    }
    else
    {
      soc_rtc_isWakeUpAlarm[1] = 1;
    }
  }
  /* RTC Alarm Interrupt Configuration */
  EXTI_ClearITPendingBit(EXTI_Line17);
  EXTI_InitStructure.EXTI_Line    = EXTI_Line17;
  EXTI_InitStructure.EXTI_Mode    = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable the RTC Alarm Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel                    = RTC_Alarm_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  if( RTC_Alarm == RTC_Alarm_A )
  {
    /* Enable the RTC Alarm A Interrupt */
    RTC_ITConfig(RTC_IT_ALRA, ENABLE);
  }
  else
  {
    /* Enable the RTC Alarm A Interrupt */
    RTC_ITConfig(RTC_IT_ALRA, ENABLE);
  }
  /* Disable the Alarm */
  RTC_AlarmCmd(RTC_Alarm, DISABLE);
}
/*---------------------------------------------------------------------------*/
void
soc_rtc_alarm_set_handler(uint32_t RTC_Alarm, RTC_AlarmIrqHandler handler)
{
  if( RTC_Alarm == RTC_Alarm_A )
  {
    RTC_AlarmIRQ[RTC_ALARM_IRQ_A] = handler;
  }
  else
  {
    RTC_AlarmIRQ[RTC_ALARM_IRQ_B] = handler;
  }
}
/*---------------------------------------------------------------------------*/
void
soc_rtc_schedule(soc_rtc_clock_t timeout)
{
  soc_rtc_set_wakeup_alarm( timeout );
}
/*---------------------------------------------------------------------------*/
soc_rtc_clock_t
soc_rtc_get_clock(void)
{
  soc_rtc_clock_t rtc_clock = 0;
  soc_rtc_get_rtc_clock_from_datetime(NULL, &rtc_clock);
  return rtc_clock;
}
/*---------------------------------------------------------------------------*/
soc_rtc_clock_t
soc_rtc_get_elapsed_alarm_clock(void)
{
  soc_rtc_clock_t elapsep_clock = 0;
  soc_rtc_clock_t current_clock = 0;
  soc_rtc_clock_t context_clock = 0;

  soc_rtc_get_rtc_clock_from_datetime(NULL, &current_clock);
  soc_rtc_get_rtc_clock_from_datetime(&calendar_context, &context_clock);

  if( current_clock < context_clock )
  {
    elapsep_clock = ( current_clock + ( 0xFFFFFFFF - context_clock ) );
  }
  else
  {
    elapsep_clock = ( current_clock - context_clock );
  }
  return elapsep_clock;
}
/*---------------------------------------------------------------------------*/
void
soc_rtc_get_adjusted_clock(soc_rtc_clock_t *timeout)
{
  if( *timeout > mcu_wakeup_time )
  {
    // we have waken up from a GPIO and we have lost "mcu_wakeup_time" that we need to compensate on next event
    if( non_scheduled_wakeup == 1)
    {
      non_scheduled_wakeup = 0;
      *timeout -= mcu_wakeup_time;
    }
  }

  if( *timeout > mcu_wakeup_time )
  {
    // we don't go in Low Power mode for delay below 50ms (needed for LEDs)
    if( *timeout < soc_rtc_time_msec_to_rtc_clock(50) ) // 50ms
    {
      soc_rtc_timer_event_allows_lpm = 0;
    }
    else
    {
      soc_rtc_timer_event_allows_lpm = 1;
      *timeout -= mcu_wakeup_time;
    }
  }
}
/*---------------------------------------------------------------------------*/
uint8_t
soc_rtc_allows_lpm(void)
{
  return soc_rtc_timer_event_allows_lpm;
}
/*---------------------------------------------------------------------------*/
soc_rtc_clock_t
soc_rtc_compute_future_event_clock(soc_rtc_clock_t futureEventInTime)
{
  return soc_rtc_get_clock() + futureEventInTime;
}
/*---------------------------------------------------------------------------*/
soc_rtc_clock_t
soc_rtc_compute_elapsed_clock(soc_rtc_clock_t eventInTime)
{
  soc_rtc_clock_t elapsedTime = 0;

  /* Needed at boot, cannot compute with 0 or elapsed time will be equal to
     current time */
  if( eventInTime == 0 )
  {
    return 0;
  }
  // first get the current value of the timer in tick
  soc_rtc_get_rtc_clock_from_datetime(NULL, &elapsedTime);

  // compare "eventInTime" with "elapsedTime" while watching for roll over due to 32-bit
  if( elapsedTime < eventInTime ) // roll over of the counter
  {
    // due to convertion tick to ms, roll over value is 0x7D000000 (0x7D000000 * 2.048 = 0xFFFFFFFF)
    return ( elapsedTime + (0xFFFFFFFF - eventInTime ) );
  }
  else
  {
    return ( elapsedTime - eventInTime );
  }
}
/*---------------------------------------------------------------------------*/
void
rtc_alarm_B_IRQHandler(void)
{
  /* Nothing to do at the moment, can be used in the future. */
}
/*---------------------------------------------------------------------------*/
soc_rtc_result_t
soc_rtc_set_alarm_datetime(uint32_t RTC_Alarm,
                           soc_rtc_td_map *data,
                           uint32_t RTC_AlarmMask,
                           uint32_t RTC_AlarmDateWeekDaySel,
                           uint8_t RTC_AlarmDateWeekDay)
{
  RTC_AlarmTypeDef RTC_AlarmStructure;

  if(soc_rtc_check_td_format(data, 0) == SOC_RTC_RESULT_ERROR)
  {
    PRINTF("\nRTC: Invalid time/date values !!\n");
    return SOC_RTC_RESULT_ERROR;
  }

  /* Disable the Alarm */
  RTC_AlarmCmd(RTC_Alarm, DISABLE);

  /* Set the alarm time */
  RTC_AlarmSubSecondConfig(RTC_Alarm,
                           data->subsecond,
                           RTC_AlarmSubSecondMask_None);
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours = data->hour;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = data->minute;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = data->second;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_H12 = data->h12;

  /* Set the alarm Masks */
  RTC_AlarmStructure.RTC_AlarmDateWeekDay = RTC_AlarmDateWeekDay;
  RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel;
  RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask;
  RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm, &RTC_AlarmStructure);

  /* Enable Alarm interrupt */
  if( RTC_Alarm == RTC_Alarm_A )
  {
    RTC_ITConfig(RTC_IT_ALRA, ENABLE);
  }
  else
  {
    RTC_ITConfig(RTC_IT_ALRB, ENABLE);
  }

  /* Enable the alarm */
  RTC_AlarmCmd(RTC_Alarm, DISABLE);

  return SOC_RTC_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
void
RTC_Alarm_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);

  if(RTC_GetITStatus(RTC_IT_ALRA) != RESET)
  {
    if( RTC_AlarmIRQ[RTC_ALARM_IRQ_A] != NULL)
    {
      RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
      if( soc_rtc_isWakeUpAlarm[0] )
      {
        /* Exit stop mode */
        lpm_wakeup();
        soc_rtc_compute_wakeup_time();
      }

      RTC_AlarmIRQ[RTC_ALARM_IRQ_A]();
    }

    /* Clear RTC AlarmA Flags */
    RTC_ClearITPendingBit(RTC_IT_ALRA);
  }
  if(RTC_GetITStatus(RTC_IT_ALRB) != RESET)
  {
    if( RTC_AlarmIRQ[RTC_ALARM_IRQ_B] != NULL)
    {
      RTC_AlarmCmd(RTC_Alarm_B, DISABLE);
      if( soc_rtc_isWakeUpAlarm[1] )
      {
        /* Exit stop mode */
        lpm_wakeup();
        soc_rtc_compute_wakeup_time();
      }
      RTC_AlarmIRQ[RTC_ALARM_IRQ_B]();
    }

    /* Clear RTC AlarmB Flags */
    RTC_ClearITPendingBit(RTC_IT_ALRB);
  }
  /* Clear the EXTI line 17 */
  EXTI_ClearITPendingBit(EXTI_Line17);

  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
/*---------------------------------------------------------------------------*/
