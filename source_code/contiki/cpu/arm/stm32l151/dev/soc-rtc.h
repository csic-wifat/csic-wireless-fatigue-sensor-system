/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */
/* -------------------------------------------------------------------------- */
/**
 * \addtogroup stm32l1xx
 * @{
 *
 * \defgroup stm32l1xx-rtc STM32L1xx real-time clock (RTC)
 *
 * STM32L1xx real-time clock (RTC)
 *
 * @{
 *
 * \file
 * Header file for the management of STM32L1 real-time clock (RTC)
 */
/* -------------------------------------------------------------------------- */
#ifndef _SOC_RTC_H_
#define _SOC_RTC_H_

#include "stm32l1xx_rtc.h"

/*!
 * Hardware RTC Alarm IRQ callback function definition
 */
typedef void ( RTC_AlarmIrqHandler )( void );
extern RTC_AlarmIrqHandler *RTC_AlarmIRQ[2];
typedef enum soc_rtc_alarm_irq_type
{
  RTC_ALARM_IRQ_A = 0,       /* 0 */
  RTC_ALARM_IRQ_B = 1       /* 1 */
} soc_rtc_alarm_irq;    /* The state machine states of a control pipe */

typedef unsigned long long soc_rtc_clock_t;

typedef enum
{
  SOC_RTC_RESULT_OK,
  SOC_RTC_RESULT_ERROR
} soc_rtc_result_t;

/** @} */
/* -------------------------------------------------------------------------- */
/** \name Readable Date and time memory map implementation
 *
 * This simplified structure allows the user to set date/alarms with a
 * reduced structure, without the bit-defined restrictions of the memory map,
 * using decimal values
 *
 * @{
 */
typedef struct soc_rtc_struct_td_reg {
  uint32_t subsecond;   /*!< Specifies the RTC Sub Second register content.
                          This parameter corresponds to a time unit range between [0-1]
                          Second with [1 Sec / SecondFraction + 1] granularity. */

  uint8_t second;       /*!< Specifies the RTC Time Seconds.
                          This parameter must be set to a value in the 0-59 range. */

  uint8_t minute;       /*!< Specifies the RTC Time Minutes.
                          This parameter must be set to a value in the 0-59 range. */

  uint8_t hour;         /*!< Specifies the RTC Time Hour.
                          This parameter must be set to a value in the 0-12 range
                          if the RTC_HourFormat_12 is selected or 0-23 range if
                          the RTC_HourFormat_24 is selected. */

  uint8_t day;            /*!< Specifies the RTC Date.
                          This parameter must be set to a value in the 1-31 range. */

  uint8_t dow;            /*!< Specifies the RTC Date WeekDay.
                          This parameter can be a value of @ref RTC_WeekDay_Definitions */

  uint8_t month;          /*!< Specifies the RTC Date Month (in BCD format).
                          This parameter can be a value of @ref RTC_Month_Date_Definitions */

  uint16_t year;          /*!< Specifies the RTC Date Year.
                          This parameter must be set to a value in the 0-99 range. */

  uint8_t h12;            /*!< Specifies the RTC AM/PM Time.
                          This parameter can be a value of @ref RTC_AM_PM_Definitions */
}  __attribute__ ((packed)) soc_rtc_td_map;

/**
 * \brief Initialize the RTC
 */
void soc_rtc_init(void);

/**
 * \brief Initialize the Alarm RTC
 */
void soc_rtc_alarm_init(uint32_t RTC_Alarm, uint8_t isWakeUpAlarm);

void soc_rtc_alarm_set_handler(uint32_t RTC_Alarm, RTC_AlarmIrqHandler handler);

/**
 * \brief Set the time and date
 * \param *data Time and date value (decimal format)
 * \return
 * \           SOC_RTC_RESULT_OK date/time set
 * \           SOC_RTC_RESULT_ERROR failed to set time/date (enable DEBUG for more info)
 */
soc_rtc_result_t soc_rtc_set_datetime(soc_rtc_td_map *data);

/**
 * \brief Configure the RTCC to match an alarm counter
 * \param data date and time values (in decimal) to match against
 * \param RTC_Alarm RTC Alarm to be set: RTC_Alarm_A or RTC_Alarm_B
 * \param RTC_AlarmMask RTC Alarm Masks
 * \param RTC_AlarmDateWeekDaySel RTC Alarm is on Date or WeekDay
 * \param RTC_AlarmDateWeekDay RTC Alarm Date/WeekDay
 * \return
 * \           SOC_RTC_RESULT_OK date/time set
 * \           SOC_RTC_RESULT_ERROR failed to set time/date (enable DEBUG for more info)
 */
soc_rtc_result_t soc_rtc_set_alarm_datetime(uint32_t RTC_Alarm,
                                            soc_rtc_td_map *data,
                                            uint32_t RTC_AlarmMask,
                                            uint32_t RTC_AlarmDateWeekDaySel,
                                            uint8_t RTC_AlarmDateWeekDay);

/**
 * \brief Get the current time and date
 * \param *data buffer to store the results
 */
void soc_rtc_get_datetime(soc_rtc_td_map *data);


uint32_t soc_rtc_get_milliseconds_from_subseconds(const soc_rtc_td_map *data);


void soc_rtc_get_datetime_from_rtc_clock(soc_rtc_td_map *rtc_time,
                                         soc_rtc_clock_t rtc_clock);

void soc_rtc_get_rtc_clock_from_datetime(const soc_rtc_td_map *rtc_time,
                           soc_rtc_clock_t *rtc_clock);

void soc_rtc_get_adjusted_clock(soc_rtc_clock_t *timeout);


void soc_rtc_schedule(soc_rtc_clock_t timeout);

soc_rtc_clock_t soc_rtc_get_clock(void);
soc_rtc_clock_t soc_rtc_get_elapsed_alarm_clock(void);

soc_rtc_clock_t soc_rtc_compute_future_event_clock(soc_rtc_clock_t futureEventInTime);
soc_rtc_clock_t soc_rtc_compute_elapsed_clock(soc_rtc_clock_t eventInTime);

soc_rtc_clock_t soc_rtc_rtc_clock_to_time_msec(soc_rtc_clock_t rtc_clock);
soc_rtc_clock_t soc_rtc_time_msec_to_rtc_clock(soc_rtc_clock_t msec);

uint8_t soc_rtc_allows_lpm(void);

/**
  * @brief  This function handles RTC Alarms interrupt request.
  * @param  None
  * @retval None
  */
void RTC_Alarm_IRQHandler(void);

/**
  * @brief  This function handles Tamper interrupt request.
  * @param  None
  * @retval None
  */
//void TAMPER_STAMP_IRQHandler(void);


/*!
 * Flag used to indicates a the MCU has waken-up from an external IRQ
 */
extern volatile uint8_t non_scheduled_wakeup;

/* -------------------------------------------------------------------------- */
#endif /* _SOC_RTC_H_ */
/* -------------------------------------------------------------------------- */
/**
 * @}
 * @}
 */
