/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */
/**
 * \addtogroup stm32l151-aes
 * @{
 *
 * \defgroup stm32l151-aes-128 STM32L151 AES-128
 *
 * AES-128 driver for the STM32L151 SoC
 * @{
 *
 * \file
 *         Header file of the AES-128 driver for the STM32L151 SoC
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */
#ifndef STM32L151_AES_128_H_
#define STM32L151_AES_128_H_

#include "lib/aes-128.h"

typedef enum {
	AES_ECB = 1,	/* Electronic CodeBook */
	AES_CBC = 2,	/* Cipher Block Chaining */
	AES_CTR = 3		/* Counter Mode */
} stm32l151_aes_128_mode;

uint8_t stm32l151_aes_128_set_encryption_mode(stm32l151_aes_128_mode mode);

/*---------------------------------------------------------------------------*/
extern const struct aes_128_driver stm32l151_aes_128_driver;

#endif /* STM32L151_AES_128_H_ */

/**
 * @}
 * @}
 */
