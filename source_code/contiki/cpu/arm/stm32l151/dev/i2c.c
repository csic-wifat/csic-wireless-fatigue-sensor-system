/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "i2c.h"
#include "stm32l151.h"
#include "sys/clock.h"
#include "sys/energest.h"
/*---------------------------------------------------------------------------*/
#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"
/*---------------------------------------------------------------------------*/
static uint8_t 				 init_done[2] = {0, 0};
static i2c_addr_size_t i2c_addr_size = I2C_ADDR_SIZE_8;
/*---------------------------------------------------------------------------*/
#define Timed(x) 													\
 	Timeout = 0xFFFF;												\
 	while (x) 															\
 	{  																			\
 		if (Timeout-- == 0) return I2C_ERROR;	\
 	} 																			\
 	bStaus++;
/*----------------------------------------------------------------------------*/
/* Maximum number of trials for i2c_wait_standby_state() function */
#define I2C_MAX_TRIALS_NUMBER     		 300
/*---------------------------------------------------------------------------*/
static i2c_result_t
i2c_write_16bit(I2C_TypeDef* I2Cx, uint16_t buf)
{
  uint8_t buf_t;

  // Send the MSB of the buf
  buf_t = (uint8_t)((buf & 0xFF00) >> 8);
  if( i2c_write_buf(I2Cx,
                    &buf_t,
                    sizeof(uint8_t)) != I2C_SUCCESS )
  {
    PRINTF("i2c: Could not send MSB of buf (0x%04x): 0x%02x\n",
            buf, buf_t);
    return I2C_ERROR;
  }

  // Send the LSB of the buf
  buf_t = (uint8_t)(buf & 0x00FF);
  if( i2c_write_buf(I2Cx,
                    &buf_t,
                    sizeof(uint8_t)) != I2C_SUCCESS )
  {
    PRINTF("i2c: Could not send LSB of buf (0x%04x): 0x%02x\n",
            buf, buf_t);
    return I2C_ERROR;
  }
}
/*---------------------------------------------------------------------------*/
void
i2c_acknowledge_config(I2C_TypeDef* I2Cx, FunctionalState NewState)
{
	I2C_AcknowledgeConfig(I2Cx, NewState);
}
/*---------------------------------------------------------------------------*/
void
i2c_init(I2C_TypeDef* I2Cx,
				 uint32_t baudrate,
				 uint16_t mode,
				 uint16_t duty_cycle,
				 i2c_addr_t own_addr1,
				 uint16_t ack_en,
				 uint16_t acked_addr)
{
  I2C_InitTypeDef I2C_InitStructure;

  if ( ((I2Cx == I2C1) && !init_done[0]) ||
  		 ((I2Cx == I2C2) && !init_done[1]) )
  {
	  i2c_arch_init(I2Cx);

		/* Configure I2Cx */
	  I2C_StructInit(&I2C_InitStructure);
	  I2C_InitStructure.I2C_ClockSpeed 					= baudrate;
		I2C_InitStructure.I2C_Mode 								= mode;
		I2C_InitStructure.I2C_DutyCycle 					= duty_cycle;
		I2C_InitStructure.I2C_OwnAddress1 				= own_addr1;
		I2C_InitStructure.I2C_Ack 								= ack_en;
		I2C_InitStructure.I2C_AcknowledgedAddress = acked_addr;
		I2C_Init(I2Cx, &I2C_InitStructure);

		/* Turn on I2Cx */
		I2C_Cmd(I2Cx, ENABLE);
		if (I2Cx == I2C1) {
			init_done[0] = 1;
		}
		else {
			init_done[1] = 1;
		}
	}
}
/*---------------------------------------------------------------------------*/
void
i2c_deinit(I2C_TypeDef* I2Cx)
{
  /*!< Disable I2C */
  I2C_Cmd(I2Cx, DISABLE);

  /*!< De-initializes the I2C */
  I2C_DeInit(I2Cx);

  /* De-initializes the gpio lines */
  i2c_arch_deinit(I2Cx);

  if(I2Cx == I2C1)
	{
	  init_done[0] = 0;
	}
	else
	{
	  init_done[1] = 0;
	}
}
/*---------------------------------------------------------------------------*/
void
i2c_set_addr_size(i2c_addr_size_t size)
{
  i2c_addr_size = size;
}
/*---------------------------------------------------------------------------*/
i2c_result_t
i2c_read_ack(I2C_TypeDef* I2Cx, uint8_t *buf)
{
	__IO uint32_t Timeout = 0;
	__IO uint8_t bStaus = 0;
	/*
	 * This function reads one byte from the slave device
	 * and acknowledges the byte (requests another byte)
	 */

	/* Enable acknowledge of received data */
	I2C_AcknowledgeConfig(I2Cx, ENABLE);

	/* Wait until one byte has been received */
  Timed( !I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED) );
	*buf = I2C_ReceiveData(I2Cx);

	return I2C_SUCCESS;
}
/*---------------------------------------------------------------------------*/
i2c_result_t
i2c_read_nack(I2C_TypeDef* I2Cx, uint8_t *buf)
{
	__IO uint32_t Timeout = 0;
	__IO uint8_t bStaus = 0;

	/* Disable acknowledge of received data
	 * nack also generates stop condition after last byte received
	 * see reference manual for more info
	 */

	/* Disable acknowledge of received data */
	I2C_AcknowledgeConfig(I2Cx, DISABLE);

	/* EV6_1 -- must be atomic -- Clear ADDR, generate STOP */
  dint();
  (void) I2Cx->SR2;
  I2C_GenerateSTOP(I2Cx, ENABLE);
	eint();

	/* Wait until one byte has been received */
  Timed( !I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED) );
	*buf = I2C_ReceiveData(I2Cx);

	return I2C_SUCCESS;
}
/*---------------------------------------------------------------------------*/
i2c_result_t
i2c_read_buf(I2C_TypeDef* I2Cx,
						 uint8_t *buf,
						 i2c_addr_t slaveAddress,
						 uint8_t len)
{
	__IO uint32_t Timeout = 0;
	__IO uint8_t bStaus = 0;
	uint8_t *buf_t = buf;

	if (!len)
		return I2C_SUCCESS;

	/* Begin read transfer */
	i2c_start_xfer(I2Cx, I2C_Direction_Receiver, slaveAddress);

	I2C_AcknowledgeConfig(I2Cx, ENABLE);
	I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Current);

	if (len == 1)
	{
		/* Read 1 byte */
		if (i2c_read_nack(I2Cx, buf_t) != I2C_SUCCESS) {
			return I2C_ERROR;
		}
	}
	else if (len == 2)
	{
		/* Read 2 bytes */
		/* Set POS flag */
    I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Next);

		/* EV6_1 -- must be atomic -- Clear ADDR, generate STOP */
	  dint();
	  (void) I2Cx->SR2;											// Clear ADDR flag
	  I2C_AcknowledgeConfig(I2Cx, DISABLE);	// Clear Ack bit
		eint();

		/* EV7_3  -- Wait for BTF, program stop, read data twice */
		Timed( !I2C_GetFlagStatus(I2Cx, I2C_FLAG_BTF) );

		dint();
  	I2C_GenerateSTOP(I2Cx, ENABLE);
  	*buf_t++ = I2Cx->DR;
		eint();

		*buf_t++ = I2Cx->DR;
	}
	else
	{
		/* Read 3 or more bytes */
		(void) I2Cx->SR2; // Clear ADDR flag
		while (len-- != 3)
		{
			/*  EV7 -- cannot guarantee 1 transfer completion time */
			/* Wait for BTF instead of RXNE */
			Timed(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_BTF));
		  *buf_t++ = I2C_ReceiveData(I2Cx);
		}

		Timed(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_BTF));

		/* EV7_2 -- Figure 1 has an error, doesn't read N-2 ! */
		I2C_AcknowledgeConfig(I2Cx, DISABLE); // clear ack bit

		dint();
    *buf_t++ = I2C_ReceiveData(I2Cx); // receive byte N-2
		I2C_GenerateSTOP(I2Cx, ENABLE); // program stop
		eint();

		*buf_t++ = I2C_ReceiveData(I2Cx); // receive byte N-1

		/* Wait for byte N */
    Timed(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED));
		*buf_t++ = I2C_ReceiveData(I2Cx);
	}

	/* Wait for stop */
  Timed( I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF) );
  return I2C_SUCCESS;
}
/*---------------------------------------------------------------------------*/
i2c_result_t
i2c_write_buf(I2C_TypeDef* I2Cx,
							const uint8_t *buf,
							uint8_t len)
{
	__IO uint32_t Timeout = 0;
	__IO uint8_t bStaus = 0;

	while(len--)
	{
	  /* Send the data */
  	I2C_SendData(I2Cx, (uint8_t)*buf++);

  	/* Wait for EV8 */
		Timed(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }

	return I2C_SUCCESS;
}
/*---------------------------------------------------------------------------*/
i2c_result_t
i2c_start_xfer(I2C_TypeDef* I2Cx,
							 uint8_t xferDirection,
							 i2c_addr_t slaveAddress)
{
	__IO uint32_t Timeout = 0;
	__IO uint8_t bStaus = 0;

	PRINTF("%s: Test on busy flag\n", __FUNCTION__);
	if (xferDirection == I2C_Direction_Transmitter)
	{
		/* Test on BUSY Flag */
		Timed(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));
	}

	PRINTF("%s: Enable i2c peripheral\n", __FUNCTION__);
	/* Enable the I2C peripheral */
  I2C_GenerateSTART(I2Cx, ENABLE);

  PRINTF("%s: Test on Ev5\n", __FUNCTION__);
  /* Test on EV5 and clear it (cleared by reading SR1 then writing to DR) */
  Timed(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));

  /* Send device address for write */
  I2C_Send7bitAddress(I2Cx, slaveAddress, xferDirection);
  /* Test on EV6 and clear it */
  if(xferDirection == I2C_Direction_Transmitter)
  {
  	Timed(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
  }
  else if(xferDirection == I2C_Direction_Receiver)
  {
  	Timed(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
  }
  else
  {
  	return I2C_ERROR;
  }

	return I2C_SUCCESS;
}
/*---------------------------------------------------------------------------*/
i2c_result_t
i2c_stop_xfer(I2C_TypeDef* I2Cx)
{
	__IO uint32_t Timeout = 0;
	__IO uint8_t bStaus = 0;

	/* Send STOP Condition */
	I2C_GenerateSTOP( I2Cx, ENABLE );

	/* Test STOP flag */
	Timed( I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF) );
	return I2C_SUCCESS;
}
/*----------------------------------------------------------------------------*/
i2c_result_t
i2c_wait_standby_state(I2C_TypeDef* I2Cx, i2c_addr_t slaveAddress)
{
	__IO uint32_t Timeout = 0;
	__IO uint8_t bStaus = 0;

	__IO uint16_t tmpSR1 = 0;
  __IO uint32_t i2c_trials = 0;

	/*!< While the bus is busy */
	Timed( I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY) );

	/* Keep looping till the slave acknowledge his address or maximum number
     of trials is reached (this number is defined by I2C_MAX_TRIALS_NUMBER) */

  while(1)
  {
  	/*!< Send START condition */
  	I2C_GenerateSTART(I2Cx, ENABLE);

  	/*!< Test on EV5 and clear it (cleared by reading SR1 then writing to DR) */
  	Timed(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));

  	/*!< Send EEPROM address for write */
 	 	I2C_Send7bitAddress(I2Cx, slaveAddress, I2C_Direction_Transmitter);

 	 	/* Wait for ADDR flag to be set (Slave acknowledged his address) */
 	 	Timed(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTING));

 	 	/* Wait for ADDR flag to be set (Slave acknowledged his address) */
 	 	Timeout = 0xFFFF;
 	 	do
 	 	{
 	 		/* Get the current value of the SR1 register */
 	 		tmpSR1 = I2Cx->SR1;
 	 		/* Update the timeout value and exit if it reach 0 */
 	 		if ((Timeout--) == 0)  return I2C_ERROR;
 	 	}
 	 	/* Keep looping till the Address is acknowledged or the AF flag is
       set (address not acknowledged at time) */
 	 	while((tmpSR1 & (I2C_SR1_ADDR | I2C_SR1_AF)) == 0);

 	 	/* Check if the ADDR flag has been set */
 	 	if (tmpSR1 & I2C_SR1_ADDR)
    {
      /* Clear ADDR Flag by reading SR1 then SR2 registers (SR1 have already
         been read) */
    	dint();
      (void)I2Cx->SR2;

      /*!< STOP condition */
      I2C_GenerateSTOP(I2Cx, ENABLE);
      eint();

      /* Exit the function */
      return I2C_SUCCESS;
    }
    else
    {
      /*!< Clear AF flag */
      I2C_ClearFlag(I2Cx, I2C_FLAG_AF);
    }

    /* Check if the maximum allowed number of trials has bee reached */
    if (i2c_trials++ == I2C_MAX_TRIALS_NUMBER)
    {
      /* If the maximum number of trials has been reached, exit the function */
      return I2C_ERROR;
    }
  }
}
/*---------------------------------------------------------------------------*/
void
I2C1_ER_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);
  /* Check on I2C1 SMBALERT flag and clear it */
  if (I2C_GetITStatus(I2C1, I2C_IT_SMBALERT))
  {
    I2C_ClearITPendingBit(I2C1, I2C_IT_SMBALERT);
  }
  /* Check on I2C1 Time out flag and clear it */
  if (I2C_GetITStatus(I2C1, I2C_IT_TIMEOUT))
  {
    I2C_ClearITPendingBit(I2C1, I2C_IT_TIMEOUT);
  }
  /* Check on I2C1 Arbitration Lost flag and clear it */
  if (I2C_GetITStatus(I2C1, I2C_IT_ARLO))
  {
    I2C_ClearITPendingBit(I2C1, I2C_IT_ARLO);
  }

  /* Check on I2C1 PEC error flag and clear it */
  if (I2C_GetITStatus(I2C1, I2C_IT_PECERR))
  {
    I2C_ClearITPendingBit(I2C1, I2C_IT_PECERR);
  }
  /* Check on I2C1 Overrun/Under-run error flag and clear it */
  if (I2C_GetITStatus(I2C1, I2C_IT_OVR))
  {
    I2C_ClearITPendingBit(I2C1, I2C_IT_OVR);
  }
  /* Check on I2C1 Acknowledge failure error flag and clear it */
  if (I2C_GetITStatus(I2C1, I2C_IT_AF))
  {
    I2C_ClearITPendingBit(I2C1, I2C_IT_AF);
  }
  /* Check on I2C1 Bus error flag and clear it */
  if (I2C_GetITStatus(I2C1, I2C_IT_BERR))
  {
    I2C_ClearITPendingBit(I2C1, I2C_IT_BERR);
  }
  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
/*---------------------------------------------------------------------------*/
void I2C2_ER_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);
  /* Check on I2C1 SMBALERT flag and clear it */
  if (I2C_GetITStatus(I2C2, I2C_IT_SMBALERT))
  {
    I2C_ClearITPendingBit(I2C2, I2C_IT_SMBALERT);
  }
  /* Check on I2C1 Time out flag and clear it */
  if (I2C_GetITStatus(I2C2, I2C_IT_TIMEOUT))
  {
    I2C_ClearITPendingBit(I2C2, I2C_IT_TIMEOUT);
  }
  /* Check on I2C1 Arbitration Lost flag and clear it */
  if (I2C_GetITStatus(I2C2, I2C_IT_ARLO))
  {
    I2C_ClearITPendingBit(I2C2, I2C_IT_ARLO);
  }

  /* Check on I2C1 PEC error flag and clear it */
  if (I2C_GetITStatus(I2C2, I2C_IT_PECERR))
  {
    I2C_ClearITPendingBit(I2C2, I2C_IT_PECERR);
  }
  /* Check on I2C1 Overrun/Underrun error flag and clear it */
  if (I2C_GetITStatus(I2C2, I2C_IT_OVR))
  {
    I2C_ClearITPendingBit(I2C2, I2C_IT_OVR);
  }
  /* Check on I2C1 Acknowledge failure error flag and clear it */
  if (I2C_GetITStatus(I2C2, I2C_IT_AF))
  {
    I2C_ClearITPendingBit(I2C2, I2C_IT_AF);
  }
  /* Check on I2C1 Bus error flag and clear it */
  if (I2C_GetITStatus(I2C2, I2C_IT_BERR))
  {
    I2C_ClearITPendingBit(I2C2, I2C_IT_BERR);
  }
  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
/*---------------------------------------------------------------------------*/
