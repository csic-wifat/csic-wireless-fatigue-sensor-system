/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

#ifndef SOC_ADC_H_
#define SOC_ADC_H_
/*---------------------------------------------------------------------------*/
#include "lib/sensors.h"
/*---------------------------------------------------------------------------*/
#define SOC_ADC "SOC ADC"
/*---------------------------------------------------------------------------*/
extern const struct sensors_sensor soc_adc;
/*---------------------------------------------------------------------------*/
typedef enum {
  SOC_ADC_RESULT_OK,
  SOC_ADC_RESULT_NOT_SUPPORTED,
  SOC_ADC_RESULT_INVALID_VALUE,
  SOC_ADC_RESULT_ERROR
} soc_adc_result_t;
/*---------------------------------------------------------------------------*/
enum {
  SOC_ADC_VALUE = 0,
	SOC_ADC_PARAM_CHANNEL,
	SOC_ADC_PARAM_SAMPLE_TIME,
	SOC_ADC_PARAM_RESOLUTION,
	SOC_ADC_PARAM_SCAN_MODE,
	SOC_ADC_PARAM_CONVERSION_MODE,
	SOC_ADC_PARAM_EXTERNAL_TRIGGER_CONV_EDGE,
	SOC_ADC_PARAM_EXTERNAL_TRIGGER_CONVERSION,
  SOC_ADC_PARAM_DATA_ALIGN,
  SOC_ADC_PARAM_NBR_OF_CONVERSION,
  SOC_ADC_PARAM_TEMPSENSOR_VREFINT_ENABLE
};
/*----------------------------------------------------------------------------*/
typedef struct soc_adc_struct_reg {
	uint32_t Channels;
	uint32_t SampleTime;
	uint32_t Resolution;
  uint32_t ScanConvMode;
  uint32_t ContinuousConvMode;
  uint32_t ExternalTrigConvEdge;
  uint32_t ExternalTrigConv;
  uint32_t DataAlign;
  uint32_t NbrOfConversion;
}  __attribute__ ((packed)) soc_adc_reg_map;

extern soc_adc_reg_map adc_sensor_config;
/*----------------------------------------------------------------------------*/
/*!
 * ADC maximum value
 */
#define SOC_ADC_MAX_VALUE           ( ( 1 << 12 ) - 1 )
#define SOC_ADC_MAX_U16_VALUE       ( 65535 )
/*!
 * VREF calibration value
 */
#define SOC_ADC_VREFINT_CAL         ( *( uint16_t* )0x1FF80078 )
/*----------------------------------------------------------------------------*/
#ifndef SOC_ADC_BUFFER_LENGTH
#define SOC_ADC_BUFFER_LENGTH				4096
#endif /* SOC_ADC_BUFFER_LENGTH */
uint16_t soc_adc_dma_values[SOC_ADC_BUFFER_LENGTH];
/*----------------------------------------------------------------------------*/
#if SOC_ADC_WITH_DMA_TRANSFER
process_event_t soc_adc_dma_eot_event;

#define PROCESS_EVENT_SOC_ADC_DMA_EOT soc_adc_dma_eot_event
#endif /* SOC_ADC_WITH_DMA_TRANSFER */

extern volatile uint8_t is_active;
/*----------------------------------------------------------------------------*/
void soc_adc_arch_init(void);
void soc_adc_arch_deinit(void);
/*---------------------------------------------------------------------------*/
int soc_adc_trigger_conversion(uint8_t trigger);
/*---------------------------------------------------------------------------*/
#define SOC_ADC_SENSORS_TRIGGER_CONVERSION(x)  soc_adc_trigger_conversion(x)
/*---------------------------------------------------------------------------*/
#endif /* SOC_ADC_H_ */
/*---------------------------------------------------------------------------*/
/**
 * @}
 * @}
 */
