/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup stm32l151-adc-sensor
 * @{
 *
 * \file
 * Driver for the stm32l151 ADC
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "lib/sensors.h"
#include "dev/soc-adc.h"
#include "sys/timer.h"
#include "sys/energest.h"
#include "stm32l151.h"

#include <stdbool.h>
/*---------------------------------------------------------------------------*/
#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"
/*---------------------------------------------------------------------------*/
volatile uint8_t is_active      = 0;
static uint8_t hw_init_done = 0;
soc_adc_reg_map soc_adc_config = {
  0,
  ADC_SampleTime_192Cycles,
  ADC_Resolution_12b,
  DISABLE,
  DISABLE,
  ADC_ExternalTrigConvEdge_None,
  ADC_ExternalTrigConv_T6_TRGO,
  ADC_DataAlign_Right,
  0
};
/*---------------------------------------------------------------------------*/
static int
configure(int type, int value)
{
  switch(type) {
  case SENSORS_HW_INIT:
    if(configure(SOC_ADC_PARAM_CHANNEL, value) != SOC_ADC_RESULT_OK){
      return SOC_ADC_RESULT_ERROR;
    }
    return SOC_ADC_RESULT_OK;

  case SENSORS_ACTIVE:
    if(value == is_active) {
      return SOC_ADC_RESULT_OK;
    }

    is_active = value;
    if(is_active)
    {
      if(!hw_init_done)
      {
        if(!soc_adc_config.Channels) {
          return SOC_ADC_RESULT_ERROR;
        }
#if SOC_ADC_WITH_DMA_TRANSFER
        soc_adc_dma_eot_event = process_alloc_event();
#endif /* SOC_ADC_WITH_DMA_TRANSFER */
        soc_adc_arch_init();
        hw_init_done = 1;
      }

      /* Enable ADC1 */
      ADC_Cmd(ADC1, ENABLE);

      /* Wait for ADC to be ready */
      while(ADC_GetFlagStatus(ADC1, ADC_FLAG_ADONS) == RESET);
    } else {
      if(!hw_init_done) {
        return SOC_ADC_RESULT_OK;
      }
      hw_init_done = 0;
      soc_adc_arch_deinit();
    }
    return SOC_ADC_RESULT_OK;

  case SOC_ADC_PARAM_CHANNEL:
    if(IS_ADC_CHANNEL((uint32_t)value)) {
      soc_adc_config.Channels |= (1 << (uint32_t)value);
      soc_adc_config.NbrOfConversion++;
    }
    return SOC_ADC_RESULT_OK;

  case SOC_ADC_PARAM_SAMPLE_TIME:
    if(IS_ADC_SAMPLE_TIME((uint8_t)value)) {
      soc_adc_config.SampleTime = (uint8_t)value;
    }
    return SOC_ADC_RESULT_OK;

  case SOC_ADC_PARAM_RESOLUTION:
    if(IS_ADC_RESOLUTION((uint32_t)value)) {
      soc_adc_config.Resolution = (uint32_t)value;
    }
    return SOC_ADC_RESULT_OK;

  case SOC_ADC_PARAM_SCAN_MODE:
    soc_adc_config.ScanConvMode = (FunctionalState)value;
    return SOC_ADC_RESULT_OK;

  case SOC_ADC_PARAM_CONVERSION_MODE:
    soc_adc_config.ContinuousConvMode = (FunctionalState)value;
    if(is_active) {
      ADC_ContinuousModeCmd(ADC1, (FunctionalState)value);
    }
    return SOC_ADC_RESULT_OK;

  case SOC_ADC_PARAM_EXTERNAL_TRIGGER_CONV_EDGE:
    soc_adc_config.ExternalTrigConvEdge = (uint32_t)value;
    return SOC_ADC_RESULT_OK;

  case SOC_ADC_PARAM_EXTERNAL_TRIGGER_CONVERSION:
    soc_adc_config.ExternalTrigConv = (uint32_t)value;
    return SOC_ADC_RESULT_OK;

  case SOC_ADC_PARAM_DATA_ALIGN:
    soc_adc_config.DataAlign = (uint32_t)value;
    return SOC_ADC_RESULT_OK;

  case SOC_ADC_PARAM_NBR_OF_CONVERSION:
    soc_adc_config.NbrOfConversion = (uint8_t)value;
    return SOC_ADC_RESULT_OK;

  case SOC_ADC_PARAM_TEMPSENSOR_VREFINT_ENABLE:
    ADC_TempSensorVrefintCmd((FunctionalState)value);
    return SOC_ADC_RESULT_OK;

  default:
    return SOC_ADC_RESULT_NOT_SUPPORTED;
  }
}
/*---------------------------------------------------------------------------*/
static int
status(int type)
{
  switch(type) {
    case SENSORS_HW_INIT:
      return hw_init_done;
    case SENSORS_ACTIVE:
      return is_active;
    default:
      return 0;
  }
}
/*---------------------------------------------------------------------------*/
static uint16_t
soc_adc_read_u16(void)
{
  uint16_t value = ADC_GetConversionValue(ADC1);
  // 12-bit to 16-bit conversion
  value = ((value << 4) & (uint16_t)0xFFF0) | ((value >> 8) & (uint16_t)0x000F);
  return value;
}
/*---------------------------------------------------------------------------*/
static int
value(int type)
{
  switch(type) {
    case SOC_ADC_VALUE:
      if(!is_active) {
        PRINTF("ADC not active\n");
        return SOC_ADC_RESULT_OK;
      }
      return soc_adc_read_u16();
    case SOC_ADC_PARAM_CHANNEL:
      return soc_adc_config.Channels;
    case SOC_ADC_PARAM_SAMPLE_TIME:
      return soc_adc_config.SampleTime;
    case SOC_ADC_PARAM_RESOLUTION:
      return soc_adc_config.Resolution;
    case SOC_ADC_PARAM_SCAN_MODE:
      return soc_adc_config.ScanConvMode;
    case SOC_ADC_PARAM_CONVERSION_MODE:
      return soc_adc_config.ContinuousConvMode;
    case SOC_ADC_PARAM_EXTERNAL_TRIGGER_CONV_EDGE:
      return soc_adc_config.ExternalTrigConvEdge;
    case SOC_ADC_PARAM_EXTERNAL_TRIGGER_CONVERSION:
      return soc_adc_config.ExternalTrigConv;
    case SOC_ADC_PARAM_DATA_ALIGN:
      return soc_adc_config.DataAlign;
    case SOC_ADC_PARAM_NBR_OF_CONVERSION:
      return soc_adc_config.NbrOfConversion;
    default:
      return SOC_ADC_RESULT_NOT_SUPPORTED;
  }
}
/*---------------------------------------------------------------------------*/
void
ADC1_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);
  if(ADC_GetITStatus(ADC1, ADC_IT_AWD) != RESET)
  {
    /* Clear ADC1 AWD pending interrupt bit */
    ADC_ClearITPendingBit(ADC1, ADC_IT_AWD);
  }
  if(ADC_GetITStatus(ADC1, ADC_IT_EOC) != RESET)
  {
    /* Clear EOC Flag */
    ADC_ClearITPendingBit(ADC1, ADC_IT_EOC);
  }
  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
#if SOC_ADC_WITH_DMA_TRANSFER
void
SOC_ADC_DMA_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);
  if(DMA_GetITStatus(DMA1_IT_TC1))
  {
    /* DMA1 finished the transfer of data */
    process_post(PROCESS_BROADCAST, PROCESS_EVENT_SOC_ADC_DMA_EOT, NULL);
    //Clear DMA1 interrupt pending bits
    DMA_ClearITPendingBit(DMA1_IT_GL1);
  }
  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
#endif /* SOC_ADC_WITH_DMA_TRANSFER */
/*---------------------------------------------------------------------------*/
int
soc_adc_trigger_conversion(uint8_t trigger)
{
  if(!is_active) {
    PRINTF("ADC not active\n");
    return SOC_ADC_RESULT_ERROR;
  }
  if(trigger)
  {
    PRINTF("Trigger conversion\n");
    /* Trigger conversion */
    ADC_SoftwareStartConv(ADC1);

    if(soc_adc_config.ScanConvMode == DISABLE) {
      /* Wait until end of conversion */
      while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
    }
  }
  return SOC_ADC_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
void
TIM6_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);
  if(TIM_GetITStatus(TIM6, TIM_IT_Update) == SET)
  {
    /* Clear the TIM2 Update flag */
    TIM_ClearITPendingBit(TIM6, TIM_IT_Update);
  }
  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
/*---------------------------------------------------------------------------*/
SENSORS_SENSOR(soc_adc, SOC_ADC, value, configure, status);
/*---------------------------------------------------------------------------*/
/** @} */
