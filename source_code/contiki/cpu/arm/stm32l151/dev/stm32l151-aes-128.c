/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */
/**
 * \addtogroup stm32l151-aes
 * @{
 *
 * \file
 *         Implementation of the AES-128 driver for the stm32l151 SoC
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */
#include "contiki.h"
#include "stm32l1xx.h"
#include "dev/stm32l151-aes-128.h"

#include <stdint.h>
#include <stdio.h>
/*---------------------------------------------------------------------------*/
#define MODULE_NAME     "stm32l151-aes-128"

#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#ifdef STM32_AES_CONF_KEY
#define STM32_AES_KEY STM32_AES_CONF_KEY
#else /* STM32_AES_CONF_KEY */
#define STM32_AES_KEY { 0x2B , 0x7E , 0x15 , 0x16 , \
                        0x28 , 0xAE , 0xD2 , 0xA6 , \
                        0xAB , 0xF7 , 0x15 , 0x88 , \
                        0x09 , 0xCF , 0x4F , 0x3C }
#endif /* STM32_AES_CONF_KEY */
uint8_t aes_init_vector[AES_128_KEY_LENGTH] = { 0x00 , 0x01 , 0x02 , 0x03 , \
                                                0x04 , 0x05 , 0x06 , 0x07 , \
                                                0x08 , 0x09 , 0x0A , 0x0B , \
                                                0x0C , 0x0D , 0x0E , 0x0F };

static uint8_t aes_key[AES_128_KEY_LENGTH] = STM32_AES_KEY;
static uint8_t encryption_mode = AES_ECB; // Default mode
/*---------------------------------------------------------------------------*/
static void
enable_crypto(void)
{
  /* Enable AES AHB clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_AES, ENABLE);
}
/*---------------------------------------------------------------------------*/
static void
disable_crypto(void)
{
  /* Disable AES AHB clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_AES, DISABLE);
}
/*---------------------------------------------------------------------------*/
static void
set_key(const uint8_t *key)
{
  memset(aes_key, 0, AES_128_KEY_LENGTH);
  memcpy(aes_key, key, AES_128_KEY_LENGTH);
}
/*---------------------------------------------------------------------------*/
static void
encrypt(uint8_t *plaintext_and_result)
{
  uint8_t plaintext[AES_128_BLOCK_SIZE];
  uint8_t ret;

  memcpy(plaintext, plaintext_and_result, AES_128_BLOCK_SIZE);
  memset(plaintext_and_result, 0, AES_128_BLOCK_SIZE);

  enable_crypto();

  if(encryption_mode == AES_ECB) {
    ret = AES_ECB_Encrypt(aes_key, plaintext, AES_128_BLOCK_SIZE, plaintext_and_result);
  } else if(encryption_mode == AES_CBC) {
    ret = AES_CBC_Encrypt(aes_key, aes_init_vector, plaintext,
                          AES_128_BLOCK_SIZE, plaintext_and_result);
  } else {
    ret = AES_CTR_Encrypt(aes_key, aes_init_vector, plaintext,
                          AES_128_BLOCK_SIZE, plaintext_and_result);
  }

  disable_crypto();
}
/*---------------------------------------------------------------------------*/
uint8_t
stm32l151_aes_128_set_encryption_mode(stm32l151_aes_128_mode mode)
{
  switch(mode)
  {
    case AES_ECB:
    case AES_CBC:
    case AES_CTR:
      encryption_mode = mode;
      return 1;
    default:
      return 0;
  }
}
/*---------------------------------------------------------------------------*/
const struct aes_128_driver stm32l151_aes_128_driver = {
  set_key,
  encrypt
};

/** @} */
