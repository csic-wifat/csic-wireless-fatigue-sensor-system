/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

#ifndef _EEPROM_H_
#define _EEPROM_H_

typedef enum {
  EEPROM_RESULT_OK,
  EEPROM_RESULT_ERROR
} eeprom_result_t;

typedef uint8_t eeprom_value_t;
typedef uint16_t eeprom_addr_t;

void eeprom_init(void);
void eeprom_deinit(void);
eeprom_result_t eeprom_read(eeprom_addr_t addr, uint8_t *buffer, uint16_t *size);
eeprom_result_t eeprom_write(eeprom_addr_t addr, uint8_t *buffer, uint16_t size);

/* Arch functions */
eeprom_result_t eeprom_arch_init(void);
eeprom_result_t eeprom_arch_deinit(void);
eeprom_result_t eeprom_arch_read(eeprom_addr_t addr, uint8_t *buffer, uint16_t *size);
eeprom_result_t eeprom_arch_write(eeprom_addr_t addr, uint8_t *buffer, uint16_t size);
void eeprom_arch_set_dev_address(eeprom_addr_t addr);
eeprom_addr_t eeprom_arch_get_dev_address(void);

/*---------------------------------------------------------------------------*/
#endif /* _EEPROM_H_ */
/*---------------------------------------------------------------------------*/
/**
 * @}
 * @}
 */
