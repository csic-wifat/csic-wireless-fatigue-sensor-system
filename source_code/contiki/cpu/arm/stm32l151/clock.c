/*
 * Copyright (c) 2012, STMicroelectronics.
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
#include <stdio.h>
#include "contiki.h"
#include "platform-conf.h"
#include "contiki-conf.h"
#include "dev/leds.h"
#include "stm32l151.h"
/*---------------------------------------------------------------------------*/
#if LPM_USE_LP
#include "soc-rtc.h"
#include "lpm.h"
/*---------------------------------------------------------------------------*/
/* The counter assumes the timer is sourced with the LSE and uses Div16 */
#define RTC_WKUPCOUNTER ((F_LSE / 16) / CLOCK_SECOND)
/*---------------------------------------------------------------------------*/
#endif /* LPM_USE_LP */
/*---------------------------------------------------------------------------*/
#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#define DEBUG_LEDS DEBUG
#undef LEDS_ON
#undef LEDS_OFF
#if DEBUG_LEDS
#define LEDS_ON(x) leds_on(x)
#define LEDS_OFF(x) leds_off(x)
#define LEDS_TOGGLE(x) leds_toggle(x)
#else
#define LEDS_ON(x)
#define LEDS_OFF(x)
#define LEDS_TOGGLE(x)
#endif
/*---------------------------------------------------------------------------*/
/* After how many clock cycles should the systick interrupt be fired */
#define RELOAD_VALUE ((F_CPU / CLOCK_CONF_SECOND) - 1)
/*---------------------------------------------------------------------------*/
static volatile unsigned long seconds;
static volatile clock_time_t ticks;
/*---------------------------------------------------------------------------*/
#if USE_LPM_STOP_MODE

void RTC_WKUP_IRQHandler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);

  /* Check on the WakeUp flag */
  if(RTC_GetITStatus(RTC_IT_WUT) != RESET)
  {
    ticks++;
    if((ticks % CLOCK_SECOND) == 0){
      seconds++;
      energest_flush();
      /* Toggle LED 1 */
      LEDS_TOGGLE(LEDS_RED);
      PRINTF("second %li (%li ticks)\n", seconds, ticks);
    }

    /* If an etimer expired, continue its process */
    if(etimer_pending()){
      lpm_wakeup();
      etimer_request_poll();
    }

    /* Clear RTC WakeUp flags */
    RTC_ClearITPendingBit(RTC_IT_WUT);
  }

  /* Clear the EXTI line 20 */
  EXTI_ClearITPendingBit(EXTI_Line20);

  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}
#endif  /* USE_LPM_STOP_MODE */
/*---------------------------------------------------------------------------*/
void SystemCoreClockUpdate( void );
/*---------------------------------------------------------------------------*/
void
SysTick_Handler(void)
{
  ENERGEST_ON(ENERGEST_TYPE_IRQ);

  ticks++;
  if((ticks % CLOCK_SECOND) == 0) {
    seconds++;
    energest_flush();
    /* Toggle LED 2 */
    LEDS_TOGGLE(LEDS_GREEN);
  }

  /* If an etimer expired, continue its process */
  if (etimer_pending()) {
    etimer_request_poll();
  }
  ENERGEST_OFF(ENERGEST_TYPE_IRQ);
}

/*---------------------------------------------------------------------------*/
void SysTickConfig(void)
{
#if !USE_LPM_STOP_MODE
  /* System function that updates the SystemCoreClock variable. */
  SystemCoreClockUpdate();

  /* Systick is fed from HCLK/8. */
  SysTick_CLKSourceConfig( SysTick_CLKSource_HCLK );

  /* Set MSI clock range to ~4.194MHz */
  RCC_MSIRangeConfig( RCC_MSIRange_6 );

  /* Set SysTick Preemption Priority to 1 */
  NVIC_SetPriority(SysTick_IRQn, 0x04);

  if (SysTick_Config(RELOAD_VALUE) ) {
    /* Capture error */
    while(1);
  }
#endif /* USE_LPM_STOP_MODE */
}
/*---------------------------------------------------------------------------*/
void
clock_init()
{
  ticks = 0;
  seconds = 0;

  SysTickConfig();

#if USE_LPM_STOP_MODE
  /* Initialize the RTC clock */
  soc_rtc_init();

  /* Initialize the RTC WakeUp interrupt */
  EXTI_InitTypeDef EXTI_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  /* EXTI configuration */
  EXTI_ClearITPendingBit(EXTI_Line20);
  EXTI_InitStructure.EXTI_Line    = EXTI_Line20;
  EXTI_InitStructure.EXTI_Mode    = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable the RTC Wakeup Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel                    = RTC_WKUP_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  /* RTC Wakeup Interrupt Generation: Clock Source: RTCDiv_16, Wakeup Time Base: ~4ms at (LSE=) 32768Hz / 16 = 2048Hz */
  RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div16);
  RTC_SetWakeUpCounter(RTC_WKUPCOUNTER);

  /* Enable the Wakeup Interrupt */
  RTC_ClearITPendingBit(RTC_IT_WUT);
  RTC_ITConfig(RTC_IT_WUT, ENABLE);

  /* Enable Wakeup Counter */
  RTC_WakeUpCmd(ENABLE);
#endif  /* USE_LPM_STOP_MODE */
}
/*---------------------------------------------------------------------------*/
unsigned long
clock_seconds(void)
{
  return seconds;
}
/*---------------------------------------------------------------------------*/
void
clock_set_seconds(unsigned long sec)
{
  seconds = sec;
}
/*---------------------------------------------------------------------------*/
clock_time_t
clock_time(void)
{
  return ticks;
}
/*---------------------------------------------------------------------------*/
void
clock_delay_usec(uint16_t usec)
{
  clock_delay(usec);
}
/*---------------------------------------------------------------------------*/
void
clock_delay(unsigned int i)
{
  i += 1; // Necessary to meet timing
  asm volatile (  "MOV R0,%[loops]\n\t"\
      "1: \n\t"\
      "SUB R0, #1\n\t"\
      "CMP R0, #0\n\t"\
      "BNE 1b \n\t" : : [loops] "r" (4 * i) : "memory"\
          );
}
/*---------------------------------------------------------------------------*/
/* Wait for a multiple of clock ticks (31.25ns per tick at 32MHz) */
void
clock_wait(clock_time_t i)
{
  clock_time_t start;
  start = clock_time();
  while(clock_time() - start < i);
}
/*---------------------------------------------------------------------------*/
