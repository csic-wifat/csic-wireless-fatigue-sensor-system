/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

#ifndef STM32L151_H
#define STM32L151_H

#include "stm32l1xx.h"
#include "mtarch.h"
/*---------------------------------------------------------------------------*/
#ifdef STM32L1_CONF_VCORE
#define STM32L1_VCORE             STM32L1_CONF_VCORE
#else /* STM32L1_CONF_VCORE */
#define STM32L1_VCORE             PWR_VoltageScaling_Range1
#endif /* STM32L1_CONF_VCORE */

#ifdef STM32L1_CONF_FLASH_LATENCY
#define STM32L1_FLASH_LATENCY     STM32L1_CONF_FLASH_LATENCY
#else /* STM32L1_CONF_FLASH_LATENCY */
#define STM32L1_FLASH_LATENCY     FLASH_Latency_1
#endif /* STM32L1_CONF_FLASH_LATENCY */
/*---------------------------------------------------------------------------*/
#define dint() __disable_irq()
#define eint() __enable_irq()

void stm32l1xx_cpu_init(void);
void stm32l1xx_cpu_deinit(void);

#define cpu_init(void) stm32l1xx_cpu_init()
#define cpu_deinit(void) stm32l1xx_cpu_deinit()

/*!
 * Hardware IO IRQ callback function definition
 */
typedef void ( EXTIIrqHandler )( void );
extern EXTIIrqHandler *ExtiIRQ[16];

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void);

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void);

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void);

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void);

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void);

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
//void SVC_Handler(void);

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void);

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
//void PendSV_Handler(void);

/**
  * @brief  This function handles Systick exception.
  *         Implementation in clock.c
  *
  * @param  None
  * @retval None
  */
void SysTick_Handler(void);

/**
  * Add here the Interrupt Handler for the used peripheral(s) (PPP), for the
	* available peripheral interrupt handler's name please refer to the startup
	* file (startup_stm32l1xx_md_gnu.S)
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void);*/

/**
  * @brief  This function handles External line 0 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI0_IRQHandler(void);

/**
  * @brief  This function handles External line 1 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI1_IRQHandler(void);

/**
  * @brief  This function handles External line 2 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI2_IRQHandler(void);

/**
  * @brief  This function handles External line 3 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI3_IRQHandler(void);

/**
  * @brief  This function handles External line 4 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI4_IRQHandler(void);

/**
  * @brief  This function handles External lines 5-9 interrupt request
  * @param  None
  * @retval None
  */
void EXTI9_5_IRQHandler(void);

/**
  * @brief  This function handles External line 10-15 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI15_10_IRQHandler(void);

#endif /* STM32L151_H */
