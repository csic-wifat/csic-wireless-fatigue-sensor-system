/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup stm32l1xx
 * @{
 *
 * \defgroup stm32l1xx-lpm STM32L1xx Low-Power management
 *
 * STM32L1xx low-power operation
 *
 * @{
 *
 * \file
 * Header file for the management of STM32L1 low-power operation
 */
/* -------------------------------------------------------------------------- */
#ifndef _LPM_H_
#define _LPM_H_

/**
  * @brief  This function initializes the system to enter low power mode
  *
  * @param  None
  * @retval None
  */
void lpm_init(void);

/**
  * @brief  This function configures the system to enter low power mode
  *
  * @param  None
  * @retval None
  */
void lpm_enter(void);

/**
  * @brief  This function configures the system to exit low power mode
  *
  * @param  None
  * @retval None
  */
void lpm_wakeup(void);

/**
  * @brief  This function configures the system to enter deep sleep mode
  *         (standby mode)
  *
  * @param  None
  * @retval None
  */
void lpm_deepsleep(void);

/**
 * Low-power module implementation
 */
void lpm_arch_init(void);
void lpm_arch_enter(void);
void lpm_arch_deepsleep(void);
void lpm_arch_wakeup(void);

/** @} */
/* -------------------------------------------------------------------------- */
#endif /* ifndef _LPM_H_ */
/* -------------------------------------------------------------------------- */
/**
 * @}
 * @}
 */
