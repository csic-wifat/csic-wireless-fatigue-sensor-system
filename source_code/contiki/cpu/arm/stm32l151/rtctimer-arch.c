/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "rtctimer.h"
#include "rtctimer-arch.h"
#include "stm32l151.h"
#include "platform-conf.h"
/*---------------------------------------------------------------------------*/
#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...) do {} while (0)
#endif
/*---------------------------------------------------------------------------*/
#include "lpm.h"
#include "soc-rtc.h"
#include <math.h>
/*---------------------------------------------------------------------------*/
void rtc_alarm_A_IRQHandler(void);
void rtc_alarm_A_IRQHandler(void)
{
  rtctimer_run_next();
}
/*---------------------------------------------------------------------------*/
void
rtctimer_arch_init(void)
{
  PRINTF("%s\n", __FUNCTION__);
  lpm_init();
#if !USE_LPM_STOP_MODE
  soc_rtc_init();
  soc_rtc_alarm_init(LPM_RTC_ALARM, 1);
#endif  /* USE_LPM_STOP_MODE */
  soc_rtc_alarm_set_handler(RTC_Alarm_A, rtc_alarm_A_IRQHandler);
}
/*---------------------------------------------------------------------------*/
rtctimer_clock_t
rtctimer_arch_now(void)
{
  soc_rtc_clock_t rtc_clock_msec;
  rtc_clock_msec = soc_rtc_rtc_clock_to_time_msec ( soc_rtc_get_clock( ) );
  return rtc_clock_msec;
}
/*---------------------------------------------------------------------------*/
void
rtctimer_arch_schedule(rtctimer_clock_t wakeup_time)
{
  soc_rtc_clock_t timestamp = soc_rtc_time_msec_to_rtc_clock( wakeup_time );
#if !USE_LPM_STOP_MODE
  soc_rtc_get_adjusted_clock( (soc_rtc_clock_t *)&timestamp );
#endif /* USE_LPM_STOP_MODE */
  soc_rtc_schedule( timestamp );
}
/*---------------------------------------------------------------------------*/
