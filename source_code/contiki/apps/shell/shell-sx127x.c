/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

#include "contiki.h"
#include "shell.h"
#include <stdio.h>

#include "net/netstack.h"
#include "shell-sx127x.h"
#include "dev/radio.h"

#include "lora-radio.h"
/*---------------------------------------------------------------------------*/
#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"
/*---------------------------------------------------------------------------*/
#define RADIO_PARAM_ON      1
#define RADIO_PARAM_OFF     0
/*---------------------------------------------------------------------------*/
static radio_value_t value;
/*---------------------------------------------------------------------------*/
static radio_result_t
get_param(struct shell_command *c, radio_param_t param, radio_value_t *value)
{
  char buf[10];
  radio_result_t rv;

  rv = NETSTACK_RADIO.get_value(param, value);

  switch(rv) {
  case RADIO_RESULT_ERROR:
    shell_output_str(c, "Radio returned an error", "");
    break;
  case RADIO_RESULT_INVALID_VALUE:
    sprintf(buf, "Value %d", *value);
    shell_output_str(c, buf, " is invalid");
    break;
  case RADIO_RESULT_NOT_SUPPORTED:
    sprintf(buf, "Param %d", param);
    shell_output_str(c, buf, " not supported");
    break;
  case RADIO_RESULT_OK:
    break;
  default:
    shell_output_str(c, "Unknown return value", "");
    break;
  }

  return rv;
}
/*---------------------------------------------------------------------------*/
static radio_result_t
set_param(struct shell_command *c, radio_param_t param, radio_value_t value)
{
  char buf[10];
  radio_result_t rv;

  rv = NETSTACK_RADIO.set_value(param, value);

  switch(rv) {
  case RADIO_RESULT_ERROR:
    shell_output_str(c, "Radio returned an error", "");
    break;
  case RADIO_RESULT_INVALID_VALUE:
    sprintf(buf, "Value %d", value);
    shell_output_str(c, buf, " is invalid");
    break;
  case RADIO_RESULT_NOT_SUPPORTED:
    sprintf(buf, "Param %d", param);
    shell_output_str(c, buf, " not supported");
    break;
  case RADIO_RESULT_OK:
    break;
  default:
    shell_output_str(c, "Unknown return value", "");
    break;
  }

  return rv;
}
/*---------------------------------------------------------------------------*/
PROCESS(shell_tx_power_process, "txpow");
SHELL_COMMAND(tx_power_command,
        "txpow",
        "txpow <%d dBm>: get/set tx power",
        &shell_tx_power_process);
PROCESS(shell_preample_length_process, "prel");
SHELL_COMMAND(preample_length_command,
        "prel",
        "prel <%u symbols (LoRa) / bytes (Fsk)>: get/set preamble length",
        &shell_preample_length_process);
PROCESS(shell_crcOn_process, "crcon");
SHELL_COMMAND(crcOn_command,
        "crcon",
        "crcon <0,1>: turn CRC on (1) or off (0)",
        &shell_crcOn_process);
PROCESS(shell_fixed_payload_length_on_process, "payfix");
SHELL_COMMAND(payload_fixed_length_on_command,
        "payfix",
        "payfix <0,1>: turn fixed payload length on (1) or off (0)",
        &shell_fixed_payload_length_on_process);
PROCESS(shell_payload_length_process, "payl");
SHELL_COMMAND(payload_length_command,
        "payl",
        "payl <%u symbols (LoRa) / bytes (Fsk)>: get/set payload length",
        &shell_payload_length_process);
PROCESS(shell_rssi_process, "rssi");
SHELL_COMMAND(rssi_command,
        "rssi",
        "rssi: get RSSI (dBm)",
        &shell_rssi_process);
PROCESS(shell_rssi_threshold_process, "rssit");
SHELL_COMMAND(rssi_threshold_command,
        "rssit",
        "rssit <%d dBm>: (RX) get/set RSSI threshold",
        &shell_rssi_threshold_process);
PROCESS(shell_snr_process, "snr");
SHELL_COMMAND(snr_command,
        "snr",
        "snr: get SNR (dB)",
        &shell_snr_process);
PROCESS(shell_public_network_process, "pubnet");
SHELL_COMMAND(public_network_command,
        "pubnet",
        "pubnet <0,1>: turn public network on (1) or off (0)",
        &shell_public_network_process);
PROCESS(shell_coding_rate_process, "cr");
SHELL_COMMAND(coding_rate_command,
        "cr",
        "cr <1-4>: get/set coding rate",
        &shell_coding_rate_process);
PROCESS(shell_spreading_factor_process, "sf");
SHELL_COMMAND(spreading_factor_command,
        "sf",
        "sf <7-12>: get/set spreading factor",
        &shell_spreading_factor_process);
PROCESS(shell_bandwidth_process, "bw");
SHELL_COMMAND(bandwidth_command,
        "bw",
        "bw <0-2 for SX1272 or 0-9 for SX1276/8>: get/set bandwidth",
        &shell_bandwidth_process);
PROCESS(shell_fchannel_process, "ch");
SHELL_COMMAND(fchannel_command,
        "ch",
        "ch <%u Hz>: get/set frequency channel",
        &shell_fchannel_process);
PROCESS(shell_afc_bandwidth_process, "bwafc");
SHELL_COMMAND(afc_bandwidth_command,
        "bwafc",
        "bwafc <%u>: get/set AFC bandwidth (Fsk only)",
        &shell_afc_bandwidth_process);
PROCESS(shell_freqHopOn_process, "lhpon");
SHELL_COMMAND(freqHopOn_command,
        "lhpon",
        "lhpon <0,1>: turn frequency hopping on (1) or off (0)",
        &shell_freqHopOn_process);
PROCESS(shell_hop_period_process, "lhp");
SHELL_COMMAND(hop_period_command,
        "lhp",
        "lhp <%u>: get/set hop period",
        &shell_hop_period_process);
PROCESS(shell_sync_word_process, "sync_word");
SHELL_COMMAND(sync_word_command,
        "sync_word",
        "sync_word <0,1>: get/set private (0) or public (1) sync word",
        &shell_sync_word_process);
PROCESS(shell_tx_timeout_process, "txtout");
SHELL_COMMAND(tx_timeout_command,
        "txtout",
        "txtout <%u us>: (TX) get/set transmission timeout",
        &shell_tx_timeout_process);
PROCESS(shell_radio_power_mode_process, "ronoff");
SHELL_COMMAND(radio_power_mode_command,
        "ronoff",
        "ronoff <0,1>: turn radio power mode off (0) or on (1)",
        &shell_radio_power_mode_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_tx_power_process, ev, data)
{
  char buf[10];
  radio_value_t modem;
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&tx_power_command, RADIO_PARAM_TXPOWER, value);
  }

  if(get_param(&tx_power_command, RADIO_PARAM_TXPOWER, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%d dBm", value);
    shell_output_str(&preample_length_command, "sx127x: Transmit power ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_preample_length_process, ev, data)
{
  char buf[10];
  radio_value_t modem;
  const char *next;

  PROCESS_BEGIN();

  if(get_param(&preample_length_command, RADIO_PARAM_MODEM, &modem) == RADIO_RESULT_OK) {
    value = shell_strtolong((char *)data, &next);
    if(next != data) {
      set_param(&preample_length_command, RADIO_PARAM_PREAMBLE_LENGTH, value);
    }

    if(get_param(&preample_length_command, RADIO_PARAM_PREAMBLE_LENGTH, &value) == RADIO_RESULT_OK) {
      if(modem == MODEM_LORA) {
        sprintf(buf, "%d symbols", value);
      } else {
        sprintf(buf, "%d bytes", value);
      }
      shell_output_str(&preample_length_command, "sx127x: Preamble length ", buf);
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_crcOn_process, ev, data)
{
  char buf[10];
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&crcOn_command, RADIO_PARAM_CRC_ON, value);
  }

  if(get_param(&crcOn_command, RADIO_PARAM_CRC_ON, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%s", (value == RADIO_PARAM_ON) ? "on" : "off");
    shell_output_str(&crcOn_command, "sx127x: CRC ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_fixed_payload_length_on_process, ev, data)
{
  char buf[10];
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&payload_fixed_length_on_command, RADIO_PARAM_FIXED_PAYLOAD_LENGTH_ON, value);
  }

  if(get_param(&payload_fixed_length_on_command, RADIO_PARAM_FIXED_PAYLOAD_LENGTH_ON, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%s", (value == RADIO_PARAM_ON) ? "on" : "off");
    shell_output_str(&payload_fixed_length_on_command, "sx127x: Fixed payload length ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_payload_length_process, ev, data)
{
  char buf[10];
  radio_value_t modem;
  const char *next;

  PROCESS_BEGIN();

  if(get_param(&payload_length_command, RADIO_PARAM_MODEM, &modem) == RADIO_RESULT_OK) {
    value = shell_strtolong((char *)data, &next);
    if(next != data) {
      set_param(&payload_length_command, RADIO_PARAM_PAYLOAD_LENGTH, value);
    }

    if(get_param(&payload_length_command, RADIO_PARAM_PAYLOAD_LENGTH, &value) == RADIO_RESULT_OK) {
      if(modem == MODEM_LORA) {
        sprintf(buf, "%d symbols", value);
      } else {
        sprintf(buf, "%d bytes", value);
      }
      shell_output_str(&payload_length_command, "sx127x: Payload length ", buf);
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_rssi_process, ev, data)
{
  char buf[10];
  const char *next;

  PROCESS_BEGIN();

  if(get_param(&rssi_command, RADIO_PARAM_RSSI, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%d dBm", value);
    shell_output_str(&rssi_command, "sx127x: RSSI ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_rssi_threshold_process, ev, data)
{
  char buf[10];
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&rssi_threshold_command, RADIO_PARAM_RSSI_THRESHOLD, value);
  }

  if(get_param(&rssi_threshold_command, RADIO_PARAM_RSSI_THRESHOLD, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%d dBm", value);
    shell_output_str(&rssi_threshold_command, "sx127x: RSSI threshold ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_snr_process, ev, data)
{
  char buf[10];
  const char *next;

  PROCESS_BEGIN();

  if(get_param(&snr_command, RADIO_PARAM_LAST_SNR, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%d dB", value);
    shell_output_str(&snr_command, "sx127x: SNR ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_public_network_process, ev, data)
{
  char buf[10];
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&public_network_command, RADIO_PARAM_PUBLIC_NETWORK, value);
  }

  if(get_param(&public_network_command, RADIO_PARAM_PUBLIC_NETWORK, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%s", (value == RADIO_PARAM_ON) ? "on" : "off");
    shell_output_str(&public_network_command, "sx127x: Public Network ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_coding_rate_process, ev, data)
{
  char buf[10];
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&coding_rate_command, RADIO_PARAM_CODINGRATE, value);
  }

  if(get_param(&coding_rate_command, RADIO_PARAM_CODINGRATE, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%d (%s)",
            value,
           (value == 1) ? "4/5" :
          ((value == 2) ? "4/6" :
          ((value == 3) ? "4/7" : "4/8")));
    shell_output_str(&coding_rate_command, "sx127x: CR ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_spreading_factor_process, ev, data)
{
  char buf[20];
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&spreading_factor_command, RADIO_PARAM_DATARATE, value);
  }

  if(get_param(&spreading_factor_command, RADIO_PARAM_DATARATE, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%d (%d chips)",
            value,
          ( value == 6) ?    64 :
          ((value == 7) ?   128 :
          ((value == 8) ?   256 :
          ((value == 9) ?   512 :
          ((value == 10) ? 1024 :
          ((value == 11) ? 2048 : 4096 ))))));
    shell_output_str(&spreading_factor_command, "sx127x: SF ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_fchannel_process, ev, data)
{
  char buf[30];
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&fchannel_command, RADIO_PARAM_CHANNEL, value);
  }

  if(get_param(&fchannel_command, RADIO_PARAM_CHANNEL, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%u Hz", value);
    shell_output_str(&fchannel_command, "sx127x: CH ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_bandwidth_process, ev, data)
{
  char buf[40];
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&bandwidth_command, RADIO_PARAM_BANDWIDTH, value);
  }

  if(get_param(&bandwidth_command, RADIO_PARAM_BANDWIDTH, &value) == RADIO_RESULT_OK) {
    if (value < 3)
    {
      sprintf(buf, "%d (SX1272: %s kHz; SX1276/8: %s)",
             value,
             value == 0 ? "125" :
             value == 1 ? "250" : "500",
             value == 0 ? "7.8"   :
             value == 1 ? "10.4"  : "15.6");
    }
    else
    {
      sprintf(buf, "%d (%s kHz)",
             value,
             value == 3 ? "20.8"  :
             value == 4 ? "31.2"  :
             value == 5 ? "41.7"  :
             value == 6 ? "62.5"  :
             value == 7 ? "125"   :
             value == 8 ? "250"   : "500" );
    }
    shell_output_str(&bandwidth_command, "sx127x: BW ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_afc_bandwidth_process, ev, data)
{
  char buf[10];
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&afc_bandwidth_command, RADIO_PARAM_AFC_BANDWIDTH, value);
  }

  if(get_param(&afc_bandwidth_command, RADIO_PARAM_AFC_BANDWIDTH, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%d Hz)", value);
    shell_output_str(&afc_bandwidth_command, "sx127x: AFC BW ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_freqHopOn_process, ev, data)
{
  char buf[10];
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&freqHopOn_command, RADIO_PARAM_FREQ_HOP_ON, value);
  }

  if(get_param(&freqHopOn_command, RADIO_PARAM_FREQ_HOP_ON, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%s", (value == RADIO_PARAM_ON) ? "on" : "off");
    shell_output_str(&freqHopOn_command, "sx127x: Frequency hopping ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_hop_period_process, ev, data)
{
  char buf[10];
  radio_value_t modem;
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&hop_period_command, RADIO_PARAM_HOP_PERIOD, value);
  }

  if(get_param(&hop_period_command, RADIO_PARAM_HOP_PERIOD, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%d", value);
    shell_output_str(&hop_period_command, "sx127x: Hop period ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_sync_word_process, ev, data)
{
  char buf[10];
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&sync_word_command, RADIO_PARAM_PUBLIC_NETWORK, value);
  }

  if(get_param(&sync_word_command, RADIO_PARAM_PUBLIC_NETWORK, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%s (0x%02x)", (value) ? "public" : "private", (value) ? LORA_MAC_PUBLIC_SYNCWORD : LORA_MAC_PRIVATE_SYNCWORD);
    shell_output_str(&sync_word_command, "sx127x: Sync word ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_tx_timeout_process, ev, data)
{
  char buf[20];
  radio_value_t modem;
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&tx_timeout_command, RADIO_PARAM_TX_TIMEOUT, value);
  }

  if(get_param(&tx_timeout_command, RADIO_PARAM_TX_TIMEOUT, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%d us", value);
    shell_output_str(&tx_timeout_command, "sx127x: Tx timeout ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_radio_power_mode_process, ev, data)
{
  char buf[20];
  radio_value_t modem;
  const char *next;

  PROCESS_BEGIN();

  value = shell_strtolong((char *)data, &next);
  if(next != data) {
    set_param(&radio_power_mode_command, RADIO_PARAM_POWER_MODE, value);
  }

  if(get_param(&radio_power_mode_command, RADIO_PARAM_POWER_MODE, &value) == RADIO_RESULT_OK) {
    sprintf(buf, "%s", (value == RADIO_PARAM_ON) ? "on" : "off");
    shell_output_str(&freqHopOn_command, "sx127x: Power mode ", buf);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
void
shell_sx127x_init(void)
{
  shell_register_command(&tx_power_command);
  shell_register_command(&preample_length_command);
  shell_register_command(&crcOn_command);
  shell_register_command(&payload_fixed_length_on_command);
  shell_register_command(&payload_length_command);
  shell_register_command(&rssi_command);
  shell_register_command(&rssi_threshold_command);
  shell_register_command(&snr_command);
  shell_register_command(&public_network_command);
  shell_register_command(&coding_rate_command);
  shell_register_command(&spreading_factor_command);
  shell_register_command(&fchannel_command);
  shell_register_command(&bandwidth_command);
  shell_register_command(&afc_bandwidth_command);
  shell_register_command(&freqHopOn_command);
  shell_register_command(&hop_period_command);
  shell_register_command(&sync_word_command);
  shell_register_command(&tx_timeout_command);
  shell_register_command(&radio_power_mode_command);
}
/*---------------------------------------------------------------------------*/
