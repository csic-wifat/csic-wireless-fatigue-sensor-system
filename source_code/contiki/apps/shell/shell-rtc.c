/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

#include "contiki.h"
#include "shell.h"
#include <stdio.h>

#include "platform-conf.h"
/*---------------------------------------------------------------------------*/
#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"
/*---------------------------------------------------------------------------*/
PROCESS(shell_rtc_process, "rtc");
SHELL_COMMAND(rtc_command,
        "rtc",
        "rtc <YYYY-MM-DD hh:mm:ss> : show RTC [or set RTC time]",
        &shell_rtc_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_rtc_process, ev, data)
{
  static RTC_TIME calendar, current_time;
  uint8_t res = RTC_TIME_RESULT_OK;

  uint8_t ok = 1;
  const char *nextptr;

  PROCESS_BEGIN();

  if (data == NULL)
    PRINTF("data: NULL\n");
  else
    PRINTF("data: [%s]\n", (char*) data);

  if(data != NULL) {
    calendar.year    = shell_strtolong(data, &nextptr);
    if(nextptr == data) ok = 0;
    PRINTF("  year=%i\n", calendar.year);

    if(ok && *nextptr == '-') {
      nextptr++;
      data = (process_data_t) nextptr;
      calendar.month = shell_strtolong(data, &nextptr);
      if (nextptr == data) ok = 0;
      PRINTF("  month=%i\n", calendar.month);
    }
    if(ok && *nextptr == '-') {
      nextptr++;
      data = (process_data_t) nextptr;
      calendar.day = shell_strtolong(data, &nextptr);
      if (nextptr == data) ok = 0;
      PRINTF("  day=%i\n", calendar.day);
    }

    if(ok) {
      data = (process_data_t) nextptr;
      calendar.hour = shell_strtolong(data, &nextptr);
      if (nextptr == data) ok = 0;
      PRINTF("  hour=%i\n", calendar.hour);
    }
    if (nextptr != data && *nextptr == ':') {
      nextptr++;
      data = (process_data_t) nextptr;
      calendar.minute = shell_strtolong(data, &nextptr);
      if (nextptr == data) ok = 0;
      PRINTF("  minute=%i\n", calendar.minute);
    }
    if (nextptr != data && *nextptr == ':') {
      nextptr++;
      data = (process_data_t) nextptr;
      calendar.second = shell_strtolong(data, &nextptr);
      if (nextptr == data) ok = 0;
      PRINTF("  second=%i\n", calendar.second);
    }

    if(ok) {
      RTC_SET_TIME(&calendar, &res);
      if(res != RTC_TIME_RESULT_OK) {
        shell_output_str(&rtc_command, "rtc: could not set clock", "");
      } else {
        shell_output_str(&rtc_command, "rtc: clock set", "");
      }
    }
  }

  char buf[40];
  ok = 0;

  res = RTC_TIME_RESULT_OK;
  RTC_GET_TIME(&current_time, &res);
  if(res != RTC_TIME_RESULT_OK)
  {
    snprintf(buf, 40, "Fail: could not read time");
  }
  else
  {
    ok = 1;
  }

  if(ok == 1)
  {
    /* Show current time */
    snprintf(buf, 40, "Clock: %04i-%02i-%02i %02i:%02i:%02i ...\n",
      current_time.year,
      current_time.month,
      current_time.day,
      current_time.hour,
      current_time.minute,
      current_time.second);
  }

  shell_output_str(&rtc_command, buf, "");

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
void
shell_rtc_init(void)
{
  shell_register_command(&rtc_command);
}
/*---------------------------------------------------------------------------*/
