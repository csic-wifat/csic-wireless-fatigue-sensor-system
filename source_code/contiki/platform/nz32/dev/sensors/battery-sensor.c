/*
 * Copyright (c) 2006, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup nz32-peripherals
 * @{
 *
 * \file
 * Driver for the NZ32 battery monitor
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "lib/sensors.h"
#include "stm32l1xx.h"
#include "dev/soc-adc.h"
#include "battery-sensor.h"

#include <stdio.h>
/*---------------------------------------------------------------------------*/
#ifndef POWER_SUPPLY
#define POWER_SUPPLY        ( 3300 )
#endif  /* POWER_SUPPLY */
/*---------------------------------------------------------------------------*/
const struct sensors_sensor battery_sensor;
/*---------------------------------------------------------------------------*/
static int _active;
static uint8_t init_done = 0;
/*---------------------------------------------------------------------------*/
static void
init(void)
{
#if PLATFORM_HAS_BATTERY_SENSOR
  if(!init_done)
  {
    /* Configure ADC channel and perform ADC hardware initialization */
    soc_adc.configure(SENSORS_HW_INIT, ADC_Channel_15);

    init_done = 1;
  }
#endif /* PLATFORM_HAS_BATTERY_SENSOR */
}
/*---------------------------------------------------------------------------*/
static void
activate(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /* Vbatt monitor is in PC5 (ADC channel 15) */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

  GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_5;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  /* To measure Vbatt, PA13 has to be pulled down (ST-Link pulls it high) */
  GPIO_WriteBit( GPIOA, GPIO_Pin_13, Bit_RESET );
  GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_13;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  clock_delay(150000);  // 150ms delay
  _active = 1;
}
/*---------------------------------------------------------------------------*/
static void
deactivate(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_5;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
  GPIO_Init( GPIOC, &GPIO_InitStructure );

  GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_13;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  _active = 0;
}
/*---------------------------------------------------------------------------*/
static int
value(int type)
{
  uint16_t batteryLevel = BAT_LEVEL_NO_MEASURE;
  float batteryVoltage = 0;
  uint16_t measuredLevel = 0;
#if PLATFORM_HAS_BATTERY_SENSOR
  if(_active)
  {
    /* if low-power mcu enabled, the adc hw needs to be re-enabled */
    if(!soc_adc.status(SENSORS_HW_INIT)) {
      SENSORS_ACTIVATE(soc_adc);
    }

    if(type == SINGLE_SHOT_TRIGGER_CONVERSION)
    {
      printf("%s: Trigger conversion\n", __FUNCTION__);
      /* Trigger conversion */
      SOC_ADC_SENSORS_TRIGGER_CONVERSION(1);
    }

    /* Read ADC conversion result */
    measuredLevel = (uint16_t)soc_adc.value(SOC_ADC_VALUE);
    //printf("%s: %u\n", __FUNCTION__, measuredLevel);
    /* Convert to voltage (mV). Resistor divider = 2 */
    batteryVoltage = (float)measuredLevel * (2.0 * (float)POWER_SUPPLY / (float)SOC_ADC_MAX_U16_VALUE);

    if( batteryVoltage >= BATTERY_MAX_LEVEL )
    {
      batteryLevel = BAT_LEVEL_FULL;
    }
    else if( (batteryVoltage > BATTERY_MIN_LEVEL) && (batteryVoltage < BATTERY_MAX_LEVEL) )
    {
      batteryLevel = ( ( (BAT_LEVEL_FULL - 1) * ( batteryVoltage - BATTERY_MIN_LEVEL ) ) / ( BATTERY_MAX_LEVEL - BATTERY_MIN_LEVEL ) ) + 1;
    }
    else //if( batteryVoltage <= BATTERY_SHUTDOWN_LEVEL )
    {
      batteryLevel = BAT_LEVEL_NO_MEASURE;
    }
  }
#endif /* PLATFORM_HAS_BATTERY_SENSOR */
  return batteryLevel;
}
/*---------------------------------------------------------------------------*/
static int
status(int type)
{
  switch(type)
  {
    case SENSORS_READY:
      return _active;
    default:
      return 0;
  }
}
/*---------------------------------------------------------------------------*/
static int
configure(int type, int value)
{
  int status = SOC_ADC_RESULT_ERROR;
#if PLATFORM_HAS_BATTERY_SENSOR
  switch(type)
  {
    case SENSORS_HW_INIT:
      if(!init_done)
      {
        /* value = 0 ==> ADC is configured & controlled externally */
        init();
      }
      status = SOC_ADC_RESULT_OK;
      break;

    case SENSORS_ACTIVE:
      /* Check hardware initialization */
      if(!init_done)
      {
        init();
      }

      if(value)
      {
        activate();
        status = SENSORS_ACTIVATE(soc_adc);
      }
      else
      {
        deactivate();
        status = SENSORS_DEACTIVATE(soc_adc);
      }
      break;

    default:
      status = SOC_ADC_RESULT_NOT_SUPPORTED;
      break;
  }
#endif /* PLATFORM_HAS_BATTERY_SENSOR */
  return status;
}
/*---------------------------------------------------------------------------*/
SENSORS_SENSOR(battery_sensor, BATTERY_SENSOR, value, configure, status);
/*---------------------------------------------------------------------------*/
/** @} */
