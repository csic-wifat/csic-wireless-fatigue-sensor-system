/*
 * Copyright (c) 2018, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup nz32-sensors
 * @{
 *
 * \defgroup nz32-fatigue-sensor NZ32 Fatigue Sensor Driver
 *
 * Driver for the NZ32 fatigue sensor
 *
 *
 * \file
 * Header file for the fatigue sensor
 */
/*---------------------------------------------------------------------------*/
#ifndef FATIGUE_SENSOR_H_
#define FATIGUE_SENSOR_H_
/*---------------------------------------------------------------------------*/
#include "lib/sensors.h"
/*---------------------------------------------------------------------------*/
#define FATIGUE_SENSOR "Fatigue"

extern const struct sensors_sensor fatigue_sensor;
/*---------------------------------------------------------------------------*/
#define FATIGUE_SENSOR_PEAK_VALUE                 ( 0 )
#define FATIGUE_SENSOR_PEAK_LEVEL                 ( 1 )
#define FATIGUE_SENSOR_PEAK_EVENT_COUNT           ( 2 )

#define FATIGUE_SENSOR_POSITIVE_LEVEL             ( 0 )
#define FATIGUE_SENSOR_NEGATIVE_LEVEL             ( 8 )
/*---------------------------------------------------------------------------*/
#endif /* FATIGUE_SENSOR_H_ */
/*---------------------------------------------------------------------------*/
/**
 * @}
 * @}
 */
