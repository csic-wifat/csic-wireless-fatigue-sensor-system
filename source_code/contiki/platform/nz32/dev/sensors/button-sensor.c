/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup nz32-peripherals
 * @{
 *
 * \file
 * Driver for the NZ32 User Button
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "button-sensor.h"
#include "sys/timer.h"
#include "sys/ctimer.h"
#include "sys/process.h"

#include "stm32l151.h"
#include "platform-conf.h"

#include <stdint.h>
#include <string.h>
/*---------------------------------------------------------------------------*/
#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"
/*---------------------------------------------------------------------------*/
#define DEBOUNCE_DURATION (CLOCK_SECOND >> 4)
static struct timer debouncetimer;
/*---------------------------------------------------------------------------*/
static clock_time_t press_duration = 0;
static struct ctimer press_counter;
static uint8_t press_event_counter;

NVIC_InitTypeDef NVIC_InitStructure;
EXTI_InitTypeDef EXTI_InitStructure;

process_event_t button_press_duration_exceeded;
/*---------------------------------------------------------------------------*/
static int _active = 0;
/*---------------------------------------------------------------------------*/
/*!
 * \brief Button IRQ callback
 */
void ButtonIrqHandler(void);
/*---------------------------------------------------------------------------*/
static void
duration_exceeded_callback(void *data)
{
#if PLATFORM_HAS_BUTTON
  press_event_counter++;
  process_post(PROCESS_BROADCAST, button_press_duration_exceeded,
               &press_event_counter);
  ctimer_set(&press_counter, press_duration, duration_exceeded_callback,
             NULL);
#endif /* PLATFORM_HAS_BUTTON */
}
/*---------------------------------------------------------------------------*/
static void
init(void)
{
#if PLATFORM_HAS_BUTTON
  GPIO_InitTypeDef GPIO_InitStructure;

  RCC_AHBPeriphClockCmd( BUTTON_USER_GPIO_CLK, ENABLE );
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_SYSCFG , ENABLE );

  /* Configures Button pin as input */
  GPIO_InitStructure.GPIO_Pin   = BUTTON_USER_PIN;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init( BUTTON_USER_GPIO_PORT, &GPIO_InitStructure );

  /* Connects Button EXTI Line to Button GPIO Pin */
  SYSCFG_EXTILineConfig(BUTTON_USER_EXTI_PORT_SOURCE, BUTTON_USER_EXTI_PIN_SOURCE);

  /* Configures Button EXTI line */
  EXTI_ClearITPendingBit(BUTTON_USER_EXTI_LINE);
  EXTI_InitStructure.EXTI_Line    = BUTTON_USER_EXTI_LINE;
  EXTI_InitStructure.EXTI_Mode    = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enables and sets Button EXTI Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel                    = BUTTON_USER_EXTI_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 0x0F;
  //NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  ExtiIRQ[BUTTON_USER_EXTI_PIN_SOURCE] = (EXTIIrqHandler *)ButtonIrqHandler;
#endif /* PLATFORM_HAS_BUTTON */
}
/*---------------------------------------------------------------------------*/
static void
activate(void)
{
#if PLATFORM_HAS_BUTTON
  _active = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
#endif /* PLATFORM_HAS_BUTTON */
}
/*---------------------------------------------------------------------------*/
static void
deactivate(void)
{
#if PLATFORM_HAS_BUTTON
  _active = 0;

  NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
  NVIC_Init(&NVIC_InitStructure);
#endif /* PLATFORM_HAS_BUTTON */
}
/*---------------------------------------------------------------------------*/
static int
active(void)
{
  return _active;
}
/*---------------------------------------------------------------------------*/
static int
value(int type)
{
#if PLATFORM_HAS_BUTTON
  switch(type) {
  case BUTTON_SENSOR_VALUE_TYPE_LEVEL:
    return (GPIO_ReadInputDataBit(BUTTON_USER_GPIO_PORT, BUTTON_USER_PIN) ? Bit_SET: Bit_RESET);
  case BUTTON_SENSOR_VALUE_TYPE_PRESS_DURATION:
    return press_event_counter;
  }
#endif /* PLATFORM_HAS_BUTTON */
  return 0;
}
/*---------------------------------------------------------------------------*/
void ButtonIrqHandler(void)
{
#if PLATFORM_HAS_BUTTON
  if(!timer_expired(&debouncetimer)) {
    return;
  }

  timer_set(&debouncetimer, DEBOUNCE_DURATION);

  if(press_duration) {
    press_event_counter = 0;
    if(value(BUTTON_SENSOR_VALUE_TYPE_LEVEL) == BUTTON_SENSOR_PRESSED_LEVEL) {
      ctimer_set(&press_counter, press_duration, duration_exceeded_callback,
                 NULL);
    } else {
      ctimer_stop(&press_counter);
    }
  }
  sensors_changed(&button_sensor);
#endif /* PLATFORM_HAS_BUTTON */
}
/*---------------------------------------------------------------------------*/
static int
configure(int type, int value)
{
#if PLATFORM_HAS_BUTTON
  switch(type) {
  case SENSORS_HW_INIT:
    button_press_duration_exceeded = process_alloc_event();
    init();
  case SENSORS_ACTIVE:
    if(value) {
      activate();
    } else {
      deactivate();
    }
  case BUTTON_SENSOR_CONFIG_TYPE_INTERVAL:
    press_duration = (clock_time_t)value;
    break;
  default:
    break;
  }
#endif /* PLATFORM_HAS_BUTTON */
  return 1;
}
/*---------------------------------------------------------------------------*/
static int
status(int type)
{
  switch(type) {
  case SENSORS_READY:
    return active();
  }

  return 0;
}
/*---------------------------------------------------------------------------*/
SENSORS_SENSOR(button_sensor, BUTTON_SENSOR, value, configure, status);
/*---------------------------------------------------------------------------*/
/** @} */
