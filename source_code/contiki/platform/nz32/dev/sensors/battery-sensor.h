/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * -----------------------------------------------------------------
 *
 * Author  : David Rodenas-Herraiz
 * Created : 2017-02-13
 * Updated : $Date: $
 *           $Revision: $
 */
/*---------------------------------------------------------------------------*/
#ifndef BATTERY_SENSOR_H_
#define BATTERY_SENSOR_H_
/*---------------------------------------------------------------------------*/
#include "lib/sensors.h"
/*---------------------------------------------------------------------------*/

/*!
 * Battery thresholds (battery dependent)
 */

/* Battery maximum capacity level */
#ifndef BATTERY_MAX_LEVEL
#define BATTERY_MAX_LEVEL         ( 3700 )  // mV
#endif /* BATTERY_MAX_LEVEL */

/* Limit of operation for the battery */
#ifndef BATTERY_MIN_LEVEL
#define BATTERY_MIN_LEVEL         ( 3100 )  // mV
#endif /* BATTERY_MIN_LEVEL */

/* Battery shutdown level */
#ifndef BATTERY_SHUTDOWN_LEVEL
#define BATTERY_SHUTDOWN_LEVEL    ( 3000 )  // mV
#endif /* BATTERY_SHUTDOWN_LEVEL */
/*---------------------------------------------------------------------------*/
#define SINGLE_SHOT_TRIGGER_CONVERSION  1
/*---------------------------------------------------------------------------*/
/*!
 * Battery level indicator
 */
typedef enum BatteryLevel
{
	/*!
   * External power source
   */
  BAT_LEVEL_EXT_SRC                = 0x00,
  /*!
   * Battery level empty
   */
  BAT_LEVEL_EMPTY                  = 0x01,
  /*!
   * Battery level full
   */
  BAT_LEVEL_FULL                   = 0xFE,
  /*!
   * Battery level - no measurement available
   */
  BAT_LEVEL_NO_MEASURE             = 0xFF,
}BatteryLevel_t;
/*---------------------------------------------------------------------------*/
extern const struct sensors_sensor battery_sensor;
/*---------------------------------------------------------------------------*/
#define BATTERY_SENSOR "Battery"
/*---------------------------------------------------------------------------*/
#endif /* BATTERY_SENSOR_H_ */
/*---------------------------------------------------------------------------*/
