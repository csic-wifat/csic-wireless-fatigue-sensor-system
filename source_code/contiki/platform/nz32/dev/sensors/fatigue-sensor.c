/*
 * Copyright (c) 2018, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup nz32-peripherals
 * @{
 *
 * \file
 * Driver for the Nz32 Fatigue Sensor
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "stm32l151.h"
#include "dev/soc-adc.h"
#include "sensors/fatigue-sensor.h"
/*---------------------------------------------------------------------------*/
#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...) do {} while (0)
#endif
/*---------------------------------------------------------------------------*/
#ifndef POWER_SUPPLY
#define POWER_SUPPLY                              ( 3300 )
#endif  /* POWER_SUPPLY */
/*---------------------------------------------------------------------------*/
/* Pin connected to trigger output */
#define FATIGUE_SENSOR_TRIGGER_GPIO_PIN           ( GPIO_Pin_1 )
#define FATIGUE_SENSOR_TRIGGER_GPIO_PORT          ( GPIOA )
#define FATIGUE_SENSOR_TRIGGER_GPIO_CLK           ( RCC_AHBPeriph_GPIOA )
#define FATIGUE_SENSOR_TRIGGER_EXTI_LINE          ( EXTI_Line1 )
#define FATIGUE_SENSOR_TRIGGER_EXTI_PORT_SOURCE   ( EXTI_PortSourceGPIOA )
#define FATIGUE_SENSOR_TRIGGER_EXTI_PIN_SOURCE    ( GPIO_PinSource1 )
#define FATIGUE_SENSOR_TRIGGER_EXTI_IRQn          ( EXTI1_IRQn )
#define FATIGUE_SENSOR_TRIGGER_EXTI_IRQn_HANDLER  ( EXTI1_IRQHandler )

/* Pin connected to data output */
#define FATIGUE_SENSOR_DATA_ADC_CHANNEL           ( ADC_Channel_0 )
#define FATIGUE_SENSOR_DATA_GPIO_PIN              ( GPIO_Pin_0 )
#define FATIGUE_SENSOR_DATA_GPIO_PORT             ( GPIOA )
#define FATIGUE_SENSOR_DATA_GPIO_CLK              ( RCC_AHBPeriph_GPIOA )
/*---------------------------------------------------------------------------*/
static uint32_t peak_event_counter;
/*---------------------------------------------------------------------------*/
static int _active = 0;         // Sensor active?
/*---------------------------------------------------------------------------*/
/*!
 * \brief Fatigue Sensor Peak Indicator IRQ callback
 */
void fatigue_sensor_peak_handler(void);
/*---------------------------------------------------------------------------*/
static void
init(void)
{
#if PLATFORM_HAS_FATIGUE_SENSOR
  /* GPIO interrupt line configuration */
  GPIO_InitTypeDef GPIO_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;
  EXTI_InitTypeDef EXTI_InitStructure;

  RCC_AHBPeriphClockCmd( FATIGUE_SENSOR_TRIGGER_GPIO_CLK, ENABLE );
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_SYSCFG , ENABLE );

  /* Configures Button pin as input */
  GPIO_InitStructure.GPIO_Pin = FATIGUE_SENSOR_TRIGGER_GPIO_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init( FATIGUE_SENSOR_TRIGGER_GPIO_PORT, &GPIO_InitStructure );

  /* Connects Button EXTI Line to Button GPIO Pin */
  SYSCFG_EXTILineConfig( FATIGUE_SENSOR_TRIGGER_EXTI_PORT_SOURCE,
                         FATIGUE_SENSOR_TRIGGER_EXTI_PIN_SOURCE );

  /* Configures Button EXTI line */
  EXTI_ClearITPendingBit( FATIGUE_SENSOR_TRIGGER_EXTI_LINE );
  EXTI_InitStructure.EXTI_Line = FATIGUE_SENSOR_TRIGGER_EXTI_LINE;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init( &EXTI_InitStructure );

  /* Enables and sets Button EXTI Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel = FATIGUE_SENSOR_TRIGGER_EXTI_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init( &NVIC_InitStructure );

  ExtiIRQ[FATIGUE_SENSOR_TRIGGER_EXTI_PIN_SOURCE] =
    (EXTIIrqHandler *)fatigue_sensor_peak_handler;

  /* ADC configuration */
  /* Since the ADC is is clocked by a HSI (16MHz) frequency, and the sampling
   * time is 192 cycles, so the ADC conversion frequency (fADCconv) is
   * 16MHz / 192 ~= 83KHz.
   */
  soc_adc.configure(SOC_ADC_PARAM_SAMPLE_TIME, ADC_SampleTime_4Cycles);

  /* Configure channel */
  soc_adc.configure(SENSORS_HW_INIT, FATIGUE_SENSOR_DATA_ADC_CHANNEL);
#endif /* PLATFORM_HAS_FATIGUE_SENSOR */
}
/*---------------------------------------------------------------------------*/
static void
deinit(void)
{
#if PLATFORM_HAS_FATIGUE_SENSOR
  GPIO_InitTypeDef GPIO_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;
  EXTI_InitTypeDef EXTI_InitStructure;

  /*!< Configure SPI NSS pin as output*/
  GPIO_InitStructure.GPIO_Pin   = FATIGUE_SENSOR_TRIGGER_GPIO_PIN;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init( FATIGUE_SENSOR_TRIGGER_GPIO_PORT, &GPIO_InitStructure );
  GPIO_WriteBit( FATIGUE_SENSOR_TRIGGER_GPIO_PORT, FATIGUE_SENSOR_TRIGGER_GPIO_PIN, Bit_RESET );


  NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
  EXTI_InitStructure.EXTI_LineCmd       = DISABLE;

  EXTI_InitStructure.EXTI_Line          = FATIGUE_SENSOR_TRIGGER_EXTI_LINE;
  EXTI_Init(&EXTI_InitStructure);
  NVIC_InitStructure.NVIC_IRQChannel    = FATIGUE_SENSOR_TRIGGER_EXTI_IRQn;
  NVIC_Init(&NVIC_InitStructure);
#endif /* PLATFORM_HAS_FATIGUE_SENSOR */
}
/*---------------------------------------------------------------------------*/
static void
activate(void)
{
  if(!_active)
  {
    init();
    _active = 1;
  }
}
/*---------------------------------------------------------------------------*/
static void
deactivate(void)
{
  if(_active)
  {
    deinit();
    _active = 0;
  }
}
/*---------------------------------------------------------------------------*/
static int
active(void)
{
  return _active;
}
/*---------------------------------------------------------------------------*/
static int
value(int type)
{
#if PLATFORM_HAS_FATIGUE_SENSOR
  uint16_t adc_value = 0;
  float adc_voltage = 0.0;

  switch(type) {
    case SENSORS_READY:
      return active();

    case FATIGUE_SENSOR_PEAK_VALUE:
      if(_active)
      {
        /* if low-power mcu enabled, the adc hw needs to be re-enabled */
        if(!soc_adc.status(SENSORS_HW_INIT)) {
          SENSORS_ACTIVATE(soc_adc);
        }

        /* Trigger conversion */
        SOC_ADC_SENSORS_TRIGGER_CONVERSION(1);

        /* Read ADC conversion result */
        adc_value = (uint16_t)soc_adc.value(SOC_ADC_VALUE);
        /* Convert to voltage */
        adc_voltage = (float)adc_value * ((float)POWER_SUPPLY / (float)SOC_ADC_MAX_U16_VALUE);

        SENSORS_DEACTIVATE(soc_adc);
      }
      return (int16_t)( adc_voltage );
      //return (uint16_t)( adc_value );

    case FATIGUE_SENSOR_PEAK_LEVEL:
      return ( GPIO_ReadInputDataBit(FATIGUE_SENSOR_TRIGGER_GPIO_PORT,
                                     FATIGUE_SENSOR_TRIGGER_GPIO_PIN) ? Bit_SET: Bit_RESET );

    case FATIGUE_SENSOR_PEAK_EVENT_COUNT:
      return peak_event_counter;
  }
#endif /* PLATFORM_HAS_FATIGUE_SENSOR */
  return 0;
}
/*---------------------------------------------------------------------------*/
void fatigue_sensor_peak_handler(void)
{
#if PLATFORM_HAS_FATIGUE_SENSOR
  peak_event_counter++;
  sensors_changed(&fatigue_sensor);
#endif /* PLATFORM_HAS_FATIGUE_SENSOR */
}
/*---------------------------------------------------------------------------*/
static int
configure(int type, int value)
{
  int status = SOC_ADC_RESULT_NOT_SUPPORTED;
#if PLATFORM_HAS_FATIGUE_SENSOR
  switch(type) {
    case SENSORS_HW_INIT:
      status = SOC_ADC_RESULT_OK;
      break;

    case SENSORS_ACTIVE:
      if(value)
      {
        /* Activate sensor */
        activate();
      }
      else
      {
        /* Deactivate sensor */
        deactivate();
      }
      break;

    default:
      break;
  }
#endif /* PLATFORM_HAS_FATIGUE_SENSOR */
  return status;
}
/*---------------------------------------------------------------------------*/
static int
status(int type)
{
  switch(type) {
  case SENSORS_READY:
    return active();
  }

  return 0;
}
/*---------------------------------------------------------------------------*/
SENSORS_SENSOR(fatigue_sensor, FATIGUE_SENSOR, value, configure, status);
/*---------------------------------------------------------------------------*/
/** @} */
