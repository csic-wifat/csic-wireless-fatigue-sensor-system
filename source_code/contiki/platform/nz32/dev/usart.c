/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include "stm32l1xx.h"
#include "dev/watchdog.h"
#include "platform-conf.h"
#include "dev/leds.h"
/* USART Library */
#include "dev/usart.h"
/* USB Libraries */
#include "usb_lib.h"

#if defined(USE_USB_CDC)
#include "usb_pwr.h"
#include "usb_desc.h"

#define END_CHAR(c) (c == 0x0a) || (c == 0x0d)

#define USB_TX_BUF_SIZE       2048        // Size of USB data buffer (to send data to USB)

uint8_t  USB_TX_BUF[USB_TX_BUF_SIZE]; // Data buffer for USB send
uint32_t USB_TX_ptr_in  = 0; // Head byte in USB buffer
uint32_t USB_TX_ptr_out = 0; // Tail byte in USB buffer
uint32_t USB_TX_len     = 0; // Amount of data in USB buffer

void Handle_USBAsynchXfer(void);
/*---------------------------------------------------------------------------*/
/* UARTx definitions */
/*---------------------------------------------------------------------------*/
#else

void EP1_IN_Callback (void) {}
void EP3_OUT_Callback (void) {}
#endif /* USE_USB_CDC */
/*---------------------------------------------------------------------------*/
static int (*usart1_input_handler) (unsigned char c) = NULL;
static int (*usart2_input_handler) (unsigned char c) = NULL;
static int (*usart3_input_handler) (unsigned char c) = NULL;
static int (*usb_input_handler) (unsigned char c) = NULL;

static void (*usart1_idle_handler) (void) = NULL;
static void (*usart2_idle_handler) (void) = NULL;
static void (*usart3_idle_handler) (void) = NULL;

#define USARTx_USART1       (1 << 0)
#define USARTx_USART2       (1 << 1)
#define USARTx_USART3       (1 << 2)
#define USARTx_USB_CDC      (1 << 3)

static volatile uint8_t rx_in_progress = 0;
static volatile uint8_t tx_in_progress = 0;
/*---------------------------------------------------------------------------*/
extern bool Virtual_ComPort_IsOpen(void);
/*---------------------------------------------------------------------------*/
void
usart_init(USART_TypeDef* USARTx,
           uint32_t baudrate, uint16_t wordLength, uint16_t stopbits,
           uint16_t parity, uint16_t mode, uint16_t hwFlowCtrl)
{
#if defined(USE_USB_CDC)
  Set_System();
  Set_USBClock();
  USB_Interrupts_Config();
  USB_Init();
#else
  NVIC_InitTypeDef NVIC_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  USART_InitTypeDef USART_InitStructure;

  /* USART conf */
  USART_InitStructure.USART_BaudRate            = baudrate;
  USART_InitStructure.USART_WordLength          = wordLength;
  USART_InitStructure.USART_StopBits            = stopbits;
  USART_InitStructure.USART_Parity              = parity;
  USART_InitStructure.USART_Mode                = mode;
  USART_InitStructure.USART_HardwareFlowControl = hwFlowCtrl;

  if(USARTx == USART1)
  {
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

    NVIC_InitStructure.NVIC_IRQChannel                    = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 0x0F;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 0x0F;
    NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* GPIO pin configuration */
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    USART_Init(USART1, &USART_InitStructure);

    /* Alternate function conf */
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_USART1);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_USART1);

    /* Enable RXNE interrupt */
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

    /* Enable USART1 global interrupt */
    NVIC_EnableIRQ(USART1_IRQn);

    USART_Cmd(USART1, ENABLE);
  }
  else if(USARTx == USART2)
  {
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

    NVIC_InitStructure.NVIC_IRQChannel                    = USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 0x0F;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 0x0F;
    NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* GPIO pin configuration */
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    USART_Init(USART2, &USART_InitStructure);

    /* Alternate function conf */
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

    /* Enable RXNE interrupt */
    USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

    /* Enable USART2 global interrupt */
    NVIC_EnableIRQ(USART2_IRQn);

    USART_Cmd(USART2, ENABLE);
  }
  else if(USARTx == USART3)
  {
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

    NVIC_InitStructure.NVIC_IRQChannel                    = USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 0x0F;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 0x0F;
    NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* GPIO pin configuration */
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10 | GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    USART_Init(USART3, &USART_InitStructure);

    /* Alternate function conf */
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART2);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART2);

    /* Enable RXNE interrupt */
    USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);

    /* Enable USART2 global interrupt */
    NVIC_EnableIRQ(USART3_IRQn);

    USART_Cmd(USART3, ENABLE);
  }
  else
  {
    while(1);
  }
#endif /* USE_USB_CDC */
}
/*---------------------------------------------------------------------------*/
void
usart_deinit(USART_TypeDef* USARTx)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;

  if(USARTx == USART1)
  {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, DISABLE);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    NVIC_DisableIRQ(USART1_IRQn);
  }
  else if(USARTx == USART2)
  {
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, DISABLE);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    NVIC_DisableIRQ(USART2_IRQn);
  }
  else if(USARTx == USART3)
  {
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, DISABLE);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10 | GPIO_Pin_11;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    NVIC_DisableIRQ(USART3_IRQn);
  }
  else
  {
    while(1);
  }
  USART_ITConfig(USARTx, USART_IT_RXNE, DISABLE);
  USART_Cmd(USARTx, DISABLE);
  USART_DeInit(USARTx);
}
/*---------------------------------------------------------------------------*/
#if defined(USE_USB_CDC)
void
Handle_USBAsynchXfer(void)
{
  uint16_t tx_ptr; // Pointer to first byte in USB buffer
  uint16_t tx_len; // Amount of data to be transfered

  if (USB_TX_ptr_out == USB_TX_BUF_SIZE) USB_TX_ptr_out = 0;
  if (USB_TX_ptr_out == USB_TX_ptr_in) return; // Nothing to send

  if (USB_TX_ptr_out > USB_TX_ptr_in) {
    // rollback
    USB_TX_len = USB_TX_BUF_SIZE - USB_TX_ptr_out;
  } else {
    USB_TX_len = USB_TX_ptr_in - USB_TX_ptr_out;
  }

  if (USB_TX_len > VIRTUAL_COM_PORT_DATA_SIZE)
  {
    tx_ptr = USB_TX_ptr_out;
    tx_len = VIRTUAL_COM_PORT_DATA_SIZE;
    USB_TX_ptr_out += VIRTUAL_COM_PORT_DATA_SIZE;
    USB_TX_len -= VIRTUAL_COM_PORT_DATA_SIZE;
  } else {
    tx_ptr = USB_TX_ptr_out;
    tx_len = USB_TX_len;
    USB_TX_ptr_out += USB_TX_len;
    USB_TX_len = 0;
  }
  tx_in_progress |= USARTx_USB_CDC;
  UserToPMABufferCopy(&USB_TX_BUF[tx_ptr],ENDP1_TXADDR,tx_len);
  SetEPTxCount(ENDP1,tx_len);
  SetEPTxValid(ENDP1);
  tx_in_progress &= ~USARTx_USB_CDC;
}
#endif /* USE_USB_CDC */
/*---------------------------------------------------------------------------*/
void
usart_write(USART_TypeDef* USARTx, char *buffer, uint32_t len)
{
  int i;
  if ( len == 0 ) return;
  for (i = 0 ; i < len ; i++) {
    usart_putc(USARTx, *buffer++ );
  }
#if defined(USE_USB_CDC) && defined(REDIRECT_STDIO_STRINGMODE)
  Handle_USBAsynchXfer();
#endif /* USE_USB_CDC && REDIRECT_STDIO_STRINGMODE */
}
/*---------------------------------------------------------------------------*/
void
usart_putc(USART_TypeDef* USARTx, unsigned char c)
{
#ifdef USE_USB_CDC
  // Put byte into data buffer
  USB_TX_BUF[USB_TX_ptr_in++] = c;

  // To avoid buffer overflow
  if (USB_TX_ptr_in >= USB_TX_BUF_SIZE) USB_TX_ptr_in = 0;

#ifndef REDIRECT_STDIO_STRINGMODE
  if(END_CHAR(c)) {
    Handle_USBAsynchXfer();
  }
#endif /* REDIRECT_STDIO_STRINGMODE */

#else /* USE_USB_CDC */
  tx_in_progress |= (USARTx == USART1) ? USARTx_USART1 : (USARTx == USART2) ? USARTx_USART2 : USARTx_USART3;
  USART_SendData(USARTx, (uint8_t) c);
  /* Loop until the end of transmission */
  while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);
  tx_in_progress &= ~((USARTx == USART1) ? USARTx_USART1 : (USARTx == USART2) ? USARTx_USART2 : USARTx_USART3);
#endif /* USE_USB_CDC */
}
/*---------------------------------------------------------------------------*/
void
usart_set_input(USART_TypeDef* USARTx, int (*input) (unsigned char c))
{
#ifdef USE_USB_CDC
  usb_input_handler = input;
#else /* USE_USB_CDC */
  if(USARTx == USART1)
  {
    usart1_input_handler = input;
  }
  else if(USARTx == USART2)
  {
    usart2_input_handler = input;
  }
  else if(USARTx == USART3)
  {
    usart3_input_handler = input;
  }
  else
  {
    while(1);
  }
#endif /* USE_USB_CDC */
}
/*---------------------------------------------------------------------------*/
void
usart_set_idle(USART_TypeDef* USARTx, void (*idle) (void))
{
  if(USARTx == USART1)
  {
    usart1_idle_handler = idle;
  }
  else if(USARTx == USART2)
  {
    usart2_idle_handler = idle;
  }
  else if(USARTx == USART3)
  {
    usart3_idle_handler = idle;
  }
  else
  {
    while(1);
  }
}
/*---------------------------------------------------------------------------*/
uint8_t
usart_active(void)
{
  return rx_in_progress | tx_in_progress | Virtual_ComPort_IsOpen();
}
/*---------------------------------------------------------------------------*/
#if defined(USE_USB_CDC)
void EP1_IN_Callback (void)
{
  Handle_USBAsynchXfer();
}
/*---------------------------------------------------------------------------*/
void EP3_OUT_Callback (void)
{
  rx_in_progress |= USARTx_USB_CDC;
  if (bDeviceState == CONFIGURED)
  {
    unsigned char x;
    uint32_t len = GetEPRxCount(ENDP3);
    PMAToUserBufferCopy((unsigned char*)&x, ENDP3_RXADDR, len);

    if(usb_input_handler != NULL) usb_input_handler(x);
    SetEPRxValid(ENDP3);
  }
  rx_in_progress &= ~USARTx_USB_CDC;
}

#else /* USE_USB_CDC */
/*---------------------------------------------------------------------------*/
void USART1_IRQHandler(void)
{
  rx_in_progress |= USARTx_USART1;
  if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
  {
    unsigned char x;

    x = USART_ReceiveData(USART1);
    USART_ClearITPendingBit(USART1, USART_IT_RXNE);

    /* Loop until the end of reception */
    while (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == SET);

    if(usart1_input_handler != NULL) usart1_input_handler(x);
  }
  if (USART_GetITStatus(USART1, USART_IT_IDLE) != RESET)
  {
    USART_ReceiveData(USART1);
    while (USART_GetFlagStatus(USART1, USART_FLAG_IDLE) == SET);

    if(usart1_idle_handler != NULL) usart1_idle_handler ();
  }
  if (USART_GetITStatus(USART1, USART_IT_ORE) != RESET)
  {
    printf("ORE 1\n");
    USART_ReceiveData(USART1);
    while (USART_GetFlagStatus(USART1, USART_IT_ORE) == SET);
  }
  rx_in_progress &= ~USARTx_USART1;
}
/*---------------------------------------------------------------------------*/
void USART2_IRQHandler(void)
{
  rx_in_progress |= USARTx_USART1;
  if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
  {
    unsigned char x;

    x = USART_ReceiveData(USART2);
    USART_ClearITPendingBit(USART2, USART_IT_RXNE);

    /* Loop until the end of reception */
    while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE) == SET);

    if(usart2_input_handler != NULL) usart2_input_handler(x);
  }
  if (USART_GetITStatus(USART2, USART_IT_IDLE) != RESET)
  {
    USART_ReceiveData(USART2);
    while (USART_GetFlagStatus(USART2, USART_FLAG_IDLE) == SET);

    if(usart2_idle_handler != NULL) usart2_idle_handler ();
  }
  if (USART_GetITStatus(USART2, USART_IT_ORE) != RESET)
  {
    USART_ReceiveData(USART2);
    while (USART_GetFlagStatus(USART2, USART_IT_ORE) == SET);
  }
  rx_in_progress &= ~USARTx_USART2;
}
/*---------------------------------------------------------------------------*/
void USART3_IRQHandler(void)
{
  rx_in_progress |= USARTx_USART1;
  if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
  {
    unsigned char x;

    x = USART_ReceiveData(USART3);
    USART_ClearITPendingBit(USART3, USART_IT_RXNE);

    /* Loop until the end of reception */
    while (USART_GetFlagStatus(USART3, USART_FLAG_RXNE) == SET);
    if(usart3_input_handler != NULL) usart3_input_handler(x);
  }
  if (USART_GetITStatus(USART3, USART_IT_IDLE) != RESET)
  {
    USART_ReceiveData(USART3);
    while (USART_GetFlagStatus(USART3, USART_FLAG_IDLE) == SET);

    if(usart3_idle_handler != NULL) usart3_idle_handler ();
  }
  if (USART_GetITStatus(USART3, USART_IT_ORE) != RESET)
  {
    USART_ReceiveData(USART3);
    while (USART_GetFlagStatus(USART3, USART_IT_ORE) == SET);
  }
  rx_in_progress &= ~USARTx_USART3;
}
#endif /* USE_USB_CDC */
/*---------------------------------------------------------------------------*/
