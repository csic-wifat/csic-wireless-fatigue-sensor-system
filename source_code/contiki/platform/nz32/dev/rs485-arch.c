/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup nz32-sc151-peripherals
 * @{
 *
 * \file
 * Driver for the RS485 transceiver module connected to nz32-sc151 platform
 */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include "contiki.h"

#include "stm32l151.h"
#include "usart.h"

#include "rs485.h"
/*---------------------------------------------------------------------------*/
/* USARTx configured as follow:
- BaudRate = 19200 baud
- Word Length = 8 Bits
- Two Stop Bits
- No parity
- Receive and transmit enabled
- Hardware flow control disabled (RTS and CTS signals)
*/
#define RS485_USARTx								USART1
#define RS485_MODE									USART_Mode_Rx | USART_Mode_Tx
#define RS485_HARDWAREFLOWCONTROL		USART_HardwareFlowControl_None

#define RS485_CS_GPIO_PIN						GPIO_Pin_8
#define RS485_CS_GPIO_PORT					GPIOA
#define RS485_CS_GPIO_CLK           RCC_AHBPeriph_GPIOA
/*---------------------------------------------------------------------------*/
/*!
 * \brief set the CS pin of the RS485 transceiver
 *
 * \param [IN] value : 1 or 0
 */
static void rs485_arch_set_cs(uint8_t value);
/*---------------------------------------------------------------------------*/
static void
rs485_arch_set_cs(uint8_t value)
{
	GPIO_WriteBit( RS485_CS_GPIO_PORT,
                 RS485_CS_GPIO_PIN,
                 (value == 1) ? Bit_SET:Bit_RESET);
}
/*---------------------------------------------------------------------------*/
rs485_result_t
rs485_arch_init(void (*c)(void), uint32_t baudrate, uint8_t bytesize,
								uint8_t stopbits, uint8_t parity)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	uint16_t wordsize, bits, par;

	/* Check baudrate */
	switch (baudrate)
	{
		case (2400):
		case (4800):
		case (9600):
		case (19200):
		case (38400):
		case (115200):
			break;
		default:
			return RS485_RESULT_ERROR;
	}

	/* Check word size */
	switch(bytesize)
	{
		case (0x00):
			wordsize = USART_WordLength_8b;
			break;
		case (0x01):
			wordsize = USART_WordLength_9b;
			break;
		default:
			return RS485_RESULT_ERROR;
	}

	/* Check stop bits */
	switch (stopbits)
	{
		case (0x00):
			bits = USART_StopBits_1;
			break;
		case (0x01):
			bits = USART_StopBits_0_5;
			break;
		case (0x02):
			bits = USART_StopBits_2;
			break;
		case (0x03):
			bits = USART_StopBits_1_5;
			break;
		default:
			return RS485_RESULT_ERROR;
	}

	/* Check parity */
	switch (parity)
	{
		case 0x00:
			par = USART_Parity_No;
			break;
		case 0x01:
			par = USART_Parity_Even;
			break;
		case 0x02:
			par = USART_Parity_Odd;
			break;
		default:
			return RS485_RESULT_ERROR;
	}

	/* Set usart input handler */
	usart_set_input(RS485_USARTx, rs485_input_byte);
	usart_set_idle(RS485_USARTx, c);

	/* Enable usart tx and rx pins */
	usart_init(RS485_USARTx, baudrate, wordsize,
    bits, par, RS485_MODE, RS485_HARDWAREFLOWCONTROL);

	/* Enable Idle Line Detected (IDLE) interrupt */
  USART_ITConfig(RS485_USARTx, USART_IT_IDLE, ENABLE);

  /* Disable RXNE interrupt */
  //USART_ITConfig(RS485_USARTx, USART_IT_RXNE, DISABLE);

	/*
	 * Enable gpio pin to control the cs (transmit enable) pin of the
	 * rs485 transceiver
	 */
	RCC_AHBPeriphClockCmd(RS485_CS_GPIO_CLK, ENABLE);

	GPIO_InitStructure.GPIO_Pin   = RS485_CS_GPIO_PIN;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_Init(RS485_CS_GPIO_PORT, &GPIO_InitStructure);

	/* Default mode: receive */
  rs485_arch_set_cs(0);

	return RS485_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
rs485_result_t
rs485_arch_deinit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Deinitialise usart */
	usart_deinit(RS485_USARTx);

	/* Deinitialise cs pin */
	GPIO_InitStructure.GPIO_Pin   = RS485_CS_GPIO_PIN;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
  GPIO_Init(RS485_CS_GPIO_PORT, &GPIO_InitStructure);

  /* Disable Idle Line Detected (IDLE) interrupt */
  USART_ITConfig(RS485_USARTx, USART_IT_IDLE, DISABLE);

	return RS485_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
rs485_result_t
rs485_arch_writeb(unsigned char ch)
{
	rs485_arch_set_cs(1);

	usart_putc(RS485_USARTx, ch);

	rs485_arch_set_cs(0);
	return RS485_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
rs485_result_t
rs485_arch_write(const unsigned char *buffer, int size)
{
  if ( size == 0 ) return RS485_RESULT_INVALID_VALUE;
  rs485_arch_set_cs(1);
	usart_write(RS485_USARTx, (char *)buffer, size + 1);
	rs485_arch_set_cs(0);
	return RS485_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
