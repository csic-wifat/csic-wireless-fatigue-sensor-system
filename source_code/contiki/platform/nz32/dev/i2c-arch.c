/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup stm32l1xx-i2c
 * @{
 *
 * Implementation of STM32L1xx low-power operation functionality
 *
 * @{
 *
 * \file
 * Driver for STM32L1xx low-power operation
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "stm32l151.h"
#include "i2c.h"
#include "platform-conf.h"
/*---------------------------------------------------------------------------*/
void
i2c_arch_init(I2C_TypeDef* I2Cx)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	// Enable GPIOB clocks
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

	// Configure I2C clock and GPIO
  GPIO_StructInit(&GPIO_InitStructure);

	if(I2Cx == I2C1)
	{
		/*!< I2C1 peripheral clock enable */
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);

		/* Configure I2C1 SCL and SDA pins */
		GPIO_InitStructure.GPIO_Pin 	= GPIO_Pin_6 | GPIO_Pin_7;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_AF;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
		GPIO_Init(GPIOB, &GPIO_InitStructure);

		// Connect I2C1 pins to AF
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_I2C1);  // SCL
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_I2C1);  // SDA

		/* I2C1 Reset */
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C1, ENABLE);
    RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C1, DISABLE);
	}
	else
	{
		/*!< I2C1 peripheral clock enable */
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);

		/* Configure I2C2 SCL and SDA pins */
		GPIO_InitStructure.GPIO_Pin 	= GPIO_Pin_13 | GPIO_Pin_14;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
		GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_AF;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
		GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
		GPIO_Init(GPIOB, &GPIO_InitStructure);

		// Connect I2C2 pins to AF
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_I2C2);  // SCL
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_I2C2);  // SDA

		/* I2C2 Reset */
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C2, ENABLE);
    RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C2, DISABLE);
	}
}
/*---------------------------------------------------------------------------*/
void
i2c_arch_deinit(I2C_TypeDef* I2Cx)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	if(I2Cx == I2C1)
	{
		/*!< I2C Periph clock disable */
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, DISABLE);
		/*!< Configure I2C pins: SCL */
	  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;

	}
	else
	{
		/*!< I2C Periph clock disable */
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, DISABLE);
		/*!< Configure I2C pins: SCL */
	  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14;
	}

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}
/*---------------------------------------------------------------------------*/
