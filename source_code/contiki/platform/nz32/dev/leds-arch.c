/*
 * Copyright (c) 2005, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include <stdint.h>
#include <stdio.h>
#include "dev/leds.h"
#include "contiki-conf.h"
#include "platform-conf.h"
#include "stm32l1xx.h"
/*---------------------------------------------------------------------------*/
void
leds_arch_init(void)
{
#if PLATFORM_HAS_LEDS
  GPIO_InitTypeDef GPIO_InitStructure;

  RCC_AHBPeriphClockCmd( LED1_GPIO_CLK, ENABLE );

  /* GPIO configuration */
  GPIO_InitStructure.GPIO_Pin   = LED1_PIN;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init( LED_GPIO_PORT, &GPIO_InitStructure );

  GPIO_WriteBit( LED_GPIO_PORT, LED1_PIN, Bit_RESET );
#endif /* PLATFORM_HAS_LEDS */
}
/*---------------------------------------------------------------------------*/
unsigned char
leds_arch_get(void)
{
#if PLATFORM_HAS_LEDS
  /* Create LED bit mask using bitwise OR’ing the seperate values */
  return (GPIO_ReadOutputDataBit(LED_GPIO_PORT, LED1_PIN) ? LEDS_RED : 0);
#else
  return 0;
#endif /* PLATFORM_HAS_LEDS */
}
/*---------------------------------------------------------------------------*/
void
leds_arch_set(unsigned char leds)
{
#if PLATFORM_HAS_LEDS
  /* Test the LED bit mask and set the LEDs accordingly */
  GPIO_WriteBit( LED_GPIO_PORT, LED1_PIN, (leds & LEDS_RED) ? Bit_SET : Bit_RESET);
#endif /* PLATFORM_HAS_LEDS */
}
/*---------------------------------------------------------------------------*/
