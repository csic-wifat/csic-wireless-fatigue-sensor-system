/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup
 * @{
 *
 *
 * \file
 * Header file for USART related definitions.
 */
/*---------------------------------------------------------------------------*/
#ifndef __USART_H__
#define __USART_H__

#include "stm32l1xx.h"
/*---------------------------------------------------------------------------*/
/* USART1 Function Declarations */
void usart_init(USART_TypeDef* USARTx,
           			uint32_t baudrate, uint16_t wordLength, uint16_t stopbits,
           			uint16_t parity, uint16_t mode, uint16_t hwFlowCtrl);
void usart_deinit(USART_TypeDef* USARTx);
void usart_putc(USART_TypeDef* USARTx, unsigned char c);
void usart_write(USART_TypeDef* USARTx, char *buffer, uint32_t len);
void usart_set_input(USART_TypeDef* USARTx, int (* input)(unsigned char c));
void usart_set_idle(USART_TypeDef* USARTx, void (*idle)(void));
uint8_t usart_active(void);
/*---------------------------------------------------------------------------*/
#endif /* __USART_H__ */
/*---------------------------------------------------------------------------*/
/** @} */
