/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/**
 * \addtogroup stm32l151-soc-adc
 * @{
 *
 * \file
 * Arch driver for the stm32l151 ADC
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "lib/sensors.h"
#include "dev/soc-adc.h"
#include "stm32l1xx.h"
/*---------------------------------------------------------------------------*/
#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"
/*---------------------------------------------------------------------------*/
#define SOC_ADC_DR_ADDRESS    ((uint32_t)0x40012458)
/*---------------------------------------------------------------------------*/
extern soc_adc_reg_map soc_adc_config;
/*---------------------------------------------------------------------------*/
#if SOC_ADC_WITH_DMA_TRANSFER
static void soc_adc_arch_dma_init(void);
#endif /* SOC_ADC_WITH_DMA_TRANSFER */
/*---------------------------------------------------------------------------*/
#if SOC_ADC_WITH_DMA_TRANSFER
static void
soc_adc_arch_dma_init(void)
{
  DMA_InitTypeDef DMA_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Enable DMA1 clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

  /* DMA1 channel configuration */
  // Reset DMA1 channel to default values;
  DMA_DeInit(SOC_ADC_DMA_CHANNEL);
  // Source and destination start addresses
  DMA_InitStructure.DMA_PeripheralBaseAddr  = SOC_ADC_DR_ADDRESS;
  DMA_InitStructure.DMA_MemoryBaseAddr      = (uint32_t)soc_adc_dma_values;
  // Location assigned to peripheral register will be source
  DMA_InitStructure.DMA_DIR                 = DMA_DIR_PeripheralSRC;
  // Chunk of data to be transfered
  DMA_InitStructure.DMA_BufferSize          = SOC_ADC_BUFFER_LENGTH;
  // Source address increment disable
  DMA_InitStructure.DMA_PeripheralInc       = DMA_PeripheralInc_Disable;
  // Automatic memory destination increment enable
  DMA_InitStructure.DMA_MemoryInc           = DMA_MemoryInc_Enable;
  // Source and destination data size word=32bit
  DMA_InitStructure.DMA_PeripheralDataSize  = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize      = DMA_MemoryDataSize_HalfWord;
  // Setting circular mode
  DMA_InitStructure.DMA_Mode                = DMA_Mode_Circular;
  // Medium priority
  DMA_InitStructure.DMA_Priority            = DMA_Priority_High;
  // Channel will be used for memory to memory transfer
  DMA_InitStructure.DMA_M2M                 = DMA_M2M_Disable;
  DMA_Init(SOC_ADC_DMA_CHANNEL, &DMA_InitStructure);

  // Enable DMA1 Channel Transfer Complete interrupt
  DMA_ITConfig(DMA1_Channel1, DMA_IT_TC, ENABLE);

  //Enable DMA1 channel IRQ Channel */
  NVIC_InitStructure.NVIC_IRQChannel = SOC_ADC_DMA_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  /* Enable DMA1 channel transfer*/
  DMA_Cmd(SOC_ADC_DMA_CHANNEL, ENABLE);
}
#endif /* SOC_ADC_WITH_DMA_TRANSFER */
/*---------------------------------------------------------------------------*/
void
soc_adc_arch_init(void)
{
	ADC_InitTypeDef ADC_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

#if SOC_ADC_WITH_DMA_TRANSFER
  soc_adc_arch_dma_init();
#endif /* SOC_ADC_WITH_DMA_TRANSFER */

  /* Enable the HSI */
  RCC_HSICmd(ENABLE);

  /* Check that HSI oscillator is ready */
  while(RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET);

  /* Enable ADC1 clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

  /* ADC1 configuration */
  ADC_StructInit(&ADC_InitStructure);
  ADC_InitStructure.ADC_Resolution            = soc_adc_config.Resolution;
  ADC_InitStructure.ADC_ScanConvMode          = soc_adc_config.ScanConvMode;
  ADC_InitStructure.ADC_ContinuousConvMode    = soc_adc_config.ContinuousConvMode;
  ADC_InitStructure.ADC_ExternalTrigConvEdge  = soc_adc_config.ExternalTrigConvEdge;
  ADC_InitStructure.ADC_ExternalTrigConv      = soc_adc_config.ExternalTrigConv;
  ADC_InitStructure.ADC_DataAlign             = soc_adc_config.DataAlign;
  ADC_InitStructure.ADC_NbrOfConversion       = soc_adc_config.NbrOfConversion;
  ADC_Init(ADC1, &ADC_InitStructure);

  uint8_t ADC_ch, num_adc_ch = 0;
  uint32_t adc_channel_t = 0;

  PRINTF("Scan Conv mode %s\n", soc_adc_config.ScanConvMode ? "ENABLED":"DISABLED");
  PRINTF("Cont Conv mode %s\n", soc_adc_config.ContinuousConvMode ? "ENABLED":"DISABLED");
  PRINTF("Nbr Of Conversion %lu\n", soc_adc_config.NbrOfConversion);

  for(ADC_ch = 0 ; ADC_ch <= ADC_Channel_31 ; ADC_ch++)
  {
    adc_channel_t = (soc_adc_config.Channels & (1 << ADC_ch));
    if(adc_channel_t)
    {
      num_adc_ch++;

      /* ADC1 regular channel configuration */
      ADC_RegularChannelConfig(ADC1, ADC_ch, num_adc_ch, soc_adc_config.SampleTime);
      PRINTF("Configuring ADC1 channel 0x%02x\n", ADC_ch);
    }
  }
  PRINTF("Nb of adc channels: %u (0x%04lx)\n", num_adc_ch, soc_adc_config.Channels);

#if SOC_ADC_WITH_DMA_TRANSFER

  /* Enable the request after last transfer for DMA Circular mode */
  ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);

  /* Enable ADC1 DMA */
  ADC_DMACmd(ADC1, ENABLE);

#else /* SOC_ADC_WITH_DMA_TRANSFER */

  /* Define delay between ADC1 conversions */
  ADC_DelaySelectionConfig(ADC1, ADC_DelayLength_Freeze);

  /* Enable ADC1 power down during idle and delay phases */
  ADC_PowerDownCmd(ADC1, ADC_PowerDown_Idle_Delay, ENABLE);

#endif /* SOC_ADC_WITH_DMA_TRANSFER */
}
/*---------------------------------------------------------------------------*/
void
soc_adc_arch_deinit(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

#if SOC_ADC_WITH_DMA_TRANSFER
  /* Disable ADC1 DMA */
  ADC_DMACmd(ADC1, DISABLE);

  /* Disable DMA1 channel */
  DMA_Cmd(SOC_ADC_DMA_CHANNEL, DISABLE);
#endif /* SOC_ADC_WITH_DMA_TRANSFER */

  /* Disable ADC1 IRQ */
  NVIC_InitStructure.NVIC_IRQChannel = ADC1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
  NVIC_Init(&NVIC_InitStructure);

	/* Disable ADC1 */
  ADC_Cmd(ADC1, DISABLE);

	/* Disable the HSI */
  RCC_HSICmd(DISABLE);

  /* Disable ADC1 clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, DISABLE);

  if(is_active) {
    is_active = 0;
  }
}
/*---------------------------------------------------------------------------*/
