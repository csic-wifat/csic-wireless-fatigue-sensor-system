/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup stm32l1xx-lpm
 * @{
 *
 * Implementation of STM32L1xx low-power operation functionality
 *
 * @{
 *
 * \file
 * Driver for STM32L1xx low-power operation
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "lpm.h"
#include "dev/soc-rtc.h"
#include "stm32l151.h"
#include "platform-conf.h"
#include "dev/leds.h"

#include <stdio.h>
#include <stdlib.h>
/*---------------------------------------------------------------------------*/
static int lpm_active = 0;
/*---------------------------------------------------------------------------*/
static void lpm_config_pre_lpsleep_mode(void);
static void lpm_exit_lpsleep_mode(void);
/*---------------------------------------------------------------------------*/
static void lpm_config_pre_stop_mode(void);
static void lpm_exit_stop_mode(void);
/*---------------------------------------------------------------------------*/
static void lpm_config_pre_standby_mode(void);
static void lpm_exit_standby_mode(void);
/*---------------------------------------------------------------------------*/
static void
lpm_config_pre_lpsleep_mode(void)
{
  /*!
   * ToDo: TIM2 slows down when this mode is activated
   *       Investigate why
   */

  /* Keep TIM2 (rtimer source clock) running */
  RCC_APB1PeriphClockCmd(RCC_APB1LPENR_TIM2LPEN, ENABLE);

  /* Disable the Power Voltage Detector */
  PWR_PVDCmd(DISABLE);

  /* Set MCU in ULP (Ultra Low Power) */
  PWR_UltraLowPowerCmd(ENABLE);

  /* Enable the power down mode during sleep mode */
  FLASH_SLEEPPowerDownCmd(ENABLE);
}
/*---------------------------------------------------------------------------*/
static void
lpm_exit_lpsleep_mode(void)
{
  if( !lpm_active ){
    return;
  }
  RCC_APB1PeriphClockCmd(RCC_APB1LPENR_TIM2LPEN, DISABLE);

  /* Disable ULP (Ultra Low Power) */
  PWR_UltraLowPowerCmd(DISABLE); // add up to 3ms wakeup time

  /* Disable the power down mode */
  FLASH_SLEEPPowerDownCmd(DISABLE);

  /* Enable the Power Voltage Detector */
  PWR_PVDCmd(ENABLE);
}
/*---------------------------------------------------------------------------*/
static void
lpm_config_pre_stop_mode(void)
{
  /* Turn off LSE and HSE clocks */
  cpu_deinit();

  /* Disable the Power Voltage Detector */
  PWR_PVDCmd(DISABLE);

  /* Clear Wakeup flag */
  PWR->CR |= PWR_CR_CWUF;

  /* Set MCU in ULP (Ultra Low Power) */
  PWR_UltraLowPowerCmd(ENABLE);

  /* Clear PWR WakeUp flag */
  PWR_ClearFlag(PWR_FLAG_WU);

  /* Clear RTC WakeUp (WUTF) flag */
  RTC_ClearFlag(RTC_FLAG_WUTF);

  /* Disable Wakeup Counter */
  RTC_WakeUpCmd(DISABLE);

  /* Enable the fast wake up from Ultra low power mode */
  PWR_FastWakeUpCmd(ENABLE);
}
/*---------------------------------------------------------------------------*/
static void
lpm_exit_stop_mode(void)
{
extern void SetSysClock( void );

  if( !lpm_active ){
    return;
  }

  /*
   * The STOP low power mode has been exited.
   */
  dint();

  if( PWR_GetFlagStatus(PWR_FLAG_WU) != RESET )
  {
    PWR_ClearFlag(PWR_FLAG_WU);
  }
  else
  {
    non_scheduled_wakeup = 1;
  }

  /* Reconfigure the system clocks */
  SetSysClock();
  soc_rtc_init();

  /* Clear PWR WakeUp flag */
  PWR_ClearFlag(PWR_FLAG_WU);

  /* Clear RTC WakeUp (WUTF) flag */
  RTC_ClearFlag(RTC_FLAG_WUTF);

  /* Disable Wakeup Counter */
  RTC_WakeUpCmd(ENABLE);

  /* Disable ULP (Ultra Low Power) */
  PWR_UltraLowPowerCmd(DISABLE); // add up to 3ms wakeup time

  /* Enable the Power Voltage Detector */
  PWR_PVDCmd(ENABLE);

  /* Set internal voltage regulator */
  PWR_VoltageScalingConfig( PWR_VoltageScaling_Range1 );

  eint();
}
/*---------------------------------------------------------------------------*/
void
lpm_arch_deepsleep(void)
{
#if ENABLE_DEEPSLEEP == 1
  /* Turn off LSE and HSE clocks */
  cpu_deinit();

  /* Disable the Power Voltage Detector */
  PWR_PVDCmd(DISABLE);

  /* Enable Ultra low power mode */
  PWR_UltraLowPowerCmd(ENABLE);

  /* Clear PWR WakeUp flag */
  PWR_ClearFlag(PWR_FLAG_WU);

  /* Clear RTC WakeUp (WUTF) flag */
  RTC_ClearFlag(RTC_FLAG_WUTF);

  /* Disable Wakeup Counter */
  RTC_WakeUpCmd(DISABLE);

  /* Request to enter STANDBY mode */
  PWR_EnterSTANDBYMode();
#endif /* ENABLE_DEEPSLEEP */
}
/*---------------------------------------------------------------------------*/
void
lpm_arch_init(void)
{
  static uint8_t lpm_init;

  if(!lpm_init)
  {
#if USE_LPM_STOP_MODE
    soc_rtc_init();
    soc_rtc_alarm_init(LPM_RTC_ALARM, 1);
#endif /* USE_LPM_STOP_MODE */
    lpm_init = 1;
  }
}
/*---------------------------------------------------------------------------*/
void
lpm_arch_enter(void)
{
#if LPM_USE_LP
  lpm_active = 1;
#if USE_LPM_STOP_MODE
  lpm_config_pre_stop_mode();

  /* Enter STOP mode with regulator in low power mode */
  PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);
#else /* USE_LPM_STOP_MODE */
  lpm_config_pre_lpsleep_mode();
  /* Enter SLEEP mode with regulator in low power mode */
  PWR_EnterSleepMode(PWR_Regulator_LowPower, PWR_SLEEPEntry_WFI);
#endif /* USE_LPM_STOP_MODE */

#endif /* LPM_USE_LP */
}
/*---------------------------------------------------------------------------*/
void
lpm_arch_wakeup(void)
{
#if LPM_USE_LP
#if USE_LPM_STOP_MODE
  lpm_exit_stop_mode();
#else /* USE_LPM_STOP_MODE */
  lpm_exit_lpsleep_mode();
#endif /* USE_LPM_STOP_MODE */
  lpm_active = 0;
#endif /* LPM_USE_LP */
}
/*---------------------------------------------------------------------------*/
