/*
 * Copyright (c) 2016, Benoît Thébaudeau <benoit@wsystem.com>
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/**
 * \addtogroup mmc-arch
 * @{
 *
 * \file
 * Implementation of the SD/MMC device driver NZ32-specific definitions.
 */

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include "stm32l1xx.h"
#include "mmc-arch.h"
#include "platform-conf.h"

/* card-present socket-switch */
#ifndef SOCKET_CD_CONNECTED
#define SOCKET_CD_CONNECTED      				0
#endif  /* SOCKET_CD_CONNECTED */

/* Clock frequency in card identification mode: fOD <= 400 kHz */
#define CLOCK_FREQ_CARD_ID_MODE         400000
/*
 * Clock frequency in data transfer mode: fPP <= 20 MHz, limited by the
 * backward-compatible MMC interface timings
 */
#define CLOCK_FREQ_DATA_XFER_MODE       20000000

/* Card-Select Controls  (Platform dependent) */
#define SELECT()    GPIO_ResetBits(USD_SPI_NSS_GPIO_PORT, USD_SPI_NSS_PIN)    /* MMC CS = L */
#define DESELECT()  GPIO_SetBits(USD_SPI_NSS_GPIO_PORT, USD_SPI_NSS_PIN)      /* MMC CS = H */
/*----------------------------------------------------------------------------*/
static void
mmc_arch_io_init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

#if SOCKET_CD_CONNECTED
	RCC_AHBPeriphClockCmd(USD_CD_GPIO_CLK, ENABLE);

  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd 	= GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;

	GPIO_InitStructure.GPIO_Pin   = USD_CD_PIN;
	GPIO_Init(USD_CD_GPIO_PORT, &GPIO_InitStructure);
#endif /* SOCKET_CD_CONNECTED */
}
/*----------------------------------------------------------------------------*/
static void
mmc_arch_spi_init(void)
{
	SPI_InitTypeDef SPI_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;

	/* Enable peripheral clocks */
  RCC_AHBPeriphClockCmd( USD_SPI_NSS_GPIO_CLK | USD_SPI_MISO_GPIO_CLK | USD_SPI_MOSI_GPIO_CLK | USD_SPI_CLK_GPIO_CLK, ENABLE );
  USD_SPI_CLK_CMD( USD_SPI_CLK, ENABLE );

  //GPIO_StructInit(&GPIO_InitStructure);

	/*!< Configure SPI pins: NSS */
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStructure.GPIO_Pin 	= USD_SPI_NSS_PIN;
  GPIO_Init( USD_SPI_NSS_GPIO_PORT, &GPIO_InitStructure );

  /* De-select the Card: Chip Select high */
  DESELECT();

  /*!< Configure SPI pins: MISO, MOSI and SCK */
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;

  GPIO_InitStructure.GPIO_Pin   = USD_SPI_CLK_PIN;
  GPIO_Init(USD_SPI_CLK_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin   = USD_SPI_MISO_PIN;
  GPIO_Init(USD_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin   = USD_SPI_MOSI_PIN;
  GPIO_Init(USD_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

  /* Configure alternate functions */
  /* SPI CLK */
  GPIO_PinAFConfig(USD_SPI_CLK_GPIO_PORT, USD_SPI_CLK_AF_PIN, GPIO_AF_SPI3);
  /* SPI MISO */
  GPIO_PinAFConfig(USD_SPI_MISO_GPIO_PORT, USD_SPI_MISO_AF_PIN, GPIO_AF_SPI3);
  /* SPI MOSI */
  GPIO_PinAFConfig(USD_SPI_MOSI_GPIO_PORT, USD_SPI_MOSI_AF_PIN, GPIO_AF_SPI3);

  /* SPI configuration */
  SPI_InitStructure.SPI_Direction 				= SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode							= SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize 					= SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL 							= SPI_CPOL_High; //SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA 							= SPI_CPHA_2Edge; // SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_NSS 							= SPI_NSS_Soft;
	/*
	 * 32000kHz/2=16000kHz
	 * 32000kHz/4=8000kHz
	 * 32000kHz/256=125kHz < 400kHz
	 */
	//SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
	SPI_InitStructure.SPI_FirstBit 					= SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial 		= 7;
	SPI_Init(USD_SPI, &SPI_InitStructure);

	//SPI_CalculateCRC(USD_SPI, DISABLE);
	SPI_Cmd(USD_SPI, ENABLE);
}
/*----------------------------------------------------------------------------*/
static void
mmc_arch_init(void)
{
	static uint8_t init_done;
	volatile uint8_t dummyread;

  if(init_done) {
    return;
  }

  mmc_arch_io_init();
  mmc_arch_spi_init();

	/* drain SPI */
	while (SPI_I2S_GetFlagStatus(USD_SPI, SPI_I2S_FLAG_TXE) == RESET);
	dummyread = SPI_I2S_ReceiveData(USD_SPI);

  init_done = 1;
}
/*----------------------------------------------------------------------------*/
bool
mmc_arch_get_cd(uint8_t dev)
{
  mmc_arch_init();

#if SOCKET_CD_CONNECTED
  return GPIO_ReadInputDataBit(USD_CD_GPIO_PORT, USD_CD_PIN) ? true : false;
#else
  return true;
#endif /* SOCKET_CD_CONNECTED */
}
/*----------------------------------------------------------------------------*/
bool
mmc_arch_get_wp(uint8_t dev)
{
  return false;
}
/*----------------------------------------------------------------------------*/
void
mmc_arch_spi_select(uint8_t dev, bool sel)
{
  if(sel) {
    SELECT();
  } else {
    DESELECT();
  }
}
/*----------------------------------------------------------------------------*/
void
mmc_arch_spi_set_clock_freq(uint8_t dev, uint32_t freq)
{
	unsigned long tmp = USD_SPI->CR1;
	if ( freq == CLOCK_FREQ_CARD_ID_MODE ) {
		/* Set slow clock (100k-400k) */
		tmp = ( tmp | SPI_BaudRatePrescaler_64 );
	} else {
		/* Set fast clock (depends on the CSD) */
		tmp = ( tmp & ~SPI_BaudRatePrescaler_64 ) | USD_SPI_BAUDRATE_PREESCALER;
	}
	USD_SPI->CR1 = tmp;
}
/*----------------------------------------------------------------------------*/
void
mmc_arch_spi_xfer(uint8_t dev, const void *tx_buf, size_t tx_cnt,
                  void *rx_buf, size_t rx_cnt)
{

  const uint8_t *tx_buf_u8 = tx_buf;
  uint8_t *rx_buf_u8 = rx_buf;

  while(tx_cnt || rx_cnt) {
  	/* Loop while DR register in not empty */
  	while (SPI_I2S_GetFlagStatus(USD_SPI, SPI_I2S_FLAG_TXE) == RESET);
  	/* Send byte through the SPI peripheral */
  	if(tx_cnt) {
			SPI_I2S_SendData(USD_SPI, *tx_buf_u8++);
			tx_cnt--;
  	} else {
  		SPI_I2S_SendData(USD_SPI, 0xff);
  	}
  	/* Wait to receive a byte */
		while (SPI_I2S_GetFlagStatus(USD_SPI, SPI_I2S_FLAG_RXNE) == RESET);
		/* Receive data from the SPI bus */
		if(rx_cnt) {
			*rx_buf_u8++ = SPI_I2S_ReceiveData(USD_SPI);
			rx_cnt--;
		} else {
			SPI_I2S_ReceiveData(USD_SPI);
		}
  }
}
/*----------------------------------------------------------------------------*/

/** @} */
