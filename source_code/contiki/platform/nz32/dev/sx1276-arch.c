/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "sys/clock.h"

#include "stm32l151.h"
#include "sx1276-arch.h"

#include "platform-conf.h"
/*---------------------------------------------------------------------------*/
#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"
/*---------------------------------------------------------------------------*/
#define BOARD_TCXO_WAKEUP_TIME                      0
/*---------------------------------------------------------------------------*/
static uint8_t spi_io_init_done = 0;
/*---------------------------------------------------------------------------*/
/*!
 * Flag used to set the RF switch control pins in low power mode when the radio is not active.
 */
static bool RadioIsActive = false;
/*---------------------------------------------------------------------------*/
void
SX1276IoInit( void )
{
  GPIO_InitTypeDef GPIO_InitStructure;

  RCC_AHBPeriphClockCmd(DIOx_GPIO_PORT_CLK, ENABLE );

  /*!< Configure SPI NSS pin as output*/
  GPIO_InitStructure.GPIO_Pin   = SX127x_SPI_NSS_PIN;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_Init( SX127x_SPI_NSS_GPIO_PORT, &GPIO_InitStructure );
  GPIO_WriteBit( SX127x_SPI_NSS_GPIO_PORT, SX127x_SPI_NSS_PIN, Bit_SET );

  /*!< Configure VS pin as output */
  GPIO_InitStructure.GPIO_Pin = SX127x_VS_GPIO_PIN;
  GPIO_Init(SX127x_VS_GPIO_PORT , &GPIO_InitStructure );
  GPIO_WriteBit(SX127x_VS_GPIO_PORT, SX127x_VS_GPIO_PIN, Bit_RESET);

  /*!< Configure radio DIOs as inputs */
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;

  GPIO_InitStructure.GPIO_Pin   =  DIO0_PIN;
  GPIO_Init( DIO0_GPIO_PORT, &GPIO_InitStructure );

  GPIO_InitStructure.GPIO_Pin   =  DIO1_PIN;
  GPIO_Init( DIO1_GPIO_PORT, &GPIO_InitStructure );

  GPIO_InitStructure.GPIO_Pin   =  DIO2_PIN;
  GPIO_Init( DIO2_GPIO_PORT, &GPIO_InitStructure );

  GPIO_InitStructure.GPIO_Pin   =  DIO3_PIN;
  GPIO_Init( DIO3_GPIO_PORT, &GPIO_InitStructure );

  SX1276.DIOx = 0x0F;  /* DIO0-3 connected. DIO4 and DIO5 disconnected */

  SX1276SetPower( 1 );
}
/*---------------------------------------------------------------------------*/
void
SX1276IoIrqInit( DioIrqHandler **irqHandlers )
{
  ExtiIRQ[DIO0_EXTI_PIN_SOURCE] = irqHandlers[0];
  ExtiIRQ[DIO1_EXTI_PIN_SOURCE] = irqHandlers[1];
  ExtiIRQ[DIO2_EXTI_PIN_SOURCE] = irqHandlers[2];
  ExtiIRQ[DIO3_EXTI_PIN_SOURCE] = irqHandlers[3];
}
/*---------------------------------------------------------------------------*/
void
SX1276IoDeInit( void )
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /*!< Configure SPI NSS pin as output*/
  GPIO_InitStructure.GPIO_Pin   = SX127x_SPI_NSS_PIN;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_Init( SX127x_SPI_NSS_GPIO_PORT, &GPIO_InitStructure );
  GPIO_WriteBit( SX127x_SPI_NSS_GPIO_PORT, SX127x_SPI_NSS_PIN, Bit_SET );

  /*!< Configure radio DIOs as inputs */
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;

  GPIO_InitStructure.GPIO_Pin   =  DIO0_PIN;
  GPIO_Init( DIO0_GPIO_PORT, &GPIO_InitStructure );
  GPIO_WriteBit( DIO0_GPIO_PORT, DIO0_PIN, Bit_RESET );

  GPIO_InitStructure.GPIO_Pin   =  DIO1_PIN;
  GPIO_Init( DIO1_GPIO_PORT, &GPIO_InitStructure );
  GPIO_WriteBit( DIO1_GPIO_PORT, DIO1_PIN, Bit_RESET );

  GPIO_InitStructure.GPIO_Pin   =  DIO2_PIN;
  GPIO_Init( DIO2_GPIO_PORT, &GPIO_InitStructure );
  GPIO_WriteBit( DIO2_GPIO_PORT, DIO2_PIN, Bit_RESET );

  GPIO_InitStructure.GPIO_Pin   =  DIO3_PIN;
  GPIO_Init( DIO3_GPIO_PORT, &GPIO_InitStructure );
  GPIO_WriteBit( DIO3_GPIO_PORT, DIO3_PIN, Bit_RESET );
}
/*---------------------------------------------------------------------------*/
void
SX1276IoIrqDisable( void )
{
  NVIC_InitTypeDef NVIC_InitStructure;
  EXTI_InitTypeDef EXTI_InitStructure;

  NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
  EXTI_InitStructure.EXTI_LineCmd       = DISABLE;

  EXTI_InitStructure.EXTI_Line          = DIO0_EXTI_LINE;
  EXTI_Init(&EXTI_InitStructure);
  NVIC_InitStructure.NVIC_IRQChannel    = DIO0_EXTI_IRQn;
  NVIC_Init(&NVIC_InitStructure);
  EXTI_InitStructure.EXTI_Line          = DIO1_EXTI_LINE;
  EXTI_Init(&EXTI_InitStructure);
  NVIC_InitStructure.NVIC_IRQChannel    = DIO1_EXTI_IRQn;
  NVIC_Init(&NVIC_InitStructure);
  EXTI_InitStructure.EXTI_Line          = DIO2_EXTI_LINE;
  EXTI_Init(&EXTI_InitStructure);
  NVIC_InitStructure.NVIC_IRQChannel    = DIO2_EXTI_IRQn;
  NVIC_Init(&NVIC_InitStructure);
  EXTI_InitStructure.EXTI_Line          = DIO3_EXTI_LINE;
  EXTI_Init(&EXTI_InitStructure);
  NVIC_InitStructure.NVIC_IRQChannel    = DIO3_EXTI_IRQn;
  NVIC_Init(&NVIC_InitStructure);
}
/*---------------------------------------------------------------------------*/
void
SX1276IoIrqEnable( void )
{
  NVIC_InitTypeDef NVIC_InitStructure;
  EXTI_InitTypeDef EXTI_InitStructure;

  /*!< Clear All interrupt pending bits */
  EXTI_ClearITPendingBit(DIO0_EXTI_LINE);
  EXTI_ClearITPendingBit(DIO1_EXTI_LINE);
  EXTI_ClearITPendingBit(DIO2_EXTI_LINE);
  EXTI_ClearITPendingBit(DIO3_EXTI_LINE);

  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;

  EXTI_InitStructure.EXTI_Mode        = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger     = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd     = ENABLE;

  SYSCFG_EXTILineConfig(DIO0_EXTI_PORT_SOURCE, DIO0_EXTI_PIN_SOURCE);
  EXTI_ClearFlag(DIO0_EXTI_LINE);
  EXTI_InitStructure.EXTI_Line        = DIO0_EXTI_LINE;
  EXTI_Init(&EXTI_InitStructure);
  NVIC_InitStructure.NVIC_IRQChannel  = DIO0_EXTI_IRQn;
  NVIC_Init(&NVIC_InitStructure);

  SYSCFG_EXTILineConfig(DIO1_EXTI_PORT_SOURCE, DIO1_EXTI_PIN_SOURCE);
  EXTI_ClearFlag(DIO1_EXTI_LINE);
  EXTI_InitStructure.EXTI_Line        = DIO1_EXTI_LINE;
  EXTI_Init(&EXTI_InitStructure);
  NVIC_InitStructure.NVIC_IRQChannel  = DIO1_EXTI_IRQn;
  NVIC_Init(&NVIC_InitStructure);

  SYSCFG_EXTILineConfig(DIO2_EXTI_PORT_SOURCE, DIO2_EXTI_PIN_SOURCE);
  EXTI_ClearFlag(DIO2_EXTI_LINE);
  EXTI_InitStructure.EXTI_Line        = DIO2_EXTI_LINE;
  EXTI_Init(&EXTI_InitStructure);
  NVIC_InitStructure.NVIC_IRQChannel  = DIO2_EXTI_IRQn;
  NVIC_Init(&NVIC_InitStructure);

  SYSCFG_EXTILineConfig(DIO3_EXTI_PORT_SOURCE, DIO3_EXTI_PIN_SOURCE);
  EXTI_ClearFlag(DIO3_EXTI_LINE);
  EXTI_InitStructure.EXTI_Line        = DIO3_EXTI_LINE;
  EXTI_Init(&EXTI_InitStructure);
  NVIC_InitStructure.NVIC_IRQChannel  = DIO3_EXTI_IRQn;
  NVIC_Init(&NVIC_InitStructure);
}
/*---------------------------------------------------------------------------*/
void
SX1276SetReset( uint8_t state )
{
  GPIO_InitTypeDef GPIO_InitStructure;

  GPIO_InitStructure.GPIO_Pin   = RESET_PIN;

  if( state == RADIO_RESET_ON )
  {
    /*!< Configure RESET as output */
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
    GPIO_Init( RESET_GPIO_PORT, &GPIO_InitStructure );

    /*!< Set RESET pin to 1 */
    GPIO_WriteBit( RESET_GPIO_PORT, RESET_PIN, Bit_SET );
  }
  else
  {
    /*!< Configure RESET as input */
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
    GPIO_Init( RESET_GPIO_PORT, &GPIO_InitStructure );
  }
}
/*---------------------------------------------------------------------------*/
void
SX1276SpiInit( void )
{
  SPI_InitTypeDef SPI_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;

  /* Enable peripheral clocks */
  /* Enable SPIy clock and GPIO clock for SPIy */
  RCC_AHBPeriphClockCmd( SX127x_SPI_GPIO_PORT_CLK, ENABLE );
  if( SX127x_SPI_INTERFACE == SPI1)
  {
    RCC_APB2PeriphClockCmd( SX127x_SPI_CLK, ENABLE );
  }
  else
  {
    RCC_APB1PeriphClockCmd( SX127x_SPI_CLK, ENABLE );
  }

  GPIO_StructInit(&GPIO_InitStructure);

  /*!< Configure SPI pins: MISO, MOSI and SCK */
  GPIO_InitStructure.GPIO_Pin   = SX127x_SPI_SCK_PIN;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(SX127x_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

  /*!< Configure SPI pins: MISO */
  GPIO_InitStructure.GPIO_Pin = SX127x_SPI_MISO_PIN;
  GPIO_Init(SX127x_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

  /*!< Configure SPI pins: MOSI */
  GPIO_InitStructure.GPIO_Pin = SX127x_SPI_MOSI_PIN;
  GPIO_Init(SX127x_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

  /* Connect SPI SCK */
  GPIO_PinAFConfig(SX127x_SPI_SCK_GPIO_PORT, SX127x_SPI_SCK_AF_PIN, SX127x_SPI_AF);

  /* Connect SPI MISO */
  GPIO_PinAFConfig(SX127x_SPI_MISO_GPIO_PORT, SX127x_SPI_MISO_AF_PIN, SX127x_SPI_AF);

  /* Connect SPI MOSI */
  GPIO_PinAFConfig(SX127x_SPI_MOSI_GPIO_PORT, SX127x_SPI_MOSI_AF_PIN, SX127x_SPI_AF);

  /* SX127x_SPI_INTERFACE Config */
  SPI_InitStructure.SPI_Direction         = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode              = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize          = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL              = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA              = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS               = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;    // 8 MHz
  SPI_InitStructure.SPI_FirstBit          = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial     = 7;
  SPI_Init( SX127x_SPI_INTERFACE, &SPI_InitStructure );
  SPI_Cmd( SX127x_SPI_INTERFACE, ENABLE );

  spi_io_init_done = 1;
}
/*---------------------------------------------------------------------------*/
void
SX1276SpiDeInit( void )
{
  GPIO_InitTypeDef GPIO_InitStructure;

  if(spi_io_init_done)
  {
    GPIO_StructInit(&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;

    /*!< Configure SPI pins: Configure SCK */
    GPIO_InitStructure.GPIO_Pin   = SX127x_SPI_SCK_PIN;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_Init(SX127x_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);
    GPIO_WriteBit( SX127x_SPI_SCK_GPIO_PORT, SX127x_SPI_SCK_PIN,  Bit_RESET);

    /*!< Configure SPI pins: MISO */
    GPIO_InitStructure.GPIO_Pin   = SX127x_SPI_MISO_PIN;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
    GPIO_Init(SX127x_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);
    GPIO_WriteBit( SX127x_SPI_MISO_GPIO_PORT, SX127x_SPI_MISO_PIN,  Bit_RESET);

    /*!< Configure SPI pins: MOSI */
    GPIO_InitStructure.GPIO_Pin   = SX127x_SPI_MOSI_PIN;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_Init(SX127x_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);
    GPIO_WriteBit( SX127x_SPI_MOSI_GPIO_PORT, SX127x_SPI_MOSI_PIN,  Bit_RESET);

    /*!< Configure SPI pins: NSS */
    GPIO_InitStructure.GPIO_Pin   = SX127x_SPI_NSS_PIN;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_Init(SX127x_SPI_NSS_GPIO_PORT, &GPIO_InitStructure);
    GPIO_WriteBit( SX127x_SPI_NSS_GPIO_PORT, SX127x_SPI_NSS_PIN,  Bit_SET);
    spi_io_init_done = 0;
  }
}
/*---------------------------------------------------------------------------*/
uint8_t
SX1276SpiInOut( uint8_t outData )
{
  /* Send SPIy data */
  SPI_I2S_SendData( SX127x_SPI_INTERFACE, outData );
  while( SPI_I2S_GetFlagStatus( SX127x_SPI_INTERFACE,
                                SPI_I2S_FLAG_RXNE ) == RESET );
  return SPI_I2S_ReceiveData( SX127x_SPI_INTERFACE );
}
/*---------------------------------------------------------------------------*/
/*!
 * \brief Enables/disables the TCXO if available on board design.
 *
 * \param [IN] state TCXO enabled when true and disabled when false.
 */
static void
SX1276SetBoardTcxo( uint8_t state )
{
    // No TCXO component available on this board design.
#if 0
    if( state == true )
    {
        TCXO_ON( );
        DelayMs( BOARD_TCXO_WAKEUP_TIME );
    }
    else
    {
        TCXO_OFF( );
    }
#endif
}
/*---------------------------------------------------------------------------*/
uint32_t
SX1276GetBoardTcxoWakeupTime( void )
{
  return BOARD_TCXO_WAKEUP_TIME;
}
/*---------------------------------------------------------------------------*/
bool
SX1276CheckRfFrequency( uint32_t frequency )
{
  // Implement check. Currently all frequencies are supported
  return true;
}
/*---------------------------------------------------------------------------*/
void
SX1276Reset( void )
{
  // Enables the TCXO if available on the board design
  SX1276SetBoardTcxo( true );

  // Set RESET pin to 0
  SX1276SetReset( RADIO_RESET_ON );

  // Wait 1 ms
  DelayMs( 1 );

  // Configure RESET as input
  SX1276SetReset( RADIO_RESET_OFF );

  // Wait 6 ms
  DelayMs( 6 );
}
/*---------------------------------------------------------------------------*/
void
SX1276SetRfTxPower( int8_t power )
{
  uint8_t paConfig = 0;
  uint8_t paDac = 0;

  paConfig = SX1276Read( REG_PACONFIG );
  paDac = SX1276Read( REG_PADAC );

  paConfig = ( paConfig & RF_PACONFIG_PASELECT_MASK ) |
              SX1276GetPaSelect( SX1276.Settings.Channel );
  paConfig = ( paConfig & RF_PACONFIG_MAX_POWER_MASK ) | 0x70;

  if( ( paConfig & RF_PACONFIG_PASELECT_PABOOST ) == RF_PACONFIG_PASELECT_PABOOST )
  {
    if( power > 17 )
    {
      paDac = ( paDac & RF_PADAC_20DBM_MASK ) | RF_PADAC_20DBM_ON;
    }
    else
    {
      paDac = ( paDac & RF_PADAC_20DBM_MASK ) | RF_PADAC_20DBM_OFF;
    }
    if( ( paDac & RF_PADAC_20DBM_ON ) == RF_PADAC_20DBM_ON )
    {
      if( power < 5 )
      {
        power = 5;
      }
      if( power > 20 )
      {
        power = 20;
      }
      paConfig = ( paConfig & RFLR_PACONFIG_OUTPUTPOWER_MASK ) |
                  ( uint8_t )( ( uint16_t )( power - 5 ) & 0x0F );
    }
    else
    {
      if( power < 2 )
      {
        power = 2;
      }
      if( power > 17 )
      {
        power = 17;
      }
      paConfig = ( paConfig & RFLR_PACONFIG_OUTPUTPOWER_MASK ) |
                  ( uint8_t )( ( uint16_t )( power - 2 ) & 0x0F );
    }
  }
  else
  {
    if( power < -1 )
    {
      power = -1;
    }
    if( power > 14 )
    {
      power = 14;
    }
    paConfig = ( paConfig & RFLR_PACONFIG_OUTPUTPOWER_MASK ) | ( uint8_t )( ( uint16_t )( power + 1 ) & 0x0F );
  }
  SX1276Write( REG_PACONFIG, paConfig );
  SX1276Write( REG_PADAC, paDac );
}
/*---------------------------------------------------------------------------*/
uint8_t
SX1276GetPaSelect( uint32_t channel )
{
  return RF_PACONFIG_PASELECT_RFO;
}
/*---------------------------------------------------------------------------*/
void
SX1276SetAntSwLowPower( bool status )
{
  if( RadioIsActive != status )
  {
    RadioIsActive = status;

    if( status == false )
    {
      SX1276SetBoardTcxo( true );
      SX1276AntSwInit( );
    }
    else
    {
      SX1276SetBoardTcxo( false );
      SX1276AntSwDeInit( );
    }
  }
}
/*---------------------------------------------------------------------------*/
void
SX1276AntSwInit( void )
{
  GPIO_InitTypeDef GPIO_InitStructure;

  GPIO_InitStructure.GPIO_Pin   = ANT_RXTX_LF_PIN;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_Init( ANT_RXTX_LF_GPIO_PORT, &GPIO_InitStructure );

  GPIO_WriteBit( ANT_RXTX_LF_GPIO_PORT, ANT_RXTX_LF_PIN,  Bit_RESET);
}
/*---------------------------------------------------------------------------*/
void
SX1276AntSwDeInit ( void )
{
  GPIO_InitTypeDef GPIO_InitStructure;

  GPIO_InitStructure.GPIO_Pin   = ANT_RXTX_LF_PIN;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_Init( ANT_RXTX_LF_GPIO_PORT, &GPIO_InitStructure );

  GPIO_WriteBit( ANT_RXTX_LF_GPIO_PORT, ANT_RXTX_LF_PIN,  Bit_RESET);
}
/*---------------------------------------------------------------------------*/
void
SX1276SetAntSw( uint8_t opMode )
{
  switch( opMode )
  {
    case RFLR_OPMODE_TRANSMITTER:
      GPIO_WriteBit( ANT_RXTX_LF_GPIO_PORT,
                     ANT_RXTX_LF_PIN, Bit_SET);
      break;
    case RFLR_OPMODE_RECEIVER:
    case RFLR_OPMODE_RECEIVER_SINGLE:
    case RFLR_OPMODE_CAD:
    default:
      GPIO_WriteBit( ANT_RXTX_LF_GPIO_PORT,
                     ANT_RXTX_LF_PIN, Bit_RESET);
      break;
  }
}
/*---------------------------------------------------------------------------*/
inline void
SX1276SetPower( uint8_t onoff )
{
  GPIO_WriteBit( SX127x_VS_GPIO_PORT,
                 SX127x_VS_GPIO_PIN,
                 (onoff == 1) ? Bit_SET:Bit_RESET );
}
/*---------------------------------------------------------------------------*/
inline void
SX1276SetNSS( uint8_t value )
{
  /* Set NSS for SPI */
  GPIO_WriteBit( SX127x_SPI_NSS_GPIO_PORT,
                 SX127x_SPI_NSS_PIN,
                 (value == 1) ? Bit_SET:Bit_RESET);
}
/*---------------------------------------------------------------------------*/
void
DelayMs( uint32_t time_in_ms )
{
  /* Perform a delay */
  clock_wait( (time_in_ms * CLOCK_SECOND) / 1000 );
}
/*---------------------------------------------------------------------------*/
void
SX1276WriteBuffer( uint16_t addr, uint8_t *buffer, uint8_t size )
{
  uint8_t i;

  GPIO_WriteBit( SX127x_SPI_NSS_GPIO_PORT, SX127x_SPI_NSS_PIN, Bit_RESET );

  SX1276SpiInOut( addr | 0x80 );
  for( i = 0; i < size; i++ )
  {
    SX1276SpiInOut( buffer[i] );
  }

  GPIO_WriteBit( SX127x_SPI_NSS_GPIO_PORT, SX127x_SPI_NSS_PIN, Bit_SET );
}
/*---------------------------------------------------------------------------*/
void
SX1276ReadBuffer( uint16_t addr, uint8_t *buffer, uint8_t size )
{
  uint8_t i;

  GPIO_WriteBit( SX127x_SPI_NSS_GPIO_PORT, SX127x_SPI_NSS_PIN, Bit_RESET );

  SX1276SpiInOut( addr & 0x7F );

  for( i = 0; i < size; i++ )
  {
    buffer[i] = SX1276SpiInOut( 0 );
  }

  GPIO_WriteBit( SX127x_SPI_NSS_GPIO_PORT, SX127x_SPI_NSS_PIN, Bit_SET );
}
/*---------------------------------------------------------------------------*/
inline uint8_t
SX1276ReadDio0( void )
{
  return GPIO_ReadInputDataBit( DIO0_GPIO_PORT, DIO0_PIN );
}
/*---------------------------------------------------------------------------*/
inline uint8_t
SX1276ReadDio1( void )
{
  return GPIO_ReadInputDataBit( DIO1_GPIO_PORT, DIO1_PIN );
}
/*---------------------------------------------------------------------------*/
inline uint8_t
SX1276ReadDio2( void )
{
  return GPIO_ReadInputDataBit( DIO2_GPIO_PORT, DIO2_PIN );
}
/*---------------------------------------------------------------------------*/
inline uint8_t
SX1276ReadDio3( void )
{
  return GPIO_ReadInputDataBit( DIO3_GPIO_PORT, DIO3_PIN );
}
/*---------------------------------------------------------------------------*/
inline uint8_t
SX1276ReadDio4( void )
{
  return 0;
}
/*---------------------------------------------------------------------------*/
inline uint8_t
SX1276ReadDio5( void )
{
  return 0;
}
/*---------------------------------------------------------------------------*/
inline void
SX1276WriteRxTx( uint8_t txEnable )
{
  if( txEnable != 0 )
  {
    GPIO_WriteBit( ANT_RXTX_LF_GPIO_PORT, ANT_RXTX_LF_PIN, Bit_SET );
  }
  else
  {
    GPIO_WriteBit( ANT_RXTX_LF_GPIO_PORT, ANT_RXTX_LF_PIN, Bit_RESET );
  }
}
/*---------------------------------------------------------------------------*/
