/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "contiki.h"
#include "contiki-conf.h"

#include <sys/process.h>
#include <sys/procinit.h>
#include "sys/autostart.h"
#include "sys/node-id.h"

#include "lib/random.h"

#include "dev/leds.h"
#include "dev/serial-line.h"
#include "dev/watchdog.h"
#include "dev/button-sensor.h"
#include "dev/serial-line-arch.h"
#include "dev/slip.h"
#include "dev/usart.h"
#include "rs485.h"

#include "rtctimer.h"
#include "soc-rtc.h"

#include "net/rime/rime.h"
#include "net/netstack.h"
#if NETSTACK_CONF_WITH_IPV6
#include "net/ipv6/uip-ds6.h"
#endif /* NETSTACK_CONF_WITH_IPV6 */

#include "stm32l151.h"
#include "lpm.h"
#include "sx1276.h"
#include "sx1276-radio-driver.h"
#include "lora-timer.h"

#if WITH_LORAMAC
#include "lorawan-conf.h"
#include "loramac-mac.h"
#endif /* WITH_LORAMAC */

#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...) do {} while (0)
#endif

#define DEBUG_LEDS DEBUG
#undef LEDS_ON
#undef LEDS_OFF
#undef LEDS_TOGGLE
#if DEBUG_LEDS
#define LEDS_INIT() leds_init()
#define LEDS_ON(x) leds_on(x)
#define LEDS_OFF(x) leds_off(x)
#define LEDS_TOGGLE(x) leds_toggle(x)
#else
#define LEDS_INIT()
#define LEDS_ON(x)
#define LEDS_OFF(x)
#define LEDS_TOGGLE(x)
#endif

#if UIP_CONF_ROUTER

#ifndef UIP_ROUTER_MODULE
#ifdef UIP_CONF_ROUTER_MODULE
#define UIP_ROUTER_MODULE UIP_CONF_ROUTER_MODULE
#else /* UIP_CONF_ROUTER_MODULE */
#define UIP_ROUTER_MODULE rimeroute
#endif /* UIP_CONF_ROUTER_MODULE */
#endif /* UIP_ROUTER_MODULE */

extern const struct uip_router UIP_ROUTER_MODULE;
#endif /* UIP_CONF_ROUTER */

#ifndef NETSTACK_CONF_WITH_IPV4
#define NETSTACK_CONF_WITH_IPV4 0
#endif

#if NETSTACK_CONF_WITH_IPV4
#include "net/ip/uip.h"
#include "net/ipv4/uip-fw.h"
#include "net/ipv4/uip-fw-drv.h"
#include "net/ipv4/uip-over-mesh.h"
static struct uip_fw_netif slipif =
  {UIP_FW_NETIF(192,168,1,2, 255,255,255,255, slip_send)};
static struct uip_fw_netif meshif =
  {UIP_FW_NETIF(172,16,0,0, 255,255,0,0, uip_over_mesh_send)};

#endif /* NETSTACK_CONF_WITH_IPV4 */

#define UIP_OVER_MESH_CHANNEL 8
#if NETSTACK_CONF_WITH_IPV4
static uint8_t is_gateway;
#endif /* NETSTACK_CONF_WITH_IPV4 */
/*---------------------------------------------------------------------------*/
/* USARTx configured as follow:
- BaudRate = 115200 baud
- Word Length = 8 Bits
- One Stop Bit
- No parity
- Receive and transmit enabled
- Hardware flow control disabled (RTS and CTS signals)
*/
#define SERIAL_BAUDRATE               115200
#define SERIAL_WORDLENGTH             USART_WordLength_8b
#define SERIAL_STOPBITS               USART_StopBits_1
#define SERIAL_PARITY                 USART_Parity_No
#define SERIAL_MODE                   USART_Mode_Rx | USART_Mode_Tx
#define SERIAL_HARDWAREFLOWCONTROL    USART_HardwareFlowControl_None
/*---------------------------------------------------------------------------*/
void init_platform(void);
/*---------------------------------------------------------------------------*/
static void print_processes(struct process * const processes[]);
static void set_rime_addr(void);
static void init_rtc(void);
/*---------------------------------------------------------------------------*/
static void
init_rtc(void)
{
#if RTC_CONF_INIT
#if RTC_CONF_SET_FROM_SYS
  char *next;
  RTC_TIME td;
  uint8_t r = RTC_TIME_RESULT_OK;
#endif /* RTC_CONF_SET_FROM_SYS */

  /* Initialize the RTC clock */
  soc_rtc_init();

#if RTC_CONF_SET_FROM_SYS
#ifndef DATE
  #error Could not retrieve date from system
#endif /* DATE */
  /* Alternatively, for test only, undefine DATE and define it on your own as:
   * #define DATE "04 07 07 2016 13 19 09 PM"
   * Also note that if you restart the node at a given time, it will use the
   * already defined DATE, so if you want to update the device date/time you
   * need to reflash the node.
   */

  /* Get the system date in the following format: wd dd mm yy hh mm ss locale*/

  //PRINTF("Setting RTC from system date: %s\n", DATE);

  /* Initialize the settings to operate the RTC in calendar mode */
  td.dow    = (uint8_t) strtol(DATE, &next, 10);
  td.day    = (uint8_t) strtol(next, &next, 10);
  td.month  = (uint8_t) strtol(next, &next, 10);
  //td.year   = (uint16_t)strtol(next, &next, 10)  % 100; //Extract two last digits
  td.year   = (uint16_t)strtol(next, &next, 10);
  td.hour   = (uint8_t) strtol(next, &next, 10);
  td.minute = (uint8_t) strtol(next, &next, 10);
  td.second = (uint8_t) strtol(next, &next, 10);
  //td.h12    = strcmp(next, "PM") ? RTC_H12_PM : RTC_H12_AM;
  td.h12    = RTC_H12_AM;

  /* Set the time and date */
  RTC_SET_TIME(&td, &r);

  if(r != RTC_TIME_RESULT_OK) {
    PRINTF("Failed to set time and date\n");
    LEDS_TOGGLE(LEDS_RED);
  }
#endif /* RTC_CONF_SET_FROM_SYS */
#endif /* RTC_CONF_INIT */
}
/*---------------------------------------------------------------------------*/
static void
set_rime_addr(void)
{
  linkaddr_t addr;
  int i;

  memset(&addr, 0, sizeof(linkaddr_t));

#if NETSTACK_CONF_WITH_IPV6
  memcpy(addr.u8, node_mac, sizeof(addr.u8));
#else
  if(node_id == 0) {
    for(i = 0; i < sizeof(linkaddr_t); ++i) {
      addr.u8[i] = node_mac[7 - i];
    }
  } else {
    addr.u8[0] = node_id & 0xff;
    addr.u8[1] = node_id >> 8;
  }
#endif /* NETSTACK_CONF_WITH_IPV6 */
  linkaddr_set_node_addr(&addr);
  if (strcmp(NETSTACK_RDC.name, "LoRaMac") != 0)
  {
    PRINTF("Rime started with address ");
    for(i = 0; i < sizeof(addr.u8) - 1; i++) {
      PRINTF("%d.", addr.u8[i]);
    }
    PRINTF("%d\n", addr.u8[i]);
  }
}
/*---------------------------------------------------------------------------*/
#if !PROCESS_CONF_NO_PROCESS_NAMES
static void
print_processes(struct process * const processes[])
{
  /* const struct process * const * p = processes; */
  PRINTF("Starting");
  while(*processes != NULL) {
    PRINTF(" ’%s’", (*processes)->name);
    processes++;
  }
  PRINTF("\n");
}
#endif /* !PROCESS_CONF_NO_PROCESS_NAMES */
/*--------------------------------------------------------------------------*/
#if NETSTACK_CONF_WITH_IPV4
static void
set_gateway(void)
{
  if(!is_gateway) {
    leds_on(LEDS_RED);
    PRINTF("%d.%d: making myself the IP network gateway.\n\n",
     linkaddr_node_addr.u8[0], linkaddr_node_addr.u8[1]);
    PRINTF("IPv4 address of the gateway: %d.%d.%d.%d\n\n",
     uip_ipaddr_to_quad(&uip_hostaddr));
    uip_over_mesh_set_gateway(&linkaddr_node_addr);
    uip_over_mesh_make_announced_gateway();
    is_gateway = 1;
  }
}
#endif /* NETSTACK_CONF_WITH_IPV4 */
/*---------------------------------------------------------------------------*/
int
main(int argc, char **argv)
{
  /* CPU initialization */
  cpu_init();
  /* Clock initialization. */
  clock_init();

  leds_init();
#if 0
#if DEBUG_LEDS
  /* Configure LEDs */
  LEDS_INIT();
#endif
#endif
  LEDS_ON(LEDS_RED);

#if WITH_SERIAL_LINE_INPUT
  usart_init(USARTx_SERIAL_LINE, SERIAL_BAUDRATE, SERIAL_WORDLENGTH,
    SERIAL_STOPBITS, SERIAL_PARITY, SERIAL_MODE, SERIAL_HARDWAREFLOWCONTROL);
  serial_line_arch_init();
#endif /* WITH_SERIAL_LINE_INPUT */

  lpm_init();
  rtimer_init();
  rtctimer_init();
  /* Check if the system has resumed from IWDG reset */
  if (RCC_GetFlagStatus(RCC_FLAG_IWDGRST) != RESET)
  {
    PRINTF("IWDG Reset!\n");
    /* IWDGRST flag set */
    /* Clear reset flags */
    RCC_ClearFlag();
  }
  else
  {
    LEDS_OFF(LEDS_RED);
  }

  /*
   * Hardware initialization done!
   */

  /* Initialize energest first (but after rtimer)
   */
  energest_init();
  ENERGEST_ON(ENERGEST_TYPE_CPU);

  /* Restore node id if such has been stored in external mem */
#ifdef NODEID
  node_id = NODEID;
#else/* NODE_ID */
  node_id_restore(); /* also configures node_mac[] */
#endif /* NODE_ID */

  random_init(( *( uint32_t* )ID1 ) ^ ( *( uint32_t* )ID2 ) ^ ( *( uint32_t* )ID3 ) );

  /*
   * Initialize Contiki and our processes.
   */
  process_init();
  process_start(&etimer_process, NULL);

  ctimer_init();

  init_platform();

#if WITH_SERIAL_LINE_INPUT
#if NETSTACK_CONF_WITH_IPV4
#if defined(LORA_IP_BORDER_ROUTER)
  usart_init(USARTx_SLIP, SERIAL_BAUDRATE, SERIAL_WORDLENGTH,
    SERIAL_STOPBITS, SERIAL_PARITY, SERIAL_MODE, SERIAL_HARDWAREFLOWCONTROL);
#endif /* defined(LORA_IP_BORDER_ROUTER) */
  slip_arch_init(SERIAL_BAUDRATE);
#endif /* NETSTACK_CONF_WITH_IPV4 */
#endif /* WITH_SERIAL_LINE_INPUT */

  init_rtc();

  set_rime_addr();

  {
    uint8_t longaddr[8];
    uint16_t shortaddr;

    shortaddr = (linkaddr_node_addr.u8[0] << 8) + linkaddr_node_addr.u8[1];
    memset(longaddr, 0, sizeof(longaddr));
    linkaddr_copy((linkaddr_t *)&longaddr, &linkaddr_node_addr);
    if (strcmp(NETSTACK_RDC.name, "LoRaMac") != 0)
    {
      PRINTF("MAC %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x ",
             node_mac[0], node_mac[1], node_mac[2], node_mac[3],
             node_mac[4], node_mac[5], node_mac[6], node_mac[7]);
      PRINTF("Short MAC Address %u.%u\n",
             *((uint8_t *) & shortaddr + 1), *((uint8_t *) & shortaddr));
    }
  }

  PRINTF("\n\n" CONTIKI_VERSION_STRING " started. \n");

  if(node_id) {
    PRINTF(" Node id is set to %d\n\n", node_id);
  } else{
    PRINTF(" Node id is not set.\n\n");
  }

  queuebuf_init();
#if PLATFORM_HAS_RADIO
  netstack_init();

  if (strcmp(NETSTACK_RDC.name, "LoRaMac") == 0)
  {
    PRINTF("LoRAWAN end-device\n");
#if LORAWAN_DEVICE_CLASS_C
    PRINTF(" Class: C\n");
#elif LORAWAN_DEVICE_CLASS_B
    PRINTF(" Class: B\n");
#else /* LORAWAN_DEVICE_CLASS_ */
    PRINTF(" Class: A\n");
#endif /* LORAWAN_DEVICE_CLASS_ */

#if defined( REGION_AS923 )
    PRINTF(" Region: AS923\n");
#elif defined( REGION_AU915 )
    PRINTF(" Region: AU915\n");
#elif defined( REGION_CN779 )
    PRINTF(" Region: CN779\n");
#elif defined( REGION_EU433 )
    PRINTF(" Region: EU433\n");
#elif defined( REGION_EU868 )
    PRINTF(" Region: EU868\n");
#elif defined( REGION_IN865 )
    PRINTF(" Region: IN865\n");
#elif defined( REGION_KR920 )
    PRINTF(" Region: KR920\n");
#elif defined( REGION_US915 )
    PRINTF(" Region: US915\n");
#elif defined( REGION_US915_HYBRID )
    PRINTF(" Region: US915 (HYBRID)\n");
#else /* REGION_ */
    PRINTF(" Region: UNDEFINED\n");
#endif /* REGION_ */

    uint32_t dev_addr = 0;
#if WITH_LORAMAC
    dev_addr = LORAWAN_DEVICE_ADDRESS;
#endif /* WITH_LORAMAC */

    if(dev_addr == 0)
    {
      dev_addr = node_id;
    }
    PRINTF(" Device Address: %08lx\n", dev_addr);
#if WITH_LORAMAC
    PRINTF(" Device EUI: ");
    if ( OVER_THE_AIR_ACTIVATION == 1)
    {
      PRINTF("%02x%02x%02x%02x%02x%02x%02x%02x\n",
             node_mac[0], node_mac[1], node_mac[2], node_mac[3],
             node_mac[4], node_mac[5], node_mac[6], node_mac[7]);
    }
    else
    {
      uint8_t DevEui[] = LORAWAN_DEVICE_EUI;
      PRINTF("%02x%02x%02x%02x%02x%02x%02x%02x\n",
             DevEui[0], DevEui[1], DevEui[2], DevEui[3],
             DevEui[4], DevEui[5], DevEui[6], DevEui[7]);
    }
    PRINTF(" MAC: %s\n", NETSTACK_RDC.name);
#endif /* WITH_LORAMAC */
  }
  else
  {
    radio_value_t ch;
    NETSTACK_RADIO.get_value(RADIO_PARAM_CHANNEL, &ch);

    PRINTF(" NET: ");
    PRINTF("%s\n", NETSTACK_NETWORK.name);
    PRINTF(" LLSEC: ");
    PRINTF("%s\n", NETSTACK_LLSEC.name);
    PRINTF(" MAC: ");
    PRINTF("%s\n", NETSTACK_MAC.name);
    PRINTF(" RDC: ");
    PRINTF("%s\n", NETSTACK_RDC.name);
    PRINTF(" RF: %u Hz", ch);
    if(NETSTACK_RDC.channel_check_interval() != 0)
    {
      PRINTF("\n Channel Check Interval: %u ticks",
             NETSTACK_RDC.channel_check_interval());
    }
  }
  PRINTF("\n\n");

  /* Turn the radio to sleep mode */
  NETSTACK_RADIO.off();

#if NETSTACK_CONF_WITH_IPV6
  memcpy(&uip_lladdr.addr, node_mac, sizeof(uip_lladdr.addr));

  process_start(&tcpip_process, NULL);

#if DEBUG
  PRINTF("Tentative link-local IPv6 address ");
  {
    uip_ds6_addr_t *lladdr;
    int i;
    lladdr = uip_ds6_get_link_local(-1);
    for(i = 0; i < 7; ++i) {
      PRINTF("%02x%02x:", lladdr->ipaddr.u8[i * 2], lladdr->ipaddr.u8[i * 2 + 1]);
    }
    PRINTF("%02x%02x\n", lladdr->ipaddr.u8[14], lladdr->ipaddr.u8[15]);
  }
#endif /* DEBUG */

  if(!UIP_CONF_IPV6_RPL) {
    int i;
    uip_ipaddr_t ipaddr;
    //uip_ip6addr(&ipaddr, 0xfc00, 0, 0, 0, 0, 0, 0, 0);
    uip_ip6addr(&ipaddr, UIP_DS6_DEFAULT_PREFIX, 0, 0, 0, 0, 0, 0, 0);
    uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
    //uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
    uip_ds6_addr_add(&ipaddr, 0, ADDR_TENTATIVE);
    PRINTF("Tentative global IPv6 address ");
    for(i = 0; i < 7; ++i) {
      PRINTF("%02x%02x:", ipaddr.u8[i * 2], ipaddr.u8[i * 2 + 1]);
    }
    PRINTF("%02x%02x\n", ipaddr.u8[7 * 2], ipaddr.u8[7 * 2 + 1]);
  }
#endif /* NETSTACK_CONF_WITH_IPV6 */

  /* Start user processes */
#if WITH_SERIAL_LINE_INPUT
#if defined(LORA_IP_BORDER_ROUTER) || !NETSTACK_CONF_WITH_IPV4 && !NETSTACK_CONF_WITH_IPV6
  serial_line_init();
#endif
#endif /* WITH_SERIAL_LINE_INPUT */

#if NETSTACK_CONF_WITH_IPV4
  process_start(&tcpip_process, NULL);
  process_start(&uip_fw_process, NULL); /* Start IP output */

#if !defined(LORA_IP_BORDER_ROUTER)
  process_start(&slip_process, NULL);
  slip_set_input_callback(set_gateway);
#endif /* !defined(LORA_IP_BORDER_ROUTER) */

  {
    uip_ipaddr_t hostaddr, netmask;

    uip_init();

    uip_ipaddr(&hostaddr, 172,16,
         linkaddr_node_addr.u8[0],linkaddr_node_addr.u8[1]);
    uip_ipaddr(&netmask, 255,255,0,0);
    uip_ipaddr_copy(&meshif.ipaddr, &hostaddr);

    uip_sethostaddr(&hostaddr);
    uip_setnetmask(&netmask);
    uip_over_mesh_set_net(&hostaddr, &netmask);
    /*    uip_fw_register(&slipif);*/
    uip_over_mesh_set_gateway_netif(&slipif);
    uip_fw_default(&meshif);
    uip_over_mesh_init(UIP_OVER_MESH_CHANNEL);
    PRINTF("uIP started with IP address %d.%d.%d.%d\n",
     uip_ipaddr_to_quad(&hostaddr));
  }
#endif /* NETSTACK_CONF_WITH_IPV4 */

#else

 /* Start user processes */
#if WITH_SERIAL_LINE_INPUT
#if !NETSTACK_CONF_WITH_IPV4 && !NETSTACK_CONF_WITH_IPV6
  serial_line_init();
#endif
#endif /* WITH_SERIAL_LINE_INPUT */

#endif /* PLATFORM_HAS_RADIO */

#if !PROCESS_CONF_NO_PROCESS_NAMES
  print_processes(autostart_processes);
#endif /* !PROCESS_CONF_NO_PROCESS_NAMES */
  autostart_start(autostart_processes);

  /*
   * This is the scheduler loop.
   */
#ifdef SYSTEM_CORE_CLOCK_HSE
  watchdog_start();
#else /* SYSTEM_CORE_CLOCK_HSE */
#if !USE_LPM_STOP_MODE
  /* If HSI is used, the low power stop mode will trigger a IWGD reset */
  watchdog_start();
#endif /* USE_LPM_STOP_MODE */
#endif /* SYSTEM_CORE_CLOCK_HSE */

  //usart_deinit(USARTx_SERIAL_LINE);
  //usart_deinit(USARTx_SLIP);

  while(1) {
    int r = 0;
    do {
      /* Reset watchdog. */
      watchdog_periodic();
      r = process_run();
    } while(r > 0);

      //if( process_nevents() == 0 && !usart_active() && !rs485_active() && Radio.GetStatus() == RF_IDLE && TimerAllowsLpm() ){
#if WITH_LORAMAC
      if( process_nevents() == 0 && !usart_active() && !rs485_active() && MacAllowsLpm() )
#else /* WITH_LORAMAC */
      if( process_nevents() == 0 && !usart_active() && !rs485_active() )
#endif /* WITH_LORAMAC */
      {
        static unsigned long irq_energest = 0;

        /* Re-enable interrupts and go to sleep atomically. */
        ENERGEST_SWITCH(ENERGEST_TYPE_CPU, ENERGEST_TYPE_LPM);
        /* We only want to measure the processing done in IRQs when we
           are asleep, so we discard the processing time done when we
           were awake. */
        energest_type_set(ENERGEST_TYPE_IRQ, irq_energest);
        watchdog_stop();  // The watchdog cannot be stopped

        lpm_enter();

        dint();
        irq_energest = energest_type_time(ENERGEST_TYPE_IRQ);
        eint();
#ifdef SYSTEM_CORE_CLOCK_HSE
        watchdog_start();
#else /* SYSTEM_CORE_CLOCK_HSE */
#if !USE_LPM_STOP_MODE
        /* If HSI is used, the low power stop mode will trigger a IWGD reset */
        watchdog_start();
#endif /* USE_LPM_STOP_MODE */
#endif /* SYSTEM_CORE_CLOCK_HSE */

        ENERGEST_SWITCH(ENERGEST_TYPE_LPM, ENERGEST_TYPE_CPU);
      }
  }

  return 0;
}
/*---------------------------------------------------------------------------*/
