/*
 * Copyright (c) 2010, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
#ifndef __CONTIKI_CONF_H__
#define __CONTIKI_CONF_H__
/*---------------------------------------------------------------------------*/
#ifdef PLATFORM_CONF_H
#include PLATFORM_CONF_H
#else
#include "platform-conf.h"
#endif /* PLATFORM_CONF_H */
/*---------------------------------------------------------------------------*/

#define WITH_ASCII 																					1

#define STR_HELPER(x) 																			#x
#define STR(x) 																							STR_HELPER(x)
/* Examples:
#pragma message "content of HSE_VALUE: " STR(HSE_VALUE)
#pragma message "content of HSI_VALUE: " STR(HSI_VALUE)
*/

/**
 * \name Generic Configuration directives
 *
 * @{
 */
/**< Energest Module */
#ifndef ENERGEST_CONF_ON
#define ENERGEST_CONF_ON            												0
#endif /* ENERGEST_CONF_ON */

/*---------------------------------------------------------------------------*/
/**
 * \name Network Stack Configuration
 *
 * @{
 */

#ifndef NETSTACK_CONF_NETWORK
#if NETSTACK_CONF_WITH_IPV6
#define NETSTACK_CONF_NETWORK 															sicslowpan_driver
#else
#define NETSTACK_CONF_NETWORK 															rime_driver
#endif /* NETSTACK_CONF_WITH_IPV6 */
#endif /* NETSTACK_CONF_NETWORK */

#ifndef NETSTACK_CONF_MAC
#define NETSTACK_CONF_MAC     															nullmac_driver
#endif /* NETSTACK_CONF_MAC */

#ifndef NETSTACK_CONF_RDC
#define NETSTACK_CONF_RDC     															nullrdc_driver
#endif /* NETSTACK_CONF_RDC */
#define NETSTACK_RDC_HEADER_LEN 														0

#ifndef NETSTACK_CONF_FRAMER
#if NETSTACK_CONF_WITH_IPV6
#define NETSTACK_CONF_FRAMER  															framer_802154
#else /* NETSTACK_CONF_WITH_IPV6 */
#define NETSTACK_CONF_FRAMER  															framer_nullmac
#endif /* NETSTACK_CONF_WITH_IPV6 */
#endif /* NETSTACK_CONF_FRAMER */

#ifndef NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE
#define NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE    						8
#endif  /* NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE */

#undef NETSTACK_CONF_RADIO
#define NETSTACK_CONF_RADIO     														sx1276_radio_driver

/* Configure LPPMAC for when it's selected */
#define LPPMAC_CONF_ON_TIME																	(CLOCK_SECOND / 2)
#define LPPMAC_CONF_OFF_TIME																(16 * CLOCK_SECOND)
/*---------------------------------------------------------------------------*/
/**
 * \name RF configuration
 *
 * @{
 */
/* RF Config */

#ifndef NETSTACK_CONF_RADIO_DEFAULT
#define NETSTACK_CONF_RADIO_DEFAULT 												1
#endif /* NETSTACK_CONF_RADIO_DEFAULT */

#if NETSTACK_CONF_RADIO_DEFAULT

#ifdef RF_CHANNEL
#define LORA_CONF_RF_FREQUENCY               								RF_CHANNEL
#endif /* RF_CHANNEL */

#ifndef LORA_CONF_RF_FREQUENCY
#define LORA_CONF_RF_FREQUENCY              								CH_18_868
#endif /* LORA_CONF_RF_FREQUENCY */

#define SX1276_CONF_RESET_RX_DURATION 											( 1800 * CLOCK_SECOND )

// LoRa default configuration
#define TX_OUTPUT_POWER                       							14        // Power (dBm)

#if defined( USE_MODEM_LORA )
/* LoRa modem configuration */
#define LORA_BANDWIDTH                        							0         /* Signal Bandwidth:
							                                                         * [0: 125 kHz,
							                                                         *  1: 250 kHz,
							                                                         *  2: 500 kHz]
							                                                         */
#define LORA_SPREADING_FACTOR                 							7         /* Spreading Factor:
																																			 * [6: 64,
																																			 * 7: 128,
																																			 * 8: 256,
																																			 * 9: 512,
																																			 * 10: 1024,
																																			 * 11: 2048,
																																			 * 12: 4096  chips]
																																			 */
#define LORA_CODINGRATE                       							1         /* Error Coding:
							 																												 * [1: 4/5,
							 																												 *  2: 4/6,
							 																												 *  3: 4/7,
							 																												 *  4: 4/8]
							 																												 */
#define LORA_PREAMBLE_LENGTH                  							8         // Same for Tx and Rx
#define LORA_SYMBOL_TIMEOUT                   							5         // Symbols
#define LORA_FIX_LENGTH_PAYLOAD_ON            							false
#define LORA_IQ_INVERSION_ON                  							false
#define LORA_IMPLICIT_HEADER_ON               							false			/* Implicit Header On: [0: OFF, 1: ON] */
#define LORA_RX_SINGLE_ON                     							0					/* Rx Single On: [0: Continuous, 1 Single] */

#define LORA_CRC_ON                          								true      /* Crc On: [0: OFF, 1: ON] */
#define LORA_FREQUENCY_HOPPING_ON             							0					/* Frequency Hoppping On: [0: OFF, 1: ON] */
#define LORA_HOPPING_PERIOD                   							4					/* Hopping Period: Hops every frequency hopping period symbols */

#define LORA_MAX_PAYLOAD_SIZE                 							128				/* PayloadLength (used for implicit header mode) */
#define LORA_FIXED_PAYLOAD_LENGTH            								LORA_MAX_PAYLOAD_SIZE

#elif defined( USE_MODEM_FSK )
/* Fsk modem configuration */

#define FSK_FDEV                              							25e3      // Fdev (Hz)
#define FSK_DATARATE                          							50e3      // Bitrate (bps)
#define FSK_BANDWIDTH                         							50e3      // Hz
#define FSK_AFC_BANDWIDTH                     							83.333e3  // Hz
#define FSK_PREAMBLE_LENGTH                   							5         // Same for Tx and Rx
#define FSK_FIX_LENGTH_PAYLOAD_ON             							false
#define FSK_AFC_ON                            							true
#define FSK_CRC_ENABLED                       							true
#define FSK_PAYLOAD_SIZE                      							255

#else
  #error "Please define a modem in the compiler options."
#endif

#define LORA_CLEAR_CHANNEL_RSSI_THRESHOLD     							(-90)     // RSSI threshold in dBm for CSMA

#define TX_TIMEOUT_VALUE                      							3000  	 /* TxPacketTimeout in ms*/
#define RX_TIMEOUT_VALUE                      							1000     /* RxPacketTimeout in ms*/
#define RX_CONTINUOUS_MODE                    							true

#endif /* NETSTACK_CONF_RADIO_DEFAULT */

#define PACKETBUF_CONF_SIZE																	256

/*---------------------------------------------------------------------------*/
/**
 * \name IPv6, RIME and network buffer configuration
 *
 * @{
 */

/* Don't let contiki-default-conf.h decide if we are an IPv6 build */
#ifndef NETSTACK_CONF_WITH_IPV6
#define NETSTACK_CONF_WITH_IPV6              								0
#endif /* NETSTACK_CONF_WITH_IPV6 */

#define UIP_CONF_MAX_CONNECTIONS            								4

#define UIP_CONF_BYTE_ORDER 																UIP_LITTLE_ENDIAN
#define UIP_CONF_TCP_SPLIT                   								0
#define UIP_CONF_LOGGING                    								0


#if NETSTACK_CONF_WITH_IPV6
/* Addresses, Sizes and Interfaces */
/* 8-byte addresses here, 2 otherwise */
#define LINKADDR_CONF_SIZE                   								8
#define UIP_CONF_LL_802154                   								0
#define UIP_CONF_LLH_LEN                     								0
#define UIP_CONF_NETIF_MAX_ADDRESSES         								3

/* TCP, UDP, ICMP */
#ifndef UIP_CONF_TCP
#define UIP_CONF_TCP                         								1
#endif /* UIP_CONF_TCP */
#ifndef UIP_CONF_TCP_MSS
#define UIP_CONF_TCP_MSS                    								64
#endif /* UIP_CONF_TCP_MSS */
#define UIP_CONF_UDP                         								1
#define UIP_CONF_UDP_CHECKSUMS               								1

/* ND and Routing */
#ifndef UIP_CONF_ROUTER
#define UIP_CONF_ROUTER                      								1
#endif /* UIP_CONF_ROUTER */
#ifndef UIP_CONF_IPV6_RPL
#define UIP_CONF_IPV6_RPL               		 								0
#endif /* UIP_CONF_IPV6_RPL */

#define UIP_CONF_ND6_SEND_RA                 								0
#define UIP_CONF_IP_FORWARD                  								0
#define RPL_CONF_STATS                       								0

#define UIP_CONF_ND6_REACHABLE_TIME     										600000
#define UIP_CONF_ND6_RETRANS_TIMER       										10000

#ifndef NBR_TABLE_CONF_MAX_NEIGHBORS
#define NBR_TABLE_CONF_MAX_NEIGHBORS        								16
#endif /* NBR_TABLE_CONF_MAX_NEIGHBORS */
#ifndef UIP_CONF_MAX_ROUTES
#define UIP_CONF_MAX_ROUTES                 								16
#endif /* UIP_CONF_MAX_ROUTES */

/* uIP */
#ifndef UIP_CONF_BUFFER_SIZE
#define UIP_CONF_BUFFER_SIZE              									256
#endif /* UIP_CONF_BUFFER_SIZE */

#define UIP_CONF_IPV6_QUEUE_PKT              								0
#define UIP_CONF_IPV6_CHECKS                 								1
#define UIP_CONF_IPV6_REASSEMBLY             								0
#define UIP_CONF_MAX_LISTENPORTS             								8

/* 6lowpan */
#define SICSLOWPAN_CONF_COMPRESSION          								SICSLOWPAN_COMPRESSION_HC06
#ifndef SICSLOWPAN_CONF_COMPRESSION_THRESHOLD
#define SICSLOWPAN_CONF_COMPRESSION_THRESHOLD 							63
#endif /* SICSLOWPAN_CONF_COMPRESSION_THRESHOLD */
#ifndef SICSLOWPAN_CONF_FRAG
#define SICSLOWPAN_CONF_FRAG                 								1
#endif /* SICSLOWPAN_CONF_FRAG */
#define SICSLOWPAN_CONF_MAXAGE               								8

/* Define our IPv6 prefixes/contexts here */
#define SICSLOWPAN_CONF_MAX_ADDR_CONTEXTS    								1
#ifndef SICSLOWPAN_CONF_ADDR_CONTEXT_0
#define SICSLOWPAN_CONF_ADDR_CONTEXT_0 { \
  addr_contexts[0].prefix[0] = UIP_DS6_DEFAULT_PREFIX_0; \
  addr_contexts[0].prefix[1] = UIP_DS6_DEFAULT_PREFIX_1; \
}
#endif /* SICSLOWPAN_CONF_ADDR_CONTEXT_0 */

#define MAC_CONF_CHANNEL_CHECK_RATE          								8

#define UIP_CONF_DHCP_LIGHT
#ifndef UIP_CONF_RECEIVE_WINDOW
#define UIP_CONF_RECEIVE_WINDOW             								48
#endif /* UIP_CONF_RECEIVE_WINDOW */

#define UIP_CONF_UDP_CONNS                  								12
#define UIP_CONF_FWCACHE_SIZE               								30
#define UIP_CONF_BROADCAST                   								1
#define UIP_ARCH_IPCHKSUM                    								0
#define UIP_CONF_PINGADDRCONF                								0

#define CXMAC_CONF_ANNOUNCEMENTS         										0
#define XMAC_CONF_ANNOUNCEMENTS          										0

#ifndef QUEUEBUF_CONF_NUM
#define QUEUEBUF_CONF_NUM                    								32
#endif /* QUEUEBUF_CONF_NUM */
/*---------------------------------------------------------------------------*/
#else /* NETSTACK_CONF_WITH_IPV6 */
/* Network setup for non-IPv6 (rime). */

#define COLLECT_CONF_ANNOUNCEMENTS       										0
#define CXMAC_CONF_ANNOUNCEMENTS         										0
#define XMAC_CONF_ANNOUNCEMENTS          										0
#define CONTIKIMAC_CONF_ANNOUNCEMENTS    										0

#define UIP_CONF_IP_FORWARD                  								1

#ifndef UIP_CONF_BUFFER_SIZE
#define UIP_CONF_BUFFER_SIZE               									256
#endif /* UIP_CONF_BUFFER_SIZE */

#ifndef QUEUEBUF_CONF_NUM
#define QUEUEBUF_CONF_NUM                    								32
#endif /* QUEUEBUF_CONF_NUM */

#endif /* NETSTACK_CONF_WITH_IPV6 */
/*---------------------------------------------------------------------------*/
/**
 * \name RTC
 *
 * @{
 */
/**< Whether to initialize the RTC */
#ifndef RTC_CONF_INIT
#define RTC_CONF_INIT   																		1
#endif /* RTC_CONF_INIT */

/**< Whether to set the RTC from the build system */
#ifndef RTC_CONF_SET_FROM_SYS
#define RTC_CONF_SET_FROM_SYS    														1
#endif /* RTC_CONF_SET_FROM_SYS */

/*---------------------------------------------------------------------------*/
/* include the project config */
/* PROJECT_CONF_H might be defined in the project Makefile */
#ifdef PROJECT_CONF_H
#include PROJECT_CONF_H
#endif /* PROJECT_CONF_H */
/*---------------------------------------------------------------------------*/
#endif /* CONTIKI_CONF_H_ */
/*---------------------------------------------------------------------------*/
