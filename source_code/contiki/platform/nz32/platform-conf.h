/*
 * Copyright (c) 2010, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup nz32
 * @{
 *
 * \defgroup nz32-peripherals on NZ32
 *
 * Defines some of the platforms capabilities
 * @{
 *
 * \file
 * Header file for the nz32 platform configuration
 */
/*---------------------------------------------------------------------------*/
#ifndef __PLATFORM_CONF_H__
#define __PLATFORM_CONF_H__
/*---------------------------------------------------------------------------*/
#include <inttypes.h>
#include <string.h>
#include <math.h>

#include <sx1276.h>

#include "soc-rtc.h"
/*---------------------------------------------------------------------------*/
#define STM32_DEV_ID                          0x427
#define STM32_CAT											        3

/* Indicate that LEDs are available to use */
#define PLATFORM_HAS_LEDS             				1
/* Indicate that a button is present for use */
#define PLATFORM_HAS_BUTTON           				0
/* Indicate that a radio is present for use */
#define PLATFORM_HAS_RADIO            				1
/* Indicate that a battery is present for use */
#ifndef PLATFORM_HAS_BATTERY_SENSOR
#define PLATFORM_HAS_BATTERY_SENSOR   				0
#endif /* PLATFORM_HAS_BATTERY_SENSOR */
/* Indicate that a fatigue sensor is present for use */
#ifndef PLATFORM_HAS_FATIGUE_SENSOR
#define PLATFORM_HAS_FATIGUE_SENSOR           1
#endif /* PLATFORM_HAS_FATIGUE_SENSOR */
/*---------------------------------------------------------------------------*/
#define WITH_SERIAL_LINE_INPUT                1
/*---------------------------------------------------------------------------*/
/* The frequency of the main processor */
#define F_CPU                                 32000000ul
#define RTIMER_ARCH_SECOND                    32768
#define RTIMER_PRESCALER 							        (F_CPU / (RTIMER_SECOND * 2))
/*---------------------------------------------------------------------------*/
/* LSI clock frequency */
#define F_LSI                                 37000
/* Start watchdog */
#define ENABLE_DEEPSLEEP                      1
/* Start watchdog */
#if ENABLE_DEEPSLEEP == 1
#define ENABLE_WATCHDOG                       0
#else /* ENABLE_DEEPSLEEP */
#define ENABLE_WATCHDOG                       1
#endif /* ENABLE_DEEPSLEEP */
/* Use independent watchdog */
#define WATCHDOG_USE_IWDG                     1
/* Independent watchdog timeout in milliseconds */
#define WATCHDOG_IWDG_TIMEOUT                 250
/*---------------------------------------------------------------------------*/
#ifdef HSE_VALUE
#undef HSE_VALUE
#endif
#define HSE_VALUE                             ((uint32_t)16000000)

#define SYSTEM_CORE_CLOCK_HSE

#define STM32L1_CONF_VCORE                    PWR_VoltageScaling_Range1
#define STM32L1_CONF_FLASH_LATENCY            FLASH_Latency_1

/* The frequency of the LSE clock */
#define F_LSE                                 32768
/* Time in microseconds to wake up from low power mode */
#define MCU_WAKE_UP_TIME                      3400
#define MCU_WAKE_UP_TIME_TICKS                (MCU_WAKE_UP_TIME / \
                                                (1000000 / RTIMER_SECOND))

/* Time in mini seconds to wake the radio up from low power mode */
#define RADIO_WAKEUP_TIME                     SX1276_WAKEUP_TIME

/* Use stop low power mode. Comment out the following line if no use */
#if defined(SINKID) || defined(WITH_SLEEP_OFF)
#define LPM_CONF_USE_LP							          0
#define USE_LPM_CONF_STOP_MODE			          0
#else /* defined(NODEID) && defined(SINKID) */
#define LPM_CONF_USE_LP							          1
#define USE_LPM_CONF_STOP_MODE                1
#endif /* defined(NODEID) && defined(SINKID) */

#ifdef LPM_CONF_USE_LP
#define LPM_USE_LP                            LPM_CONF_USE_LP
#else /* LPM_CONF_USE_LP */
#define LPM_USE_LP                            1
#endif /* LPM_CONF_USE_LP */

#if LPM_USE_LP
#ifdef USE_LPM_CONF_STOP_MODE
#define USE_LPM_STOP_MODE	                    USE_LPM_CONF_STOP_MODE
#else /* LPM_USE_LP */
#define USE_LPM_STOP_MODE	                    1
#endif /* USE_LPM_CONF_STOP_MODE */
#endif /* LPM_USE_LP */

#define LPM_RTC_ALARM                         RTC_Alarm_A
/*---------------------------------------------------------------------------*/
/* define ticks/second for slow and fast clocks. Notice that these should be a
   power of two, eg 64,128,256,512 etc, for efficiency as POT's can be optimized
   well. */
#if USE_LPM_STOP_MODE
#define CLOCK_CONF_SECOND                     32
#else
#define CLOCK_CONF_SECOND                     256
#endif
#define RTIMER_CLOCK_DIFF(a, b)               ((signed)((a) - (b)))

enum {
 RTC_TIME_RESULT_OK  = 0,
 RTC_TIME_RESULT_ERROR
};

#ifndef RTC_TIME
#define RTC_TIME soc_rtc_td_map
#endif /* RTC_TIME */
#ifndef RTC_GET_TIME
#define RTC_GET_TIME(x, r)   {                              \
          soc_rtc_get_datetime(x);                          \
          *r = RTC_TIME_RESULT_OK;                          \
        }
#endif /* RTC_GET_TIME */
#ifndef RTC_SET_TIME
#define RTC_SET_TIME(x, r)   {                              \
          if(soc_rtc_set_datetime(x) != SOC_RTC_RESULT_OK) { \
            printf("Fail! could not set clock\n");          \
            *r = RTC_TIME_RESULT_ERROR;                     \
          }                                                 \
          *r = RTC_TIME_RESULT_OK;                          \
        }
#endif /* RTC_SET_TIME */
/*---------------------------------------------------------------------------*/
typedef unsigned long clock_time_t;
typedef unsigned long long rtimer_clock_t;
/*---------------------------------------------------------------------------*/
#define CC_CONF_REGISTER_ARGS                 0
#define CC_CONF_FUNCTION_POINTER_ARGS         1
#define CC_CONF_FASTCALL
#define CC_CONF_VA_ARGS                       1
#define CC_CONF_INLINE                        inline

#define CCIF
#define CLIF
/*---------------------------------------------------------------------------*/
/* These names are deprecated, use C99 names. */
typedef uint8_t   u8_t;
typedef uint16_t u16_t;
typedef uint32_t u32_t;
typedef int8_t    s8_t;
typedef int16_t  s16_t;
typedef int32_t  s32_t;
typedef unsigned short uip_stats_t;
/*---------------------------------------------------------------------------*/
#include "stm32l1xx.h"
/*---------------------------------------------------------------------------*/
extern unsigned char node_mac[8];
/*---------------------------------------------------------------------------*/
#define PRINTFLOAT(prefix, a, suffix)         printf("%s%ld.%04u%s", \
                prefix, (long)a, (unsigned)((a - floor(a)) * 10000), suffix)
/*---------------------------------------------------------------------------*/
/* LED definitions */
/*---------------------------------------------------------------------------*/
#define LEDS_RED                              1
#define LEDS_GREEN                            2

/* PB2 */
#define LED_GPIO_PORT                         GPIOB
// RED
#define LED1_PIN                              GPIO_Pin_2
#define LED1_GPIO_PORT                        GPIOA
#define LED1_GPIO_CLK                         RCC_AHBPeriph_GPIOA
/*---------------------------------------------------------------------------*/
/* User button definitions */
/*---------------------------------------------------------------------------*/
/* PC3 */
#define BUTTON_USER_PIN                       GPIO_Pin_3
#define BUTTON_USER_GPIO_PORT                 GPIOC
#define BUTTON_USER_GPIO_CLK                  RCC_AHBPeriph_GPIOC
#define BUTTON_USER_EXTI_LINE                 EXTI_Line3
#define BUTTON_USER_EXTI_PORT_SOURCE          EXTI_PortSourceGPIOC
#define BUTTON_USER_EXTI_PIN_SOURCE           GPIO_PinSource3
#define BUTTON_USER_EXTI_IRQn                 EXTI3_IRQn
#define BUTTON_USER_EXTI_IRQn_HANDLER         EXTI3_IRQHandler
/*---------------------------------------------------------------------------*/
/* I2Cx definitions */
/*---------------------------------------------------------------------------*/
/* Definition for I2Cx's NVIC */
#define I2Cx_EV_IRQn                          I2C1_EV_IRQn
#define I2Cx_ER_IRQn                          I2C1_ER_IRQn
#define I2Cx_EV_IRQHandler                    I2C1_EV_IRQHandler
#define I2Cx_ER_IRQHandler                    I2C1_ER_IRQHandler
/*---------------------------------------------------------------------------*/
#define JTAG_TMS_GPIO_PORT                    GPIOA
#define JTAG_TMS_PIN                          GPIO_Pin_13
#define JTAG_TCK_GPIO_PORT                    GPIOA
#define JTAG_TCK_PIN                          GPIO_Pin_14
#define JTAG_TDI_GPIO_PORT                    GPIOA
#define JTAG_TDI_PIN                          GPIO_Pin_15
#define JTAG_TDO_GPIO_PORT                    GPIOB
#define JTAG_TDO_PIN                          GPIO_Pin_3
#define JTAG_NRST_GPIO_PORT                   GPIOB
#define JTAG_NRST_PIN                         GPIO_Pin_4
/*---------------------------------------------------------------------------*/
/* USARTx */
/*---------------------------------------------------------------------------*/
#if defined(USE_USART1)
#define USARTx_SERIAL_LINE                    USART1
#elif defined (USE_USART2)
#define USARTx_SERIAL_LINE                    USART2
#elif defined (USE_USART3)
#define USARTx_SERIAL_LINE                    USART3
#elif defined (USE_USB_CDC)
#define USARTx_SERIAL_LINE                    NULL
#else
#define USARTx_SERIAL_LINE                    USART1
#endif
#define USARTx_SLIP                           USART2
/*---------------------------------------------------------------------------*/
/* SX127x SPI definitions */
/*---------------------------------------------------------------------------*/
/* iMod 2 - InAir 4/9 */

/* PB3 */
#define SX127x_SPI_INTERFACE                  SPI1
#define SX127x_SPI_CLK                        RCC_APB2Periph_SPI1
#define SX127x_SPI_AF                         GPIO_AF_SPI1
#define SX127x_SPI_GPIO_PORT_CLK              RCC_AHBPeriph_GPIOB
#define SX127x_SPI_SCK_GPIO_PORT              GPIOB
#define SX127x_SPI_SCK_PIN                    GPIO_Pin_3
#define SX127x_SPI_SCK_AF_PIN                 GPIO_PinSource3

/* PB4 */
#define SX127x_SPI_MISO_GPIO_PORT             GPIOB
#define SX127x_SPI_MISO_PIN                   GPIO_Pin_4
#define SX127x_SPI_MISO_AF_PIN                GPIO_PinSource4

/* PB5 */
#define SX127x_SPI_MOSI_GPIO_PORT             GPIOB
#define SX127x_SPI_MOSI_PIN                   GPIO_Pin_5
#define SX127x_SPI_MOSI_AF_PIN                GPIO_PinSource5

/* PC8 */
#define SX127x_SPI_NSS_GPIO_PORT              GPIOC
#define SX127x_SPI_NSS_PIN                    GPIO_Pin_8

/* PA9 */
#define RESET_GPIO_PORT                       GPIOA
#define RESET_PIN                             GPIO_Pin_9

/* PC13 - On/Off */
#define SX127x_VS_GPIO_PORT                   GPIOC
#define SX127x_VS_GPIO_PIN                    GPIO_Pin_13

/* SX127x DIO pins I/O definitions */
#define DIOx_GPIO_PORT_CLK                    RCC_AHBPeriph_GPIOA |  \
                                              RCC_AHBPeriph_GPIOB |  \
                                              RCC_AHBPeriph_GPIOC

/* PB0 - DIO0 */
#define DIO0_GPIO_PORT                        GPIOB
#define DIO0_PIN                              GPIO_Pin_0
#define DIO0_EXTI_LINE      						      EXTI_Line0
#define DIO0_EXTI_PIN_SOURCE                  EXTI_PinSource0
#define DIO0_EXTI_PORT_SOURCE	                EXTI_PortSourceGPIOB
#define DIO0_EXTI_IRQn                        EXTI0_IRQn
#define DIO0_EXTI_IRQn_HANDLER                EXTI0_IRQHandler

/* PB1 - DIO1 */
#define DIO1_GPIO_PORT                        GPIOB
#define DIO1_PIN                              GPIO_Pin_1
#define DIO1_EXTI_LINE                        EXTI_Line1
#define DIO1_EXTI_PIN_SOURCE                  EXTI_PinSource1
#define DIO1_EXTI_PORT_SOURCE	                EXTI_PortSourceGPIOB
#define DIO1_EXTI_IRQn                        EXTI1_IRQn
#define DIO1_EXTI_IRQn_HANDLER                EXTI1_IRQHandler

/* PC6 - DIO2 */
#define DIO2_GPIO_PORT                        GPIOC
#define DIO2_PIN                              GPIO_Pin_6
#define DIO2_EXTI_LINE      						      EXTI_Line6
#define DIO2_EXTI_PIN_SOURCE                  EXTI_PinSource6
#define DIO2_EXTI_PORT_SOURCE                 EXTI_PortSourceGPIOC
#define DIO2_EXTI_IRQn                        EXTI9_5_IRQn
#define DIO2_EXTI_IRQn_HANDLER                EXTI9_5_IRQHandler

/* PA10 - DIO3  */
#define DIO3_GPIO_PORT                        GPIOA
#define DIO3_PIN                              GPIO_Pin_10
#define DIO3_EXTI_LINE                        EXTI_Line10
#define DIO3_EXTI_PIN_SOURCE                  EXTI_PinSource10
#define DIO3_EXTI_PORT_SOURCE                 EXTI_PortSourceGPIOA
#define DIO3_EXTI_IRQn                        EXTI15_10_IRQn
#define DIO3_EXTI_IRQn_HANDLER                EXTI15_10_IRQHandler

/* PC13 */
#define ANT_RXTX_LF_GPIO_PORT                 GPIOC
#define ANT_RXTX_LF_PIN                       GPIO_Pin_13
/*---------------------------------------------------------------------------*/
/* 32.768 kHz crystal */
#define OSC_LSE_CLK                           RCC_AHBPeriph_GPIOC
#define OSC_LSE_GPIO_PORT                     GPIOC
#define OSC_LSE_IN_PIN                        GPIO_Pin_14
#define OSC_LSE_OUT_PIN                       GPIO_Pin_15
/* 16 MHz crystal */
#define OSC_HSE_CLK                           RCC_AHBPeriph_GPIOH
#define OSC_HSE_GPIO_PORT                     GPIOH
#define OSC_HSE_IN_PIN                        GPIO_Pin_0
#define OSC_HSE_OUT_PIN                       GPIO_Pin_1
/*---------------------------------------------------------------------------*/
#define SOC_ADC_WITH_DMA_TRANSFER             0
#define SOC_ADC_TOTAL_CHANNELS                8
#define SOC_ADC_DMA_CHANNEL                   DMA1_Channel1
#define SOC_ADC_DMA_IRQn                      DMA1_Channel1_IRQn
#define SOC_ADC_DMA_IRQHandler                DMA1_Channel1_IRQHandler
/*---------------------------------------------------------------------------*/
/* uSDCard SPI definitions */
/*---------------------------------------------------------------------------*/
#define USD_SPI                               SPI3
#define USD_SPI_AF                            GPIO_AF_SPI3
/* - for SPI3 and full-speed APB1: 36MHz/2 */
#define USD_SPI_BAUDRATE_PREESCALER           SPI_BaudRatePrescaler_2

/* PC1 */
#define USD_SPI_NSS_GPIO_PORT                 GPIOC
#define USD_SPI_NSS_GPIO_CLK                  RCC_AHBPeriph_GPIOC
#define USD_SPI_NSS_PIN                       GPIO_Pin_1

/* PC11 */
#define USD_SPI_MISO_GPIO_PORT                GPIOC
#define USD_SPI_MISO_GPIO_CLK                 RCC_AHBPeriph_GPIOC
#define USD_SPI_MISO_PIN                      GPIO_Pin_11
#define USD_SPI_MISO_AF_PIN                   GPIO_PinSource11

/* PC12 */
#define USD_SPI_MOSI_GPIO_PORT                GPIOC
#define USD_SPI_MOSI_GPIO_CLK                 RCC_AHBPeriph_GPIOC
#define USD_SPI_MOSI_PIN                      GPIO_Pin_12
#define USD_SPI_MOSI_AF_PIN                   GPIO_PinSource12

/* PC10 */
#define USD_SPI_CLK                           RCC_APB1Periph_SPI3
#define USD_SPI_CLK_CMD                       RCC_APB1PeriphClockCmd
#define USD_SPI_CLK_GPIO_PORT                 GPIOC
#define USD_SPI_CLK_GPIO_CLK                  RCC_AHBPeriph_GPIOC
#define USD_SPI_CLK_PIN                       GPIO_Pin_10
#define USD_SPI_CLK_AF_PIN                    GPIO_PinSource10

#define SOCKET_CD_CONNECTED                   0
#if SOCKET_CD_CONNECTED
/* PC2 */
#define USD_CD_GPIO_PORT                      GPIOC
#define USD_CD_GPIO_CLK                       RCC_AHBPeriph_GPIOC
#define USD_CD_PIN                            GPIO_Pin_2
#endif /* SOCKET_CD_CONNECTED */
/*---------------------------------------------------------------------------*/
/**
 * The STM32 factory-programmed UUID memory.
 */
/*---------------------------------------------------------------------------*/
#if STM32_CAT == 1 || STM32_CAT == 2
#define STM32_UUID                            (0x1FF80050)
#define STM32_FSISE                           (0x1FF8004C)
#else
#define STM32_UUID                            (0x1FF800D0)
#define STM32_FSISE                           (0x1FF800CC)
#endif

#define ID1                                   (STM32_UUID + 0x00)
#define ID2                                   (STM32_UUID + 0x04)
#define ID3                                   (STM32_UUID + 0x14)
/*---------------------------------------------------------------------------*/
 /*
  For STM32L15xx devices it is possible to use the internal USB pullup
  controlled by register SYSCFG_PMC (refer to RM0038 reference manual for
  more details).
  It is also possible to use external pullup (and disable the internal pullup)
  by setting the define USB_USE_EXTERNAL_PULLUP in file platform_config.h
  and configuring the right pin to be used for the external pull up
  configuration.
  To have more details on how to use an external pull up, please refer to
  STM3210E-EVAL evaluation board manuals.
  */
 /* Uncomment the following define to use an external pull up instead of the
    integrated STM32L15xx internal pull up. In this case make sure to set up
    correctly the external required hardware and the GPIO defines below.*/
/* #define USB_USE_EXTERNAL_PULLUP */

#define STM32L15_USB_CONNECT                  SYSCFG_USBPuCmd(ENABLE)
#define STM32L15_USB_DISCONNECT               SYSCFG_USBPuCmd(DISABLE)

#define USB_DM_GPIO_PORT                      GPIOA
#define USB_DM_PIN                            GPIO_Pin_11
#define USB_DP_PIN                            GPIO_Pin_12
/*---------------------------------------------------------------------------*/
#endif /* __PLATFORM_CONF_H__ */
/*---------------------------------------------------------------------------*/
/**
 * @}
 * @}
 */
