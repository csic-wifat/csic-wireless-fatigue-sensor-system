/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

#include "dev/radio-sensor.h"

#if PLATFORM_HAS_BATTERY_SENSOR
#include "battery-sensor.h"
#endif /* PLATFORM_HAS_BATTERY_SENSOR */

#if PLATFORM_HAS_BUTTON
#include "button-sensor.h"
#endif /* PLATFORM_HAS_BUTTON */

#if PLATFORM_HAS_FATIGUE_SENSOR
#include "sensors/fatigue-sensor.h"
#endif /* PLATFORM_HAS_FATIGUE_SENSOR */

SENSORS(&radio_sensor

#if PLATFORM_HAS_BATTERY_SENSOR
        , &battery_sensor
#endif /* PLATFORM_HAS_BATTERY_SENSOR */

#if PLATFORM_HAS_BUTTON
        , &button_sensor
#endif /* PLATFORM_HAS_BUTTON */

#if PLATFORM_HAS_FATIGUE_SENSOR
        , &fatigue_sensor
#endif /* PLATFORM_HAS_FATIGUE_SENSOR */

        );

void
init_platform(void)
{
  process_start(&sensors_process, NULL);
}
