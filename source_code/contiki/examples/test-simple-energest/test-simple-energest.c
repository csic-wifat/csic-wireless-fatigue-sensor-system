/**
 * \file
 *         An example of how to use the Simple Energest module on
 *         the NZ32 platform.
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "contiki.h"

#include "shell.h"
#include "serial-shell.h"

#include "simple-energest.h"

#ifndef DATE
#define DATE "Unknown"
#endif
/*---------------------------------------------------------------------------*/
PROCESS(test_simple_energest_process, "Test Simple Energest");
AUTOSTART_PROCESSES(&test_simple_energest_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(test_simple_energest_process, ev, data)
{
	static struct etimer periodic;

  PROCESS_BEGIN();

  /* Init simple energest */
  simple_energest_init();

  /* Init serial shell */
  serial_shell_init();

  /* Set periodic print timer */
  etimer_set(&periodic, CLOCK_SECOND * 10);

  /* Infinite loop */
  while(1) {
    simple_energest_t energy;

    PROCESS_WAIT_EVENT();

    if(etimer_expired(&periodic)) {
      /* Periodic timer event - print RTC time */
      etimer_reset(&periodic);
      if(simple_energest_step(&energy) == SIMPLE_ENERGEST_ERROR)
      {
        printf("Simple energest module error\n");
      }
      else
      {
        printf("E(time) %lu %lu %lu %lu\n",
                energy.tx, energy.rx, energy.cpu, energy.lpm);
      }
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
