/**
 * \file
 *         An example of how to use the button on
 *         the NZ32 platform.
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */

#include <stdio.h>
#include "contiki.h"
#include "battery-sensor.h"
#include "dev/leds.h"
#include "dev/soc-adc.h"
#include <stdio.h>
/*---------------------------------------------------------------------------*/
#define PRINT_INTERVAL  2 * CLOCK_SECOND
/*---------------------------------------------------------------------------*/
PROCESS(test_battery_sensor_process, "Test button");
AUTOSTART_PROCESSES(&test_battery_sensor_process);
/*---------------------------------------------------------------------------*/
static uint8_t active;
PROCESS_THREAD(test_battery_sensor_process, ev, data)
{
  static struct etimer etimer;
  int status;

  PROCESS_BEGIN();
  PROCESS_PAUSE();

  /* Activate sensor HW. GPIO and ADC is initialized here for first time */
  battery_sensor.configure(SENSORS_HW_INIT, 0);

  /* Set periodic timer */
  etimer_set(&etimer, PRINT_INTERVAL);

  /* Infinite loop */
  while(1) {
    PROCESS_WAIT_EVENT();
    if (ev == PROCESS_EVENT_TIMER)
    {
      /* Activate battery sensor */
      SENSORS_ACTIVATE(battery_sensor);
      /* Print battery level */
      printf("Battery level: %d\n", battery_sensor.value(0));
      /* Deactivate battery sensor */
      SENSORS_DEACTIVATE(battery_sensor);
      /* Reset periodic timer*/
      etimer_reset(&etimer);
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
