/**
 * \file
 *         An example of how to use the button on
 *         the NZ32 platform.
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */

#include <stdio.h>
#include "contiki.h"
#include "button-sensor.h"
#include "dev/leds.h"
#include <stdio.h>
/*---------------------------------------------------------------------------*/
#define BUTTON_PRESS_EVENT_INTERVAL (CLOCK_SECOND)
/*---------------------------------------------------------------------------*/
PROCESS(test_button_process, "Test button");
AUTOSTART_PROCESSES(&test_button_process);
/*---------------------------------------------------------------------------*/
static uint8_t active;
PROCESS_THREAD(test_button_process, ev, data)
{
  PROCESS_BEGIN();
  SENSORS_ACTIVATE(button_sensor);

  /* Configure the user button */
  button_sensor.configure(BUTTON_SENSOR_CONFIG_TYPE_INTERVAL,
                          BUTTON_PRESS_EVENT_INTERVAL);

  while(1) {
  	PROCESS_YIELD();

    if(ev == sensors_event && data == &button_sensor) {
	    if(button_sensor.value(BUTTON_SENSOR_VALUE_TYPE_LEVEL) == BUTTON_SENSOR_PRESSED_LEVEL) {
	    	leds_toggle(LEDS_ALL);
	      printf("Button pressed\n");
	    } else {
	    	leds_toggle(LEDS_ALL);
	      printf("...and released!\n");
	    }
    } else if(ev == button_press_duration_exceeded) {
      printf("Button pressed for %d ticks [%u events]\n",
             (*((uint8_t *)data) * BUTTON_PRESS_EVENT_INTERVAL),
             button_sensor.value(BUTTON_SENSOR_VALUE_TYPE_PRESS_DURATION));
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
