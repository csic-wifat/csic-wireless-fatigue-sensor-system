/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         An example of how to use sensors on
 *         the NZ32 platform.
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"

#include <stdio.h> /* For printf() */

#include "dev/leds.h"
#include "dev/radio.h"
#include "dev/soc-adc.h"
#include "dev/radio-sensor.h"
#include "button-sensor.h"
#include "battery-sensor.h"

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

#define CONT_CONV_MODE        DISABLE

#define PRINT_INTERVAL        5 * CLOCK_SECOND

#define VREF                  2.986
/*---------------------------------------------------------------------------*/
static unsigned long _button_pressed;
/*---------------------------------------------------------------------------*/
static void
sensors_state(void)
{
  uint16_t soc_adc_raw;
  double soc_adc_volt;

#if PLATFORM_HAS_BUTTON
  printf("Button state:\t%s (pressed %lu times)\n",
    button_sensor.value(0) ? "Released" : "Pressed", _button_pressed);
#endif /* PLATFORM_HAS_BUTTON */

  printf("Radio (RSSI):%d dBm\n", radio_sensor.value(RADIO_PARAM_RSSI));
  printf("Radio (SNR): %d dB\n", radio_sensor.value(RADIO_PARAM_LAST_SNR));

#if PLATFORM_HAS_BATTERY_SENSOR
  /* It was configured using channel 2, so ADC conversion value comes second */
  printf("Battery level: %d\n", battery_sensor.value(0));
#endif /* PLATFORM_HAS_BATTERY_SENSOR */

  printf("\n");
}
/*---------------------------------------------------------------------------*/
static void
sensors_trigger(void)
{
  /* Activate sensors */
#if PLATFORM_HAS_BATTERY_SENSOR
  SENSORS_ACTIVATE(battery_sensor);
#endif /* PLATFORM_HAS_BATTERY_SENSOR */


#if PLATFORM_HAS_BATTERY_SENSOR
  /* Trigger conversion mode */
  SOC_ADC_SENSORS_TRIGGER_CONVERSION(1);
#endif /* PLATFORM_HAS_BATTERY_SENSOR & PLATFORM_HAS_PHOTOINTERRUPTER_SENSOR */

  /* Show sensors state */
  sensors_state();

  /* Deactivate sensors */
#if PLATFORM_HAS_BATTERY_SENSOR
  SENSORS_DEACTIVATE(battery_sensor);
#endif /* PLATFORM_HAS_BATTERY_SENSOR */
}
/*---------------------------------------------------------------------------*/
PROCESS(sensor_demo_process, "Sensor demo process");
AUTOSTART_PROCESSES(&sensor_demo_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(sensor_demo_process, ev, data)
{
  static struct etimer etimer;
  uint8_t i;
  PROCESS_BEGIN();
  PROCESS_PAUSE();
#if PLATFORM_HAS_BUTTON
  /* Activate button sensor */
  SENSORS_ACTIVATE(button_sensor);
#endif /* PLATFORM_HAS_BUTTON */

  /* Activate radio sensor */
  SENSORS_ACTIVATE(radio_sensor);

#if PLATFORM_HAS_BATTERY_SENSOR
  soc_adc.configure(SENSORS_HW_INIT, ADC_Channel_2);
  /* Alternatively */
  /* battery_sensor.configure(SENSORS_HW_INIT, 1); */
#endif /* PLATFORM_HAS_BATTERY_SENSOR */

  etimer_set(&etimer, PRINT_INTERVAL);
  while(1) {
    PROCESS_WAIT_EVENT();
#if PLATFORM_HAS_BUTTON
    if(ev == sensors_event && data == &button_sensor) {
      if(button_sensor.value(BUTTON_SENSOR_VALUE_TYPE_LEVEL) ==
         BUTTON_SENSOR_PRESSED_LEVEL) {
        printf("Sensor event detected: Button Pressed.\n\n");
        printf("Toggling Leds\n");
        _button_pressed++;
        leds_toggle(LEDS_ALL);
        /* Trigger sensors */
        sensors_trigger();
      }
    }
    else
#endif /* PLATFORM_HAS_BUTTON */
    {
      if (ev == PROCESS_EVENT_TIMER)
      {
        /* Trigger sensors */
        sensors_trigger();
        /* Reset timer */
        etimer_reset(&etimer);
      }
    }
#if SOC_ADC_WITH_DMA_TRANSFER
    else if(ev == PROCESS_EVENT_SOC_ADC_DMA_EOT) {
      /* Print first eight samples stored in DMA buffer */
      for (i = 0; i < 8; i++) {
        printf("%i: %u\n", i, soc_adc_dma_values[i]);
      }
    }
#endif /* SOC_ADC_WITH_DMA_TRANSFER */
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
