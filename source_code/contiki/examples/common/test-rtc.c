/**
 * \file
 *         An example of how to use the Real Time Clock (RTC) on
 *         the NZ32 platform.
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "contiki.h"
#include "button-sensor.h"
#include "soc-rtc.h"
#include "dev/leds.h"

//#define DATE "04 07 07 2016 13 19 09 PM"
#ifndef DATE
#define DATE "Unknown"
#endif
/*---------------------------------------------------------------------------*/
PROCESS(test_rtc_process, "Test RTC");
AUTOSTART_PROCESSES(&test_rtc_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(test_rtc_process, ev, data)
{
	static char *next;
  static uint8_t res = RTC_TIME_RESULT_OK;
	RTC_TIME calendar;

  PROCESS_BEGIN();

  /* Get the system date in the following format: wd dd mm yy hh mm ss locale*/
  printf("RTC test, system date: %s\n", DATE);

  /* Sanity check */
  if(strcmp("Unknown", DATE) == 0) {
    printf("Fail: could not retrieve date from system\n");
    leds_toggle(LEDS_RED);
    PROCESS_EXIT();
  }

  /* Initialize the RTC clock */
  soc_rtc_init();

  /* Initialize the settings to operate the RTC in calendar mode */
  calendar.dow    = (uint8_t) strtol(DATE, &next, 10);
  calendar.day  	= (uint8_t) strtol(next, &next, 10);
  calendar.month  = (uint8_t) strtol(next, &next, 10);
  //calendar.year   = (uint16_t)strtol(next, &next, 10)  % 100;	//Extract two last digits
  calendar.year   = (uint16_t)strtol(next, &next, 10);
  calendar.hour   = (uint8_t) strtol(next, &next, 10);
  calendar.minute = (uint8_t) strtol(next, &next, 10);
  calendar.second = (uint8_t) strtol(next, &next, 10);
 	calendar.h12 		= strcmp(next, "PM") ? RTC_H12_PM : RTC_H12_AM;

 	/* Set time */
  RTC_SET_TIME(&calendar, &res)
 	if(res == RTC_TIME_RESULT_ERROR) {
		printf("Fail: could not set date\n");
    leds_toggle(LEDS_RED);
    PROCESS_EXIT();
 	}

  /* Activate button sensor */
  SENSORS_ACTIVATE(button_sensor);

  while(1) {
    RTC_TIME now;

    PROCESS_WAIT_EVENT_UNTIL(ev == sensors_event &&
			     data == &button_sensor);

    RTC_GET_TIME(&now, &res);
    printf("%04i-%02i-%02i %02i:%02i:%02i\n",
                now.year, now.month, now.day,
                now.hour, now.minute, now.second);
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
