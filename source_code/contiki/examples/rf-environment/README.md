Non-intrusive monitoring of the RF-environment
==============================================

rf_environment runs the clear channel assessment (CCA) test over
all LoRa channels and reports stats per channel. The CCA test
is run for different CCA thresholds from -60dBm to -90dBm. CCA is
a non-destructive for the rf-environment as it's just listens.
Best and worst channel is printed as average rf activity.
See example below from David Rodenas-Herraiz. 2017-06-22
Originally developed for the Atmel avr-rss2
platform.

Probability for not passing a CCA check in percent per channel.
Of course this just snapshots to illustrate functionality

<pre>
						 Band:  868 MHz
				--------------------------------------------------------------------------
        Chan(MHz):  865.20  865.50  865.80  866.10  866.40  866.70  867.00  868.00
        --------------------------------------------------------------------------
cca_thresh=-90dBm   0   		0   		0   		2   		0   		0   		0   		2 		Best=865.20 Worst=866.10 Ave=0.29
cca_thresh=-88dBm   0   		0   		0   		0   		0   		2   		0   		0 		Best=865.20 Worst=866.70 Ave=0.15
cca_thresh=-86dBm   0   		0   		0   		2  			0   		0   		0   		0 		Best=865.20 Worst=866.10 Ave=0.15
cca_thresh=-84dBm   0   		0   		0   		0   		0   		0   		1   		2 		Best=865.20 Worst=868.00 Ave=0.18
cca_thresh=-82dBm   0   		0   		0   		2   		0   		0   		0   		0 		Best=865.20 Worst=866.10 Ave=0.16
cca_thresh=-80dBm   0   		0   		0   		0   		0   		0  		 	1   		0 		Best=865.20 Worst=867.00 Ave=0.03
cca_thresh=-78dBm   0   		0   		0   		2   		0   		0   		0   		2 		Best=865.20 Worst=866.10 Ave=0.31
cca_thresh=-76dBm   0   		0   		0   		0   		0   		0   		1   		0 		Best=865.20 Worst=867.00 Ave=0.02
cca_thresh=-74dBm   0   		0   		0   		0   		1  			0   		0   		0 		Best=865.20 Worst=866.40 Ave=0.09
cca_thresh=-72dBm   0   		0   		0   		0   		0   		0   		1   		2 		Best=865.20 Worst=868.00 Ave=0.17
cca_thresh=-70dBm   0   		0   		0   		0   		1   		0   		0   		0 		Best=865.20 Worst=866.40 Ave=0.06
cca_thresh=-68dBm   0   		0   		0   		0   		0   		0   		0   		2 		Best=865.20 Worst=868.00 Ave=0.15
cca_thresh=-66dBm   0   		0   		2   		0   		1   		0   		0   		2 		Best=865.20 Worst=866.40 Ave=0.36
cca_thresh=-64dBm   0   		0   		1   		0   		0   		0   		0   		2 		Best=865.20 Worst=868.00 Ave=0.20
cca_thresh=-62dBm   0   		0   		0   		0   		1   		0   		0   		0 		Best=865.20 Worst=866.40 Ave=0.02
cca_thresh=-60dBm   0   		0   		1   		0   		0   		0   		0   		3 		Best=865.20 Worst=868.00 Ave=0.37

						 Band:  900 MHz
				------------------------------------------------------------------------------------------------------------------
        Chan(MHz):  903.08  905.24  907.40  909.56  911.72  913.88  916.04  918.20  920.36  922.52  924.68  926.84  915.00
        ------------------------------------------------------------------------------------------------------------------
cca_thresh=-90dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		1   		0   		1   		0 		Best=903.08 Worst=922.52 Ave=0.01
cca_thresh=-88dBm   0   		0   		0   		0   		0  			0  		 	0   		0   		0   		0   		0   		1   		0 		Best=903.08 Worst=926.84 Ave=0.01
cca_thresh=-86dBm   0   		0   		0   		0   		0   		0   		0   		0   		1   		0   		1   		0   		0 		Best=903.08 Worst=920.36 Ave=0.01
cca_thresh=-84dBm   0   		0   		0   		0   		0   		0   		1   		0   		0   		0   		0   		0   		0 		Best=903.08 Worst=916.04 Ave=0.00
cca_thresh=-82dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		1 		Best=903.08 Worst=915.00 Ave=0.00
cca_thresh=-80dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0 		Best=903.08 Worst=903.08 Ave=0.00
cca_thresh=-78dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		1   		1   		0   		0 		Best=903.08 Worst=922.52 Ave=0.02
cca_thresh=-76dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0 		Best=903.08 Worst=903.08 Ave=0.00
cca_thresh=-74dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0 		Best=903.08 Worst=903.08 Ave=0.00
cca_thresh=-72dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0 		Best=903.08 Worst=903.08 Ave=0.00
cca_thresh=-70dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0 		Best=903.08 Worst=903.08 Ave=0.00
cca_thresh=-68dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0 		Best=903.08 Worst=903.08 Ave=0.00
cca_thresh=-66dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0 		Best=903.08 Worst=903.08 Ave=0.00
cca_thresh=-64dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0 		Best=903.08 Worst=903.08 Ave=0.00
cca_thresh=-62dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0 		Best=903.08 Worst=903.08 Ave=0.00
cca_thresh=-60dBm   0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0   		0 		Best=903.08 Worst=903.08 Ave=0.00
</pre>