/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "dev/radio.h"
#include "net/netstack.h"
#include "net/packetbuf.h"
#include "sys/process.h"
#include "sys/etimer.h"
#include <dev/watchdog.h>
#include "dev/leds.h"
#include "dev/radio.h"
#include "net/rime/rime.h"
#include <string.h>
#include <stdint.h>
#include <stdio.h>

#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif
/*---------------------------------------------------------------------------*/
#define ABC_DATA_CHANNEL          ( 147 )
/*---------------------------------------------------------------------------*/
struct rf_consts {
  radio_value_t txpower_min;
  radio_value_t txpower_max;
};

static struct rf_consts consts;
static radio_value_t value;
/*---------------------------------------------------------------------------*/
static void OnRadioTxDone( void );
static void OnRadioRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
static void OnRadioRxError( void );
static void OnRadioTxTimeout( void );
static void OnRadioRxTimeout( void );
/*---------------------------------------------------------------------------*/
static void
OnRadioTxDone( void )
{
  NETSTACK_RADIO.off();
}
/*---------------------------------------------------------------------------*/
static void
OnRadioRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
  NETSTACK_RADIO.off();
}
/*---------------------------------------------------------------------------*/
static void
OnRadioRxError( void )
{
  NETSTACK_RADIO.off();
}
/*---------------------------------------------------------------------------*/
static void
OnRadioTxTimeout( void )
{
  NETSTACK_RADIO.off();
}
/*---------------------------------------------------------------------------*/
static void
OnRadioRxTimeout( void )
{
  NETSTACK_RADIO.off();
}
/*---------------------------------------------------------------------------*/
static radio_result_t
get_param(radio_param_t param, radio_value_t *value)
{
  radio_result_t rv;

  rv = NETSTACK_RADIO.get_value(param, value);

  switch(rv) {
  case RADIO_RESULT_ERROR:
    printf("Radio returned an error\n");
    break;
  case RADIO_RESULT_INVALID_VALUE:
    printf("Value %d is invalid\n", *value);
    break;
  case RADIO_RESULT_NOT_SUPPORTED:
    printf("Param %u not supported\n", param);
    break;
  case RADIO_RESULT_OK:
    break;
  default:
    printf("Unknown return value\n");
    break;
  }

  return rv;
}
/*---------------------------------------------------------------------------*/
static radio_result_t
set_param(radio_param_t param, radio_value_t value)
{
  radio_result_t rv;

  rv = NETSTACK_RADIO.set_value(param, value);

  switch(rv) {
  case RADIO_RESULT_ERROR:
    printf("Radio returned an error\n");
    break;
  case RADIO_RESULT_INVALID_VALUE:
    printf("Value %d is invalid\n", value);
    break;
  case RADIO_RESULT_NOT_SUPPORTED:
    printf("Param %u not supported\n", param);
    break;
  case RADIO_RESULT_OK:
    break;
  default:
    printf("Unknown return value\n");
    break;
  }

  return rv;
}
/*---------------------------------------------------------------------------*/
static void
get_rf_consts(void)
{
  PRINTF("====================================\n");
  PRINTF("RF Constants\n");

  PRINTF("Min TX Power: ");
  if(get_param(RADIO_CONST_TXPOWER_MIN, &consts.txpower_min) == RADIO_RESULT_OK) {
    PRINTF("%3d dBm\n", consts.txpower_min);
  }

  PRINTF("Max TX Power: ");
  if(get_param(RADIO_CONST_TXPOWER_MAX, &consts.txpower_max) == RADIO_RESULT_OK) {
    PRINTF("%3d dBm\n", consts.txpower_max);
  }
}
/*---------------------------------------------------------------------------*/
static void
recv_abc(struct abc_conn *ptr)
{
}
/*---------------------------------------------------------------------------*/
static void
sent_abc(struct abc_conn *ptr, int status, int num_tx)
{
  PRINTF("Message sent\n");
}
/*---------------------------------------------------------------------------*/
static const struct abc_callbacks abc_call = {recv_abc, sent_abc};
static struct abc_conn abc;
/*---------------------------------------------------------------------------*/
PROCESS(rf_tx_power_process, "rf_tx_power process");
AUTOSTART_PROCESSES(&rf_tx_power_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(rf_tx_power_process, ev, data)
{
  static struct etimer et;
  static int txPowerdBm;
  PROCESS_BEGIN();

  radio_callbacks.tx_done    = OnRadioTxDone;
  radio_callbacks.rx_done    = OnRadioRxDone;
  radio_callbacks.rx_error   = OnRadioRxError;
  radio_callbacks.tx_timeout = OnRadioTxTimeout;
  radio_callbacks.rx_timeout = OnRadioRxTimeout;

  get_rf_consts();

  /* Open abc rime connection */
  abc_open( &abc, ABC_DATA_CHANNEL, &abc_call );

  etimer_set(&et, 5 * CLOCK_SECOND);

  txPowerdBm = consts.txpower_min;
  while(1)
  {
    PROCESS_YIELD();
    if (ev == PROCESS_EVENT_TIMER)
    {
      set_param(RADIO_PARAM_TXPOWER, txPowerdBm);

      if(++txPowerdBm > consts.txpower_max)
      {
        txPowerdBm = consts.txpower_min;
      }

      /* Create data message */
      packetbuf_clear();
      packetbuf_set_datalen(sprintf(packetbuf_dataptr(), "Hello"));
      abc_send( &abc );

      etimer_reset(&et);
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/