/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "dev/radio.h"
#include "net/netstack.h"
#include "net/packetbuf.h"
#include "sys/process.h"
#include "sys/etimer.h"
#include <dev/watchdog.h>
#include "dev/leds.h"
#include "sx1272.h"

#include <string.h>
#include <stdint.h>
#include <stdio.h>

#define SAMPLES 1000

#define DEBUG 1
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

PROCESS(rf_scan_process, "rf_scan process");
AUTOSTART_PROCESSES(&rf_scan_process);

/*
   rf_environment runs clear channel assessment (CCA) test for over
   all 802.15.4 channels and reports stats per channel. The CCA test
   is run for different CCA thresholds from -60 to -190 dBm. CCA is a
   non-destructive for the rf-environment it's just listens.

   Best and worst channel is printed as average rf activity.

   Originally developed for the Atmel avr-rss2 platform.

 */

static struct etimer et;
static int cca_868[LORA_CHANNELS_868_LENGTH], cca_900[LORA_CHANNELS_900_LENGTH], cca_thresh, chan, i, j, k;
static uint16_t best, best_sum;
static uint16_t worst, worst_sum;
static double ave;

static uint32_t lora_channels_868[LORA_CHANNELS_868_LENGTH] = LORA_CHANNELS_868;
static uint32_t lora_channels_900[LORA_CHANNELS_900_LENGTH] = LORA_CHANNELS_900;

static radio_value_t
get_chan(void)
{
  radio_value_t chan;
  if(NETSTACK_RADIO.get_value(RADIO_PARAM_CHANNEL, &chan) ==
     RADIO_RESULT_OK) {
    return chan;
  }
  return 0;
}
static void
set_chan(uint32_t chan)
{
  if(NETSTACK_RADIO.set_value(RADIO_PARAM_CHANNEL, chan) ==
     RADIO_RESULT_OK) {
  }
}
static radio_value_t
get_chan_min(void)
{
  radio_value_t chan;
  if(NETSTACK_RADIO.get_value(RADIO_CONST_CHANNEL_MIN, &chan) ==
     RADIO_RESULT_OK) {
    return chan;
  }
  return 0;
}
static radio_value_t
get_chan_max(void)
{
  radio_value_t chan;

  if(NETSTACK_RADIO.get_value(RADIO_CONST_CHANNEL_MAX, &chan) ==
     RADIO_RESULT_OK) {
    return chan;
  }
  return 0;
}
static radio_value_t
get_cca_thresh(void)
{
  radio_value_t cca;

  if(NETSTACK_RADIO.get_value(RADIO_PARAM_CCA_THRESHOLD, &cca) ==
     RADIO_RESULT_OK) {
    return cca;
  }
  return 0;
}
static radio_value_t
set_cca_thresh(radio_value_t thresh)
{
  if(NETSTACK_RADIO.set_value(RADIO_PARAM_CCA_THRESHOLD, thresh) ==
     RADIO_RESULT_OK) {
    return RADIO_RESULT_OK;
  }
  return 0;
}

void
do_all_chan_868_cca(int *cca, int try)
{
  int j;
  for(j = 0 ; j < LORA_CHANNELS_868_LENGTH ; j++)
  {
    set_chan(lora_channels_868[j]);

    cca[j] = 0;
    NETSTACK_RADIO.on();
    for(i = 0 ; i < try ; i++)
    {
#ifdef CONTIKI_TARGET_NZ32
      watchdog_periodic();
#endif
      cca[j] += NETSTACK_RADIO.channel_clear();
    }
    NETSTACK_RADIO.off();
  }
}

void
do_all_chan_900_cca(int *cca, int try)
{
  int j;
  for(j = 0 ; j < LORA_CHANNELS_900_LENGTH ; j++)
  {
    set_chan(lora_channels_900[j]);

    cca[j] = 0;
    NETSTACK_RADIO.on();
    for(i = 0 ; i < try ; i++)
    {
#ifdef CONTIKI_TARGET_NZ32
      watchdog_periodic();
#endif
      cca[j] += NETSTACK_RADIO.channel_clear();
    }
    NETSTACK_RADIO.off();
  }
}

PROCESS_THREAD(rf_scan_process, ev, data)
{
  PROCESS_BEGIN();
  PROCESS_PAUSE();

  leds_init();
  leds_on(LEDS_RED);
  leds_on(LEDS_YELLOW);

  printf("Chan min=%d Hz\n", get_chan_min());
  chan = get_chan();
  printf("Chan cur=%d Hz\n", chan);
  printf("Chan max=%d Hz\n", get_chan_max());
  cca_thresh = get_cca_thresh();
  printf("Default CCA thresh=%d\n", cca_thresh);

  etimer_set(&et, CLOCK_SECOND / 2);

  while(1) {

    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

    printf("\nBand: 868 MHz\n");
    for(k = -90; k <= -60; k += 2) {
      set_cca_thresh(k);

      do_all_chan_868_cca(cca_868, SAMPLES);

      printf("cca_thresh=%-3ddBm", get_cca_thresh());

      worst = 0;
      worst_sum = 0xFFFF;
      best = 0;
      best_sum = 0;
      ave = 0;

      for(j = 0 ; j < LORA_CHANNELS_868_LENGTH ; j++) {
        ave += cca_868[j];
        printf(" %3d", 100 - (100 * cca_868[j]) / SAMPLES);
        if(cca_868[j] > best_sum) {
          best_sum = cca_868[j];
          best = j;
        }
        if(cca_868[j] < worst_sum) {
          worst_sum = cca_868[j];
          worst = j;
        }
      }

      ave = 100 - (100 * (ave / LORA_CHANNELS_868_LENGTH) / SAMPLES);
      printf(" Best=%lu Worst=%lu Ave=%ld.%02u\n",
        lora_channels_868[best],
        lora_channels_868[worst],
        (long)ave, (unsigned)((ave - floor(ave)) * 100));
    }

    printf("\nBand: 900 MHz\n");
    for(k = -90; k <= -60; k += 2) {
      set_cca_thresh(k);

      do_all_chan_900_cca(cca_900, SAMPLES);

      printf("cca_thresh=%-3ddBm", get_cca_thresh());

      worst = 0;
      worst_sum = 0xFFFF;
      best = 0;
      best_sum = 0;
      ave = 0;

      for(j = 0 ; j < LORA_CHANNELS_900_LENGTH ; j++) {
        ave += cca_900[j];
        printf(" %3d", 100 - (100 * cca_900[j]) / SAMPLES);
        if(cca_900[j] > best_sum) {
          best_sum = cca_900[j];
          best = j;
        }
        if(cca_900[j] < worst_sum) {
          worst_sum = cca_900[j];
          worst = j;
        }
      }

      ave = 100 - (100 * (ave / LORA_CHANNELS_900_LENGTH) / SAMPLES);
      printf(" Best=%lu Worst=%lu Ave=%ld.%02u\n",
        lora_channels_900[best],
        lora_channels_900[worst],
        (long)ave, (unsigned)((ave - floor(ave)) * 100));
    }

    etimer_set(&et, CLOCK_SECOND / 2);
  }
  PROCESS_END();
}
