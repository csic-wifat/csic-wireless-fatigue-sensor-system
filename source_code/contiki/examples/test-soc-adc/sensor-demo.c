/**
 * \file
 *         An example of how to use the STM32L151's system-on-chip 16-bit ADC
 *         available on NZ32 platform.
 *         The ADC channel one is configured in single-triggering mode to
 *         sample data every 96 cycles (ADC conversion frequency of ~167KHz)
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include <stdio.h>
#include <stdlib.h>
#include "dev/leds.h"
#include "dev/soc-adc.h"
#include "dev/serial-line.h"

#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"

#define DEFAULT_SAMPLE_INTERVAL   ( 1 )

#define VREF                      ( 3300 )  // mV
/*---------------------------------------------------------------------------*/
static float
sensors_state(void)
{
  /* Read ADC conversion result */
  uint16_t adc_value = (uint16_t)soc_adc.value(SOC_ADC_VALUE);
  /* Convert the adc value into a voltage value*/
  float adc_volt = (float)adc_value * ((float) VREF / (float)SOC_ADC_MAX_U16_VALUE);
  /* Return measured voltage */
  return adc_volt;
}
/*---------------------------------------------------------------------------*/
static void
sensors_trigger(void)
{
  RTC_TIME current_time;
  uint8_t res = RTC_TIME_RESULT_OK;

  /* Get current RTC time */
  RTC_GET_TIME(&current_time, &res);

  /* Activate adc */
  SENSORS_ACTIVATE(soc_adc);

  /* Trigger single conversion */
  SOC_ADC_SENSORS_TRIGGER_CONVERSION(1);

  /* Show sensors state */
  printf("%04i-%02i-%02i %02i:%02i:%02i.%u,",
                current_time.year,
                current_time.month,
                current_time.day,
                current_time.hour,
                current_time.minute,
                current_time.second,
                (unsigned int) soc_rtc_get_milliseconds_from_subseconds(&current_time));

  PRINTFLOAT(" ", sensors_state(),  ",");
  PRINTFLOAT(" ", sensors_state(),  "\n");

  /* Deactivate adc */
  SENSORS_DEACTIVATE(soc_adc);
}
/*---------------------------------------------------------------------------*/
PROCESS(sensor_demo_process, "Sensor demo process");
AUTOSTART_PROCESSES(&sensor_demo_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(sensor_demo_process, ev, data)
{
  static struct etimer etimer;
  static float sample_interval = DEFAULT_SAMPLE_INTERVAL;

  PROCESS_BEGIN();
  PROCESS_PAUSE();

  /* Enable sequencer to read from several channels */
  soc_adc.configure(SOC_ADC_PARAM_SCAN_MODE, ENABLE);

  /* Since the ADC is is clocked by a HSI (16MHz) frequency, and the sampling
   * time is 96 cycles, so the ADC conversion frequency (fADCconv) is
   * 16MHz / 96 ~= 167KHz.
   */
  soc_adc.configure(SOC_ADC_PARAM_SAMPLE_TIME, ADC_SampleTime_96Cycles);

  /* ADC channel 0 and 1 (PA0 and PA1) */
  soc_adc.configure(SENSORS_HW_INIT, ADC_Channel_0);
  soc_adc.configure(SENSORS_HW_INIT, ADC_Channel_1);

  /* Infinite loop */
  while(1)
  {
    static volatile uint8_t sample_init;

    PROCESS_YIELD();
    if(ev == serial_line_event_message)
    {
      if(strncmp((char *)data, "help", 4) == 0)
      {
        printf("\n\n");
        printf("Available commands:\n");
        printf("  help              -- Shows this help\n");
        printf("  start             -- Starts data acquisition\n");
        printf("  stop              -- Stops data acquisition\n");
        printf("  sample <interval> -- Get/Set sample interval (minimum <0.1> sec)\n");
        printf("\n\n");
      }
      else if(strncmp((char *)data, "start", 5) == 0)
      {
        if(!sample_init)
        {
          /* Set periodic timer */
          etimer_set(&etimer, sample_interval * CLOCK_SECOND);
          sample_init = 1;

          PRINTFLOAT("Starting sampling every ", sample_interval, " seconds\n");
        }
      }
      else if(strncmp((char *)data, "stop", 5) == 0)
      {
        if(sample_init)
        {
          etimer_stop(&etimer);
          sample_init = 0;
          printf("Sampling stopped\n");
        }
      }
      else if(strncmp((char *)data, "sample", 6) == 0)
      {
        if(!sample_init)
        {
          char *p = (char *)data + 6;
          //uint16_t sample_t = (uint16_t)strtol(p, &p, 10);
          float sample_t = (float)strtof(p, &p);
          if(sample_t >= 0.1)
          {
            sample_interval = sample_t;
          }
          PRINTFLOAT("Sampling interval configured to ", sample_interval, " seconds\n");
        }
        else
        {
          printf("Please type 'stop' to interrupt current data acquisition\n");
        }
      }
    }
    if (ev == PROCESS_EVENT_TIMER)
    {
      /* Trigger sensors */
      sensors_trigger();

      /* Reset timer */
      if(sample_init)
      {
        etimer_reset(&etimer);
      }
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
