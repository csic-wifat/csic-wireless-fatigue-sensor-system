/*
 * Copyright (c) 2014, George Oikonomou (george@contiki-os.org)
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/**
 *   Example project demonstrating the extended RF API functionality
 */
#include "contiki.h"
#include "net/netstack.h"
#include "dev/radio.h"
#include "platform-conf.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*---------------------------------------------------------------------------*/
struct rf_consts {
  radio_value_t channel_min;
  radio_value_t channel_max;
  radio_value_t txpower_min;
  radio_value_t txpower_max;
};

static struct rf_consts consts;

static radio_value_t value;
static uint8_t ext_addr[8];
/*---------------------------------------------------------------------------*/
#define  S_TO_US(x)   x * 1000000
#define  US_TO_S(x)   x / 1000000
/*---------------------------------------------------------------------------*/
#define RADIO_PARAM_ON      1
#define RADIO_PARAM_OFF     0
/*---------------------------------------------------------------------------*/
PROCESS(extended_rf_api_process, "Extended RF API demo process");
AUTOSTART_PROCESSES(&extended_rf_api_process);
/*---------------------------------------------------------------------------*/
static void
print_64bit_addr(const uint8_t *addr)
{
  unsigned int i;
  for(i = 0; i < 7; i++) {
    printf("%02x:", addr[i]);
  }
  printf("%02x (network order)\n", addr[7]);
}
/*---------------------------------------------------------------------------*/
static radio_result_t
get_object(radio_param_t param, void *dest, size_t size)
{
  radio_result_t rv;

  rv = NETSTACK_RADIO.get_object(param, dest, size);

  switch(rv) {
  case RADIO_RESULT_ERROR:
    printf("Radio returned an error\n");
    break;
  case RADIO_RESULT_INVALID_VALUE:
    printf("Value is invalid\n");
    break;
  case RADIO_RESULT_NOT_SUPPORTED:
    printf("Param %u not supported\n", param);
    break;
  case RADIO_RESULT_OK:
    break;
  default:
    printf("Unknown return value\n");
    break;
  }

  return rv;
}
/*---------------------------------------------------------------------------*/
static radio_result_t
set_object(radio_param_t param, void *src, size_t size)
{
  radio_result_t rv;

  rv = NETSTACK_RADIO.set_object(param, src, size);

  switch(rv) {
  case RADIO_RESULT_ERROR:
    printf("Radio returned an error\n");
    break;
  case RADIO_RESULT_INVALID_VALUE:
    printf("Value is invalid\n");
    break;
  case RADIO_RESULT_NOT_SUPPORTED:
    printf("Param %u not supported\n", param);
    break;
  case RADIO_RESULT_OK:
    break;
  default:
    printf("Unknown return value\n");
    break;
  }

  return rv;
}
/*---------------------------------------------------------------------------*/
static radio_result_t
get_param(radio_param_t param, radio_value_t *value)
{
  radio_result_t rv;

  rv = NETSTACK_RADIO.get_value(param, value);

  switch(rv) {
  case RADIO_RESULT_ERROR:
    printf("Radio returned an error\n");
    break;
  case RADIO_RESULT_INVALID_VALUE:
    printf("Value %d is invalid\n", *value);
    break;
  case RADIO_RESULT_NOT_SUPPORTED:
    printf("Param %u not supported\n", param);
    break;
  case RADIO_RESULT_OK:
    break;
  default:
    printf("Unknown return value\n");
    break;
  }

  return rv;
}
/*---------------------------------------------------------------------------*/
static radio_result_t
set_param(radio_param_t param, radio_value_t value)
{
  radio_result_t rv;

  rv = NETSTACK_RADIO.set_value(param, value);

  switch(rv) {
  case RADIO_RESULT_ERROR:
    printf("Radio returned an error\n");
    break;
  case RADIO_RESULT_INVALID_VALUE:
    printf("Value %d is invalid\n", value);
    break;
  case RADIO_RESULT_NOT_SUPPORTED:
    printf("Param %u not supported\n", param);
    break;
  case RADIO_RESULT_OK:
    break;
  default:
    printf("Unknown return value\n");
    break;
  }

  return rv;
}
/*---------------------------------------------------------------------------*/
static void
get_rf_consts(void)
{
  printf("====================================\n");
  printf("RF Constants\n");
  printf("Min Channel : ");
  if(get_param(RADIO_CONST_CHANNEL_MIN, &consts.channel_min) == RADIO_RESULT_OK) {
    printf("%3d\n", consts.channel_min);
  }

  printf("Max Channel : ");
  if(get_param(RADIO_CONST_CHANNEL_MAX, &consts.channel_max) == RADIO_RESULT_OK) {
    printf("%3d\n", consts.channel_max);
  }

  printf("Min TX Power: ");
  if(get_param(RADIO_CONST_TXPOWER_MIN, &consts.txpower_min) == RADIO_RESULT_OK) {
    printf("%3d dBm\n", consts.txpower_min);
  }

  printf("Max TX Power: ");
  if(get_param(RADIO_CONST_TXPOWER_MAX, &consts.txpower_max) == RADIO_RESULT_OK) {
    printf("%3d dBm\n", consts.txpower_max);
  }
}
/*---------------------------------------------------------------------------*/
static void
test_off_on(void)
{
  printf("====================================\n");
  printf("Power mode Test: Off, then On\n");

  printf("Power mode is  : ");
  if(get_param(RADIO_PARAM_POWER_MODE, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_POWER_MODE_ON) {
      printf("On\n");
    } else if(value == RADIO_POWER_MODE_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning Off    : ");
  value = RADIO_POWER_MODE_OFF;
  set_param(RADIO_PARAM_POWER_MODE, value);
  if(get_param(RADIO_PARAM_POWER_MODE, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_POWER_MODE_ON) {
      printf("On\n");
    } else if(value == RADIO_POWER_MODE_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning On     : ");
  value = RADIO_POWER_MODE_ON;
  set_param(RADIO_PARAM_POWER_MODE, value);
  if(get_param(RADIO_PARAM_POWER_MODE, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_POWER_MODE_ON) {
      printf("On\n");
    } else if(value == RADIO_POWER_MODE_OFF) {
      printf("Off\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_channels(void)
{
  int i;

  printf("====================================\n");
  printf("Channel Test: [%u , %u]\n", consts.channel_min, consts.channel_max);

  for(i = consts.channel_min; i <= consts.channel_max; i++) {
    value = i;
    printf("Switch to: %d, Now: ", value);
    set_param(RADIO_PARAM_CHANNEL, value);
    if(get_param(RADIO_PARAM_CHANNEL, &value) == RADIO_RESULT_OK) {
      printf("%d\n", value);
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_rx_modes(void)
{
  int i;

  printf("====================================\n");
  printf("RX Modes Test: [0 , 3]\n");

  for(i = 0; i <= 3; i++) {
    value = i;
    printf("Switch to: %d, Now: ", value);
    set_param(RADIO_PARAM_RX_MODE, value);
    if(get_param(RADIO_PARAM_RX_MODE, &value) == RADIO_RESULT_OK) {
      printf("Address Filtering is ");
      if(value & RADIO_RX_MODE_ADDRESS_FILTER) {
        printf("On, ");
      } else {
        printf("Off, ");
      }
      printf("Auto ACK is ");
      if(value & RADIO_RX_MODE_AUTOACK) {
        printf("On, ");
      } else {
        printf("Off, ");
      }

      printf("(value=%d)\n", value);
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_tx_powers(void)
{
  int i;

  printf("====================================\n");
  printf("TX Power Test: [%d , %d]\n", consts.txpower_min, consts.txpower_max);

  for(i = consts.txpower_min; i <= consts.txpower_max; i += 5) {
    value = i;
    printf("Switch to: %3d dBm, Now: ", value);
    set_param(RADIO_PARAM_TXPOWER, value);
    if(get_param(RADIO_PARAM_TXPOWER, &value) == RADIO_RESULT_OK) {
      printf("%3d dBm\n", value);
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_cca_thresholds(void)
{
  printf("====================================\n");
  printf("CCA Thres. Test: -105, then -81\n");

  value = -105;
  printf("Switch to: %4d dBm, Now: ", value);
  set_param(RADIO_PARAM_CCA_THRESHOLD, value);
  if(get_param(RADIO_PARAM_CCA_THRESHOLD, &value) == RADIO_RESULT_OK) {
    printf("%4d dBm [0x%04x]\n", value, (uint16_t)value);
  }

  value = -81;
  printf("Switch to: %4d dBm, Now: ", value);
  set_param(RADIO_PARAM_CCA_THRESHOLD, value);
  if(get_param(RADIO_PARAM_CCA_THRESHOLD, &value) == RADIO_RESULT_OK) {
    printf("%4d dBm [0x%04x]\n", value, (uint16_t)value);
  }
}
/*---------------------------------------------------------------------------*/
static void
test_bandwidth(void)
{
  int i;

  printf("====================================\n");
  printf("BW Test: 125, 250 and 500\n");

  for(i = 0 ; i < 10 ; i++)
  {
    value = i;
    printf("Switch to: %s, Now: ",
      (value == 0) ? "7.8 kHz"   :
      (value == 1) ? "10.4 kHz"  :
      (value == 2) ? "15.6 kHz"  :
      (value == 3) ? "20.8 kHz"  :
      (value == 4) ? "31.2 kHz"  :
      (value == 5) ? "41.7 kHz"  :
      (value == 6) ? "62.5 kHz"  :
      (value == 7) ? "125 kHz"   :
      (value == 8) ? "250 kHz"   :
      (value == 9) ? "500 kHz"   : "Error!");


    set_param(RADIO_PARAM_BANDWIDTH, value);
    if(get_param(RADIO_PARAM_BANDWIDTH, &value) == RADIO_RESULT_OK) {
      printf("%s [0x%08lx]\n",
        (value == 0) ? "7.8 kHz"   :
        (value == 1) ? "10.4 kHz"  :
        (value == 2) ? "15.6 kHz"  :
        (value == 3) ? "20.8 kHz"  :
        (value == 4) ? "31.2 kHz"  :
        (value == 5) ? "41.7 kHz"  :
        (value == 6) ? "62.5 kHz"  :
        (value == 7) ? "125 kHz"   :
        (value == 8) ? "250 kHz"   :
        (value == 9) ? "500 kHz"   : "Error!",
        (uint32_t)value);
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_datarate(void)
{
  int i;

  printf("====================================\n");
  printf("Spreading Factor Test: [6, 12]\n");

  for(i = 6 ; i < 13 ; i++)
  {
    value = i;
    printf("Switch to: %d (%d chips), Now: ", value,
      ( value == 6) ?    64 :
      ((value == 7) ?   128 :
      ((value == 8) ?   256 :
      ((value == 9) ?   512 :
      ((value == 10) ? 1024 :
      ((value == 11) ? 2048 : 4096 ))))));
    set_param(RADIO_PARAM_DATARATE, value);
    if(get_param(RADIO_PARAM_DATARATE, &value) == RADIO_RESULT_OK) {
      printf("%d (%d chips) [0x%08lx]\n", value,
      ( value == 6) ?    64 :
      ((value == 7) ?   128 :
      ((value == 8) ?   256 :
      ((value == 9) ?   512 :
      ((value == 10) ? 1024 :
      ((value == 11) ? 2048 : 4096 ))))),
      (uint32_t)value);
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_coderate(void)
{
  int i;

  printf("====================================\n");
  printf("Code Rate Test: [1, 4]\n");

  for(i = 1 ; i < 5 ; i++)
  {
    value = i;
    printf("Switch to: %d (%s), Now: ", value,
         (value == 1) ? "4/5" :
        ((value == 2) ? "4/6" :
        ((value == 3) ? "4/7" : "4/8")));
    set_param(RADIO_PARAM_CODINGRATE, value);
    if(get_param(RADIO_PARAM_CODINGRATE, &value) == RADIO_RESULT_OK) {
      printf("%d (%s) [0x%08lx]\n", value,
         (value == 1) ? "4/5" :
        ((value == 2) ? "4/6" :
        ((value == 3) ? "4/7" : "4/8")),
        (uint32_t)value);
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_fixed_payload_length_on(void)
{
  printf("====================================\n");
  printf("Fixed Payload Length Test: Off, then On\n");

  printf("Fixed Payload Length is  : ");
  if(get_param(RADIO_PARAM_FIXED_PAYLOAD_LENGTH_ON, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning Off              : ");
  value = RADIO_PARAM_OFF;
  set_param(RADIO_PARAM_FIXED_PAYLOAD_LENGTH_ON, value);
  if(get_param(RADIO_PARAM_FIXED_PAYLOAD_LENGTH_ON, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning On               : ");
  value = RADIO_PARAM_ON;
  set_param(RADIO_PARAM_FIXED_PAYLOAD_LENGTH_ON, value);
  if(get_param(RADIO_PARAM_FIXED_PAYLOAD_LENGTH_ON, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_public_network_on(void)
{
  printf("====================================\n");
  printf("Public Network Test: Off, then On\n");

  printf("Public Network is  : ");
  if(get_param(RADIO_PARAM_PUBLIC_NETWORK, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning Off        : ");
  value = RADIO_PARAM_OFF;
  set_param(RADIO_PARAM_PUBLIC_NETWORK, value);
  if(get_param(RADIO_PARAM_PUBLIC_NETWORK, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning On         : ");
  value = RADIO_PARAM_ON;
  set_param(RADIO_PARAM_PUBLIC_NETWORK, value);
  if(get_param(RADIO_PARAM_PUBLIC_NETWORK, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_crc_on(void)
{
  printf("====================================\n");
  printf("CRC Test: Off, then On\n");

  printf("CRC is       : ");
  if(get_param(RADIO_PARAM_CRC_ON, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning Off  : ");
  value = RADIO_PARAM_OFF;
  set_param(RADIO_PARAM_CRC_ON, value);
  if(get_param(RADIO_PARAM_CRC_ON, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning On   : ");
  value = RADIO_PARAM_ON;
  set_param(RADIO_PARAM_CRC_ON, value);
  if(get_param(RADIO_PARAM_CRC_ON, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_frequency_hopping_on(void)
{
  printf("====================================\n");
  printf("Frequency Hopping Test: Off, then On\n");

  printf("Frequency Hopping is : ");
  if(get_param(RADIO_PARAM_FREQ_HOP_ON, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning Off          : ");
  value = RADIO_PARAM_OFF;
  set_param(RADIO_PARAM_FREQ_HOP_ON, value);
  if(get_param(RADIO_PARAM_FREQ_HOP_ON, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning On           : ");
  value = RADIO_PARAM_ON;
  set_param(RADIO_PARAM_FREQ_HOP_ON, value);
  if(get_param(RADIO_PARAM_FREQ_HOP_ON, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_iq_inverted_on(void)
{
  printf("====================================\n");
  printf("IQ Inverted Test: Off, then On\n");

  printf("IQ Inverted is : ");
  if(get_param(RADIO_PARAM_IQ_INVERTED, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning Off    : ");
  value = RADIO_PARAM_OFF;
  set_param(RADIO_PARAM_IQ_INVERTED, value);
  if(get_param(RADIO_PARAM_IQ_INVERTED, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning On     : ");
  value = RADIO_PARAM_ON;
  set_param(RADIO_PARAM_IQ_INVERTED, value);
  if(get_param(RADIO_PARAM_IQ_INVERTED, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_rx_continuous_on(void)
{
  printf("====================================\n");
  printf("RX Continuous Test: Off, then On\n");

  printf("RX Continuous is : ");
  if(get_param(RADIO_PARAM_RX_CONTINUOUS, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning Off      : ");
  value = RADIO_PARAM_OFF;
  set_param(RADIO_PARAM_RX_CONTINUOUS, value);
  if(get_param(RADIO_PARAM_RX_CONTINUOUS, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }

  printf("Turning On       : ");
  value = RADIO_PARAM_ON;
  set_param(RADIO_PARAM_RX_CONTINUOUS, value);
  if(get_param(RADIO_PARAM_RX_CONTINUOUS, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_PARAM_ON) {
      printf("On\n");
    } else if(value == RADIO_PARAM_OFF) {
      printf("Off\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_tx_timeout(void)
{
  int i;
  radio_value_t new_val;

  printf("====================================\n");
  printf("TX Timeout Test: Increase tx timeout progressively [0, 10]s\n");

  printf("TX Timeout is: ");
  if(get_param(RADIO_PARAM_TX_TIMEOUT, &value) == RADIO_RESULT_OK) {
    printf("%d s [0x%08lx]\n", US_TO_S(value), (uint32_t)value);
  }

  for(i = 0 ; i < 11 ; i+=2)
  {
    new_val = S_TO_US(i);  // the value needs to be given in us
    printf("Switch to: %d s [0x%08lx], Now: ", i, (uint32_t)new_val);
    set_param(RADIO_PARAM_TX_TIMEOUT, new_val);
    if(get_param(RADIO_PARAM_TX_TIMEOUT, &value) == RADIO_RESULT_OK) {
      printf("%d s [0x%08lx]\n", US_TO_S(value), (uint32_t)value);
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
test_pan_id(void)
{
  radio_value_t new_val;

  printf("====================================\n");
  printf("PAN ID Test: Flip bytes and back\n");

  printf("PAN ID is: ");
  if(get_param(RADIO_PARAM_PAN_ID, &value) == RADIO_RESULT_OK) {
    printf("0x%02x%02x\n", (value >> 8) & 0xFF, value & 0xFF);
  }

  new_val = (value >> 8) & 0xFF;
  new_val |= (value & 0xFF) << 8;
  printf("Switch to: 0x%02x%02x, Now: ", (new_val >> 8) & 0xFF, new_val & 0xFF);
  set_param(RADIO_PARAM_PAN_ID, new_val);
  if(get_param(RADIO_PARAM_PAN_ID, &value) == RADIO_RESULT_OK) {
    printf("0x%02x%02x\n", (value >> 8) & 0xFF, value & 0xFF);
  }

  new_val = (value >> 8) & 0xFF;
  new_val |= (value & 0xFF) << 8;
  printf("Switch to: 0x%02x%02x, Now: ", (new_val >> 8) & 0xFF, new_val & 0xFF);
  set_param(RADIO_PARAM_PAN_ID, new_val);
  if(get_param(RADIO_PARAM_PAN_ID, &value) == RADIO_RESULT_OK) {
    printf("0x%02x%02x\n", (value >> 8) & 0xFF, value & 0xFF);
  }
}
/*---------------------------------------------------------------------------*/
static void
test_16bit_addr(void)
{
  radio_value_t new_val;

  printf("====================================\n");
  printf("16-bit Address Test: Flip bytes and back\n");

  printf("16-bit Address is: ");
  if(get_param(RADIO_PARAM_16BIT_ADDR, &value) == RADIO_RESULT_OK) {
    printf("0x%02x%02x\n", (value >> 8) & 0xFF, value & 0xFF);
  }

  new_val = (value >> 8) & 0xFF;
  new_val |= (value & 0xFF) << 8;
  printf("Switch to: 0x%02x%02x, Now: ", (new_val >> 8) & 0xFF, new_val & 0xFF);
  set_param(RADIO_PARAM_16BIT_ADDR, new_val);
  if(get_param(RADIO_PARAM_16BIT_ADDR, &value) == RADIO_RESULT_OK) {
    printf("0x%02x%02x\n", (value >> 8) & 0xFF, value & 0xFF);
  }

  new_val = (value >> 8) & 0xFF;
  new_val |= (value & 0xFF) << 8;
  printf("Switch to: 0x%02x%02x, Now: ", (new_val >> 8) & 0xFF, new_val & 0xFF);
  set_param(RADIO_PARAM_16BIT_ADDR, new_val);
  if(get_param(RADIO_PARAM_16BIT_ADDR, &value) == RADIO_RESULT_OK) {
    printf("0x%02x%02x\n", (value >> 8) & 0xFF, value & 0xFF);
  }
}
/*---------------------------------------------------------------------------*/
static void
test_64bit_addr(void)
{
  int i;
  uint8_t new_val[8];

  printf("====================================\n");
  printf("64-bit Address Test: Invert byte order\n");

  printf("64-bit Address is: ");
  if(get_object(RADIO_PARAM_64BIT_ADDR, ext_addr, 8) == RADIO_RESULT_OK) {
    print_64bit_addr(ext_addr);
  }

  for(i = 0; i <= 7; i++) {
    new_val[7 - i] = ext_addr[i];
  }

  printf("Setting to       : ");
  print_64bit_addr(new_val);

  printf("64-bit Address is: ");
  set_object(RADIO_PARAM_64BIT_ADDR, new_val, 8);
  if(get_object(RADIO_PARAM_64BIT_ADDR, ext_addr, 8) == RADIO_RESULT_OK) {
    print_64bit_addr(ext_addr);
  }
}
/*---------------------------------------------------------------------------*/
static void
print_rf_values(void)
{
  radio_value_t modem = 2;

  printf("====================================\n");
  printf("RF Values\n");

  printf("Modem: ");
  if(get_param(RADIO_PARAM_MODEM, &modem) == RADIO_RESULT_OK) {
    if(modem == 0 || modem == 1){
      printf("%s\n", modem == MODEM_LORA? "LoRa" : "Fsk");
    } else {
      printf("Error! Modem parameter is not correctly set\n");
    }
  }

  printf("Power: ");
  if(get_param(RADIO_PARAM_POWER_MODE, &value) == RADIO_RESULT_OK) {
    if(value == RADIO_POWER_MODE_ON) {
      printf("On\n");
    } else if(value == RADIO_POWER_MODE_OFF) {
      printf("Off\n");
    }
  }

  printf("Channel: ");
  if(get_param(RADIO_PARAM_CHANNEL, &value) == RADIO_RESULT_OK) {
    printf("%d\n", value);
  }

  printf("PAN ID: ");
  if(get_param(RADIO_PARAM_PAN_ID, &value) == RADIO_RESULT_OK) {
    printf("0x%02x%02x\n", (value >> 8) & 0xFF, value & 0xFF);
  }

  printf("16-bit Address: ");
  if(get_param(RADIO_PARAM_16BIT_ADDR, &value) == RADIO_RESULT_OK) {
    printf("0x%02x%02x\n", (value >> 8) & 0xFF, value & 0xFF);
  }

  printf("64-bit Address: ");
  if(get_object(RADIO_PARAM_64BIT_ADDR, ext_addr, 8) == RADIO_RESULT_OK) {
    print_64bit_addr(ext_addr);
  }

  printf("RX Mode: ");
  if(get_param(RADIO_PARAM_RX_MODE, &value) == RADIO_RESULT_OK) {
    printf("Address Filtering is ");
    if(value & RADIO_RX_MODE_ADDRESS_FILTER) {
      printf("On, ");
    } else {
      printf("Off, ");
    }
    printf("Auto ACK is ");
    if(value & RADIO_RX_MODE_AUTOACK) {
      printf("On, ");
    } else {
      printf("Off, ");
    }

    printf("(value=%d)\n", value);
  }

  printf("TX Mode: ");
  if(get_param(RADIO_PARAM_TX_MODE, &value) == RADIO_RESULT_OK) {
    printf("%d\n", value);
  }

  printf("TX Power: ");
  if(get_param(RADIO_PARAM_TXPOWER, &value) == RADIO_RESULT_OK) {
    printf("%d dBm [0x%04x]\n", value, (uint16_t)value);
  }

  printf("CCA Threshold: ");
  if(get_param(RADIO_PARAM_CCA_THRESHOLD, &value) == RADIO_RESULT_OK) {
    printf("%d dBm [0x%04x]\n", value, (uint16_t)value);
  }

  printf("RSSI: ");
  if(get_param(RADIO_PARAM_RSSI, &value) == RADIO_RESULT_OK) {
    printf("%d dBm [0x%04x]\n", value, (uint16_t)value);
  }

  printf("SNR: ");
  if(get_param(RADIO_PARAM_LAST_SNR, &value) == RADIO_RESULT_OK) {
    printf("%d dB [0x%02x]\n", value, (int8_t)value);
  }

  printf("BW: ");
  if(get_param(RADIO_PARAM_BANDWIDTH, &value) == RADIO_RESULT_OK) {
    if(modem == MODEM_LORA) {
      switch(value){
        case 0:
          printf("7.8 kHz [0x%08lx]\n", (uint32_t)value);
          break;
        case 1:
          printf("10.4 kHz [0x%08lx]\n", (uint32_t)value);
          break;
        case 2:
          printf("15.6 kHz [0x%08lx]\n", (uint32_t)value);
          break;
        case 3:
          printf("20.8 kHz [0x%08lx]\n", (uint32_t)value);
          break;
        case 4:
          printf("31.2 kHz [0x%08lx]\n", (uint32_t)value);
          break;
        case 5:
          printf("41.7 kHz [0x%08lx]\n", (uint32_t)value);
          break;
        case 6:
          printf("62.5 kHz [0x%08lx]\n", (uint32_t)value);
          break;
        case 7:
          printf("125 kHz [0x%08lx]\n", (uint32_t)value);
          break;
        case 8:
          printf("250 kHz [0x%08lx]\n", (uint32_t)value);
          break;
        case 9:
          printf("500 kHz [0x%08lx]\n", (uint32_t)value);
          break;
        default:
          printf("Error. Bandwidth parameter is not supported (%x)\n", value);
      }
    } else {
      printf("%d Hz [0x%08lx]\n", value, (uint32_t)value);
    }
  }

  printf("Spreading factor: ");
  if(get_param(RADIO_PARAM_DATARATE, &value) == RADIO_RESULT_OK) {
    if(modem == MODEM_LORA) {
      switch(value){
        case 6:
          printf("64 chips [0x%08lx]\n", (uint32_t)value);
          break;
        case 7:
          printf("128 chips [0x%08lx]\n", (uint32_t)value);
          break;
        case 8:
          printf("256 chips [0x%08lx]\n", (uint32_t)value);
          break;
        case 9:
          printf("512 chips [0x%08lx]\n", (uint32_t)value);
          break;
        case 10:
          printf("1024 chips [0x%08lx]\n", (uint32_t)value);
          break;
        case 11:
          printf("2048 chips [0x%08lx]\n", (uint32_t)value);
          break;
        case 12:
          printf("4096 chips [0x%08lx]\n", (uint32_t)value);
          break;
        default:
          printf("Error! Spreading factor parameter is not correctly set\n");
      }
    } else {
      printf("%d bps [0x%08lx]\n", value, (uint32_t)value);
    }
  }

  printf("Code rate: ");
  if(get_param(RADIO_PARAM_CODINGRATE, &value) == RADIO_RESULT_OK) {
    switch(value){
      case 1:
        printf("4/5 [0x%02x]\n", (uint8_t)value);
        break;
      case 2:
        printf("4/6 [0x%02x]\n", (uint8_t)value);
        break;
      case 3:
        printf("4/7 [0x%02x]\n", (uint8_t)value);
        break;
      case 4:
        printf("4/8 [0x%02x]\n", (uint8_t)value);
        break;
      default:
        printf("Error. Bandwidth parameter is not supported\n");
    }
  }

  printf("AFC Bandwidth: ");
  if(get_param(RADIO_PARAM_AFC_BANDWIDTH, &value) == RADIO_RESULT_OK) {
    printf("%d Hz [0x%08lx]\n", value, (uint32_t)value);
  }

  printf("Preamble length: ");
  if(get_param(RADIO_PARAM_PREAMBLE_LENGTH, &value) == RADIO_RESULT_OK) {
    if(modem == MODEM_LORA) {
      printf("%d symbols [0x%04x]\n", value, (uint16_t)value);
    } else {
      printf("%d bytes [0x%04x]\n", value, (uint16_t)value);
    }
  }

  printf("Fixed payload length: ");
  if(get_param(RADIO_PARAM_FIXED_PAYLOAD_LENGTH_ON, &value) == RADIO_RESULT_OK) {
    printf("%s\n", value ? "on" : "off");
  }

  printf("Payload length: ");
  if(get_param(RADIO_PARAM_PAYLOAD_LENGTH, &value) == RADIO_RESULT_OK) {
    if(modem == MODEM_LORA) {
      printf("%d symbols [0x%04x]\n", value, (uint16_t)value);
    } else {
      printf("%d bytes [0x%04x]\n", value, (uint16_t)value);
    }
  }

  printf("Public network: ");
  if(get_param(RADIO_PARAM_PUBLIC_NETWORK, &value) == RADIO_RESULT_OK) {
    printf("%s\n", value ? "on" : "off");
  }

  printf("CRC: ");
  if(get_param(RADIO_PARAM_CRC_ON, &value) == RADIO_RESULT_OK) {
    printf("%s\n", value ? "on" : "off");
  }

  printf("Frequency hopping: ");
  if(get_param(RADIO_PARAM_FREQ_HOP_ON, &value) == RADIO_RESULT_OK) {
    printf("%s\n", value ? "on" : "off");
  }

  printf("Hopping period: ");
  if(get_param(RADIO_PARAM_HOP_PERIOD, &value) == RADIO_RESULT_OK) {
    printf("%d [0x%04x]\n", value, (uint16_t)value);
  }

  printf("IQ inverted: ");
  if(get_param(RADIO_PARAM_IQ_INVERTED, &value) == RADIO_RESULT_OK) {
    printf("%s\n", value ? "on" : "off");
  }

  printf("RX continuous mode: ");
  if(get_param(RADIO_PARAM_RX_CONTINUOUS, &value) == RADIO_RESULT_OK) {
    printf("%s\n", value ? "on" : "off");
  }

  printf("TX timeout: ");
  if(get_param(RADIO_PARAM_TX_TIMEOUT, &value) == RADIO_RESULT_OK) {
    printf("%d s [0x%08lx]\n", US_TO_S(value), (uint32_t)value);
  }

  printf("RX single timeout: ");
  if(get_param(RADIO_PARAM_RX_SINGLE_TIMEOUT, &value) == RADIO_RESULT_OK) {
    printf("%d s [0x%08lx]\n", value, (uint32_t)value);
  }

}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(extended_rf_api_process, ev, data)
{

  PROCESS_BEGIN();

  get_rf_consts();
  print_rf_values();

  test_off_on();
  //test_channels();
  //test_rx_modes();
  test_tx_powers();
  test_cca_thresholds();
  test_bandwidth();
  test_datarate();
  test_coderate();
  test_fixed_payload_length_on();
  test_public_network_on();
  test_crc_on();
  test_frequency_hopping_on();
  test_iq_inverted_on();
  test_rx_continuous_on();
  test_tx_timeout();
  //test_pan_id();
  //test_16bit_addr();
  //test_64bit_addr();

  printf("Done\n");

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
