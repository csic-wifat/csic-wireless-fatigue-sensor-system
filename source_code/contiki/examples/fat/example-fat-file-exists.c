/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \file
 *         Example demonstrating how to use the FAT file system.
 */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include "contiki.h"
#include "soc-rtc.h"
#include "ff.h"
/*---------------------------------------------------------------------------*/
PROCESS(example_fat_process, "FAT example");
AUTOSTART_PROCESSES(&example_fat_process);
/*---------------------------------------------------------------------------*/
#define TEST_FILENAME   "test.txt"
#define TEST_LINE       "Hello world! Again"
/*---------------------------------------------------------------------------*/
static void
usd_fail(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE );

  /* GPIO configuration */
  GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_8; // I2C SCL
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init( GPIOB, &GPIO_InitStructure );

  GPIO_WriteBit( GPIOB, GPIO_Pin_8, Bit_SET );
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(example_fat_process, ev, data)
{
  static uint8_t file_substate = 0;
  static FATFS FatFs; /* Work area (file system object) for logical drive */
  FIL fil;            /* File object */
  char line[82];      /* Line buffer */
  FRESULT fr;         /* FatFs return code */

  char filename[64];  /* File name */
  soc_rtc_td_map td;  /* RTC time struct */

  PROCESS_BEGIN();

  printf("FAT example\n");

  /* Get current date and time */
  soc_rtc_get_datetime(&td);

  /* Register work area to the default drive */
  fr = f_mount(&FatFs, "", 0);
  if(fr) {
    printf("f_mount() error: %d\n", fr);
    usd_fail();
    PROCESS_EXIT();
  }

  sprintf(filename, "log_%04u%02u%02u%02u%02u%02u.txt",
          td.year, td.month, td.day, td.hour, td.minute,
          td.second);

  /* Create the test file */
  fr = f_open(&fil, filename, FA_WRITE | FA_CREATE_NEW);

  switch(fr) {
    case FR_OK:
      break;
    case FR_DISK_ERR:
      printf("A hard error occurred in the low level disk I/O layer\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_INT_ERR:
      printf("Assertion failed\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_NOT_READY:
      printf("The physical drive cannot work\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_NO_FILE:
      printf("Could not find the file\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_NO_PATH:
      printf("Could not find the path\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_INVALID_NAME:
      printf("The path name format is invalid\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_DENIED:
      printf("Access denied due to prohibited access or directory full\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_EXIST:
      while( (fr = f_open(&fil, filename, FA_WRITE | FA_CREATE_NEW)) == FR_EXIST){
        sprintf(filename, "log_%04u%02u%02u%02u%02u%02u_%02u.txt",
                td.year, td.month, td.day, td.hour, td.minute,
                td.second, ++file_substate);
      }
      break;
    case FR_INVALID_OBJECT:
      printf("The file/directory object is invalid\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_WRITE_PROTECTED:
      printf("The physical drive is write protected\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_INVALID_DRIVE:
      printf("The logical drive number is invalid\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_NOT_ENABLED:
      printf("The volume has no work area\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_NO_FILESYSTEM:
      printf("There is no valid FAT volume\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_MKFS_ABORTED:
      printf("The f_mkfs() aborted due to any problem\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_TIMEOUT:
      printf("Could not get a grant to access the volume within defined period\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_LOCKED:
      printf("The operation is rejected according to the file sharing policy\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_NOT_ENOUGH_CORE:
      printf("LFN working buffer could not be allocated\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_TOO_MANY_OPEN_FILES:
      printf("Number of open files > _FS_LOCK\n");
      usd_fail();
      PROCESS_EXIT();
    case FR_INVALID_PARAMETER:
    default:
      printf("Given parameter is invalid\n");
      usd_fail();
      PROCESS_EXIT();
  }

  printf("Writing \"%s\" to \"%s\"\n", TEST_LINE, filename);

  /* Write the test line */
  f_printf(&fil, "%s\n", TEST_LINE);

  /* Close the file */
  f_close(&fil);

  /* Open the test file */
  printf("Reading back \"%s\":\n\n", filename);

  fr = f_open(&fil, filename, FA_READ);

  if(fr) {
    printf("f_open() error: %d\n", fr);
    usd_fail();
    PROCESS_EXIT();
  }

  /* Read all the lines and display them */
  while(f_gets(line, sizeof(line), &fil)) {
    printf(line);
  }

  /* Close the file */
  f_close(&fil);

  printf("\nDone\n");

  PROCESS_END();
}
