/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \file
 *         Example demonstrating how to use the FAT file system.
 */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include "contiki.h"
#include "soc-rtc.h"
#include "ff.h"
/*---------------------------------------------------------------------------*/
PROCESS(example_fat_process, "FAT example");
AUTOSTART_PROCESSES(&example_fat_process);
/*---------------------------------------------------------------------------*/
static FRESULT
scan_files (char* path)
{
  FRESULT res;
  DIR dir;
  UINT i;
  static FILINFO fno;
  char buff[256];

  res = f_opendir(&dir, path);                       /* Open the directory */
  if (res == FR_OK)
  {
    for (;;)
    {
      res = f_readdir(&dir, &fno);                   /* Read a directory item */
      if (res != FR_OK || fno.fname[0] == 0) break;  /* Break on error or end of dir */
      if (fno.fattrib & AM_DIR)
      {                    /* It is a directory */
        i = strlen(path);
        sprintf(&path[i], "/%s", fno.fname);
        res = scan_files(path);                    /* Enter the directory */
        if (res != FR_OK) break;
        path[i] = 0;
      }
      else
      {                                       /* It is a file. */
        memset(buff, 0, sizeof(buff));
        sprintf(buff, "%s/%s", path, fno.fname);
        printf("%s\n", buff);
        res = f_unlink(buff);
        if(res != FR_OK) printf("f_unlink() error: %d\n", res);
      }
    }
    f_closedir(&dir);
  }

  return res;
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(example_fat_process, ev, data)
{
  FATFS fs;
  FRESULT fr;
  char buff[256];

  PROCESS_BEGIN();

  fr = f_mount(&fs, "", 0);

  if(fr == FR_OK)
  {
    strcpy(buff, "/");
    fr = scan_files(buff);
  }
  else
  {
    printf("f_mount() error: %d\n", fr);
    PROCESS_EXIT();
  }

  printf("Done!\n");

  PROCESS_END();
}
