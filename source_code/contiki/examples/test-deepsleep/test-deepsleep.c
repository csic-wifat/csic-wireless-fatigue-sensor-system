/**
 * \file
 *         An example of how to use the deep sleep feature of
 *         the NZ32 platform.
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */

#include "contiki.h"
#include <stdio.h> /* For printf() */
#include "dev/watchdog.h"
#include "dev/leds.h"
#include "rtctimer.h"
#include "lpm.h"
/*---------------------------------------------------------------------------*/
static void callback( struct rtctimer *timer, void *ptr );
static void callback( struct rtctimer *timer, void *ptr ) {}
/*---------------------------------------------------------------------------*/
PROCESS(deep_sleep_process, "Deep Sleep process");
AUTOSTART_PROCESSES(&deep_sleep_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(deep_sleep_process, ev, data)
{
  static struct etimer et_hello;
  static struct rtctimer rtc;

  PROCESS_BEGIN();

  etimer_set(&et_hello, 1 * CLOCK_SECOND);

  while( 1 )
  {
    static int num_hello;
    PROCESS_WAIT_EVENT();

    if (ev == PROCESS_EVENT_TIMER) {
      printf("Hello world %d!\n", ++num_hello);

      if(num_hello > 1)
      {
        rtctimer_set(&rtc, 5000, 0, callback, NULL);
        lpm_deepsleep();
      }
      else
      {
        etimer_reset(&et_hello);
      }
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
