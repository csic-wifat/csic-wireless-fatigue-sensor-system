/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 ** -------------------------------------------------------------------------
 *
 * Author  : David Rodenas-Herraiz
 * Created : 2017-10-05
 */
/*---------------------------------------------------------------------------*/

#ifndef PROJECT_UTIL_H_
#define PROJECT_UTIL_H_

/* Headers */
#include "net/rime/rime.h"
#include "net/netstack.h"

#include <stdlib.h>
#include <stdint.h>
#include <time.h>

/* Macros and definitions */
typedef enum {
  APP_RESULT_OK = 0,
  APP_RESULT_ERROR
} app_result_t;

/* Definition of application data message */
typedef struct app_header
{
  /* Message header */
  uint16_t      seqno;      // Message sequence number
  uint16_t      datalen;    // Length of data
} app_header_t;

typedef struct app_payload
{
  /* Message payload */
  uint8_t       data[1];    // Application data
} app_payload_t;

typedef struct app_msg {
  /* Message format */
  app_header_t  hdr;        // Header
  app_payload_t payload;    // Payload
} __attribute__ ((packed)) app_msg_t;

/* Functions */

/**
 * @brief      Builds the app message to be sent.
 *
 * @param      msg      Contains the data message
 * @param      msg      Contains the length of data message
 * @param      buf      Buffer containing sensor measurements (if any)
 * @param      buflen   Length of buf
 *
 * @return     APP_RESULT_OK(0) if no error, APP_RESULT_ERROR otherwise
 */
app_result_t prepare_msg(app_msg_t **msg, uint8_t *msglen,
                         const uint8_t *buf, uint8_t buflen);

#endif /* PROJECT_UTIL_H_ */
