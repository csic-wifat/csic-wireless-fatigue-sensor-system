/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/**
 * \file
 *         A Contiki application to test loramac port
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */

#include "contiki.h"
#include "net/netstack.h"
#include "dev/leds.h"

#include "shell.h"
#include "serial-shell.h"
#include "shell-rtc.h"
#include "shell-sx127x.h"

#include "lora-radio.h"
#include "loramac-mac.h"
#include "loramac-net.h"
#include "lorawan-conf.h"

#include "project-util.h"
/*---------------------------------------------------------------------------*/
#define DEBUG 1
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...) do {} while (0)
#endif

#define SEND_INTERVAL         ( 300 * CLOCK_SECOND )
#define LORAMAC_DATA_CHANNEL  ( 146 )
/*---------------------------------------------------------------------------*/
PROCESS(lorawan_process, "LoRaWAN process");
AUTOSTART_PROCESSES(&lorawan_process);
/*---------------------------------------------------------------------------*/
static void
recv_loramac(struct loramac_conn *ptr)
{
  PRINTF( "Message received: port: %d len %d '%s'\n",
    ptr->port.portno, ptr->port.buf.datalen, (char *)ptr->port.buf.data);
}
/*---------------------------------------------------------------------------*/
static void
sent_loramac(struct loramac_conn *ptr, int status, int num_tx)
{
  PRINTF( "Message sent: port %d status %d num_tx %d\n",
    ptr->port.portno, status, num_tx );
}
/*---------------------------------------------------------------------------*/
static const struct loramac_callbacks loramac_call = {recv_loramac, sent_loramac};
static struct loramac_conn loramac;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(lorawan_process, ev, data)
{
  static struct etimer et;

  PROCESS_EXITHANDLER( loramac_close( &loramac ); )
  PROCESS_BEGIN( );

  /* Init serial shell */
  serial_shell_init( );

  /* Init rtc shell */
  shell_rtc_init( );

  /* Init sx127x shell application */
  shell_sx127x_init();

  /* Open LoRaMAC connection */
  loramac_open( &loramac, LORAMAC_DATA_CHANNEL, &loramac_call );

  /* Set transmission timer */
  etimer_set( &et, 20 * CLOCK_SECOND );
  //etimer_set( &et, SEND_INTERVAL );
  while( 1 )
  {
    PROCESS_WAIT_EVENT_UNTIL( etimer_expired(&et) );
#if WITH_THINGSPEAK
    static uint8_t seqn;
    static uint8_t temp;
    packetbuf_clear();
    packetbuf_set_datalen(sprintf(packetbuf_dataptr(), "\\!SN/%02x/T/%02x", seqn++, ++temp));

    /**
     * Send data message
     */
    PRINTF( "Sending data message\n" );
    loramac_send( &loramac );
#else /* WITH_THINGSPEAK */
    app_msg_t *msg = 0;
    uint8_t msglen = 0;
    uint8_t data[4];

    /**
     * Data measurements
     */
    data[0] = 0x0B;
    data[1] = 0xAD;
    data[2] = 0xFA;
    data[3] = 0xCE;

    if( prepare_msg( &msg, &msglen, data, sizeof(data) ) == APP_RESULT_OK )
    {
      if(msg && msglen > 0)
      {
        /**
         * Send data message
         */
        PRINTF( "Sending data message\n" );
        loramac_send( &loramac );
      }
    }
#endif /* WITH_THINGSPEAK */

    etimer_set( &et, SEND_INTERVAL );
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
