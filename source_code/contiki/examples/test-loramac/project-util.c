/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 ** -------------------------------------------------------------------------
 *
 * Author  : David Rodenas-Herraiz
 * Created : 2017-10-05
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "project-util.h"

#include <stdio.h>
/*---------------------------------------------------------------------------*/
#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...) do {} while (0)
#endif
/*---------------------------------------------------------------------------*/
/* Debug purposes only - Print Hexdump of memory location *addr */
static void
hexDump (char *desc, void *addr, int len)
{
  int i;
  unsigned char buff[17];
  unsigned char *pc = (unsigned char*)addr;
  // Output description if given.
  if (desc != NULL)
  {
    printf ("%s:\n", desc);
  }

  if (len == 0)
  {
    printf("  ZERO LENGTH\n");
    return;
  }
  if (len < 0)
  {
    printf("  NEGATIVE LENGTH: %i\n",len);
    return;
  }
  // Process every byte in the data.
  for (i = 0; i < len; i++)
  {
    // Multiple of 16 means new line (with line offset).
    if ((i % 16) == 0)
    {
      // Just don't print ASCII for the zeroth line.
      if (i != 0)
      {
        printf ("  %s\n", buff);
      }

      // Output the offset.
      printf ("  %04x ", i);
    }
    // Now the hex code for the specific character.
    printf (" %02x", pc[i]);
    // And store a printable ASCII character for later.
    if ((pc[i] < 0x20) || (pc[i] > 0x7e))
    {
      buff[i % 16] = '.';
    }
    else
    {
      buff[i % 16] = pc[i];
    }
    buff[(i % 16) + 1] = '\0';
  }
  // Pad out last line if not exactly 16 characters.
  while ((i % 16) != 0)
  {
    printf ("   ");
    i++;
  }
  // And print the final ASCII bit.
  printf ("  %s\n", buff);
}
/*---------------------------------------------------------------------------*/
static app_result_t
print_msg_contents(const app_msg_t *msg, uint8_t msglen)
{
  uint8_t i;

  if(!msg) {
    printf("Cannot see msg contents\n");
    return APP_RESULT_ERROR;
  }

  printf("Seqno   : %d\n",    msg->hdr.seqno);
  printf("Data len: %d\n",    msg->hdr.datalen);

  if(msg->hdr.datalen > 0)
  {
    printf("Data    : ");
    for(i = 0 ; i < msg->hdr.datalen ; i++) {
      printf("0x%02x ",  msg->payload.data[i]);
    }
  }
  printf("\n");

  printf("-------------------\n");
  hexDump("MSG", (char *)msg, sizeof(app_msg_t) + msg->hdr.datalen - 1);
  printf("*****************************************\n");

  return APP_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
app_result_t
prepare_msg(app_msg_t **msg, uint8_t *msglen,
            const uint8_t *buf, uint8_t buflen)
{
  static unsigned int seqno;
  static app_msg_t * msg_t;

  if(!msglen) {
    return APP_RESULT_ERROR;
  }

  if(buflen > 0)
  {
    if(!buf) {
      *msglen = 0;
      return APP_RESULT_ERROR;
    }
    if((sizeof(app_msg_t) + buflen - 1) > LORA_MAX_PAYLOAD_SIZE) {
      *msglen = 0;
      return APP_RESULT_ERROR;
    }
  }

  packetbuf_clear();
  msg_t = (app_msg_t *)packetbuf_dataptr();
  packetbuf_set_datalen(sizeof(app_msg_t) + buflen - 1);

  /* Message header */
  /* Sequence number */
  msg_t->hdr.seqno          = seqno++;

  /* Data length */
  msg_t->hdr.datalen        = buflen;

  /* Message payload */
  /* Data measurements (if any) */
  if(buflen > 0)
  {
    memcpy(msg_t->payload.data, buf, buflen);
  }
  else
  {
    *msg_t->payload.data = 0;
  }

  *msg = msg_t;
  *msglen = sizeof(app_msg_t) + buflen - 1;

#if DEBUG
  print_msg_contents(*msg, *msglen);
#endif /* DEBUG */

  return APP_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
