/*
 * Copyright (c) 2018, David Rodenas-Herraiz.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

#include "loramacbuf.h"

#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif
/*---------------------------------------------------------------------------*/
static uint16_t buflen, bufport;
static uint8_t *bufptr;
/*---------------------------------------------------------------------------*/
void
loramacbuf_clear(void)
{
  buflen = bufport = 0;
  bufptr = NULL;
}
/*---------------------------------------------------------------------------*/
void *
loramacbuf_dataptr(void)
{
	return bufptr;
}
/*---------------------------------------------------------------------------*/
uint16_t
loramacbuf_datalen(void)
{
	return buflen;
}
/*---------------------------------------------------------------------------*/
uint16_t
loramacbuf_dataport(void)
{
	return bufport;
}
/*---------------------------------------------------------------------------*/
void
loramacbuf_set_dataptr(uint8_t *buf)
{
	bufptr = buf;
}
/*---------------------------------------------------------------------------*/
void
loramacbuf_set_datalen(uint16_t len)
{
	buflen = len;
}
/*---------------------------------------------------------------------------*/
void
loramacbuf_set_dataport(uint16_t portno)
{
	bufport = portno;
}
/*---------------------------------------------------------------------------*/
