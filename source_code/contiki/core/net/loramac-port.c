/*
 * Copyright (c) 2018, David Rodenas-Herraiz.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         LoRaMAC's loramac_port abstraction
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */

#include "lib/list.h"
#include "loramac-port.h"

LIST(loramac_port_list);

/*---------------------------------------------------------------------------*/
void
loramac_port_init(void)
{
  list_init(loramac_port_list);
}
/*---------------------------------------------------------------------------*/
void
loramac_port_set_attributes(uint16_t porTno,
		       const struct packetbuf_attrlist attrlist[])
{
  struct loramac_port *p;
  p = loramac_port_lookup(porTno);
  if(p != NULL) {
    p->attrlist = attrlist;
    p->buf.data = NULL;
    p->buf.datalen = 0;
  }
}
/*---------------------------------------------------------------------------*/
uint8_t
loramac_port_open(struct loramac_port *p, uint16_t porTno)
{
  if( porTno != LORAWAN_COMPLIANCE_TEST_PORT )
  {
    p->portno = porTno;
    list_add(loramac_port_list, p);
    return 0;
  }
  else
  {
    return 1;
  }
}
/*---------------------------------------------------------------------------*/
void
loramac_port_close(struct loramac_port *p)
{
  list_remove(loramac_port_list, p);
}
/*---------------------------------------------------------------------------*/
struct loramac_port *
loramac_port_lookup(uint16_t porTno)
{
  struct loramac_port *p;
  for(p = list_head(loramac_port_list); p != NULL; p = list_item_next(p)) {
    if(p->portno == porTno) {
      return p;
    }
  }
  return NULL;
}
/*---------------------------------------------------------------------------*/
