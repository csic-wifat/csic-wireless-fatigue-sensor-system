/*
 * Copyright (c) 2018, David Rodenas-Herraiz.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

#ifndef LORAMACBUF_H_
#define LORAMACBUF_H_

#include "contiki-conf.h"

/**
 * \brief      Clear and reset the loramacbuf
 *
 *             This function clears the loramacbuf and resets all
 *             internal state pointers (buf size, buf pointer,
 *             application port). It is used after receiving a
 *             packet in loramac_mac_driver.
 *
 */
void loramacbuf_clear(void);

/**
 * \brief      Get a pointer to the data in the loramacbuf
 * \return     Pointer to the loramacbuf data
 *
 *             This function is used to get a pointer to the data in
 *             the loramacbuf. The data is either stored in the loramacbuf,
 *             or referenced to an external location.
 *
 */
void *loramacbuf_dataptr(void);

/**
 * \brief      Get the length of the data in the loramacbuf
 * \return     Length of the data in the loramacbuf
 *
 */
uint16_t loramacbuf_datalen(void);

/**
 * \brief      Get the application port of the data in the loramacbuf
 * \return     Application port of the data in the loramacbuf
 *
 */
uint16_t loramacbuf_dataport(void);

/**
 * \brief      Set a pointer to the data in the loramacbuf
 * \param buf  Pointer to the data
 *
 */
void loramacbuf_set_dataptr(uint8_t *buf);

/**
 * \brief      Set the length of the data in the loramacbuf
 * \param len  The length of the data
 */
void loramacbuf_set_datalen(uint16_t len);

/**
 * \brief      Set the application port of the data in the loramacbuf
 * \param len  The application port of the data
 */
void loramacbuf_set_dataport(uint16_t portno);

#endif /* LORAMACBUF_H_ */
/** @} */
