/*
 * Copyright (c) 2018, David Rodenas-Herraiz.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         Header file for LoRaMAC's loramac_port abstraction
 * \author
	*         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */

#ifndef LORAMAC_PORT_H_
#define LORAMAC_PORT_H_

/*!
 * LoRaWAN port for running/disabling compliance test
 */
#define LORAWAN_COMPLIANCE_TEST_PORT  224

struct loramac_port;

#include "contiki-conf.h"
#include "net/packetbuf.h"

struct loramac_port {
  struct loramac_port *next;
  uint16_t portno;
  struct loramac_databuf {
  	uint8_t *data;
  	uint16_t datalen;
  } buf;
  const struct packetbuf_attrlist *attrlist;
};

struct loramac_port *loramac_port_lookup(uint16_t porTno);

void loramac_port_set_attributes(uint16_t porTno,
			    const struct packetbuf_attrlist attrlist[]);
uint8_t loramac_port_open(struct loramac_port *p, uint16_t porTno);
void loramac_port_close(struct loramac_port *p);
void loramac_port_init(void);

#endif /* LORAMAC_PORT_H_ */
