/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         An RDC implementation of LoRaMac for contiki OS.
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */

#ifndef LORAMAC_MAC_H_
#define LORAMAC_MAC_H_

#include "net/mac/rdc.h"

enum {
	LORAMAC_PARAM_NB_TRIALS,
	LORAMAC_PARAM_DATARATE,
	LORAMAC_PARAM_DEFAULT_DATARATE,
	LORAMAC_PARAM_TX_POWER,
	LORAMAC_PARAM_DEFAULT_TX_POWER,
};
typedef unsigned loramac_param_t;
typedef float loramac_value_t;

#define PROCESS_EVENT_LORAMAC_JOIN   		( 0xab )

#include "LoRaMac.h"
LoRaMacStatus_t loramac_get_value( loramac_param_t param, loramac_value_t *value );
LoRaMacStatus_t loramac_set_value( loramac_param_t param, loramac_value_t value );

uint8_t MacAllowsLpm( void );

extern const struct rdc_driver loramac_mac_driver;

#endif /* LORAMAC_DRIVER_H_ */
