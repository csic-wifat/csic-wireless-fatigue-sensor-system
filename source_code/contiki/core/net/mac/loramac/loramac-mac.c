/*
 * Copyright (c) 2017, David Rodenas-Herraiz.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         An RDC implementation of LoRaMac for Contiki OS.
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */
/*---------------------------------------------------------------------------*/
#include "LoRaMac.h"
#include "loramac-mac.h"
#include "Region.h"

#include "loramac-port.h"
#include "loramacbuf.h"

#include "lorawan-conf.h"

#include "net/packetbuf.h"
#include "net/queuebuf.h"
#include "net/netstack.h"

#include "lib/list.h"
#include "lib/memb.h"

#include "dev/leds.h"

#include "sys/node-id.h"

#include <string.h>
#include <stdio.h>
/*---------------------------------------------------------------------------*/
#define DEBUG         0
#define DEBUG_LEDS    1

#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINTDEBUG(...) printf(__VA_ARGS__)
#else /* DEBUG */
#define PRINTF(...)
#define PRINTDEBUG(...)
#endif /* DEBUG */

#undef LEDS_ON
#undef LEDS_OFF
#undef LEDS_TOGGLE
#define LEDS_ON(x) leds_on(x)
#define LEDS_OFF(x) leds_off(x)
#define LEDS_TOGGLE(x) leds_toggle(x)
#if !DEBUG_LEDS
#undef LEDS_ON
#undef LEDS_OFF
#undef LEDS_TOGGLE
#define LEDS_ON(x)
#define LEDS_OFF(x)
#define LEDS_TOGGLE(x)
#endif /* DEBUG_LEDS */
/*---------------------------------------------------------------------------*/
#if defined( REGION_AS923 )

#define ACTIVE_REGION LORAMAC_REGION_AS923

#elif defined( REGION_AU915 )

#define ACTIVE_REGION LORAMAC_REGION_AU915

#elif defined( REGION_CN779 )

#define ACTIVE_REGION LORAMAC_REGION_CN779

#elif defined( REGION_EU433 )

#define ACTIVE_REGION LORAMAC_REGION_EU433

#elif defined( REGION_EU868 )

#define ACTIVE_REGION LORAMAC_REGION_EU868

#elif defined( REGION_IN865 )

#define ACTIVE_REGION LORAMAC_REGION_IN865

#elif defined( REGION_KR920 )

#define ACTIVE_REGION LORAMAC_REGION_KR920

#elif defined( REGION_US915 )

#define ACTIVE_REGION LORAMAC_REGION_US915

#elif defined( REGION_US915_HYBRID )

#define ACTIVE_REGION LORAMAC_REGION_US915_HYBRID

#else /* ACTIVE_REGION */

#warning "No region defined, LORAMAC_REGION_EU868 will be used as default."

#define ACTIVE_REGION LORAMAC_REGION_EU868

#endif /* ACTIVE_REGION */

/*!
 * LoRaWAN driver status
 */
typedef enum loramac_driver_status
{
  LORAMAC_QUEUE_FULL  = 0xF0,
  LORAMAC_FATAL_ERROR = 0xFF,
}loramac_driver_status_t;

/*!
 * LoRaWAN compliance tests support data
 */
struct compliance_test_S
{
    bool      running;          // Whether the test is running or not
    uint8_t   state;            // MAC State
    bool      is_tx_confirmed;  // Whether confirmed or unconfirmed transmission
    uint8_t   portno;           // Application port
    uint8_t   datalen;          // Application data length
    uint8_t  *datafub;          // Application data buffer
    uint16_t  downlink_counter; // Downlink counter
    bool      link_check;       // Whether to check the wireless link
    uint8_t   demod_margin;     // Demodulation margin
    uint8_t   nb_gateways;      // Number of gateways which received the last LinkCheckReq
} compliance_test;

/*!
 * Maximum application data buffer size
 */
#define LORAWAN_APP_DATA_MAX_SIZE           (242)

static bool loop_data_back = false;
static uint8_t loop_databuf[LORAWAN_APP_DATA_MAX_SIZE];
static uint16_t loop_datalen;

/*!
 * Defines the application data transmission duty cycle. 5s, value in [ms].
 */
#ifdef LORAMAC_CONF_CYCLE_TIME
#define CYCLE_TIME          ( LORAMAC_CONF_CYCLE_TIME )
#else /* LORAMAC_CONF_CYCLE_TIME */
#define CYCLE_TIME          ( 5000 )
#endif /* LORAMAC_CONF_CYCLE_TIME */

/*!
 * Default datarate
 */
#ifdef LORAMAC_CONF_DEFAULT_DATARATE
#define LORAWAN_DEFAULT_DATARATE  ( LORAMAC_CONF_DEFAULT_DATARATE )
#else /* LORAMAC_CONF_DEFAULT_DATARATE */
#define LORAWAN_DEFAULT_DATARATE  ( DR_0 )
#endif /* LORAMAC_CONF_DEFAULT_DATARATE */

/*!
 * Datarate
 */
#ifdef LORAMAC_CONF_DATARATE
#define LORAWAN_DATARATE  ( LORAMAC_CONF_DATARATE )
#else /* LORAMAC_CONF_DEFAULT_DATARATE */
#define LORAWAN_DATARATE  ( DR_0 )
#endif /* LORAMAC_CONF_DEFAULT_DATARATE */

/*!
 * LoRaWAN confirmed messages
 */
//#define LORAMAC_CONF_CONFIRMED_MSG_ON 1
#ifdef LORAMAC_CONF_CONFIRMED_MSG_ON
#define CONFIRMED_MSG_ON    ( LORAMAC_CONF_CONFIRMED_MSG_ON )
#else /* LORAMAC_CONF_CONFIRMED_MSG_ON */
#define CONFIRMED_MSG_ON    ( 0 )
#endif /* LORAMAC_CONF_CONFIRMED_MSG_ON */

/*!
 * Number of retransmission attempts
 */
#ifdef LORAMAC_CONF_MAX_NBTRIALS
#define LORAMAC_MAX_NBTRIALS    ( LORAMAC_CONF_MAX_NBTRIALS )
#else /* LORAMAC_CONF_CONFIRMED_MSG_ON */
#define LORAMAC_MAX_NBTRIALS    ( 8 )
#endif /* LORAMAC_CONF_CONFIRMED_MSG_ON */

/*!
 * LoRaWAN Adaptive Data Rate
 *
 * \remark Please note that when ADR is enabled the end-device should be static
 */
#ifdef LORAMAC_CONF_ADR_ON
#define LORAWAN_ADR_ON              ( LORAMAC_CONF_ADR_ON )
#else /* LORAMAC_CONF_ADR_ON */
#define LORAWAN_ADR_ON              ( 0 )
#endif /* LORAMAC_CONF_ADR_ON */

#if defined( REGION_EU868 )

#include "LoRaMacTest.h"

/*!
 * LoRaWAN ETSI duty cycle control enable/disable
 *
 * \remark Please note that ETSI mandates duty cycled transmissions. Use only for test purposes
 */
#ifdef LORAMAC_CONF_DUTYCYCLE_ON
#define DUTYCYCLE_ON        ( LORAMAC_CONF_DUTYCYCLE_ON )
#else /* LORAMAC_CONF_DUTYCYCLE_ON */
#define DUTYCYCLE_ON        ( false )
#endif /* LORAMAC_CONF_DUTYCYCLE_ON */

#endif /* REGION_EU868 */

static uint8_t DevEui[] = LORAWAN_DEVICE_EUI;
static uint8_t AppEui[] = LORAWAN_APPLICATION_EUI;
static uint8_t AppKey[] = LORAWAN_APPLICATION_KEY;

#if( OVER_THE_AIR_ACTIVATION == 0 )

static uint8_t NwkSKey[] = LORAWAN_NWKSKEY;
static uint8_t AppSKey[] = LORAWAN_APPSKEY;

/*!
 * Device address
 */
static uint32_t DevAddr = LORAWAN_DEVICE_ADDRESS;

#endif /* OVER_THE_AIR_ACTIVATION */
/*---------------------------------------------------------------------------*/
/*!
 * Indicates if the node is sending confirmed or unconfirmed messages
 */
static uint8_t IsTxConfirmed = CONFIRMED_MSG_ON;

/*!
 * Datarate
 */
static int8_t datarate = LORAWAN_DATARATE;

/*!
 * Default datarate
 */
static int8_t default_datarate = LORAWAN_DEFAULT_DATARATE;

/*!
 * Number of retransmission attempts
 */
static uint8_t nb_trials =   LORAMAC_MAX_NBTRIALS;

/*!
 * Indicates if the mac allows lpm mode
 */
static uint8_t loramac_allows_lpm = 1;

/*!
 * Device states
 */
static enum eDeviceState
{
  DEVICE_STATE_INIT,
  DEVICE_STATE_JOIN,
  DEVICE_STATE_SEND,
  DEVICE_STATE_CYCLE,
  DEVICE_STATE_SLEEP
}DeviceState;
/*---------------------------------------------------------------------------*/
#define MAX_QUEUED_PACKETS 4

struct queue_list_item {
  struct queue_list_item  *next;
  struct queuebuf         *packet;
  mac_callback_t          sent_callback;
  void                    *sent_callback_ptr;
  uint16_t                portno;
  uint8_t                 num_transmissions;
};

LIST(queued_packets_list);
MEMB(queued_packets_memb, struct queue_list_item, MAX_QUEUED_PACKETS);

LoRaMacPrimitives_t LoRaMacPrimitives;
LoRaMacCallback_t   LoRaMacCallbacks;

/**
 * Indicates if a new packet can be sent
 */
uint8_t NextTx = 1;
/*---------------------------------------------------------------------------*/
static struct pt pt;
int cpowercycle( void *ptr );
static TimerEvent_t cpowercycle_ctimer;

#define CSCHEDULE_INIT( ) {                                         \
    TimerInit(&cpowercycle_ctimer, (void (*)(void *))cpowercycle);  \
  }
#define CSCHEDULE_POWERCYCLE( wait_time ) {                         \
    TimerSetValue(&cpowercycle_ctimer, wait_time);                  \
    TimerStart(&cpowercycle_ctimer);                                \
  }
#define CSCHEDULE_STOP( ) {                                         \
    TimerStop(&cpowercycle_ctimer);                                 \
  }
#define CSCHEDULE_IS_RUNNING( ) ({                                  \
    int re;                                                         \
    re = TimerIsRunning(&cpowercycle_ctimer);                       \
    re;                                                             \
  })
/*---------------------------------------------------------------------------*/
/*!
 * \brief Function to schedule next transmission
 */
static void
loramac_schedule_next_tx( void )
{
  MibRequestConfirm_t mibReq;
  LoRaMacStatus_t status;

  PRINTF("%s: ", __FUNCTION__);
  CSCHEDULE_STOP( );

  mibReq.Type = MIB_NETWORK_JOINED;
  status = LoRaMacMibGetRequestConfirm( &mibReq );

  if( status == LORAMAC_STATUS_OK )
  {
    if( mibReq.Param.IsNetworkJoined == true )
    {
      DeviceState = DEVICE_STATE_SEND;
      NextTx = 1;
    }
    else
    {
      DeviceState = DEVICE_STATE_JOIN;
    }
  }

  CSCHEDULE_POWERCYCLE( 0 );
  //PRINTF ("\n");
}
/*---------------------------------------------------------------------------*/
static void
qsend_packet( mac_callback_t sent, void *ptr )
{
  MibRequestConfirm_t mibReq;
  LoRaMacStatus_t status;
  struct queue_list_item *i;

  i = memb_alloc( &queued_packets_memb );

  if( i != NULL )
  {
    struct loramac_port *p = ptr;
    PRINTF("%s: Send message on port %d\n", __FUNCTION__, p->portno);

    i->sent_callback = sent;
    i->sent_callback_ptr = ptr;
    i->num_transmissions = 0;
    i->portno = p->portno;
    i->packet = queuebuf_new_from_packetbuf( );

    if( i->packet == NULL )
    {
      memb_free( &queued_packets_memb, i );
      mac_call_sent_callback( sent, ptr, LORAMAC_QUEUE_FULL, 0 );
    }
    else
    {
      list_add( queued_packets_list, i );

      mibReq.Type = MIB_NETWORK_JOINED;
      status = LoRaMacMibGetRequestConfirm( &mibReq );

      loramac_schedule_next_tx( );
    }
  }
  else
  {
    mac_call_sent_callback( sent, ptr, LORAMAC_FATAL_ERROR, 0 );
  }
}
/*---------------------------------------------------------------------------*/
static void
qsend_list( mac_callback_t sent, void *ptr, struct rdc_buf_list *buf_list )
{
  if( buf_list != NULL )
  {
    queuebuf_to_packetbuf( buf_list->buf );
    qsend_packet( sent, ptr );
  }
}
/*---------------------------------------------------------------------------*/
static void
packet_input( void )
{
  printf("%s: Error. This function should never be called\n", __FUNCTION__);
}
/*---------------------------------------------------------------------------*/
static int
on(void)
{
  return NETSTACK_RADIO.on( );
}
/*---------------------------------------------------------------------------*/
static int
off(int keep_radio_on)
{
  if(keep_radio_on)
  {
    return NETSTACK_RADIO.on( );
  }
  else
  {
    return NETSTACK_RADIO.off( );
  }
}
/*---------------------------------------------------------------------------*/
static unsigned short
duty_cycle( void )
{
  return ( 1ul * ( CYCLE_TIME / 1000 ) * CLOCK_SECOND );
}
/*---------------------------------------------------------------------------*/
static void
remove_queued_packet( struct queue_list_item *i, uint8_t status )
{
  mac_callback_t sent;
  void *ptr;
  int num_transmissions = 0;

  queuebuf_free( i->packet );
  list_remove( queued_packets_list, i );

  sent = i->sent_callback;
  ptr = i->sent_callback_ptr;
  num_transmissions = i->num_transmissions;

  memb_free( &queued_packets_memb, i );
  mac_call_sent_callback( sent, ptr, status, num_transmissions );
}
/*---------------------------------------------------------------------------*/
/*!
 * \brief   MCPS-Confirm event function
 *
 * \param   [IN] mcpsConfirm - Pointer to the confirm structure,
 *               containing confirm attributes.
 */
static void
loramac_mcps_confirm( McpsConfirm_t *mcpsConfirm )
{
  PRINTF("%s: ", __FUNCTION__);

  if( compliance_test.running == false )
  {
    struct queue_list_item *p;

    p = list_head( queued_packets_list );
    if(p)
    {
      if( IsTxConfirmed == false )
      {
        p->num_transmissions = 1;
      }
      else
      {
        p->num_transmissions = mcpsConfirm->NbRetries;
      }
      remove_queued_packet( p, mcpsConfirm->Status );
    }
    else
    {
      printf("Error removing packet from the queue! ");
    }
  }

  if( mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK )
  {
    switch( mcpsConfirm->McpsRequest )
    {
      case MCPS_UNCONFIRMED:
      {
        // Check Datarate
        // Check TxPower
        break;
      }
      case MCPS_CONFIRMED:
      {
        // Check Datarate
        // Check TxPower
        // Check AckReceived
        // Check NbTrials
        break;
      }
      case MCPS_PROPRIETARY:
      {
        break;
      }
      default:
        break;
    }
  }
  PRINTF("\n");
  NextTx = 1;
}
/*---------------------------------------------------------------------------*/
/*!
 * \brief   MCPS-Indication event function
 *
 * \param   [IN] mcpsIndication - Pointer to the indication structure,
 *               containing indication attributes.
 */
static void
loramac_mcps_indication( McpsIndication_t *mcpsIndication )
{
  PRINTF("%s: ", __FUNCTION__);
  if( mcpsIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK )
  {
    PRINTF("LoRaMAC event info status failure (%u)\n", mcpsIndication->Status);
    return;
  }

  loramac_allows_lpm = 1;

  switch( mcpsIndication->McpsIndication )
  {
    case MCPS_UNCONFIRMED:
    {
      PRINTF("UNCONFIRMED\n");
      break;
    }
    case MCPS_CONFIRMED:
    {
      PRINTF("CONFIRMED\n");
      break;
    }
    case MCPS_PROPRIETARY:
    {
      PRINTF("PROPRIETARY\n");
      break;
    }
    case MCPS_MULTICAST:
    {
      PRINTF("MULTICAST\n");
      break;
    }
    default:
      break;
  }

  // Check Multicast
  // Check Port
  // Check Datarate
  // Check FramePending
  if( mcpsIndication->FramePending == true )
  {
    PRINTF("Frame Pending\n");
    // The server signals that it has pending data to be sent.
    // We schedule an uplink as soon as possible to flush the server.
    loramac_schedule_next_tx( );
  }

  // Check Buffer
  // Check BufferSize
  // Check Rssi
  // Check Snr
  // Check RxSlot

  if( compliance_test.running == true )
  {

    compliance_test.downlink_counter++;
  }

  if( mcpsIndication->RxData == true )
  {
    PRINTF("RxData on port %d\n", mcpsIndication->Port);
    loramacbuf_clear();
    loramacbuf_set_dataport( mcpsIndication->Port );
    loramacbuf_set_datalen( mcpsIndication->BufferSize );
    loramacbuf_set_dataptr( mcpsIndication->Buffer );

    if(mcpsIndication->Port == LORAWAN_COMPLIANCE_TEST_PORT)
    {
      PRINTF("Compliance test: %s, buff size %d buff %s\n", compliance_test.running ? "running":"not running", mcpsIndication->BufferSize, (char*) mcpsIndication->Buffer);
      if( mcpsIndication->BufferSize == 4 )
      {
        PRINTF("buff %02x %02x %02x %02x\n",
          mcpsIndication->Buffer[0],
          mcpsIndication->Buffer[1],
          mcpsIndication->Buffer[2],
          mcpsIndication->Buffer[3] );
      }

      if( compliance_test.running == false )
      {
        if( ( mcpsIndication->BufferSize == 4 ) &&
            ( mcpsIndication->Buffer[0] == 0x01 ) &&
            ( mcpsIndication->Buffer[1] == 0x01 ) &&
            ( mcpsIndication->Buffer[2] == 0x01 ) &&
            ( mcpsIndication->Buffer[3] == 0x01 ) )
        {
          PRINTF("start test\n");

          IsTxConfirmed = false;

          compliance_test.downlink_counter = 0;
          compliance_test.link_check = false;
          compliance_test.demod_margin = 0;
          compliance_test.nb_gateways = 0;
          compliance_test.running = true;
          compliance_test.state = 1;

          MibRequestConfirm_t mibReq;
          mibReq.Type = MIB_ADR;
          mibReq.Param.AdrEnable = true;
          LoRaMacMibSetRequestConfirm( &mibReq );
#if defined( REGION_EU868 )
          LoRaMacTestSetDutyCycleOn( false );
#endif /* REGION_EU868 */
        }
      }
      else
      {
        if( mcpsIndication->BufferSize > 0 )
        {
          compliance_test.state = mcpsIndication->Buffer[0];
          switch( compliance_test.state )
          {
            case 0: // Check compliance test disable command (ii)
              PRINTF("0:: check compliance test disable command\n");
              IsTxConfirmed = CONFIRMED_MSG_ON;
              compliance_test.downlink_counter = 0;
              compliance_test.running = false;
              MibRequestConfirm_t mibReq;
              mibReq.Type = MIB_ADR;
              mibReq.Param.AdrEnable = LORAWAN_ADR_ON;
              LoRaMacMibSetRequestConfirm( &mibReq );
  #if defined( REGION_EU868 )
              LoRaMacTestSetDutyCycleOn( DUTYCYCLE_ON );
  #endif /* REGION_EU868 */
              break;
            case 1: // (iii, iv)
              PRINTF("1:: (DeviceState is %u)\n", DeviceState);
              break;
            case 2: // Enable confirmed messages (v)
              PRINTF("2:: Enable confirmed messages\n");
              IsTxConfirmed = true;
              compliance_test.state = 1;
              break;
            case 3:  // Disable confirmed messages (vi)
              PRINTF("3:: Disable confirmed messages\n");
              IsTxConfirmed = false;
              compliance_test.state = 1;
              break;
            case 4: // (vii)
              PRINTF("4:: Loop data back\n");
              loop_data_back = true;

              loop_datalen = mcpsIndication->BufferSize;
              loop_databuf[0] = 4;
              for( uint8_t i = 1; i < MIN( loop_datalen, LORAWAN_APP_DATA_MAX_SIZE ); i++ )
              {
                loop_databuf[i] = mcpsIndication->Buffer[i] + 1;
              }
              break;
            case 5: // (viii)
              PRINTF("5:: MLME link check\n");
              {
                MlmeReq_t mlmeReq;
                mlmeReq.Type = MLME_LINK_CHECK;
                LoRaMacMlmeRequest( &mlmeReq );
              }
              break;
            case 6: // (ix)
              PRINTF("6:: Disable TestMode and revert back to normal operation\n");
              {
                MlmeReq_t mlmeReq;
                // Disable TestMode and revert back to normal operation
                IsTxConfirmed = CONFIRMED_MSG_ON;
                compliance_test.downlink_counter = 0;
                compliance_test.running = false;

                MibRequestConfirm_t mibReq;
                mibReq.Type = MIB_ADR;
                mibReq.Param.AdrEnable = LORAWAN_ADR_ON;
                LoRaMacMibSetRequestConfirm( &mibReq );
  #if defined( REGION_EU868 )
                LoRaMacTestSetDutyCycleOn( DUTYCYCLE_ON );
  #endif /* REGION_EU868 */
                DeviceState = DEVICE_STATE_JOIN;
              }
              break;
            case 7: // (x)
              PRINTF("7:: Set tx continues wave\n");
              {
                if( mcpsIndication->BufferSize == 3 )
                {
                    MlmeReq_t mlmeReq;
                    mlmeReq.Type = MLME_TXCW;
                    mlmeReq.Req.TxCw.Timeout = ( uint16_t )( ( mcpsIndication->Buffer[1] << 8 ) | mcpsIndication->Buffer[2] );
                    LoRaMacMlmeRequest( &mlmeReq );
                }
                else if( mcpsIndication->BufferSize == 7 )
                {
                    MlmeReq_t mlmeReq;
                    mlmeReq.Type = MLME_TXCW_1;
                    mlmeReq.Req.TxCw.Timeout = ( uint16_t )( ( mcpsIndication->Buffer[1] << 8 ) | mcpsIndication->Buffer[2] );
                    mlmeReq.Req.TxCw.Frequency = ( uint32_t )( ( mcpsIndication->Buffer[3] << 16 ) | ( mcpsIndication->Buffer[4] << 8 ) | mcpsIndication->Buffer[5] ) * 100;
                    mlmeReq.Req.TxCw.Power = mcpsIndication->Buffer[6];
                    LoRaMacMlmeRequest( &mlmeReq );
                }
                compliance_test.state = 1;
              }
              break;
            default:
              break;
          }
        }
      }
    }
    else
    {
      NETSTACK_NETWORK.input( );
    }
  }
  else
  {
    PRINTF("\n");
  }
}
/*---------------------------------------------------------------------------*/
/*!
 * \brief   MLME-Confirm event function
 *
 * \param   [IN] mlmeConfirm - Pointer to the confirm structure,
 *               containing confirm attributes.
 */
static void
loramac_mlme_confirm( MlmeConfirm_t *mlmeConfirm )
{
  uint8_t oldDeviceState = DeviceState;
  uint8_t schedulePowerCycle = 0;

  PRINTF("%s\n", __FUNCTION__);
  switch( mlmeConfirm->MlmeRequest )
  {
    case MLME_JOIN:
      {
        loramac_allows_lpm = 1;

        if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK )
        {
          PRINTF("the node has joined the network");

          LEDS_OFF( LEDS_RED );

          process_post(PROCESS_BROADCAST, PROCESS_EVENT_LORAMAC_JOIN, (process_data_t)NULL);

          // Status is OK, node has joined the network
          DeviceState = DEVICE_STATE_SEND;

          if (list_length(queued_packets_list) > 0)
          {
            schedulePowerCycle = 1;
          }
        }
        else
        {
          // Join was not successful. Try to join again
          DeviceState = DEVICE_STATE_JOIN;
          PRINTF("join was not successful. Try to join again");
        }
        break;
      }
    case MLME_LINK_CHECK:
      {
        if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK )
        {
          // Check DemodMargin
          // Check NbGateways
          if( compliance_test.running == true )
          {
            PRINTF("compliance test - link check");
            compliance_test.link_check = true;
            compliance_test.demod_margin = mlmeConfirm->DemodMargin;
            compliance_test.nb_gateways = mlmeConfirm->NbGateways;
          }
        }
        break;
      }
    default:
      break;
  }
  PRINTF("\n");
  NextTx = 1;

  if( ( ( oldDeviceState == DEVICE_STATE_SLEEP ) && schedulePowerCycle &&
      ( list_length(queued_packets_list) > 0 ) ) )
  {
    CSCHEDULE_POWERCYCLE( 0 );
  }
  if( DeviceState == DEVICE_STATE_JOIN )
  {
    loramac_set_value( LORAMAC_PARAM_DATARATE, 0 );
    CSCHEDULE_POWERCYCLE( 10000 );
  }
}
/*---------------------------------------------------------------------------*/
/*!
 * \brief   MLME-Indication event function
 *
 * \param   [IN] mlmeIndication - Pointer to the indication structure.
 */
static void
loramac_mlme_indication( MlmeIndication_t *mlmeIndication )
{
  switch( mlmeIndication->MlmeIndication )
  {
    case MLME_SCHEDULE_UPLINK:
      // The MAC signals that we shall provide an uplink as soon as possible
      loramac_schedule_next_tx( );
      break;
    default:
      break;
  }
}
/*---------------------------------------------------------------------------*/
static uint8_t
loramac_get_battery_level( void )
{
  PRINTF("%s\n", __FUNCTION__);
  uint8_t battery_level = BAT_LEVEL_NO_MEASURE;
#if PLATFORM_HAS_BATTERY_SENSOR
  /* Activate battery sensor */
  SENSORS_ACTIVATE(battery_sensor);

  /* Read battery level */
  battery_level = (uint8_t) battery_sensor.value(SINGLE_SHOT_TRIGGER_CONVERSION);

  /* Deactivate battery sensor */
  SENSORS_DEACTIVATE(battery_sensor);
#endif /* PLATFORM_HAS_BATTERY_SENSOR */

  return battery_level;
}
/*---------------------------------------------------------------------------*/
int
cpowercycle( void *ptr )
{
  PT_BEGIN(&pt);

  while(1) {
    if( DeviceState  == DEVICE_STATE_INIT )
    {
      uint8_t init_status = LORAMAC_STATUS_OK;
      // PRINTF("%s: Initializing...\n", __FUNCTION__);
      LoRaMacPrimitives.MacMcpsConfirm = loramac_mcps_confirm;
      LoRaMacPrimitives.MacMcpsIndication = loramac_mcps_indication;
      LoRaMacPrimitives.MacMlmeConfirm = loramac_mlme_confirm;
      LoRaMacPrimitives.MacMlmeIndication = loramac_mlme_indication;
      LoRaMacCallbacks.GetBatteryLevel = loramac_get_battery_level;

      init_status = LoRaMacInitialization( &LoRaMacPrimitives, &LoRaMacCallbacks, ACTIVE_REGION );

      if( init_status != LORAMAC_STATUS_OK )
      {
        PRINTF("loramac: init fail\n");
        break;
      }
      MibRequestConfirm_t mibReq;

      mibReq.Type = MIB_ADR;
      mibReq.Param.AdrEnable = LORAWAN_ADR_ON;
      LoRaMacMibSetRequestConfirm( &mibReq );

      mibReq.Type = MIB_PUBLIC_NETWORK;
      mibReq.Param.EnablePublicNetwork = LORAWAN_PUBLIC_NETWORK;
      LoRaMacMibSetRequestConfirm( &mibReq );

#if defined( REGION_EU868 )
      LoRaMacTestSetDutyCycleOn( DUTYCYCLE_ON );
#endif /* REGION_EU868 */

#if LORAWAN_DEVICE_CLASS_C
      mibReq.Type = MIB_DEVICE_CLASS;
      mibReq.Param.Class = CLASS_C;
      LoRaMacMibSetRequestConfirm( &mibReq );
#endif /* LORAWAN_DEVICE_CLASS_C */
      DeviceState = DEVICE_STATE_JOIN;
    }
    else if( DeviceState  == DEVICE_STATE_JOIN )
    {
      LEDS_ON( LEDS_RED );
#if( OVER_THE_AIR_ACTIVATION != 0 )
      PRINTF("Attempting joining procedure...\n");
      loramac_allows_lpm = 0;
      if ( NextTx )
      {
        MlmeReq_t mlmeReq;

        // Initialize LoRaMac device unique ID
        ( *(uint32_t *)DevEui ) = ( *(uint32_t *)node_mac );
        ( *( ( (uint32_t *)DevEui ) + 1 ) ) = ( *( ( (uint32_t *)node_mac ) + 1 ) );

        mlmeReq.Type = MLME_JOIN;

        mlmeReq.Req.Join.DevEui = DevEui;
        mlmeReq.Req.Join.AppEui = AppEui;
        mlmeReq.Req.Join.AppKey = AppKey;
        mlmeReq.Req.Join.Datarate = default_datarate;

        if( LoRaMacMlmeRequest( &mlmeReq ) == LORAMAC_STATUS_OK )
        {
          DeviceState = DEVICE_STATE_SLEEP;
          NextTx = 1;
        }
        else
        {
          DeviceState = DEVICE_STATE_CYCLE;
        }
      }
#else /* OVER_THE_AIR_ACTIVATION */
      loramac_allows_lpm = 1;

      MibRequestConfirm_t mibReq;
      // Use node_id if not already defined in lorawan-conf.h
      if( DevAddr == 0)
      {
        DevAddr = (uint32_t) node_id;
      }

      mibReq.Type = MIB_NET_ID;
      mibReq.Param.NetID = LORAWAN_NETWORK_ID;
      LoRaMacMibSetRequestConfirm( &mibReq );

      mibReq.Type = MIB_DEV_ADDR;
      mibReq.Param.DevAddr = DevAddr;
      LoRaMacMibSetRequestConfirm( &mibReq );

      mibReq.Type = MIB_NWK_SKEY;
      mibReq.Param.NwkSKey = NwkSKey;
      LoRaMacMibSetRequestConfirm( &mibReq );

      mibReq.Type = MIB_APP_SKEY;
      mibReq.Param.AppSKey = AppSKey;
      LoRaMacMibSetRequestConfirm( &mibReq );

      mibReq.Type = MIB_NETWORK_JOINED;
      mibReq.Param.IsNetworkJoined = true;
      LoRaMacMibSetRequestConfirm( &mibReq );

      LEDS_OFF( LEDS_RED );
      process_post(PROCESS_BROADCAST, PROCESS_EVENT_LORAMAC_JOIN, (process_data_t)NULL);

      DeviceState = DEVICE_STATE_SEND;
#endif /* OVER_THE_AIR_ACTIVATION */
    }
    else if( DeviceState  == DEVICE_STATE_SEND )
    {
      PRINTF("%s: Sending...\n", __FUNCTION__);
      if( NextTx )
      {
        uint8_t schedule_tx = 0;
        McpsReq_t mcpsReq;
        LoRaMacTxInfo_t txInfo;
        struct queue_list_item *i = 0;

        if( compliance_test.running == true )
        {
          PRINTF("loramac: compliance test - ");
          if( compliance_test.link_check == true )
          {
            PRINTF(" link check");
            compliance_test.link_check = false;

            uint8_t testdata[3];
            testdata[0] = 5;
            testdata[1] = compliance_test.demod_margin;
            testdata[2] = compliance_test.nb_gateways;

            packetbuf_clear( );
            packetbuf_set_datalen( sizeof(testdata) );
            memcpy( packetbuf_dataptr(), testdata, sizeof(testdata) );

            compliance_test.state = 1;
            schedule_tx = 1;
          }
          else
          {
            PRINTF("state %u - ", compliance_test.state);
            switch( compliance_test.state )
            {
              case 4:
                compliance_test.state = 1;
                if(loop_data_back == true)
                {
                  PRINTF("send loop data back");
                  packetbuf_clear( );
                  packetbuf_set_datalen( loop_datalen );
                  memcpy( packetbuf_dataptr(), loop_databuf, loop_datalen );

                  schedule_tx = 1;
                }
                break;
              case 1:
                {
                  PRINTF("send downlink counter: ");
                  uint8_t testdata[2];

                  testdata[0] = compliance_test.downlink_counter >> 8;
                  testdata[1] = compliance_test.downlink_counter;

                  PRINTF("0x%02x,0x%02x", testdata[0], testdata[1]);

                  packetbuf_clear( );
                  packetbuf_set_datalen( sizeof(testdata) );
                  memcpy( packetbuf_dataptr(), testdata, sizeof(testdata) );

                  schedule_tx = 1;
                }
                break;
            }
          }
          PRINTF("\n");
        }
        else if( list_length(queued_packets_list) > 0 )
        {
          PRINTF("%s there are packets\n", __FUNCTION__);
          i = list_head(queued_packets_list);

          if( !i )
          {
            printf("%s: Error retrieving first element of the packet queue!", __FUNCTION__);
            continue;
          }

          /* Prepare buffer */
          queuebuf_to_packetbuf(i->packet);

          schedule_tx = 1;
        }
        if( schedule_tx )
        {
          if( LoRaMacQueryTxPossible( packetbuf_datalen(), &txInfo ) != LORAMAC_STATUS_OK )
          {
            // Send empty frame in order to flush MAC commands
            mcpsReq.Type = MCPS_UNCONFIRMED;
            mcpsReq.Req.Unconfirmed.fBuffer = NULL;
            mcpsReq.Req.Unconfirmed.fBufferSize = 0;
            mcpsReq.Req.Unconfirmed.Datarate = datarate;
          }
          else
          {
            if( IsTxConfirmed == false )
            {
              mcpsReq.Type = MCPS_UNCONFIRMED;
              if( compliance_test.running == true )
              {
                mcpsReq.Req.Confirmed.fPort = LORAWAN_COMPLIANCE_TEST_PORT;
              }
              else
              {
                mcpsReq.Req.Unconfirmed.fPort = i->portno;
              }
              mcpsReq.Req.Unconfirmed.fBuffer = packetbuf_hdrptr();
              mcpsReq.Req.Unconfirmed.fBufferSize = packetbuf_datalen();
              mcpsReq.Req.Unconfirmed.Datarate = datarate;
            }
            else
            {
              mcpsReq.Type = MCPS_CONFIRMED;
              if( compliance_test.running == true )
              {
                mcpsReq.Req.Confirmed.fPort = LORAWAN_COMPLIANCE_TEST_PORT;
              }
              else
              {
                mcpsReq.Req.Confirmed.fPort = i->portno;
              }
              mcpsReq.Req.Confirmed.fBuffer = packetbuf_hdrptr();
              mcpsReq.Req.Confirmed.fBufferSize = packetbuf_datalen();
              mcpsReq.Req.Confirmed.NbTrials = nb_trials;
              mcpsReq.Req.Confirmed.Datarate = datarate;
            }
          }

          PRINTF( "Sending frame... " );
          LoRaMacStatus_t status;
          status = LoRaMacMcpsRequest( &mcpsReq );

          if( status == LORAMAC_STATUS_OK )
          {
            PRINTF( "OK\n" );
            loramac_allows_lpm = 0;
            NextTx = 0;
          }
          else
          {
            PRINTF( "FAIL\n" );
            NextTx = 1;
          }
        }
      }
      if( ( compliance_test.running == true ) || ( list_length( queued_packets_list ) > 0 ) )
      {
        // Schedule next duty cycle
        DeviceState = DEVICE_STATE_CYCLE;
      }
      else
      {
        // Wait for next transmission request
        DeviceState = DEVICE_STATE_SLEEP;
      }
    }
    else if( DeviceState  == DEVICE_STATE_CYCLE )
    {
      //PRINTF("%s: cycle...\n", __FUNCTION__);
      DeviceState = DEVICE_STATE_SLEEP;

      // Schedule next frame transmission
      CSCHEDULE_POWERCYCLE( CYCLE_TIME );
      PT_YIELD(&pt);
      CSCHEDULE_STOP( );

      loramac_schedule_next_tx( );
    }
    else if( DeviceState  == DEVICE_STATE_SLEEP )
    {
      //PRINTF("%s: Sleeping...\n", __FUNCTION__);
      PT_YIELD(&pt);
      CSCHEDULE_STOP( );
    }
    else
    {
      DeviceState = DEVICE_STATE_INIT;
    }
  }

  PT_END(&pt);
}
/*---------------------------------------------------------------------------*/
static void
init( void )
{
  static uint8_t init_done;

  DeviceState = DEVICE_STATE_INIT;

  if( !init_done )
  {
    memb_init( &queued_packets_memb );
    list_init( queued_packets_list );
    CSCHEDULE_INIT( );
    PT_INIT(&pt);
    CSCHEDULE_POWERCYCLE( 0 );

    init_done = 1;
  }
}
/*---------------------------------------------------------------------------*/
const struct rdc_driver loramac_mac_driver = {
  "LoRaMac",
  init,
  qsend_packet,
  qsend_list,
  packet_input,
  on,
  off,
  duty_cycle,
};
/*---------------------------------------------------------------------------*/
uint8_t
MacAllowsLpm( void )
{
  return loramac_allows_lpm;
}
/*---------------------------------------------------------------------------*/
LoRaMacStatus_t
loramac_get_value( loramac_param_t param, loramac_value_t *value )
{
  LoRaMacStatus_t status = LORAMAC_STATUS_OK;
  MibRequestConfirm_t mibGet;

  if( !value ) {
    return LORAMAC_STATUS_PARAMETER_INVALID;
  }
  switch( param )
  {
    case LORAMAC_PARAM_NB_TRIALS:
      *value = ( loramac_value_t )nb_trials;
      break;

    case LORAMAC_PARAM_DATARATE:
      mibGet.Type = MIB_CHANNELS_DATARATE;
      if( LoRaMacMibGetRequestConfirm( &mibGet ) != LORAMAC_STATUS_OK )
      {
        status = LORAMAC_STATUS_PARAMETER_INVALID;
      }
      else
      {
        *value = ( loramac_value_t )mibGet.Param.ChannelsDatarate;
      }
      //*value = ( loramac_value_t )datarate;
      break;

    case LORAMAC_PARAM_DEFAULT_DATARATE:
      mibGet.Type = MIB_CHANNELS_DEFAULT_DATARATE;
      if( LoRaMacMibGetRequestConfirm( &mibGet ) != LORAMAC_STATUS_OK )
      {
        status = LORAMAC_STATUS_PARAMETER_INVALID;
      }
      else
      {
        *value = ( loramac_value_t )mibGet.Param.ChannelsDefaultDatarate;
      }
      break;

    case LORAMAC_PARAM_TX_POWER:
      mibGet.Type = MIB_CHANNELS_TX_POWER;
      if( LoRaMacMibGetRequestConfirm( &mibGet ) != LORAMAC_STATUS_OK )
      {
        status = LORAMAC_STATUS_PARAMETER_INVALID;
      }
      else
      {
        *value = ( loramac_value_t )mibGet.Param.ChannelsTxPower;
      }
      break;

    case LORAMAC_PARAM_DEFAULT_TX_POWER:
      mibGet.Type = MIB_CHANNELS_DEFAULT_TX_POWER;
      if( LoRaMacMibGetRequestConfirm( &mibGet ) != LORAMAC_STATUS_OK )
      {
        status = LORAMAC_STATUS_PARAMETER_INVALID;
      }
      else
      {
        *value = ( loramac_value_t )mibGet.Param.ChannelsDefaultTxPower;
      }
      break;

    default:
      return LORAMAC_STATUS_SERVICE_UNKNOWN;
  }
  return status;
}
/*---------------------------------------------------------------------------*/
LoRaMacStatus_t
loramac_set_value( loramac_param_t param, loramac_value_t value )
{
  LoRaMacStatus_t status = LORAMAC_STATUS_OK;
  MibRequestConfirm_t mibSet;

  switch( param )
  {
    case LORAMAC_PARAM_NB_TRIALS:
      PRINTF("setting Nb trials to %i\n", ( uint8_t )value);
      if( ( ( uint8_t )value > 0 ) && ( uint8_t )value < LORAMAC_MAX_NBTRIALS )
      {
        nb_trials = ( uint8_t )value;
      }
      else
      {
        status = LORAMAC_STATUS_PARAMETER_INVALID;
      }
      break;

    case LORAMAC_PARAM_DATARATE:
      PRINTF("setting DR%i\n", ( int8_t )value);
      datarate = ( int8_t )value;
      break;

    case LORAMAC_PARAM_DEFAULT_DATARATE:
      PRINTF("setting default DR%i\n", ( int8_t )value);
      default_datarate = ( int8_t )value;

      mibSet.Type = MIB_CHANNELS_DEFAULT_DATARATE;
      mibSet.Param.ChannelsDefaultDatarate = ( int8_t )value;
      status = LoRaMacMibSetRequestConfirm( &mibSet );
      break;

    case LORAMAC_PARAM_TX_POWER:
      PRINTF("setting Tx Power index %i\n", ( int8_t )value );
      mibSet.Type = MIB_CHANNELS_TX_POWER;
      mibSet.Param.ChannelsTxPower = ( int8_t )value;
      status = LoRaMacMibSetRequestConfirm( &mibSet );
      break;

    case LORAMAC_PARAM_DEFAULT_TX_POWER:
      mibSet.Type = MIB_CHANNELS_DEFAULT_TX_POWER;
      mibSet.Param.ChannelsDefaultTxPower = ( int8_t )value;
      status = LoRaMacMibSetRequestConfirm( &mibSet );

    default:
      return LORAMAC_STATUS_SERVICE_UNKNOWN;
  }
  return status;
}
/*---------------------------------------------------------------------------*/
