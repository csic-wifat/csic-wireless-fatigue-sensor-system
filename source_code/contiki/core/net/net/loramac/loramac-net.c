/*
 * Copyright (c) 2017, David Rodenas-Herraiz.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         LoRaMAC network driver initialization and common code
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */

#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#include "net/netstack.h"
#include "loramac-net.h"
/*---------------------------------------------------------------------------*/
static void
packet_sent(void *ptr, int status, int num_tx)
{
  struct loramac_port *p = ptr;
  /*
  switch(status) {
  default:
    PRINTF("rime: error %d after %d tx\n", status, num_tx);
  }
  */
  loramac_sent(p, status, num_tx);
}
/*---------------------------------------------------------------------------*/
static void
input(void)
{
	struct loramac_port *p;

  PRINTF("%s: len %d port %d\n", __FUNCTION__, loramacbuf_datalen( ), loramacbuf_dataport( ));

  p = loramac_port_lookup( loramacbuf_dataport( ) );

	if(p != NULL) {
    p->buf.data = NULL;
    p->buf.datalen = 0;

    p->buf.data = (uint8_t *)loramacbuf_dataptr( );
    p->buf.datalen = loramacbuf_datalen( );
    if( p->buf.data != NULL && p->buf.datalen > 0 )
    {
      loramac_input( p );
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
init(void)
{
  queuebuf_init();
  packetbuf_clear();
  loramacbuf_clear();
  loramac_port_init();
}
/*---------------------------------------------------------------------------*/
static const struct packetbuf_attrlist attributes[] =
  { LORAMAC_ATTRIBUTES PACKETBUF_ATTR_LAST };
/*---------------------------------------------------------------------------*/
void
loramac_open(struct loramac_conn *c, uint16_t porTno,
    const struct loramac_callbacks *callbacks)
{
  loramac_port_open(&c->port, porTno);
  c->u = callbacks;
  loramac_port_set_attributes(porTno, attributes);
}
/*---------------------------------------------------------------------------*/
void
loramac_close(struct loramac_conn *c)
{
  loramac_port_close(&c->port);
}
/*---------------------------------------------------------------------------*/
int
loramac_send(struct loramac_conn *c)
{
  PRINTF("%d.%d: LoRaMAC: loramac_send on port %d\n",
   linkaddr_node_addr.u8[0],linkaddr_node_addr.u8[1],
   c->port.portno);

  NETSTACK_MAC.send((mac_callback_t)packet_sent, (void *)&c->port);

  return 0;
}
/*---------------------------------------------------------------------------*/
void
loramac_input(struct loramac_port *port)
{
  struct loramac_conn *c = (struct loramac_conn *)port;
  PRINTF("%d.%d: LoRaMAC: loramac_input_packet on port %d\n",
   linkaddr_node_addr.u8[0],linkaddr_node_addr.u8[1],
   port->portno);

  if(c->u->recv) {
    c->u->recv(c);
  }
}
/*---------------------------------------------------------------------------*/
void
loramac_sent(struct loramac_port *port, int status, int num_tx)
{
  struct loramac_conn *c = (struct loramac_conn *)port;
  PRINTF("%d.%d: LoRaMAC: loramac_sent on port %d\n",
   linkaddr_node_addr.u8[0],linkaddr_node_addr.u8[1],
   port->portno);

  if(c->u->sent) {
    c->u->sent(c, status, num_tx);
  }
}
/*---------------------------------------------------------------------------*/
const struct network_driver loramac_net_driver = {
  "LoRaMac NET",
  init,
  input
};
/** @} */
