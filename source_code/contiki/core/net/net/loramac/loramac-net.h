/*
 * Copyright (c) 2017, David Rodenas-Herraiz.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         Header for the LoRaMAC network driver initialization and common code
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */

#ifndef LORAMAC_NET_H_
#define LORAMAC_NET_H_

#include "net/queuebuf.h"
#include "net/linkaddr.h"
#include "net/packetbuf.h"

#include "net/mac/mac.h"

#include "loramac-port.h"
#include "loramacbuf.h"

extern const struct network_driver loramac_net_driver;


struct loramac_conn;

#define LORAMAC_ATTRIBUTES

/**
 * \brief     Callback structure for LoRaMAC
 *
 */
struct loramac_callbacks {
  /** Called when a packet has been received by the LoRaMAC module. */
  void (* recv)(struct loramac_conn *ptr);
  void (* sent)(struct loramac_conn *ptr, int status, int num_tx);
};

struct loramac_conn {
  struct loramac_port port;
  const struct loramac_callbacks *u;
};

/**
 * \brief      Set up a LoRaMAC connection
 * \param c    A pointer to a struct loramac_conn
 * \param port The port on which the connection will operate
 * \param u    A struct loramac_callbacks with function pointers to functions
 * 						 that will be called when a packet has been received
 *
 *             This function sets up a LoRaMAC connection on the
 *             specified port. The caller must have allocated the
 *             memory for the struct loramac_conn, usually by declaring it
 *             as a static variable.
 *
 *             The struct loramac_callbacks pointer must point to a structure
 *             containing a pointer to a function that will be called
 *             when a packet arrives on the port.
 *
 */
void loramac_open(struct loramac_conn *c, uint16_t port,
	       const struct loramac_callbacks *u);

/**
 * \brief      Close a LoRaMAC connection
 * \param c    A pointer to a struct loramac_conn
 *
 *             This function closes a LoRaMAC connection that has
 *             previously been opened with loramac_open().
 *
 *             This function typically is called as an exit handler.
 *
 */
void loramac_close(struct loramac_conn *c);

/**
 * \brief      Send a LoRaMAC packet
 * \param c    The LoRaMAC connection on which the packet should be sent
 * \retval     Non-zero if the packet could be sent, zero otherwise
 *
 *             This function sends a LoRaMAC packet. The packet must
 *             be present in the packetbuf before this function is called.
 *
 *             The parameter c must point to a LoRaMAC connection that
 *             must have previously been set up with loramac_open().
 *
 */
int loramac_send(struct loramac_conn *c);

/**
 * \brief      Internal LoRaMAC-net function: Pass a packet to the loramac app layer
 *
 *             This function is used internally by LoRaMAC-net to pass
 *             packets to the loramac app layer. Should never be called
 *             directly.
 *
 */

void loramac_input(struct loramac_port *port);

void loramac_sent(struct loramac_port *port, int status, int num_tx);

#endif /* LORAMAC_NET_H_ */
