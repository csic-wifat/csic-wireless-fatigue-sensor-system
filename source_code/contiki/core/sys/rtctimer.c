/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

#include "rtctimer.h"
#include "contiki.h"

#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

static struct rtctimer *next_rtctimer;

/*---------------------------------------------------------------------------*/
void
rtctimer_init(void)
{
  rtctimer_arch_init();
}
/*---------------------------------------------------------------------------*/
int
rtctimer_set(struct rtctimer *rtctimer, rtctimer_clock_t time,
	   rtctimer_clock_t duration,
	   rtctimer_callback_t func, void *ptr)
{
  int first = 0;

  if(next_rtctimer == NULL) {
    first = 1;
  }

  rtctimer->func = func;
  rtctimer->ptr = ptr;

  rtctimer->time = time;
  next_rtctimer = rtctimer;

  if(first == 1) {
    rtctimer_arch_schedule(time);
  }

  return RTIMER_OK;
}
/*---------------------------------------------------------------------------*/
void
rtctimer_run_next(void)
{
  struct rtctimer *t;
  if(next_rtctimer == NULL) {
    return;
  }
  t = next_rtctimer;
  next_rtctimer = NULL;

  t->func(t, t->ptr);

  if(next_rtctimer != NULL) {
//    rtctimer_arch_schedule(next_rtctimer->time);
  }
  return;
}
/*---------------------------------------------------------------------------*/
void
rtctimer_stop(struct rtctimer *rtctimer)
{
  rtctimer->func = NULL;
  next_rtctimer = NULL;
}
/*---------------------------------------------------------------------------*/

/** @}*/
