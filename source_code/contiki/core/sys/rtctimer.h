/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

#ifndef RTCTIMER_H_
#define RTCTIMER_H_

#include "contiki-conf.h"

#ifndef RTCTIMER_CLOCK_DIFF
typedef unsigned short rtctimer_clock_t;
#define RTCTIMER_CLOCK_DIFF(a,b)     ((signed short)((a)-(b)))
#endif /* RTCTIMER_CLOCK_DIFF */

#define RTCTIMER_CLOCK_LT(a, b)      (RTCTIMER_CLOCK_DIFF((a),(b)) < 0)

#include "rtctimer-arch.h"

/**
 * \brief      Initialize the real-time scheduler.
 *
 *             This function initializes the real-time scheduler and
 *             must be called at boot-up, before any other functions
 *             from the real-time scheduler is called.
 */
void rtctimer_init(void);

struct rtctimer;
typedef void (* rtctimer_callback_t)(struct rtctimer *t, void *ptr);

/**
 * \brief      Representation of a real-time task
 *
 *             This structure represents a real-time task and is used
 *             by the real-time module and the architecture specific
 *             support module for the real-time module.
 */
struct rtctimer {
  rtctimer_clock_t time;
  rtctimer_callback_t func;
  void *ptr;
};

enum {
  RTCTIMER_OK,
  RTCTIMER_ERR_FULL,
  RTCTIMER_ERR_TIME,
  RTCTIMER_ERR_ALREADY_SCHEDULED,
};

/**
 * \brief      Post a real-time task.
 * \param task A pointer to the task variable previously declared with RTCTIMER_TASK().
 * \param time The time when the task is to be executed.
 * \param duration Unused argument.
 * \param func A function to be called when the task is executed.
 * \param ptr An opaque pointer that will be supplied as an argument to the callback function.
 * \return     Non-zero (true) if the task could be scheduled, zero
 *             (false) if the task could not be scheduled.
 *
 *             This function schedules a real-time task at a specified
 *             time in the future.
 *
 */
int rtctimer_set(struct rtctimer *task, rtctimer_clock_t time,
	       rtctimer_clock_t duration, rtctimer_callback_t func, void *ptr);


/**
 * \brief      Stop the execution of a real-time task.
 *
 *
 */
void rtctimer_stop(struct rtctimer *task);

/**
 * \brief      Execute the next real-time task and schedule the next task, if any
 *
 *             This function is called by the architecture dependent
 *             code to execute and schedule the next real-time task.
 *
 */
void rtctimer_run_next(void);

/**
 * \brief      Get the current clock time
 * \return     The current time
 *
 *             This function returns what the real-time module thinks
 *             is the current time. The current time is used to set
 *             the timeouts for real-time tasks.
 *
 * \hideinitializer
 */
#define RTCTIMER_NOW() rtctimer_arch_now()

/**
 * \brief      Get the time that a task last was executed
 * \param task The task
 * \return     The time that a task last was executed
 *
 *             This function returns the time that the task was last
 *             executed. This typically is used to get a periodic
 *             execution of a task without clock drift.
 *
 * \hideinitializer
 */
#define RTCTIMER_TIME(task) ((task)->time)

void rtctimer_arch_init(void);
void rtctimer_arch_schedule(rtctimer_clock_t t);
/*rtctimer_clock_t rtctimer_arch_now(void);*/

#define RTCTIMER_SECOND RTCTIMER_ARCH_SECOND

/* RTCTIMER_GUARD_TIME is the minimum amount of rtctimer ticks between
   the current time and the future time when a rtctimer is scheduled.
   Necessary to avoid accidentally scheduling a rtctimer in the past
   on platforms with fast rtctimer ticks. Should be >= 2. */
#ifdef RTCTIMER_CONF_GUARD_TIME
#define RTCTIMER_GUARD_TIME RTCTIMER_CONF_GUARD_TIME
#else /* RTCTIMER_CONF_GUARD_TIME */
#define RTCTIMER_GUARD_TIME (RTCTIMER_ARCH_SECOND >> 14)
#endif /* RTCTIMER_CONF_GUARD_TIME */

#endif /* RTCTIMER_H_ */

/** @} */
/** @} */
