/*
 * Copyright (c) 2017, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Application for continuously sampling a strain signal using the
 *         built-in ADC in the NZ32's microcontroller
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include "contiki.h"
#include "dev/leds.h"
#include "sys/clock.h"
#include "dev/soc-adc.h"

#define DEBUG 1
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...) do {} while (0)
#endif
/*---------------------------------------------------------------------------*/
#define SAMPLE_INTERVAL                       ( RTIMER_SECOND >> 6 )  // ~64Hz
/*---------------------------------------------------------------------------*/
#define ADC_CHANNEL                           ( ADC_Channel_0 )
/*---------------------------------------------------------------------------*/
process_event_t adc_trigger_event;
static void sample_callback( struct rtimer *timer, void *ptr );
static void sample_callback( struct rtimer *timer, void *ptr )
{
  process_post(PROCESS_BROADCAST, adc_trigger_event, (process_data_t)NULL);
}
/*---------------------------------------------------------------------------*/
PROCESS(adc_collect_process, "ADC collect process");
AUTOSTART_PROCESSES(&adc_collect_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(adc_collect_process, ev, data)
{
  static struct rtimer rt;

  PROCESS_BEGIN();
  PROCESS_PAUSE();

  adc_trigger_event = process_alloc_event();

  /* Configure ADC channel */
  soc_adc.configure(SENSORS_HW_INIT, ADC_CHANNEL);

  /* Since the ADC is is clocked by a HSI (16MHz) frequency, and the sampling
   * time is 192 cycles, so the ADC conversion frequency (fADCconv) is
   * 16MHz / 192 ~= 83KHz.
   */
  soc_adc.configure(SOC_ADC_PARAM_SAMPLE_TIME, ADC_SampleTime_4Cycles);

  /* Enable continuous ADC conversion mode */
  soc_adc.configure(SOC_ADC_PARAM_CONVERSION_MODE, ENABLE);

  /* Activate ADC */
  SENSORS_ACTIVATE(soc_adc);

  /* Start continuous conversion*/
  SOC_ADC_SENSORS_TRIGGER_CONVERSION(1);

  /* Start timer */
  rtimer_set(&rt, SAMPLE_INTERVAL, 0, sample_callback, NULL);

  /* Infinite loop */
  while(1)
  {
    static uint16_t adc_value;

    /* Wait for an event */
    PROCESS_YIELD();

    /* Timer event */
    if(ev == adc_trigger_event)
    {
      /* Read ADC value */
      adc_value = (uint16_t)soc_adc.value(SOC_ADC_VALUE);

      /* Trigger next conversion */
      rtimer_set(&rt, SAMPLE_INTERVAL, 0, sample_callback, NULL);

      /* Show voltage value */
      printf("%lu,%d\n", clock_time(), adc_value);
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
