/*
 * Copyright (c) 2018, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Application for peak-through detection and rainflow counting.
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include <stdio.h>
#include "dev/leds.h"
#include "sys/clock.h"
#include "dev/soc-adc.h"
#include "sensors/fatigue-sensor.h"
#include "rainflow.h"
/*---------------------------------------------------------------------------*/
#define DEBUG 1
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)

#define DEBUG_LPM 1
#ifdef DEBUG_LPM
#include "lpm.h"
#endif /* DEBUG_LPM */

#else /* DEBUG_LPM */
#define PRINTF(...) do {} while (0)
#endif /* DEBUG_LPM */
/*---------------------------------------------------------------------------*/
#define RAINFLOW_CONF_BUFFER_SIZE 256

#ifdef RAINFLOW_CONF_BUFFER_SIZE
#define RAINFLOW_BUFFER_SIZE    RAINFLOW_CONF_BUFFER_SIZE
#else
#define RAINFLOW_BUFFER_SIZE    256
#endif /* RAINFLOW_CONF_BUFFER_SIZE */

static int                      buffer[RAINFLOW_BUFFER_SIZE];
static struct  rainflow_count   count;

void rainflow_callback(int type, int min, int max);
/*---------------------------------------------------------------------------*/
void
rainflow_callback(int type, int min, int max)
{
  int range = min - max;
  if(range < 0) range = -range;
#ifdef DEBUG_LPM
  lpm_wakeup();
#endif
  PRINTF("%lu,%u,%d,%d,%d\n", clock_time(), type, min, max, range);
}
/*---------------------------------------------------------------------------*/
PROCESS(fatigue_sensor__main_process, "Fatigue sensor main process");
AUTOSTART_PROCESSES(&fatigue_sensor__main_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(fatigue_sensor__main_process, ev, data)
{
  static struct etimer et;

  PROCESS_BEGIN();
  PROCESS_PAUSE();

  /* Initialize rain flow counting algorithm */
  rainflow_init(&count, buffer,
    RAINFLOW_BUFFER_SIZE, rainflow_callback);

  /* Activate the sensor */
  SENSORS_ACTIVATE(fatigue_sensor);

  /* Infinite loop */
  while(1)
  {
    static int16_t peak_value;

    /* Wait for an event */
    PROCESS_YIELD();

    /* Fatigue sensor event */
    if(ev == sensors_event && data == &fatigue_sensor)
    {
      /* Read fatigue sensor's data channel */
      peak_value = fatigue_sensor.value(0);

      /* Show ADC value */
#ifdef DEBUG_LPM
      lpm_wakeup();
#endif
      PRINTF("%lu,%d\n", clock_time(), peak_value);

      /* Rainflow counting */
      rainflow_push(&count, (int32_t)peak_value);
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
