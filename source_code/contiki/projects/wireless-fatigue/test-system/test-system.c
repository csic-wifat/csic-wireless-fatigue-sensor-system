/*
 * Copyright (c) 2019, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         Application for peak-trough detection and rainflow counting.
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include <stdio.h>
#include "dev/leds.h"
#include "sys/clock.h"
#include "rtctimer.h"

#include "lora-radio.h"
#include "loramac-net.h"
#include "loramac-mac.h"
#include "lorawan-conf.h"

#include "dev/soc-adc.h"
#include "sensors/fatigue-sensor.h"

#include "rainflow.h"
#include "bucket.h"
/*---------------------------------------------------------------------------*/
#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else /* DEBUG */
#define PRINTF(...) do {} while (0)
#endif /* DEBUG */
/*---------------------------------------------------------------------------*/
#define RAINFLOW_CONF_BUFFER_SIZE 256

#ifdef RAINFLOW_CONF_BUFFER_SIZE
#define RAINFLOW_BUFFER_SIZE    RAINFLOW_CONF_BUFFER_SIZE
#else
#define RAINFLOW_BUFFER_SIZE    256
#endif /* RAINFLOW_CONF_BUFFER_SIZE */

static int                      buffer[RAINFLOW_BUFFER_SIZE];
static struct  rainflow_count   count;

void rainflow_callback(int type, int min, int max);
/*---------------------------------------------------------------------------*/
uint8_t bucket_num = 10;
uint16_t bucket_lower_bound = 500; // 50ue according to parameters on paper
uint16_t bucket_upper_bound = 3300;

static bucket_t full_cycles_bucket;
static bucket_t one_half_cycles_bucket;
static uint16_t bucket_size = 0;
static uint16_t bucket_lower_ix = 0;
bucket_result_t create_cycle_histograms(void);
/*---------------------------------------------------------------------------*/
process_event_t tx_ready_event;
process_event_t sensor_reactivate_event;
/*---------------------------------------------------------------------------*/
#define SEND_INTERVAL_SECONDS         ( 3600 )  // Every hour
#define SEND_INTERVAL                 ( SEND_INTERVAL_SECONDS )
/*!
 * Transmission interval (in seconds)
 */
static rtctimer_clock_t send_interval = SEND_INTERVAL;
static uint8_t scheduled_tx = 0;
/*---------------------------------------------------------------------------*/
/*!
* Timer used to send data
*/
static struct rtctimer periodic_timer;
static void send_callback( struct rtctimer *timer, void *ptr );

#define SCHEDULE_TX( send_time ) {                                         \
    rtctimer_set(&periodic_timer, send_time * 1000, 0, send_callback, NULL); \
  }
/*---------------------------------------------------------------------------*/
typedef enum {
  LORAMAC_DATA_PORT = 150,
} loramac_port_t;
/*---------------------------------------------------------------------------*/
static void
sent_data( struct loramac_conn *ptr, int status, int num_tx )
{
  PRINTF( "Data sent on port %d: status %d num_tx %d\n",
    ptr->port.portno, status, num_tx );
  process_post(PROCESS_BROADCAST, sensor_reactivate_event, (process_data_t)NULL);
}
static const struct loramac_callbacks loramac_data_call = { NULL, sent_data };
static struct loramac_conn loramac_data;
/*---------------------------------------------------------------------------*/
static void
send_callback( struct rtctimer *timer, void *ptr )
{
  if( !scheduled_tx )
  {
    /* Set flag */
    scheduled_tx = 1;
    /* Trigger modbus sensors reading */
    process_post(PROCESS_BROADCAST, tx_ready_event, (process_data_t)NULL);
  }
}
/*---------------------------------------------------------------------------*/
bucket_result_t
create_cycle_histograms(void)
{
  bucket_size = (uint16_t)((bucket_upper_bound - bucket_lower_bound) / \
    (bucket_num));
  bucket_lower_ix = (uint16_t)(bucket_lower_bound/bucket_size);

  /* Allocate space in memory (heap) for full-cycles histogram */
  if (bucket_init(&full_cycles_bucket, bucket_num) != BUCKET_RESULT_OK) {
    return BUCKET_RESULT_ERROR;
  }
  /* Allocate space in memory (heap) for one-half-cycles histogram */
  if (bucket_init(&one_half_cycles_bucket, bucket_num) != BUCKET_RESULT_OK) {
    return BUCKET_RESULT_ERROR;
  }

  return BUCKET_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
void
rainflow_callback(int type, int min, int max)
{
  int range = min - max;
  if(range < 0) range = -range;

  if( range >= bucket_lower_bound || range <= bucket_upper_bound) {
    uint32_t index = range/bucket_size - bucket_lower_ix;

    if(type == RAINFLOW_HALFCYCLE) {
      one_half_cycles_bucket.bucketbuf[index] +=1;
    } else if (type == RAINFLOW_CYCLE ) {
      full_cycles_bucket.bucketbuf[index] +=1;
    }
  }
}
/*---------------------------------------------------------------------------*/
PROCESS(fatigue_sensor__main_process, "Test System Main Process");
AUTOSTART_PROCESSES(&fatigue_sensor__main_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(fatigue_sensor__main_process, ev, data)
{
  PROCESS_BEGIN();
  PROCESS_PAUSE();

  /* LoRaMAC initialization */
#if OVER_THE_AIR_ACTIVATION == 1
  /* Wait for mac */
  PROCESS_WAIT_EVENT_UNTIL( ev == PROCESS_EVENT_LORAMAC_JOIN);
#endif

  /* Open LoRaMAC data connection*/
  loramac_open( &loramac_data, LORAMAC_DATA_PORT, &loramac_data_call );

  /* Create transmission event */
  tx_ready_event = process_alloc_event();

  /* We will de-activate the sensor temporarily for transmissions. We then
     create another event for reactivating the sensor after transmission
     completion */
  sensor_reactivate_event = process_alloc_event();

  /* Allocate space in memory (heap) for histogram */

  if(create_cycle_histograms() != BUCKET_RESULT_OK) {
    PROCESS_EXIT();
  }
  /* Initialize rain flow counting algorithm */
  rainflow_init(&count, buffer,
    RAINFLOW_BUFFER_SIZE, rainflow_callback);

  SCHEDULE_TX( send_interval );

  /* Activate the sensor */
  SENSORS_ACTIVATE(fatigue_sensor);

  /* Infinite loop */
  while(1)
  {
    static int16_t adc_value;
    static uint16_t seqn;
    uint8_t  *buf = NULL;
    uint8_t   len = 0;  // Data length

    /* Wait for an event */
    PROCESS_YIELD();

    /* Fatigue sensor event */
    if(ev == sensors_event && data == &fatigue_sensor)
    {
      /* Read fatigue sensor's data channel */
      adc_value = fatigue_sensor.value(0);

      /* Rainflow counting */
      rainflow_push(&count, (int32_t)adc_value);
    }

    /* Transmission event */
    if( ev == tx_ready_event )
    {
      if( scheduled_tx )
      {
        /* Deactivate fatigue sensor temporarily for transmission */
        SENSORS_DEACTIVATE(fatigue_sensor);

        /*
         * For example purposes only, we will show how to send a data message
         * containing full cycles histogram data.
         */

        /* Create data message */
        packetbuf_clear( );
        buf = packetbuf_dataptr( );
        len = 0;

        /* Sequence number */
        buf[len] = ( seqn >> 8 ) & 0xFF;              len+=1;
        buf[len] = seqn & 0xFF;                       len+=1;

        /* Histogram data */
        if ( bucket_message(&full_cycles_bucket, buf+len, &len) == BUCKET_RESULT_OK)
        {
          /* Set data length */
          packetbuf_set_datalen( len );
          /**
           * Send data message
           */
          loramac_send( &loramac_data );
        }

        /* Increase sequence number */
        seqn++;

        /* Schedule next data transmission */
        SCHEDULE_TX( send_interval );

        /* Reset flag */
        scheduled_tx = 0;
      }
    }
    /* Fatigue sensor reactivation event */
    if( ev == sensor_reactivate_event )
    {
      /* Set histogram data to zero */
      bucket_reset(&full_cycles_bucket);
      bucket_reset(&one_half_cycles_bucket);

      /* Re-activate fatigue sensor */
      SENSORS_ACTIVATE(fatigue_sensor);
    }
  }

  bucket_deinit(&full_cycles_bucket);
  bucket_deinit(&one_half_cycles_bucket);
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
