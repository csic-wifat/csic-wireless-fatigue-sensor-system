#ifndef PROJECT_CONF_H_
#define PROJECT_CONF_H_

#ifdef PLATFORM_HAS_FATIGUE_SENSOR
#undef PLATFORM_HAS_FATIGUE_SENSOR
#define PLATFORM_HAS_FATIGUE_SENSOR		(1)
#endif

#undef NETSTACK_CONF_NETWORK
#define NETSTACK_CONF_NETWORK						loramac_net_driver

#undef NETSTACK_CONF_RDC
#define NETSTACK_CONF_RDC           		loramac_mac_driver

#endif /* PROJECT_CONF_H_ */
