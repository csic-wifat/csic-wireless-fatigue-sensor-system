/*
 * Copyright (c) 2019, David Rodenas-Herraiz
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/**
 * \file
 *         This file allows to create a linear histogram
 * \author
 *         David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
 */
/*---------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h> /* memset */
#include "bucket.h"
/*---------------------------------------------------------------------------*/
#define MAX_BUCKET_BINS (12)
#define MAX_BUCKET_NUM  (3)
/*---------------------------------------------------------------------------*/
static uint8_t bucket_num = MAX_BUCKET_BINS;
static uint8_t nbuckets = 0;
/*---------------------------------------------------------------------------*/
// #define DEBUG_BUCKET
#ifdef DEBUG_BUCKET
#include <stdio.h>
bucket_result_t bucket_print(bucket_t *b)
{
    if(!b || b->nbins > MAX_BUCKET_BINS) {
        return BUCKET_RESULT_ERROR;
    }

    for (uint8_t i = 0 ; i < b->nbins ; ++i) {
        printf("bucket %u: %lu\n", i, b->bucketbuf[i]);
    }
    return BUCKET_RESULT_OK;
}
#else /* DEBUG_BUCKET */
bucket_result_t bucket_print(bucket_t *b)
{
    return BUCKET_RESULT_OK;
}
#endif /* DEBUG_BUCKET */
/*---------------------------------------------------------------------------*/
bucket_result_t
bucket_message(bucket_t *b, uint8_t *buf, uint8_t *len)
{
    if(!b || !buf || !len || b->nbins > MAX_BUCKET_BINS) {
        return BUCKET_RESULT_ERROR;
    }

    *len += sizeof(uint32_t) * b->nbins;
    memcpy(buf, b->bucketbuf, sizeof(uint32_t) * b->nbins);
    return BUCKET_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
bucket_result_t
bucket_reset(bucket_t *b)
{
    if (b != NULL && b->nbins <= MAX_BUCKET_BINS) {
        memset(b->bucketbuf, 0, sizeof(uint32_t) * b->nbins);
        return BUCKET_RESULT_OK;
    }
    return BUCKET_RESULT_ERROR;
}
/*---------------------------------------------------------------------------*/
bucket_result_t
bucket_deinit(bucket_t *b) {
    if(!b || nbuckets == 0) {
        return BUCKET_RESULT_ERROR;
    }
    free(b->bucketbuf);
    --nbuckets;
    return BUCKET_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
bucket_result_t
bucket_init(bucket_t *b, uint8_t n)
{
    /* Sanity check */
    if(!b || nbuckets >= MAX_BUCKET_NUM || n > MAX_BUCKET_BINS) {
        return BUCKET_RESULT_ERROR;
    }

    /* Limit number of buckets for memory management purposes */
    ++nbuckets;

    /* Set number of buckets */
    b->nbins = n;

    /* Allocate space in heap */
    b->bucketbuf = (uint32_t*) malloc(sizeof(uint32_t) * bucket_num);

    /* Set to zero */
    bucket_reset(b);

    /* Return */
    return BUCKET_RESULT_OK;
}
/*---------------------------------------------------------------------------*/
