# Wireless *Fatigue* Sensor System

The simulation and experimental data, data analysis scripts, and the source code of our wireless sensor system for fatigue strain cycles monitoring, published in "Power-efficient piezoelectric fatigue measurement using long-range wireless sensor networks", Smart Materials and Structures, 2019, [https://doi.org/10.1088/1361-665X/ab2c46](https://doi.org/10.1088/1361-665X/ab2c46)

Authors:

* David Rodenas-Herraiz¹: david.rodenas@eng.cam.ac.uk
* Xiaomin Xu¹:           xx787@cam.ac.uk
* Paul R. A. Fidler¹:     praf1@cam.ac.uk
* Kenichi Soga²:          soga@berkeley.edu

¹ Department of Engineering, University of Cambridge, Cambridge CB1 1PZ, UK

² Department of Civil and Environmental Engineering, University of California at Berkeley, Berkeley, CA, UK
