#!/usr/bin/python

"""
  Python script to use the High Voltage Power Monitor from Monsoon Solutions.
  Please see: https://www.msoon.com/hvpm-product-documentation

  Author: David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.style.use('seaborn')
import matplotlib.pylab as pylab
params = {'legend.fontsize': 'large',
          'figure.figsize': (12, 6),
         'axes.labelsize': 'large',
         'axes.titlesize':'large',
         'xtick.labelsize':'large',
         'ytick.labelsize':'large'}
pylab.rcParams.update(params)

import pandas as pd
import Monsoon.HVPM as HVPM
import Monsoon.sampleEngine as sampleEngine
import Monsoon.Operations as op

# Create the object appropriate to your Power Monitor hardware,
# and then call setup_usb(), which will connect to the first
# available device.
Mon = HVPM.Monsoon()
Mon.setup_usb()

# Create an instance of the sampleEngine class
engine = sampleEngine.SampleEngine(Mon)

# Disable console output
engine.ConsoleOutput(False)

def sample(filename, samples=1000, granularity=1, vpp=0, freq=0):
  # Enable the builtin CSV output to store the samples on a CSV file
  engine.enableCSVOutput(filename)
  # Start sampling
  engine.startSampling(samples=numSamples, granularity=granularity)

def enable_vout(vout=3.3):
  # Disable Vout
  Mon.setVout(vout)

def disable_vout():
  # Disable Vout
  Mon.setVout(0.00)

# Plot
def plot_samples(filename):
  # Read csv file
  df = pd.read_csv(filename)

  # Create figure and axes
  fig = plt.figure()
  ax0 = fig.add_subplot(1, 1, 1)
  ax1 = ax0.twinx()

  # Extract fields
  timeStamp = df['Time(ms)']
  current = df['Main(mA)']
  voltage = df['Main Voltage(V)']

  # Compute power (in mW)
  power = current * voltage

  print("Average current consumption: %.2f mA" % np.mean(current))
  print("Average power consumption: %.2f mW" % np.mean(power))

  ax0.plot( timeStamp, current, 'b-', label='Current');
  ax1.plot( timeStamp, power, 'r-', label='Power');

  ax0.set_xlabel('Time (s)')

  ax0.set_ylabel('Current (mA)', color='b')
  ax1.set_ylabel('Power (mW)', color='r')

  ax0.tick_params('y', colors='b')
  ax1.tick_params('y', colors='r')

  plt.tight_layout()

  plt.show()


# Wave-signal generator
vpp = '2' # peak-to-peak voltage
freq = '1' # frequency

# Voltage monitor settings
numSamples = 25000 #sample for five seconds
granularity = 1 # granularity
vout = 3.3 # Voltage supply output

# Filename where the data will be stored in csv format
filename = "data/power-monitor-%sVpp-%sHz.csv" % (vpp, freq)

# Sampling
enable_vout(vout)
sample(filename, samples=numSamples, granularity=granularity, vpp=vpp, freq=freq)
disable_vout()

# Plot
plot_samples(filename)
