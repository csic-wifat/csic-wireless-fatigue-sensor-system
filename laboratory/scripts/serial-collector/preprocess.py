#!/usr/bin/python

"""
  This script was created to preprocess the data captured with
  fatigue-sensor-collector.py and conventional-sensor-collector.py. Data from
  these applications was stored in subfolder within 'exp-20190312/exp01'
  (please see variables experiment_folder and datafolder). These subfolders
  were named, e.g., 600mVpp, where 600mV is the peak-to-peak voltage amplitude
  configured with the oscilloscope. In such folder, different data files for
  each of the previously mentioned applications where generated for individual
  oscillation frequencies, for example 'conventional-capture-1Hz.csv' for data
  obtained with the conventional approach at 1Hz, and 'fatigue-capture-1Hz.csv'
  for data obtained with our proposed solution.

  Author: David Rodenas-Herraiz <david.rodenas@eng.cam.ac.uk>
"""


################################################################################
# Import pandas library
import pandas as pd
pd.set_option('max_rows', 5)
# Import matplotlib and pylab
import matplotlib.pyplot as plt
import pylab
import matplotlib.gridspec as gridspec
# Import numpy
import numpy as np
# Import sys
import sys
# Import os and errno
import os, errno
# Import glob
import glob
# Import piezo/fatigue sensor libraries
import sys
sys.path.insert(0, "../../../simulation/scripts/lib")
from piezo import Piezo
from fatigue_sensor import FatigueSensor, create_sensor
# Import datetime
from datetime import datetime
# Import library to create zip files
import zipfile
#
import re
################################################################################
# Experiment folder
experiment_folder = 'exp-20190312'
datafolder = 'exp01'
# Plot figures
plot_figures = 0
# Save data to csv file
to_csv = 1
# Create zip with csv files
create_zip = 0
################################################################################
## GLOBAL VARIABLES
# Range of peak-to-peak voltage values which have been tested
#vmax = 2
#volt_range = np.arange(1, vmax + 1)
volt_range = [0.6, 1.6]
# Directory containing processed data
dataDir = os.path.join(os.getcwd(), experiment_folder, datafolder)
processedDatadir = os.path.join(os.getcwd(), experiment_folder, 'processed-data')
# Create sensor
sensor = create_sensor()
################################################################################
# Check if list of lists is essentially empty
def isListEmpty(inList):
  if isinstance(inList, list): # Is a list
    return all( map(isListEmpty, inList) )
  return False # Not a list
################################################################################
def process_continuous_sampling_data(plot_figures=0, to_csv=1):
  finalDf = pd.DataFrame()  # Dataframe containing all the data
  for volt in volt_range:
    # Load data
    try:
      #filedir = os.path.join(dataDir, '%dVpp' % (volt) )
      if volt < 1:
        filedir = os.path.join(dataDir, '%dmVpp' % (volt*1000) )
      else:
        filedir = os.path.join(dataDir, '%.1fVpp' % (volt) )

      datafiles = glob.glob(os.path.join(filedir, 'conventional-capture*'))
      if isListEmpty(datafiles): continue

      filename_str = [(filename.split('conventional-capture-')[1]).split('Hz.csv')[0] for filename in datafiles]
      idx = [[m.start() for m in re.finditer('-', dt_str)][-1] for dt_str in filename_str]
      datetime_str = [dt_str[:idx[i]] for i, dt_str in enumerate(filename_str)]
      # Range of frequencies which have been tested
      freq_range = np.array([int(dt_str[idx[i]+1:]) for i, dt_str in enumerate(filename_str)]).reshape(len(filename_str), 1)
    except Exception as e:
      sys.stderr.write("Exception: %s\n" % str(e))
      sys.exit(1)

    exp_datetime = [datetime.strptime(date_str, "%Y-%m-%d %H-%M-%S") for date_str in datetime_str]

    file_data = [pd.read_csv(filename) for filename in datafiles]

    # Add to database
    for i, csvdata in enumerate(file_data):
      # Tested peak-to-peak voltage
      csvdata['Vpp'] = volt
      # Tested frequency
      csvdata['Freq'] = freq_range[i][0]
      # Experiment datetime
      csvdata['ExpDatetime'] = exp_datetime[i]
      # Sample rate
      csvdata['SamplesPerSecond'] = 64
      # Piezoelectric sensor parameters
      csvdata['SensorLength'] = sensor.getPiezo().getLength()
      csvdata['SensorWidth'] = sensor.getPiezo().getWidth()
      csvdata['SensorThickness'] = sensor.getPiezo().getThickness()
      csvdata['SensorD31'] = sensor.getPiezo().getD31()
      csvdata['SensorD32'] = sensor.getPiezo().getD32()
      csvdata['SensorD33'] = sensor.getPiezo().getD33()
      csvdata['SensorYoungsModulus'] = sensor.getPiezo().getYoungsModulus()
      csvdata['SensorRelPermittivity'] = sensor.getPiezo().getRelativePermittivity()
      # Fatigue sensor parameters
      # First stage
      csvdata['SensorStage1Rf'] = sensor.getFirstStage().getRf()
      csvdata['SensorStage1Cf'] = sensor.getFirstStage().getCf()
      # Second stage
      csvdata['SensorStage2Rf'] = sensor.getSecondStage().getRf()
      csvdata['SensorStage2RnonIn'] = sensor.getSecondStage().getRnonIn()
      csvdata['SensorStage2Rd'] = sensor.getSecondStage().getRd()
      csvdata['SensorStage2Cd'] = sensor.getSecondStage().getCd()

    # Concatenate dataframes in file_data to form a single dataframe
    df = pd.concat(file_data)
    # Append to final dataframe
    finalDf = finalDf.append(df)

    ## Plotting
    if plot_figures == 1:
      # Format experiment datetime
      exp_datetime = [date.strftime("%Y-%m-%d") for date in exp_datetime]
      # Clock time
      clock_time = [csvdata.clock for csvdata in file_data]
      # Set to zero
      # clock_time = [clock - clock[0] for clock in clock_time]
      # Convert voltage into ustrain
      ue_data = [[sensor.convert2ustrain(val, vPoisson) for val in csvdata.voltage] for csvdata in file_data]

      # Create figure
      fig = pylab.figure()
      AX = gridspec.GridSpec(3,2)
      AX.update(wspace = 0.4, hspace = 0.2)

      if( len(set(exp_datetime)) == 1 ):
        fig.suptitle('Experiment date: %s' % (exp_datetime[0]))

      for i in np.arange(0, 3):
        for j in np.arange(0, 2):
          # Create subplot
          ax = plt.subplot(AX[i,j])
          # Plot
          ax.plot(clock_time[i*2 + j], ue_data[i*2 + j], '-', label='Strain (%d Vpp, %d Hz)' %(volt, freq_range[i*2 + j][0]) )
          # Format
          if( len(set(exp_datetime)) != 1 ):
            ax.set_title('Date: %s' % (exp_datetime[i*2 + j]), fontweight='bold')
          ax.set_xlabel("Clock time (s)", fontweight='bold')
          ax.set_ylabel(u'Strain (\u03bc\u03b5)', fontweight='bold')
          ax.grid(True)
          ax.spines['right'].set_visible(False)
          ax.spines['top'].set_visible(False)
          ax.yaxis.set_ticks_position('left')
          ax.xaxis.set_ticks_position('bottom')
          ax.legend(loc='upper right', shadow=True)

  # Save data to csv file
  if to_csv == 1:
    csvname = 'data-from-conventional-' + experiment_folder[experiment_folder.find('exp-')+4:] + '.csv'

    if not os.path.isdir(processedDatadir):
      os.makedirs(processedDatadir)

    finalDf.to_csv(os.path.join(processedDatadir, csvname), index=False)
################################################################################
def process_fatigue_sensor_data(plot_figures=0, to_csv=1):
  rf_df = pd.DataFrame()  # Dataframe containing all the rainflow data
  adc_rf = pd.DataFrame() # Dataframe containing all the rainflow data

  for volt in volt_range:
    # Load data
    try:
      #filedir = os.path.join(dataDir, '%dVpp' % (volt) )
      if volt < 1:
        filedir = os.path.join(dataDir, '%dmVpp' % (volt*1000) )
      else:
        filedir = os.path.join(dataDir, '%.1fVpp' % (volt) )

      datafiles = glob.glob(os.path.join(filedir, 'fatigue-capture*'))
      if isListEmpty(datafiles): continue
      filename_str = [(filename.split('fatigue-capture-')[1]).split('Hz.csv')[0] for filename in datafiles]

      idx = [[m.start() for m in re.finditer('-', dt_str)][-1] for dt_str in filename_str]
      datetime_str = [dt_str[:idx[i]] for i, dt_str in enumerate(filename_str)]
      # Range of frequencies which have been tested
      freq_range = np.array([int(dt_str[idx[i]+1:]) for i, dt_str in enumerate(filename_str)]).reshape(len(filename_str), 1)
    except Exception as e:
      sys.stderr.write("Exception: %s\n" % str(e))
      sys.exit(1)

    exp_datetime = [datetime.strptime(date_str, "%Y-%m-%d %H-%M-%S") for date_str in datetime_str]

    file_data = [pd.read_csv(filename) for filename in datafiles]

    for i, csvdata in enumerate(file_data):
      if not csvdata.empty:
        # Extract rainflow counting data
        _rf_df = csvdata[csvdata.ctype.notnull()].rename(index=str, columns={"peaksOrRange": "range"}).reset_index().drop(columns=['index'])
        # Extract sampled adc data
        _adc_rf = csvdata[csvdata.ctype.isna()].drop(columns=['ctype', 'minPeak', 'maxPeak']).rename(index=str, columns={"peaksOrRange": "voltage"}).reset_index().drop(columns=['index'])

        # Add to database
        for df in (_rf_df, _adc_rf):
          # Tested peak-to-peak voltage
          df['Vpp'] = volt
          # Tested frequency
          df['Freq'] = freq_range[i][0]
          # Experiment datetime
          df['ExpDatetime'] = exp_datetime[i]
          # Piezoelectric sensor parameters
          df['SensorLength'] = sensor.getPiezo().getLength()
          df['SensorWidth'] = sensor.getPiezo().getWidth()
          df['SensorThickness'] = sensor.getPiezo().getThickness()
          df['SensorD31'] = sensor.getPiezo().getD31()
          df['SensorD32'] = sensor.getPiezo().getD32()
          df['SensorD33'] = sensor.getPiezo().getD33()
          df['SensorYoungsModulus'] = sensor.getPiezo().getYoungsModulus()
          df['SensorRelPermittivity'] = sensor.getPiezo().getRelativePermittivity()
          # Fatigue sensor parameters
          # First stage
          df['SensorStage1Rf'] = sensor.getFirstStage().getRf()
          df['SensorStage1Cf'] = sensor.getFirstStage().getCf()
          # Second stage
          df['SensorStage2Rf'] = sensor.getSecondStage().getRf()
          df['SensorStage2RnonIn'] = sensor.getSecondStage().getRnonIn()
          df['SensorStage2Rd'] = sensor.getSecondStage().getRd()
          df['SensorStage2Cd'] = sensor.getSecondStage().getCd()

        # Append data
        rf_df = rf_df.append(_rf_df)
        adc_rf = adc_rf.append(_adc_rf)

  ## Plotting
    if plot_figures == 1:
      # Format experiment datetime
      exp_datetime = [date.strftime("%Y-%m-%d") for date in exp_datetime]
      # Clock time
      clock_time = [csvdata[csvdata.ctype.isna()].clock for csvdata in file_data]
      # Convert voltage into ustrain
      ue_data = [[sensor.convert2ustrain(val, vPoisson) for val in csvdata[csvdata.ctype.isna()].peaksOrRange] for csvdata in file_data]

      # Create figure
      fig = pylab.figure()
      AX = gridspec.GridSpec(3,2)
      AX.update(wspace = 0.4, hspace = 0.2)

      if( len(set(exp_datetime)) == 1 ):
        fig.suptitle('Experiment date: %s' % (exp_datetime[0]))

      for i in np.arange(0, 3):
        for j in np.arange(0, 2):
          # Create subplot
          ax = plt.subplot(AX[i,j])
          # Plot
          ax.plot(clock_time[i*2 + j], ue_data[i*2 + j], '-', label='Strain (%d Vpp, %d Hz)' %(volt, freq_range[i*2 + j][0]) )
          # Format
          if( len(set(exp_datetime)) != 1 ):
            ax.set_title('Date: %s' % (exp_datetime[i*2 + j]), fontweight='bold')
          ax.set_xlabel("Clock time (s)", fontweight='bold')
          ax.set_ylabel(u'Strain (\u03bc\u03b5)', fontweight='bold')
          ax.grid(True)
          ax.spines['right'].set_visible(False)
          ax.spines['top'].set_visible(False)
          ax.yaxis.set_ticks_position('left')
          ax.xaxis.set_ticks_position('bottom')
          ax.legend(loc='upper right', shadow=True)

  # Save data to csv file
  if to_csv == 1:
    rf_csvname = 'data-from-fatigue-sensor-rainflow-' + experiment_folder[experiment_folder.find('exp-')+4:] + '.csv'
    adc_csvname = 'data-from-fatigue-sensor-peaks-' + experiment_folder[experiment_folder.find('exp-')+4:] + '.csv'

    if not os.path.isdir(processedDatadir):
      os.makedirs(processedDatadir)

    rf_df.to_csv(os.path.join(processedDatadir, rf_csvname), index=False)
    adc_rf.to_csv(os.path.join(processedDatadir, adc_csvname), index=False)
################################################################################
def zipdir(path, ziph):
  # ziph is zipfile handle
  for root, dirs, files in os.walk(path):
    for file in files:
      ziph.write(os.path.join(root, file))
################################################################################
def main():
  process_continuous_sampling_data(plot_figures=plot_figures, to_csv=to_csv)
  process_fatigue_sensor_data(plot_figures=plot_figures, to_csv=to_csv)

  if plot_figures == 1:
    plt.show()

  if create_zip == 1 and os.path.isdir(processedDatadir):
    zipname = os.path.join(experiment_folder, \
      'wifat-experimentdata-' + experiment_folder[experiment_folder.find('exp-')+4:] + '.zip')
    zipf = zipfile.ZipFile(zipname, 'w', zipfile.ZIP_DEFLATED)
    zipdir(os.path.join(experiment_folder, 'processed-data'), zipf)
    zipf.close()
################################################################################
if __name__ == '__main__':    # code to execute if called from command-line
    main()
################################################################################
