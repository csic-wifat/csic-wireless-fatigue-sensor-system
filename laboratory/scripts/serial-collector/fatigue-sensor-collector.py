#!/usr/bin/python
# -*- coding: utf-8 -*-

import matplotlib as mpl
mpl.use("TkAgg")
from matplotlib import pyplot as plt
import pylab
import os, errno
import matplotlib.gridspec as gridspec
import numpy as np
import datetime
import threading
import serial
import serial.tools.list_ports
import csv
from collections import deque
from time import sleep
import time
import sys
if sys.version_info[0] < 3:
  import Tkinter as tk
  import Tkconstants, tkFileDialog, tkMessageBox
  import ttk
  import tkFont as tkfont
else:
  import tkinter as tk
  import tkinter.ttk as ttk
  import tkinter.font as tkfont
  from tkinter import filedialog as tkFileDialog
  from tkinter import messagebox as tkMessageBox

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

mpl.style.use('seaborn')
import matplotlib.pylab as pylab
params = {'legend.fontsize': 'x-large',
          #'figure.figsize': (6, 6),
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'xtick.labelsize':'x-large',
         'ytick.labelsize':'x-large'}
pylab.rcParams.update(params)
# Piezo libraries
import sys
sys.path.insert(0, "../../../simulation/scripts/lib")
from piezo import Piezo
from fatigue_sensor import FatigueSensor, create_sensor
################################################################################
SHOW_IN_USTRAIN = False # Plot raw data in voltage units
################################################################################
CAPTURE_TO_STDOUT = False # For debug
################################################################################
# Whether the device is programmed to go to sleep mode
LOW_POWER_OP = True

if LOW_POWER_OP == True:   # Low power operation
  import re
  CLOCK_SECOND = 32.0
else:                   # Always on
  CLOCK_SECOND = 256.0

# ADC maximum value for 16-bit data
SOC_ADC_MAX_U16_VALUE = 65535.0

HALF_CYCLE_TYPE = 1
FULL_CYCLE_TYPE = 2
################################################################################
class AppDatabuf:
  # constr
  def __init__(self, maxLen):
    self.ax = deque(maxlen=maxLen)
    self.ay = deque(maxlen=maxLen)
    self.maxLen = maxLen
    self.nfields = 2

  # ring buffer
  def addToBuf(self, buf, val):
    if len(buf) < self.maxLen:
      buf.append(val)
    else:
      buf.popleft()
      buf.append(val)

  # add data
  def add(self, timestamp, data):
    self.addToBuf(self.ax, timestamp)
    self.addToBuf(self.ay, data)

  def getNumFields(self):
    return self.nfields
################################################################################
# class that holds rainflow data
class RainflowDatabuf:
  # constr
  def __init__(self, maxLen):
    self.cycle_type = deque(maxlen=maxLen)
    self.strain_range = deque(maxlen=maxLen)
    self.maxLen = maxLen
    self.nfields = 5
  ##############################################################################
  # ring buffer
  def addToBuf(self, buf, val):
    if len(buf) < self.maxLen:
      buf.append(val)
    else:
      buf.popleft()
      buf.append(val)
  ##############################################################################
  # add data
  def add(self, timestamp, cycle_type, strain_range):
    self.addToBuf(self.cycle_type, cycle_type)
    self.addToBuf(self.strain_range, strain_range)
  ##############################################################################
  def getNumFields(self):
    return self.nfields
################################################################################
# class that holds analogue data from ADC sampling
class MainWindow(tk.Frame):
  #counter = 0
  def __init__(self, parent, sensor, *args, **kwargs):
    tk.Frame.__init__(self, parent, *args, **kwargs)
    self.parent = parent
    self.sensor = sensor
    self.defaultConfig()
    self.createMenuBar()
    self.createWidgets()

    ## Create data buffer
    self.buffer = AppDatabuf(1000)
    self.rainflowData = RainflowDatabuf(1000)

    self.parent.protocol("WM_DELETE_WINDOW", self.onExit)
    self.parent.after(100,self.realtimePlotter)
  ##############################################################################
  def defaultConfig(self):
    #self.grid()
    self.copyright_message =\
     u'Copyright %s 2018 - David Rodenas-Herraiz' % (u"\u00A9")
    self.parent.wm_title("Real-time Fatigue Sensor Visualizer")
    if sys.version_info[0] < 3:
      self.parent.state("zoomed")
    else:
      self.parent.wm_state('normal')
    self.parent.overrideredirect(False)

    self.parent.serial_PORT = self.serial_ports()[-1]
    self.parent.serial_BAUDRATE = 115200
    self.parent.serial_BYTESIZE = 8
    self.parent.serial_STOPBITS = 1
    self.parent.serial_PARITY = serial.PARITY_NONE
    self.parent.serial_RTSCTS = tk.IntVar()
    self.parent.serial_XONXOFF = tk.IntVar()
    self.parent.serial_DSRDTR = tk.IntVar()

    for v in (self.parent.serial_RTSCTS,
              self.parent.serial_XONXOFF,
              self.parent.serial_DSRDTR):
      v.set(0)
  ##############################################################################
  def createMenuBar(self):
    # Menu
    self.menubar = tk.Menu(self.parent)
    self.parent.config(menu=self.menubar)

    # Menu->File
    self.fileMenu = tk.Menu(self.menubar, tearoff=0)
    # Menu->File->Exit
    self.fileMenu.add_command(label='Exit', command=self.onExit)

    # Menu->Connection
    self.connMenu = tk.Menu(self.menubar, tearoff=0)
    # Menu->Help->About
    self.connMenu.add_command(label="Connect (Ctrl+K)",
                              command=self.connectEvent)
    self.parent.bind('<Control-k>', self.connectEvent)   # Start capture

    # Menu->Connection->Options
    self.connMenu.add_command(label="Options... (Ctrl+I)",
                              command=self.connOptions)
    self.parent.bind('<Control-i>', self.connOptions)    # Start capture

    # Menu->Connection->Capture
    self.captmenu = tk.Menu(self.connMenu, tearoff=0)
    self.captmenu.add_command(label='Start (Ctrl+R)',
                              command=self.startCapture, underline=0)
    self.captmenu.add_command(label='Stop (Ctrl+T)',
                              command=self.stopCapture, underline=0)
    self.captmenu.entryconfig(1, state=tk.DISABLED)
    self.connMenu.add_cascade(label='Capture to datafile',
                              menu=self.captmenu, underline=0)

    self.parent.bind('<Control-r>', self.startCapture)      # Start capture
    self.parent.bind('<Control-t>', self.stopCapture)      # Stop capture

    # Menu->Help
    self.helpmenu = tk.Menu(self.menubar, tearoff=0)
    # Menu->Help->About
    self.helpmenu.add_command(label="About", command=self.aboutWindow)

    # Create File Menu
    self.menubar.add_cascade(label='File', menu=self.fileMenu)
    self.menubar.add_cascade(label='Connection', menu=self.connMenu)
    self.menubar.add_cascade(label='Help', menu=self.helpmenu)
  ##############################################################################
  def connectEvent(self, event=None):
    try: self.serialport
    except AttributeError: self.serialport = None

    if self.serialport is None: self.connect()
    else: self.disconnect()
  ##############################################################################
  def connect(self, event=None):
    # open serial port
    try:
      self.serialport = serial.Serial(self.parent.serial_PORT,
                                      self.parent.serial_BAUDRATE,
                                      timeout=1,
                                      bytesize=self.parent.serial_BYTESIZE,
                                      parity = self.parent.serial_PARITY,
                                      stopbits = self.parent.serial_STOPBITS,
                                      xonxoff = self.parent.serial_XONXOFF.get(),
                                      rtscts=self.parent.serial_RTSCTS.get(),
                                      dsrdtr=self.parent.serial_DSRDTR.get())

      self.serialport.flushInput()
      self.serialport.flushOutput()

      # Thread serial listener
      self._stopevent = threading.Event( )
      self.connectThread = threading.Thread(target=self.data_listener)
      self.connectThread.daemon = True
      self.connectThread.start()

      self.connMenu.entryconfig(0, label="Disconnect (Ctrl+K)")
      self.connMenu.entryconfig(1, state=tk.DISABLED)

    except serial.SerialException as e:
      tkMessageBox.showerror("Error", "Could not open port %s " \
        % (self.parent.serial_PORT) )
    except Exception as e:
      tkMessageBox.showerror("Error", str(e))
  ##############################################################################
  def disconnect(self):
    try: self.serialport
    except AttributeError: self.serialport = None

    if self.serialport is not None:
      self._stopevent.set( )
      self.connectThread.join()

      self.serialport.flushInput()
      self.serialport.flushOutput()
      self.serialport.close()
      self.serialport = None

    self.connMenu.entryconfig(0, label="Connect (Ctrl+K)")
    self.connMenu.entryconfig(1, state=tk.NORMAL)
  ##############################################################################
  def serial_ports(self):
    return [v[0] for v in serial.tools.list_ports.comports()]
  ##############################################################################
  def on_combo_configure(self, event):
    font = tkfont.nametofont(str(event.widget.cget('font')))
    width = max([ font.measure(p + "0") - event.width for p in self.portlist])
    style = ttk.Style()
    style.configure('TCombobox', postoffset=(0,0,width,0))
  def connOptions(self, event=None):
    # open serial port
    try:  self.parent.serial_PORT, self.parent.serial_BAUDRATE,   \
          self.parent.serial_BYTESIZE, self.parent.serial_PARITY, \
          self.parent.serial_STOPBITS, self.parent.serial_RTSCTS, \
          self.parent.serial_XONXOFF, self.parent.serial_DSRDTR
    except AttributeError:
      self.parent.serial_PORT, self.parent.serial_BAUDRATE,   \
      self.parent.serial_BYTESIZE, self.parent.serial_PARITY, \
      self.parent.serial_STOPBITS, self.parent.serial_RTSCTS, \
      self.parent.serial_XONXOFF, self.parent.serial_DSRDTR = None, None, None,\
        None, None, None, None, None, None

    self.optionsWindow = tk.Toplevel(self)
    self.optionsWindow.geometry('300x300+50+50')

    self.optionsWindow.grab_set()
    self.optionsWindow.wm_title("Connection options")

    #Populate the serial configuration tab
    self.portlist = self.serial_ports()
    self.baudlist = (4800, 9600, 19200, 38400, 57600, 115200, 230400, 921600)
    self.databitslist = (7, 8)
    self.stopbitslist = (1, 2)
    self.paritylist = ('None', 'Even', 'Odd')

    portlabel = ttk.Label(self.optionsWindow, text='Serial port')
    porttext = tk.StringVar().set('port')
    self.portbox = ttk.Combobox(self.optionsWindow, width=10,
      textvariable=porttext, state='readonly')
    self.portbox['values'] = self.portlist
    self.portbox.bind('<Configure>', self.on_combo_configure)
    if self.parent.serial_PORT is not None:
      self.portbox.current(self.portlist.index(self.parent.serial_PORT))
    else:
      self.portbox.current(len(self.portlist)-1)

    baudlabel = ttk.Label(self.optionsWindow, text='Baudrate')
    baudtext = tk.StringVar().set('baud')
    self.baudbox = ttk.Combobox(self.optionsWindow, width=10,
      textvariable=baudtext, state='readonly')
    self.baudbox['values'] = self.baudlist
    if self.parent.serial_BAUDRATE is not None:
      self.baudbox.current(self.baudlist.index(self.parent.serial_BAUDRATE))
    else:
      self.baudbox.current(5)

    datalabel = ttk.Label(self.optionsWindow, text='Data bits')
    datatext = tk.StringVar().set('databits')
    self.databox = ttk.Combobox(self.optionsWindow, width=10,
      textvariable=datatext, state='readonly')
    self.databox['values'] = self.databitslist
    if self.parent.serial_BYTESIZE is not None:
      self.databox.current(self.databitslist.index(self.parent.serial_BYTESIZE))
    else:
      self.databox.current(1)

    stopbitslabel = ttk.Label(self.optionsWindow, text='Stop bits')
    stopbitstext = tk.StringVar().set('stopbits')
    self.stopbitsbox = ttk.Combobox(self.optionsWindow, width=10,
      textvariable=stopbitstext, state='readonly')
    self.stopbitsbox['values'] = self.stopbitslist
    if self.parent.serial_STOPBITS is not None:
      self.stopbitsbox.current(self.stopbitslist.index(self.parent.serial_STOPBITS))
    else:
      self.stopbitsbox.current(0)

    paritylabel = ttk.Label(self.optionsWindow, text='Parity')
    paritytext = tk.StringVar().set('parity')
    self.paritybox = ttk.Combobox(self.optionsWindow, width=10,
      textvariable=paritytext, state='readonly')
    self.paritybox['values'] = self.paritylist
    if self.parent.serial_PARITY is not None:
      if self.parent.serial_PARITY == serial.PARITY_NONE:
        self.paritybox.current(0)
      elif self.parent.serial_PARITY == serial.PARITY_EVEN:
        self.paritybox.current(1)
      else:
        self.paritybox.current(2)
    else:
      self.paritybox.current(0)

    self.portbox.bind('<<ComboboxSelected>>', self.on_portSelect)
    self.baudbox.bind('<<ComboboxSelected>>', self.on_baudSelect)
    self.databox.bind('<<ComboboxSelected>>', self.on_dataSelect)
    self.stopbitsbox.bind('<<ComboboxSelected>>', self.on_stopbitsSelect)
    self.paritybox.bind('<<ComboboxSelected>>', self.on_paritySelect)

    row = 1
    portlabel.grid(sticky=tk.W, row=row, column=0, padx=5)
    self.portbox.grid(row=row, column = 1, padx=5)
    row += 1
    baudlabel.grid(sticky=tk.W, row=row, column = 0, padx=5)
    self.baudbox.grid(row=row, column = 1, padx=5)
    row += 1
    datalabel.grid(sticky=tk.W, row=row, column = 0, padx=5)
    self.databox.grid(row=row, column=1, padx=5)
    row += 1
    stopbitslabel.grid(sticky=tk.W, row=row, column = 0,  padx=5)
    self.stopbitsbox.grid(row=row, column=1,  padx=5)
    row += 1
    paritylabel.grid(sticky=tk.W, row=row, column = 0,  padx=5)
    self.paritybox.grid(row=row, column=1, padx=5)
    row += 1
    flowCtrlLabel = ttk.Label(self.optionsWindow,
                              text='Flow Control').grid(sticky=tk.W,
                                                        row=row,
                                                        column = 0,
                                                        padx=5)
    ttk.Checkbutton(self.optionsWindow,
                    text="RTS/CTS",
                    variable=self.parent.serial_RTSCTS).grid(row=row,
                                                             column = 1,
                                                             sticky=tk.W,
                                                             padx=5)
    row += 1
    ttk.Checkbutton(self.optionsWindow,
                    text="XON",
                    variable=self.parent.serial_XONXOFF).grid(row=row,
                                                              column = 1,
                                                              sticky=tk.W,
                                                              padx=5)
    row += 1
    ttk.Checkbutton(self.optionsWindow,
                    text="DSR/DTR",
                    variable=self.parent.serial_DSRDTR).grid(row=row,
                                                             column = 1,
                                                             sticky=tk.W,
                                                             padx=5)
    row += 1
    self.okOptionsbutton = tk.Button(self.optionsWindow, text="Ok",
                                command=self.close_optionsWindow)
    self.okOptionsbutton.grid(row=row,
                              column=1, padx=5, pady=5,
                              sticky=tk.W+tk.E+tk.N+tk.S)
  ##############################################################################
  def on_portSelect(self, event=None):
    self.parent.serial_PORT = self.portbox.get()
  ##############################################################################
  def on_baudSelect(self, event=None):
    self.parent.serial_BAUDRATE = int(self.baudbox.get())
  ##############################################################################
  def on_dataSelect(self, event=None):
    self.parent.serial_BYTESIZE = int(self.databox.get())
  ##############################################################################
  def on_stopbitsSelect(self, event=None):
    self.parent.serial_STOPBITS = int(self.stopbitsbox.get())
  ##############################################################################
  def on_paritySelect(self, event=None):
    parity = self.paritybox.get()
    if parity == self.paritylist[0]:
      self.parent.serial_PARITY = serial.PARITY_NONE
    elif parity == self.paritylist[1]:
      self.parent.serial_PARITY = serial.PARITY_EVEN
    else:
      self.parent.serial_PARITY = serial.PARITY_ODD
  ##############################################################################
  def close_optionsWindow(self):
    self.optionsWindow.destroy()
  ##############################################################################
  def startCapture(self, event=None):
    self.fieldnames = ['timestamp','clock','peaksOrRange','ctype','minPeak','maxPeak']

    initial_filename = 'fatigue-capture-' + \
      datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S")

    try: self.parent.directory
    except AttributeError: self.parent.directory = "/"

    self.parent.filename =  tkFileDialog.asksaveasfilename(initialfile = initial_filename, \
      initialdir = self.parent.directory, \
      title = 'Capture to CSV file', \
      defaultextension = 'csv', \
      filetypes = (("csv files","*.csv"),("all files","*.*")))

    if self.parent.filename == '':
      self.parent.filename = None

    if self.parent.filename is not None:
      if(not os.path.exists(self.parent.filename)):
        csv_file = open(self.parent.filename, mode='w', newline='')
        writer = csv.DictWriter(csv_file, fieldnames=self.fieldnames)
        writer.writeheader()
        csv_file.close()

      self.parent.directory = os.path.dirname(self.parent.filename)

      self.captmenu.entryconfig(0, state=tk.DISABLED)
      self.captmenu.entryconfig(1, state=tk.NORMAL)
  ##############################################################################
  def stopCapture(self, event=None):
    if self.parent.filename is not None:
      if(os.path.exists(self.parent.filename)):
        try:
          csv_file = open(self.parent.filename, mode='a', newline='')
          csv_file.close()
        except IOError as e:
          print("Exception: %s" % str(e), file=sys.stderr)
          pass

    self.parent.filename = None
    self.captmenu.entryconfig(0, state=tk.NORMAL)
    self.captmenu.entryconfig(1, state=tk.DISABLED)
  ##############################################################################
  def aboutWindow(self):
    tkMessageBox.showinfo("About", self.copyright_message)
  ##############################################################################
  def onExit(self):
    self.disconnect()
    self.parent.quit()
    self.parent.destroy()
  ##############################################################################
  def createWidgets(self):
    # Main application title
    self.title_label = tk.Label(self.parent, text='Real-time Sensor Visualizer',
      anchor=tk.CENTER, font=("Times 20 bold"))
    self.title_label.pack(side=tk.TOP, fill=tk.X)

    # Copyright
    self.copyright_label = tk.Label(self.parent, text=self.copyright_message,
      anchor=tk.CENTER, font=("Times", 16))
    self.copyright_label.pack(side=tk.TOP, fill=tk.X)

    # Create canvas
    self.canvas = FigureCanvasTkAgg(self.draw_figure(), master=self.parent)
    self.canvas.draw()
    self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

    # Create horizontal scale
    self.wScale = tk.Scale(master=self.parent,label="View Width:", from_=3,
      to=1000, sliderlength=30, orient=tk.HORIZONTAL)
    self.wScale.pack(side=tk.BOTTOM)
    self.wScale.set(100)
  ##############################################################################
  def draw_figure(self):
    self.figure = pylab.figure(1)
    AX = gridspec.GridSpec(2,2)
    AX.update(wspace = 0.1, hspace = 0.5)

    self.ax1 = plt.subplot(AX[0,:])
    self.ax2 = plt.subplot(AX[1,:])

    self.ax1.set_xlabel("Clock time (s)")

    if SHOW_IN_USTRAIN == False:
      self.ax1.set_ylabel("Voltage (mV)")
      self.voltage_line = self.ax1.plot([],[],'-', label='Voltage signal')
    else:
      self.ax1.set_ylabel(u'Strain (\u03bc\u03b5)')
      self.strain_line = self.ax1.plot([],[],'-', label='Strain signal')

    self.range_hist = self.ax2.hist([[], []], bins=20)

    ax = self.ax1
    ax.grid(True)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.legend(loc='upper right', bbox_to_anchor=(1.02, 1.2), shadow=True)

    self.formatHist()
    return self.figure
  ##############################################################################
  def formatHist(self):
    self.ax2.grid(True)
    self.ax2.spines['right'].set_visible(False)
    self.ax2.spines['top'].set_visible(False)
    self.ax2.legend(loc='upper center', bbox_to_anchor=(0.5, 1.2),
      fontsize="large", shadow=True, ncol=2)
    self.ax2.yaxis.set_ticks_position('left')
    self.ax2.xaxis.set_ticks_position('bottom')
    self.ax2.set_xlabel(u'Strain range (\u03bc\u03b5)')
    self.ax2.set_ylabel('Strain cycles (#)')
  ##############################################################################
  def data_listener(self):
    try: self.serialport
    except AttributeError: self.serialport = None

    while not self._stopevent.isSet():
      if self.serialport is not None:
        try:

          line = self.serialport.readline().decode('ISO-8859-1')[:-1]
          if line:  # If it isn't a blank line
            if CAPTURE_TO_STDOUT == True:   print(line)
            if LOW_POWER_OP == True:
              data = [float(re.sub("[^a-z0-9]+","", val, flags=re.IGNORECASE)) for val in line.split(',')]
            else:
              data = [float(val) for val in line.split(',')]

            if(len(data) == self.buffer.getNumFields()):
              timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
              clock = data[0] / CLOCK_SECOND
              # voltage = (data[1] * Vcc * 1e3) / SOC_ADC_MAX_U16_VALUE
              voltage = data[1]
              if LOW_POWER_OP == True:
                t = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S.%f")
                self.buffer.add(time.mktime(t.timetuple()), voltage)
              else:
                self.buffer.add(clock, voltage)

              if self.parent.filename is not None:
                csv_file = open(self.parent.filename, mode='a', newline='')
                writer = csv.DictWriter(csv_file, fieldnames=self.fieldnames)
                writer.writerow({'timestamp': '%s' % (timestamp), \
                  'clock': '%.3f' % clock, \
                  'peaksOrRange': '%.6f' % (voltage * 1e-3)})
                csv_file.close()

            if(len(data) == self.rainflowData.getNumFields()):
              timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
              clock = data[0] / CLOCK_SECOND
              cycle_type = int(data[1])
              minPeak = (data[2] * 1e-3)
              maxPeak = (data[3] * 1e-3)
              cycle_range = (data[4] * 1e-3)

              cycle_range_ue = cycle_range / (self.sensor.getSensitivity()*1e-6)
              if LOW_POWER_OP == True:
                t = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S.%f")
                self.rainflowData.add(time.mktime(t.timetuple()),
                                      cycle_type,
                                      cycle_range_ue)
              else:
                self.rainflowData.add(clock, cycle_type, cycle_range_ue)

              if self.parent.filename is not None:
                csv_file = open(self.parent.filename, mode='a', newline='')
                writer = csv.DictWriter(csv_file, fieldnames=self.fieldnames)
                writer.writerow({'timestamp': '%s' % (timestamp), \
                  'clock': '%.3f' % clock, \
                  'peaksOrRange': '%.6f' % (cycle_range), \
                  'ctype': '%d' % (cycle_type), \
                  'minPeak': '%.6f' % (minPeak), \
                  'maxPeak': '%.6f' % (maxPeak)})
                csv_file.close()

        except serial.SerialException as e:
          tkMessageBox.showerror("Error", str(e))
          self.serialport = None
        except Exception as e:
          print("Exception: %s" % str(e), file=sys.stderr)
          # skip line in case serial data is corrupt
          pass
    sleep(0.1)
  ##############################################################################
  def realtimePlotter(self):
    try: self.parent.filename
    except AttributeError: self.parent.filename = None

    numberOfSamples = min(len(self.buffer.ax),self.wScale.get())

    if numberOfSamples > 2:
      # Time (x-axis)
      time = pylab.array(self.buffer.ax)[-numberOfSamples:]
      # Voltage (y-axis)
      voltage = pylab.array(self.buffer.ay)[-numberOfSamples:]
      # Strain (y-axis)
      ue_data = [self.sensor.convert2ustrain(val * 1e-3) for val in voltage]

      if SHOW_IN_USTRAIN == False:
        self.voltage_line[0].set_data(time, voltage)
        self.ax1.axis([min(time),max(time), min(voltage),max(voltage)])
      else:
        self.strain_line[0].set_data(time, ue_data)
        self.ax1.axis([min(time),max(time), min(ue_data),max(ue_data)])

      if(len(self.rainflowData.strain_range) > 2):
        half_cycle, full_cycle = [], []
        for index, item in enumerate(pylab.array(self.rainflowData.cycle_type)):
          if self.rainflowData.cycle_type[index] == HALF_CYCLE_TYPE:
            half_cycle.append(pylab.array(self.rainflowData.strain_range)[index])
          else:
            full_cycle.append(pylab.array(self.rainflowData.strain_range)[index])

        half, full = half_cycle, full_cycle

        self.ax2.clear()
        self.ax2.hist([half, full], bins=20,
          label = ['One-half cycles', 'Full cycles'])
        self.formatHist()

    self.canvas.draw()
    self.parent.after(25, self.realtimePlotter)
################################################################################
if __name__ == "__main__":
  sensor = create_sensor()
  sensor.getFirstStage().setCf(4e-9) # set C1
  sensor.getSecondStage().setCd(4e-6) # set C2

  ## Create main window
  root = tk.Tk()
  main = MainWindow(root, sensor)

  # Start application
  main.pack(side="top", fill="both", expand=True)

  while True:
    try:
      root.mainloop()
      break
    except UnicodeDecodeError:
      pass
