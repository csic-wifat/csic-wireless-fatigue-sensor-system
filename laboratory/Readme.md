# Laboratory Experiments

The experimental data and data analysis scripts, used and published in "Power-efficient piezoelectric fatigue measurement using long-range wireless sensor networks", Smart Materials and Structures, 2019, [https://doi.org/10.1088/1361-665X/ab2c46](https://doi.org/10.1088/1361-665X/ab2c46).

Please refer to subsection *4.2 Validation on real hardware*.

The following folders contain experimental data and data analysis scripts for:

 - peak-trough-performance: *4.2.2. Performance results under sinusoidal excitation*:
 - cycle-counting-performance: *4.2.3. Results under a realistic scenario*:
 - power: *4.2.4. Power profiling*
 - scripts: Scripts used to collect data from Monsoon Power Monitor and to record data sent by the fatigue sensor prototype via the serial port.

### Requirements:
- [Python 3.x](https://www.python.org/) and [Jupyter Notebook](https://jupyter.org/) for data processing scripts
