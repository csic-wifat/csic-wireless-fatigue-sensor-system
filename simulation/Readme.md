# Computer Simulation

The simulation scripts, data analysis scripts and simulation data, used and , published in "Power-efficient piezoelectric fatigue measurement using long-range wireless sensor networks", Smart Materials and Structures, 2019, [https://doi.org/10.1088/1361-665X/ab2c46](https://doi.org/10.1088/1361-665X/ab2c46).

Please refer to subsection *4.1 Simulation: Evaluation of detection performance*.

Content:

 - Tina-TI simulation files (see Figures below): *tina-circuits*
 - Scripts for processing the simulator output and plotting results: *scripts*
 - Simulation data (simulation output and processed output for ease of plotting): *data*

#### 1. Simulation file: analogue-sensor-system.tsc
![Analogue Sensor System](tina-circuits/img/analogue-sensor-system.png)

#### 2. Simulation file: peak-trough-detection-and-sampling.tsc
![Peak-Trough Detection and Sampling](tina-circuits/img/peak-trough-detection-and-sampling.png)

#### 3. Simulation file: peak-trough-detection-and-sampling-Figure6.tsc
![Peak-Trough Detection and Sampling-Figure 6b](tina-circuits/img/peak-trough-detection-and-sampling-Figure6.png)

### Requirements:
- Tina-TI (please see: [http://www.ti.com/tool/TINA-TI](http://www.ti.com/tool/TINA-TI)). Tina-TI v9 was used in our work.
- [Python 3.x](https://www.python.org/) and [Jupyter Notebook](https://jupyter.org/) for data processing scripts
