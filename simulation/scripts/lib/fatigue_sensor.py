#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Fatigue sensor class

Class that models the characteristics of the fatigue sensor circuit
"""
from __future__ import division
import scipy.constants as sc
import piezo

from sensor_params import *

__author__ = "David Rodenas-Herraiz"
__copyright__ = "Copyright 2018, WiFAT"
__credits__ = ["David Rodenas-Herraiz"]

__license__ = "GPL"
__version__ = "3.0.0"
__maintainer__ = "David Rodenas-Herraiz"
__email__ = "david.rodenas@eng.cam.ac.uk"
__status__ = "Development"

class FirstStage():
    ### Constructor
    def __init__(self, Rin, Rf, Cf, Vcc, Vee):
        self.__Rin = Rin
        self.__Rf = Rf # Feedback resistance
        self.__Cf = Cf # Feedback capacitance
        self.__Vcc = Vcc
        self.__Vee = Vee
        self.__Vref = 0.5*self.__Vcc  # default voltage reference
    ### Get the type ID of this class
    def getTypeId(self):
        return type(self).__name__
    ### Get the input resistance
    def getRin(self):
        return self.__Rin
    ### Get the feedback resistance
    def getRf(self):
        return self.__Rf
    ### Get the feedback capacitance
    def getCf(self):
        return self.__Cf
    ### Get the voltage reference
    def getVref(self):
        return self.__Vref
    ### Get the input resistance
    def setRin(self, Rin):
        self.__Rin = Rin
    ### Set the feedback resistance
    def setRf(self, Rf):
        self.__Rf = Rf
    ### Set the feedback capacitance
    def setCf(self, Cf):
        self.__Cf = Cf
    ### Set the voltage reference
    def setVref(self, Vref):
        self.__Vref = Vref
    ### Set sensor´s Vcc
    def setVcc(self, v):
        self.__Vcc = v
    ### Set sensor´s Vee
    def setVee(self, v):
        self.__Vee = v

class SecondStage():
    ### Constructor
    def __init__(self, Rin, Rf, Rd, Cd, Vcc, Vee):
        self.__RnonIn = Rin
        self.__Rf = Rf
        self.__Rd = Rd
        self.__Cd = Cd
        self.__Vcc = Vcc
        self.__Vee = Vee
        self.__Voh = Vcc
        self.__Vol = Vee
    ### Get the type ID of this class
    def getTypeId(self):
        return type(self).__name__
    ### Get the circuit hysteresis
    def getHysteresis(self):
        return (self.__Voh-self.__Vol)*(self.__RnonIn/self.__Rf)
    ### Get the sensor RC delay
    def getRCTimeDelay(self):
        return (self.__Rd*self.__Cd)
    ### Set sensor´s Vcc
    def setVcc(self, v):
        self.__Vcc = v
        self.__Voh = self.__Vcc
    ### Set sensor´s Vee
    def setVee(self, v):
        self.__Vee = v
        self.__Vol = self.__Vee
    ### Set the feedback resistance
    def setRf(self, Rf):
        self.__Rf = Rf
    ### Set resistor at the non-inverter input
    def setRnonIn(self, RnonIn):
        self.__RnonIn
    ### Set (delay) resistor at the inverter input
    def setRd(self, Rd):
        self.__Rd = Rd
    ### Set (delay) capacitance at the inverter input
    def setCd(self, Cd):
        self.__Cd = Cd
    ### Get the feedback resistance
    def getRf(self):
        return self.__Rf
    ### Get resistor at the non-inverter input
    def getRnonIn(self):
        return self.__RnonIn
    ### Get (delay) resistor at the inverter input
    def getRd(self):
        return self.__Rd
    ### Get (delay) capacitance at the inverter input
    def getCd(self):
        return self.__Cd
    ### Get VOH
    def getVoh(self):
        return self.__Voh
    ### Get VOL
    def getVol(self):
        return self.__Vol


class FatigueSensor():
    ### Constructor
    def __init__(self, piezo, R1, R2, R3, R4, R5, C1, C2, Vcc, Vee):
        self.__piezo = piezo
        self.__s1 = FirstStage(R1, R2, C1, Vcc, Vee)
        self.__s2 = SecondStage(R3, R4, R5, C2, Vcc, Vee)
        self.__Vcc = Vcc
        self.__Vee = Vee
    ### Get the type ID of this class
    def getTypeId(self):
        return type(self).__name__
    ### Get sensor´s first stage
    def getPiezo(self):
        return self.__piezo
    ### Get sensor´s first stage
    def getFirstStage(self):
        return self.__s1
    ### Get sensor´s first stage
    def getSecondStage(self):
        return self.__s2
    ### Get sensor´s Vcc
    def getVcc(self):
        return self.__Vcc
    ### Get sensor´s Vee
    def getVee(self):
        return self.__Vee
    ### Set sensor´s first stage
    def setFirstStage(self, s1):
        self.__s1 = s1
    ### Set sensor´s first stage
    def setSecondStage(self, s2):
        self.__s2 = s2
    ### Set sensor´s Vcc
    def setVcc(self, v):
        self.__Vcc = v
        self.__s1.setVcc(self.__Vcc)
        self.__s2.setVcc(self.__Vcc)
    ### Set sensor´s Vee
    def setVee(self, v):
        self.__Vee = v
        self.__s1.setVee(self.__Vee)
        self.__s2.setVee(self.__Vee)
    ### Get the high corner frequency
    def getLowCornerFrequency(self):
        return 1/(2*sc.pi*self.__s1.getRf()*self.__s1.getCf())
    ### Get the high corner frequency
    def getHighCornerFrequency(self):
        return 1/(2*sc.pi*self.__s1.getRin()*self.__piezo.getParasiticCapacitance())
    ### Get sensor sensitivity
    def getSensitivity(self, v=0.3):
        # v is the Poisson's ratio of the host structure material (e.g., for aluminum, v = 0.3)
        Yp = self.__piezo.getYoungsModulus()
        d31 = self.__piezo.getD31()
        Kp = self.__piezo.getPoissonRatioCorrectionFactor(v)
        lp = self.__piezo.getLength()
        bp = self.__piezo.getWidth()
        Cf = self.__s1.getCf()
        return Kp * (Yp*d31*lp*bp)/Cf
    ### Get the sensor hysteresis
    def getHysteresis(self):
        return self.__s2.getHysteresis()
    ### Convert voltage to uStrain
    def convert2ustrain(self, voltage, v=0.3, voffset=True):
        # v is the Poisson's ratio of the host structure material (e.g., for aluminum, v = 0.3)
        if voffset == True:
            return ((self.__s1.getVref() - voltage) / self.getSensitivity(v)) * 1e6
        else:
            return (voltage / self.getSensitivity(v)) * 1e6
    ### Convert uStrain to voltage
    def convert2voltage(self, ustrain, v=0.3, voffset=True):
        if voffset == True:
            return self.__s1.getVref() - (ustrain * self.getSensitivity(v)) * 1e-6
        else:
            return (ustrain * self.getSensitivity(v)) * 1e-6

# Easy-to-use function to create analogue sensor system object
def create_sensor():
  from piezo import Piezo
  # Create piezo electric film object
  piezo = Piezo(lp, bp, tp, d31, d32, d33, Yp, epsilon_r)
  # Create fatigue sensor object
  sensor = FatigueSensor(piezo, R1, R2, R3, R4, R5, C1, C2, Vcc, Vee)
  # Return sensor object
  return sensor
