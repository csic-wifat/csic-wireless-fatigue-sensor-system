'''
	Define some default values
'''

# The following values are from the TE Connectivity's DT1-028K
# datasheet
lp = 30e-3  # Length
bp = 12e-3  # Width
tp = 0.028e-3  # Thickness
d31 = 23e-12  # Piezoelectric constant for length change
d32 = 2.3e-12  # Piezoelectric constant for width change
d33 = -33e-12  # Piezoelectric constant for thickness change
Yp = 4e9  # Young's modulus
epsilon_r = 12 # Relative permittivity

# Host structure characteristics
vPoisson = 0.3 # Poisson's ratio of the host structure material
							 # (e.g., for aluminum, vPoisson = 0.3)

# Power supply
Vcc = 3.3
Vee = 0.0
# Resistor and capacitor values
R1 = 47
R2 = 100e6
R3 = 47e3
R4 = 10e6
R5 = 1e3
C1 = 4e-9
C2 = 5e-6
