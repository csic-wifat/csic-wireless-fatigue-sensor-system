#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Piezo class

Class that models a piezoelectric film device
"""
from __future__ import division
import scipy.constants as sc

__author__ = "David Rodenas-Herraiz"
__copyright__ = "Copyright 2018, WiFAT"
__credits__ = ["David Rodenas-Herraiz"]

__license__ = "GPL"
__version__ = "3.0.0"
__maintainer__ = "David Rodenas-Herraiz"
__email__ = "david.rodenas@eng.cam.ac.uk"
__status__ = "Development"

class Piezo():
    ### Constructor
    def __init__(self, lp, bp, tp, d31, d32, d33, Yp, epsilon_r):
        self.__lp = lp # Length
        self.__bp = bp # Width
        self.__tp = tp # Thickness
        self.__d31 = d31 # Piezoelectric constant for length change
        self.__d32 = d32 # Piezoelectric constant for width change
        self.__d33 = d33 # Piezoelectric constant for thickness change
        self.__Yp = Yp # Young's modulus
        self.__epsilon_r = epsilon_r # Relative permittivity
    ### Get the type ID of this class
    def getTypeId(self):
        return type(self).__name__
    ### Get length
    def getLength(self):
        return self.__lp
    ### Get width
    def getWidth(self):
        return self.__bp
    ### Get thickness
    def getThickness(self):
        return self.__tp
    ### Get the Piezoelectric constant for length change
    def getD31(self):
        return self.__d31
    ### Get the Piezoelectric constant for width change
    def getD32(self):
        return self.__d32
    ### Get the Piezoelectric constant for thickness change
    def getD33(self):
        return self.__d33
    ### Get the Young's modulus
    def getYoungsModulus(self):
        return self.__Yp
    ### Get the relative permittivity
    def getRelativePermittivity(self):
        return self.__epsilon_r
    ### Get the permittivity
    def getPermittivity(self):
        return (sc.epsilon_0 * self.__epsilon_r)
    ### Get the active area
    def getActiveArea(self):
        return (self.__lp*self.__bp)
    ### Get parasitic capacitance
    def getParasiticCapacitance(self):
        return (self.getPermittivity()*self.getActiveArea()/self.__tp)
    ### Get sensitivity
    def getSensitivity(self):
        return (self.__Yp*self.__d31*self.__lp*self.__bp)/self.getParasiticCapacitance()
    ### Get sensitivity
    def getPoissonRatioCorrectionFactor(self, v):
        # v is the Poisson's ratio of the host structure material (e.g., for aluminum, v = 0.3)
        return (1 - v * self.__d32/self.__d31)
    ### Set length
    def setLength(self, lp):
        self.__lp = lp
    ### Set width
    def setWidth(self, bp):
        self.__bp = bp
    ### Get thickness
    def setThickness(self, tp):
        self.__tp = tp
    ### Set the Piezoelectric constant for length change
    def setD31(self, d31):
        self.__d31 = d31
    ### Set the Piezoelectric constant for width change
    def setD32(self, d32):
        self.__d32 = d32
    ### Set the Piezoelectric constant for thickness change
    def setD33(self, d33):
        self.__d33 = d33
    ### Set the Young's modulus
    def setYoungsModulus(self, Yp):
        self.__Yp = Yp
    ### Set the relative permittivity
    def setRelativePermittivity(self, epsilon_r):
        self.__epsilon_r = epsilon_r
