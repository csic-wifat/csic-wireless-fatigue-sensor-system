### Parameters

| **Parameter**  | **Value**                |
|----------------|--------------------------|
| Op-amp (s2/s3) | OPA333                   |
| Vcc            | 3.3 V                    |
| Vee            | 0.0 V                    |
| R3             | 22, 47, 100 and 300 kOhm |
| R4             | 10 MOhm                  |
| R5             | 1 KOhm                   |
| C2             | 5 uF                     |
| Vo1 (DC Level) | 1.65                     |
| Vo1 (Signal)   | Synthetic_signal         |
| Vo1 (A)        | 0.8V                     |
| Vo1 (f)        | NA                       |
