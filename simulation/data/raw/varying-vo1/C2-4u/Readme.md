### Parameters

| **Parameter**  | **Value**              |
|----------------|------------------------|
| Op-amp (s2/s3) | OPA333                 |
| Vcc            | 3.3 V                  |
| Vee            | 0.0 V                  |
| R3             | 47 kOhm                |
| R4             | 10 MOhm                |
| R5             | 1 KOhm                 |
| C2             | 4 uF                   |
| Vo1 (DC Level) | 1.65                   |
| Vo1 (Signal)   | Sine wave              |
| Vo1 (A)        | 0.2-1.6V in 0.2V steps |
| Vo1 (f)        | 1-20Hz in 1Hz steps    |
